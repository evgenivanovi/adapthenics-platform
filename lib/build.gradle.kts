@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

/* Project Properties */
group = "com.evgenivanovi.adapthenics"
version = "1.0.0-SNAPSHOT"

val JAVA_LANG_V: String by project
val JAVA_LANG_VERSION: JavaVersion = JavaVersion.toVersion(JAVA_LANG_V)

plugins {
    `kotlin-dsl`
    id("org.gradle.java-gradle-plugin")
    id("org.gradle.distribution")
    id("org.gradle.maven-publish")
}

gradlePlugin {

    val NAME = "lib"
    val ID = "${project.group}.${project.name}"
    val CLASS = "com.evgenivanovi.adapthenics.lib.LibrariesPlugin"

    plugins {
        register(NAME) {
            id = ID
            implementationClass = CLASS
        }
    }

}

tasks {

    val javaArgs = listOf(
        "--add-opens=java.base/java.net=ALL-UNNAMED",
        "--add-opens=java.base/java.nio=ALL-UNNAMED",
        "--add-opens=java.base/java.time=ALL-UNNAMED",
        "--add-opens=java.base/java.text=ALL-UNNAMED",

        "--add-opens=java.base/java.lang=ALL-UNNAMED",
        "--add-opens=java.base/java.lang.invoke=ALL-UNNAMED",
        "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED",

        "--add-opens=java.base/java.util=ALL-UNNAMED",
        "--add-opens=java.base/java.util.concurrent=ALL-UNNAMED",
    )

    val kotlinArgs = listOf(
        "-Xemit-jvm-type-annotations"
    )

    java {

        sourceCompatibility = JAVA_LANG_VERSION
        targetCompatibility = JAVA_LANG_VERSION

        compileJava {
            sourceCompatibility = JAVA_LANG_VERSION.toString()
            targetCompatibility = JAVA_LANG_VERSION.toString()
            options.compilerArgs = options.compilerArgs.apply {
                addAll(javaArgs)
            }
        }

        compileTestJava {
            sourceCompatibility = JAVA_LANG_VERSION.toString()
            targetCompatibility = JAVA_LANG_VERSION.toString()
            options.compilerArgs = options.compilerArgs.apply {
                addAll(javaArgs)
            }
        }

    }

    kotlin {

        compileKotlin {
            kotlinOptions.jvmTarget = JAVA_LANG_VERSION.toString()
            kotlinOptions.freeCompilerArgs = kotlinArgs
        }

        compileTestKotlin {
            kotlinOptions.jvmTarget = JAVA_LANG_VERSION.toString()
            kotlinOptions.freeCompilerArgs = kotlinArgs
        }

    }

}