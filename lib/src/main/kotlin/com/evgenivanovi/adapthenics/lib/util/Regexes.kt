package com.evgenivanovi.adapthenics.lib.util

object Regexes {

    /* __________________________________________________ */

    const val START = "^"

    const val END = "$"

    const val OR = "|"

    /* __________________________________________________ */

    /**
     * Matches the preceding character exactly 1 time.
     */
    const val SINGLE = "{1}"

    /**
     * Matches the preceding character 0 or 1 time.
     * Alias for [Regexes.ZERO_OR_ONE_TIME]
     */
    const val OPTIONAL = "?"

    /**
     * Matches the preceding character 0 or more times.
     */
    const val ZERO_OR_MORE_TIMES = "*"

    /**
     * Matches the preceding character 1 or more times.
     */
    const val ONE_OR_MORE_TIMES = "+"

    /**
     * Matches the preceding character 0 or 1 time.
     * Alias for [Regexes.OPTIONAL]
     */
    const val ZERO_OR_ONE_TIME = "{0,1}"

    /* __________________________________________________ */

    /**
     * Matches any character.
     */
    const val ANY = "."

    /**
     * Matches a single white space character.
     * This includes space, tab, form feed and line feed.
     */
    const val SPACE = "\\s"

    /**
     * Matches a single letter.
     */
    const val LETTER = "[a-zA-Z]"

    /**
     * Matches a single lower case letter.
     */
    const val LETTER_LOWER_CASE = "[a-z]"

    /**
     * Matches a single upper case letter.
     */
    const val LETTER_UPPER_CASE = "[A-Z]"

    /**
     * Matches a single alphanumeric character.
     */
    const val ALPHANUMERIC = "[a-zA-Z0-9]"

    /**
     * Matches a single alphanumeric character in lowercase.
     */
    const val ALPHANUMERIC_LOWER_CASE = "[a-z0-9]"

    /**
     * Matches a single alphanumeric character in uppercase.
     */
    const val ALPHANUMERIC_UPPERCASE_CASE = "[A-Z0-9]"

    /**
     * Matches any word letter, digit, dash or underscore.
     */
    const val WORD = "[a-zA-Z0-9\\-\\_]"

    /**
     * Matches any word letter in lower case, digit, dash or underscore.
     */
    const val WORD_LOWER_CASE = "[a-z0-9\\-\\_]"

    /**
     * Matches any word letter in upper case, digit, dash or underscore.
     */
    const val WORD_UPPER_CASE = "[A-Z0-9\\-\\_]"

    /* __________________________________________________ */

    /**
     * Start group pattern
     */
    const val START_GROUP = "("

    /**
     * End group pattern
     */
    const val END_GROUP = ")"

    /**
     * Start non-capturing group pattern
     */
    const val START_NON_CAPTURING_GROUP = "(?:"

    /**
     * End group pattern with optional matching
     */
    const val END_GROUP_OPTIONAL = ")?"

    /* __________________________________________________ */

    /**
     * Negative Lookbehind (?!$)
     * Assert that the Regex before does not match the end of a line
     */
    const val BEHIND_NOT_START = "(?<!^)"

    /**
     * Negative Lookahead (?!$)
     * Assert that the Regex after does not match the end of a line
     */
    const val AHEAD_NOT_END = "(?!\$)"

    /* __________________________________________________ */

    const val AHEAD_PREFIX = "(?="
    const val AHEAD_SUFFIX = ")"

    fun assertAhead(pattern: String) = AHEAD_PREFIX + pattern + AHEAD_SUFFIX

    const val AHEAD_NOT_PREFIX = "(?!"
    const val AHEAD_NOT_SUFFIX = ")"

    fun assertNotAhead(pattern: String) = AHEAD_NOT_PREFIX + pattern + AHEAD_NOT_SUFFIX

    const val BEHIND_PREFIX = "(?<="
    const val BEHIND_SUFFIX = ")"

    fun assertBehind(pattern: String) = BEHIND_PREFIX + pattern + BEHIND_SUFFIX

    const val BEHIND_NOT_PREFIX = "(?<!"
    const val BEHIND_NOT_SUFFIX = ")"

    fun assertNotBehind(pattern: String) = BEHIND_NOT_PREFIX + pattern + BEHIND_NOT_SUFFIX

    /* __________________________________________________ */

    fun namedGroup(name: String, pattern: String): String {
        return START_GROUP +
            "?" + "<$name>" + pattern +
            END_GROUP
    }

    /* __________________________________________________ */

    object Esc {

        const val ESC_DOT = "\\."

        const val ESC_DASH = "\\-"

        const val ESC_UNDERSCORE = "\\_"

        const val ESC_SLASH = "\\/"

    }

}