package com.evgenivanovi.adapthenics.lib.util

object Numbers {

    const val ZERO = 0L
    const val ZERO_I = 0
    const val ZERO_D = 0.0

    const val ONE = 1L
    const val ONE_I = 1
    const val ONE_D = 1.0

    const val TWO = 2L
    const val TWO_I = 2
    const val TWO_D = 2.0

    const val THREE = 3L
    const val THREE_I = 3
    const val THREE_D = 3.0

    const val FOUR = 4L
    const val FOUR_I = 4
    const val FOUR_D = 4.0

    const val FIVE = 5L
    const val FIVE_I = 5
    const val FIVE_D = 5.0

    const val SIX = 6L
    const val SIX_I = 6
    const val SIX_D = 6.0

    const val SEVEN = 7L
    const val SEVEN_I = 7
    const val SEVEN_D = 7.0

    const val EIGHT = 8L
    const val EIGHT_I = 8
    const val EIGHT_D = 8.0

    const val NINE = 9L
    const val NINE_I = 9
    const val NINE_D = 9.0

    const val TEN = 10L
    const val TEN_I = 10
    const val TEN_D = 10.0

    /* __________________________________________________ */

    const val HUNDRED = 100L
    const val HUNDRED_I = 100
    const val HUNDRED_D = 100.0

    const val ONE_HUNDRED = 100L
    const val ONE_HUNDRED_I = 100
    const val ONE_HUNDRED_D = 100.0

    const val TWO_HUNDRED = 200L
    const val TWO_HUNDRED_I = 200
    const val TWO_HUNDRED_D = 200.0

    const val THREE_HUNDRED = 300L
    const val THREE_HUNDRED_I = 300
    const val THREE_HUNDRED_D = 300.0

    const val FOUR_HUNDRED = 400L
    const val FOUR_HUNDRED_I = 400
    const val FOUR_HUNDRED_D = 400.0

    const val FIVE_HUNDRED = 500L
    const val FIVE_HUNDRED_I = 500
    const val FIVE_HUNDRED_D = 500.0

    const val SIX_HUNDRED = 600L
    const val SIX_HUNDRED_I = 600
    const val SIX_HUNDRED_D = 600.0

    const val SEVEN_HUNDRED = 700L
    const val SEVEN_HUNDRED_I = 700
    const val SEVEN_HUNDRED_D = 700.0

    const val EIGHT_HUNDRED = 800L
    const val EIGHT_HUNDRED_I = 800
    const val EIGHT_HUNDRED_D = 800.0

    const val NINE_HUNDRED = 900L
    const val NINE_HUNDRED_I = 900
    const val NINE_HUNDRED_D = 900.0

    const val TEN_HUNDRED = 1000L
    const val TEN_HUNDRED_I = 1000
    const val TEN_HUNDRED_D = 1000.0

    /* __________________________________________________ */

    const val THOUSAND = 1000L
    const val THOUSAND_I = 1000
    const val THOUSAND_D = 1000.0

    const val ONE_THOUSAND = 1000L
    const val ONE_THOUSAND_I = 1
    const val ONE_THOUSAND_D = 1000.0

    const val TWO_THOUSAND = 2000L
    const val TWO_THOUSAND_I = 2000
    const val TWO_THOUSAND_D = 2000.0

    const val THREE_THOUSAND = 3000L
    const val THREE_THOUSAND_I = 3000
    const val THREE_THOUSAND_D = 3000.0

    const val FOUR_THOUSAND = 4000L
    const val FOUR_THOUSAND_I = 4000
    const val FOUR_THOUSAND_D = 4000.0

    const val FIVE_THOUSAND = 5000L
    const val FIVE_THOUSAND_I = 5000
    const val FIVE_THOUSAND_D = 5000.0

    const val SIX_THOUSAND = 6000L
    const val SIX_THOUSAND_I = 6000
    const val SIX_THOUSAND_D = 6000.0

    const val SEVEN_THOUSAND = 7000L
    const val SEVEN_THOUSAND_I = 7000
    const val SEVEN_THOUSAND_D = 7000.0

    const val EIGHT_THOUSAND = 8000L
    const val EIGHT_THOUSAND_I = 8000
    const val EIGHT_THOUSAND_D = 8000.0

    const val NINE_THOUSAND = 9000L
    const val NINE_THOUSAND_I = 9000
    const val NINE_THOUSAND_D = 9000.0

    const val TEN_THOUSAND = 10000L
    const val TEN_THOUSAND_I = 10000
    const val TEN_THOUSAND_D = 10000.0

}