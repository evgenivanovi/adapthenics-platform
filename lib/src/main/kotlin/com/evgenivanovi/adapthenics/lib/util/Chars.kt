package com.evgenivanovi.adapthenics.lib.util

object Chars {

    const val SPACE = ' '

    const val DOT = '.'
    const val COMMA = ','

    const val COLON = ':'
    const val SEMICOLON = ';'

    const val APOSTROPHE = '\''
    const val SINGLE_QUOTE = '\''
    const val DOUBLE_QUOTES = '\"'

    const val DASH = '-'
    const val UNDERSCORE = '_'

    const val SLASH = '/'
    const val BACKSLASH = '\\'

    const val PLUS = '+'
    const val MINUS = '-'

    const val STAR = '*'
    const val EQUAL = '='

    const val AT = '@'
    const val AMPERSAND = '&'

    const val QUESTION_MARK = '?'
    const val EXCLAMATION_MARK = '!'

    const val OPEN_PARENTHESIS = '('
    const val CLOSE_PARENTHESIS = ')'

    const val OPEN_CURLY_BRACKET = '{'
    const val CLOSE_CURLY_BRACKET = '}'

    const val OPEN_SQUARE_BRACKET = '['
    const val CLOSE_SQUARE_BRACKET = ']'

}