@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.lib.util

object Gradle {

    /* __________________________________________________ */

    const val API_CONFIGURATION = "api"
    const val TEST_API_CONFIGURATION = "testApi"

    const val IMPLEMENTATION_CONFIGURATION = "implementation"
    const val TEST_IMPLEMENTATION_CONFIGURATION = "testImplementation"

    const val RUNTIME_CONFIGURATION = "runtime"
    const val TEST_RUNTIME_CONFIGURATION = "testRuntime"

    const val RUNTIME_ONLY_CONFIGURATION = "runtimeOnly"
    const val TEST_RUNTIME_ONLY_CONFIGURATION = "testRuntimeOnly"

    const val COMPILE_CONFIGURATION = "compileOnly"
    const val TEST_COMPILE_CONFIGURATION = "testCompileOnly"

    const val ANNOTATION_PROCESSOR = "annotationProcessor"
    const val TEST_ANNOTATION_PROCESSOR = "testAnnotationProcessor"

    /* __________________________________________________ */

    const val BOM = "bom"
    const val CATALOG = "catalog"

}