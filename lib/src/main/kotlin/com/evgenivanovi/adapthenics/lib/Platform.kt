@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.lib

import org.gradle.api.JavaVersion

object Platform {

    const val JAVA_LANG_VERSION_VALUE: String = "21"
    val JAVA_LANG_VERSION: JavaVersion = JavaVersion.toVersion(JAVA_LANG_VERSION_VALUE)

    const val KOTLIN_VERSION_VALUE: String = "1.9.22"
    val KOTLIN_VERSION: KotlinVersion = KotlinVersion(1, 9, 22)

    const val KOTLIN_LANG_VERSION_VALUE: String = "1.9"
    val KOTLIN_LANG_VERSION: KotlinVersion = KotlinVersion(1, 9)

}