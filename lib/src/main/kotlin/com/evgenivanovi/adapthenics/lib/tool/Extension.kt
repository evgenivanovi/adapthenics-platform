@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.lib.tool

import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.plugins.ide.idea.GenerateIdeaModule
import org.gradle.plugins.ide.idea.model.IdeaModel
import java.io.File

/* __________________________________________________ */

const val JAVA_PLUGIN_ID = "java"
const val IDEA_PLUGIN_ID = "idea"

const val ARCHIVE_CLASSIFIER_SOURCES = "sources"
const val ARCHIVE_CLASSIFIER_JAVADOC = "javadoc"

const val SRC_DIR = "src"
const val SRC_MAIN_DIR = "main"
const val SRC_TEST_DIR = "test"

/* __________________________________________________ */

fun getProjectSourceSets(project: Project): SourceSetContainer {
    return project
        .extensions
        .getByType(SourceSetContainer::class.java)
}

fun getJavaSourceSets(project: Project): SourceSetContainer {
    return project
        .extensions
        .getByType(JavaPluginExtension::class.java)
        .sourceSets
}

fun getJavaMainSourceSets(project: Project): SourceSet {
    return getJavaSourceSets(project)
        .getByName(SourceSet.MAIN_SOURCE_SET_NAME)
}

fun getJavaTestSourceSets(project: Project): SourceSet {
    return getJavaSourceSets(project)
        .getByName(SourceSet.TEST_SOURCE_SET_NAME)
}

/* __________________________________________________ */

/**
 * Link:
 * - https://github.com/google/protobuf-gradle-plugin/blob/master/src/main/groovy/com/google/protobuf/gradle/ProtobufPlugin.groovy#L265
 */
fun addToIdeSources(
    project: Project,
    file: File,
    isTest: Boolean,
    isGenerated: Boolean
) {

    project.plugins.withId(IDEA_PLUGIN_ID) {

        val model: IdeaModel = project
            .extensions
            .findByType(IdeaModel::class.java)
            ?: return@withId

        when (isTest) {
            true -> {
                model
                    .module
                    .testSources
                    .plus(file)
            }

            false -> {
                model
                    .module
                    .sourceDirs
                    .plusAssign(file)
            }
        }

        if (isGenerated) {
            model
                .module
                .generatedSourceDirs
                .plusAssign(file)
        }

        project
            .tasks
            .withType(GenerateIdeaModule::class.java)
            .forEach { module ->
                module.doFirst {
                    // This is required because the intellij plugin does not allow adding source directories
                    // that do not exist. The intellij config files should be valid from the start even if a
                    // user runs './gradlew idea' before running './gradlew somePluginTaskName'.
                    file.mkdirs()
                }
            }

    }

}

/* __________________________________________________ */