package com.evgenivanovi.adapthenics.lib.tool

import com.evgenivanovi.adapthenics.lib.util.Gradle
import org.gradle.api.Project

/* __________________________________________________ */

fun Collection<Project>.onlyLibraries(): List<Project> {
    return withoutBOMs()
        .withoutCatalogs()
        .withoutAggregates()
}

/* __________________________________________________ */

fun Collection<Project>.onlyBOMs(): List<Project> {
    return filter { Gradle.BOM in it.name }
}

fun Collection<Project>.withoutBOMs(): List<Project> {
    return filterNot { Gradle.BOM in it.name }
}

/* __________________________________________________ */

fun Collection<Project>.onlyCatalog(): List<Project> {
    return filter { Gradle.CATALOG in it.name }
}

fun Collection<Project>.withoutCatalogs(): List<Project> {
    return filterNot { Gradle.CATALOG in it.name }
}

/* __________________________________________________ */

fun Project.isAggregate(): Boolean {
    return childProjects.isNotEmpty()
}

fun Collection<Project>.onlyAggregates(): List<Project> {
    return filter(Project::isAggregate)
}

fun Collection<Project>.withoutAggregates(): List<Project> {
    return filterNot(Project::isAggregate)
}