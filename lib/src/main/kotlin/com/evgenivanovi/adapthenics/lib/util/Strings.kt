package com.evgenivanovi.adapthenics.lib.util

object Strings {

    /* __________________________________________________ */

    const val TRUE = "true"
    const val FALSE = "false"

    const val ON = "on"
    const val OFF = "off"

    /* __________________________________________________ */

    const val EMPTY = ""
    const val SPACE = " "

    const val DOT = "."
    const val COMMA = ","

    const val DOT_SPACE = ". "
    const val COMMA_SPACE = ", "

    const val COLON = ":"
    const val COLON_SPACE = ": "

    const val SEMICOLON = ";"
    const val SEMICOLON_SPACE = "; "

    const val APOSTROPHE = "\'"
    const val SINGLE_QUOTE = "\'"
    const val DOUBLE_QUOTES = "\""

    const val DASH = "-"
    const val UNDERSCORE = "_"

    const val SLASH = "/"
    const val BACKSLASH = "\\"

    const val PLUS = "+"
    const val MINUS = "-"

    const val STAR = "*"
    const val EQUAL = "="

    const val AT = "@"
    const val AMPERSAND = "&"

    const val QUESTION_MARK = "?"
    const val EXCLAMATION_MARK = "!"

    const val OPEN_PARENTHESIS = "("
    const val CLOSE_PARENTHESIS = ")"

    const val OPEN_CURLY_BRACKET = "{"
    const val CLOSE_CURLY_BRACKET = "}"

    const val OPEN_SQUARE_BRACKET = "["
    const val CLOSE_SQUARE_BRACKET = "]"

}