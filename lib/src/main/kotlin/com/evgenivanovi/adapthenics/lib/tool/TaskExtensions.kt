package com.evgenivanovi.adapthenics.lib.tool

import org.gradle.api.Task

fun Task.afterDepends(others: List<Task>): Task {
    others.forEach { this.dependsOn(it) }
    return this
}

fun Task.beforeDepends(others: List<Task>): Task {
    others.forEach { it.dependsOn(this) }
    return this
}