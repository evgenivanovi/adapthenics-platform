@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.lib.tool

import com.evgenivanovi.adapthenics.lib.util.Jvm.CLASSPATH_PREFIX
import com.evgenivanovi.adapthenics.lib.util.Strings
import java.io.File

object AdapthenicsProject {

    private val MAIN_PACKAGE_PARTS = arrayOf(
        "com",
        "evgenivanovi",
        "adapthenics"
    )

    fun mainPackage(separator: String = Strings.DOT): String {
        return MAIN_PACKAGE_PARTS
            .joinToString(separator = separator)
    }

    fun mainPackageClasspath(): String {
        return CLASSPATH_PREFIX
            .plus(mainPackage(File.separator))
    }

}