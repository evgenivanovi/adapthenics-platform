package com.evgenivanovi.adapthenics.lib.tool

import com.evgenivanovi.adapthenics.lib.util.Chars
import org.gradle.api.initialization.dsl.VersionCatalogBuilder

// CatalogVersion
interface CV {
    val alias: String
    val version: String
}

// AbstractCatalogVersion
abstract class ACV(
    override val alias: String,
    override val version: String
) : CV

// ImplementationCatalogVersion
class ICV(
    override val alias: String,
    override val version: String
) : ACV(alias, version)

// CatalogLibrary
interface CL {

    val module: String

    val group: String
        get() = module.substringBeforeLast(Chars.COLON)

    val artifact: String
        get() = module.substringAfterLast(Chars.COLON)

    val alias: String
        get() = module.substringAfterLast(Chars.COLON)

    companion object {

        fun fromModule(module: String): CL {
            return object : CL {
                override val module: String
                    get() = module
            }
        }

    }

}

// AbstractCatalogLibrary
abstract class ACL(
    override val alias: String,
    override val module: String
) : CL

// CatalogPlugin
interface CP {
    val alias: String
    val id: String
}

// AbstractCatalogPlugin
abstract class ACP(
    override val alias: String,
    override val id: String
) : CP

// CatalogBundle
interface CB {
    val alias: String
    val libs: List<String>
}

// AbstractCatalogBundle
abstract class ACB(
    override val alias: String,
    override val libs: List<String>
) : CB

/* __________________________________________________ */

fun VersionCatalogBuilder.registerLibs(
    version: CV,
    vararg libraries: String,
) {
    libraries
        .map(CL.Companion::fromModule)
        .let { registerLibs(version, *it.toTypedArray()) }
}

fun VersionCatalogBuilder.registerLibs(
    version: CV,
    vararg libraries: CL,
) {
    version(version.alias, version.version)
    libraries.forEach { library(it, version) }
}

fun VersionCatalogBuilder.registerLib(
    version: CV,
    library: CL
) {
    version(version.alias, version.version)
    library(library, version)
}

/* __________________________________________________ */

fun VersionCatalogBuilder.registerPlugins(
    version: CV,
    vararg plugins: CP,
) {
    version(version.alias, version.version)
    plugins.forEach { plugin(it, version) }
}

fun VersionCatalogBuilder.registerPlugin(
    version: CV,
    plugin: CP
) {
    version(version.alias, version.version)
    plugin(plugin, version)
}

/* __________________________________________________ */

fun VersionCatalogBuilder.version(
    version: CV
) {
    version(version.alias, version.version)
}

fun VersionCatalogBuilder.library(
    lib: CL,
    version: String
) {
    library(lib.alias, lib.group, lib.artifact)
        .versionRef(version)
}

fun VersionCatalogBuilder.library(
    lib: CL,
    version: CV
) {
    library(lib.alias, lib.group, lib.artifact)
        .versionRef(version.alias)
}

fun VersionCatalogBuilder.plugin(
    plugin: CP,
    version: String
) {
    plugin(plugin.alias, plugin.id)
        .versionRef(version)
}

fun VersionCatalogBuilder.plugin(
    plugin: CP,
    version: CV
) {
    plugin(plugin.alias, plugin.id)
        .versionRef(version.alias)
}

fun VersionCatalogBuilder.bundle(
    bundle: CB
) {
    bundle(bundle.alias, bundle.libs)
}

/* __________________________________________________ */