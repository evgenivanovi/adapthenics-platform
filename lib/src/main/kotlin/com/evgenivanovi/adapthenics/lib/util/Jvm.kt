@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.lib.util

import com.evgenivanovi.adapthenics.lib.util.Regexes.ANY
import com.evgenivanovi.adapthenics.lib.util.Regexes.END
import com.evgenivanovi.adapthenics.lib.util.Regexes.Esc.ESC_DOT
import com.evgenivanovi.adapthenics.lib.util.Regexes.ONE_OR_MORE_TIMES
import com.evgenivanovi.adapthenics.lib.util.Regexes.START
import com.evgenivanovi.adapthenics.lib.util.Strings.DOT
import com.evgenivanovi.adapthenics.lib.util.Strings.STAR

object Jvm {

    const val CLASSPATH_PREFIX = "classpath:"
    const val FILESYSTEM_PREFIX = "filesystem:"

    /* __________________________________________________ */

    const val JAVA_BASE_PACKAGE = "java"
    const val JAVA_EXT = "java"
    const val JAVA_FILE_NAME_MATCHER = STAR + DOT + JAVA_EXT

    const val JAVA_FILE_NAME_PATTERN: String =
        START + ANY + ONE_OR_MORE_TIMES + ESC_DOT + JAVA_EXT + END

    val JAVA_FILE_NAME_REGEX: Regex =
        Regex(JAVA_FILE_NAME_PATTERN)

    /* __________________________________________________ */

    const val KOTLIN_BASE_PACKAGE = "kotlin"
    const val KOTLIN_EXT = "kt"
    const val KOTLIN_FILE_NAME_MATCHER = STAR + DOT + KOTLIN_EXT

    const val KOTLIN_FILE_NAME_PATTERN: String =
        START + ANY + ONE_OR_MORE_TIMES + ESC_DOT + KOTLIN_EXT + END

    val KOTLIN_FILE_NAME_REGEX: Regex =
        Regex(KOTLIN_FILE_NAME_PATTERN)

    /* __________________________________________________ */

    const val CLASS_EXT = "class"
    const val CLASS_FILE_NAME_MATCHER = STAR + DOT + CLASS_EXT

    const val CLASS_FILE_NAME_PATTERN: String =
        START + ANY + ONE_OR_MORE_TIMES + ESC_DOT + CLASS_EXT + END

    val CLASS_FILE_NAME_REGEX: Regex =
        Regex(CLASS_FILE_NAME_PATTERN)

    /* __________________________________________________ */

    const val JAR_EXT = "jar"
    const val JAR_FILE_NAME_MATCHER = STAR + DOT + JAR_EXT

    const val JAR_FILE_NAME_PATTERN: String =
        START + ANY + ONE_OR_MORE_TIMES + ESC_DOT + JAR_EXT + END

    val JAR_FILE_NAME_REGEX: Regex =
        Regex(JAR_FILE_NAME_PATTERN)

    /* __________________________________________________ */

    val JVM_FILE_NAME_MATCHERS = listOf(
        JAVA_FILE_NAME_MATCHER,
        KOTLIN_FILE_NAME_MATCHER,
        CLASS_FILE_NAME_MATCHER,
        JAR_FILE_NAME_MATCHER,
    )

    val JVM_FILE_NAME_PATTERNS = listOf(
        JAVA_FILE_NAME_PATTERN,
        KOTLIN_FILE_NAME_PATTERN,
        CLASS_FILE_NAME_PATTERN,
        JAR_FILE_NAME_PATTERN,
    )

    val JVM_FILE_NAME_REGEXPS = listOf(
        JAVA_FILE_NAME_REGEX,
        KOTLIN_FILE_NAME_REGEX,
        CLASS_FILE_NAME_REGEX,
        JAR_FILE_NAME_REGEX,
    )

}