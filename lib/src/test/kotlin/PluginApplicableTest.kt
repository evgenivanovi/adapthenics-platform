import org.gradle.api.Project
import org.gradle.internal.impldep.junit.framework.TestCase.assertTrue
import org.gradle.testfixtures.ProjectBuilder

class PluginApplicableTest {

    fun applicableTest() {

        // given
        val id = "com.evgenivanovi.adapthenics.lib"

        // when
        val project: Project = ProjectBuilder.builder().build()
        project.pluginManager.apply(id)

        // then
        assertTrue(project.pluginManager.hasPlugin(id))

    }

}