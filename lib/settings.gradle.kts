@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

dependencyResolutionManagement {

    @Suppress("UnstableApiUsage")
    repositories {
        google()
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
    }

}

pluginManagement {

    repositories {
        google()
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
    }

}
