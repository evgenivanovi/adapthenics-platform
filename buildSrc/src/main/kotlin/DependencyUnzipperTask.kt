import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Zip
import java.io.File

open class DependencyUnzipperTask : Copy() {

    @TaskAction
    fun action() {
        copy()
    }

    @Override
    fun from(dependencyName: String) {

        project.configurations.getByName(CONFIGURATION_NAME)
            .asFileTree.files
            .filter { it.matchesZip(dependencyName) }
            .forEach { from(project.zipTree(it)) }

    }

    private fun File.matchesZip(name: String): Boolean {
        return this.name.contains(name) and
            this.name.endsWith(Zip.ZIP_EXTENSION)
    }

    companion object {

        private const val CONFIGURATION_NAME = "compileOnly"

    }

}