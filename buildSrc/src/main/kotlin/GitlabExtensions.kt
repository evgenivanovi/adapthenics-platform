import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.credentials.HttpHeaderCredentials
import org.gradle.authentication.http.HttpHeaderAuthentication
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.credentials

fun RepositoryHandler.gitlab() {

    val CI: String? = System.getenv("CI")
    val CI_TOKEN_VAR = "Job-Token"
    val CI_TOKEN_VAL = System.getenv("CI_JOB_TOKEN")

    val gitlab = {

        // You can pull packages from the group or project level, but publishing only works at the project level.
        // The group level is essentially an aggregated view of all projects in the group.
        val publish = "https://gitlab.com/api/v4/projects/41857929/packages/maven"

        maven {
            name = "GitLab"
            url = java.net.URI.create(publish)
            credentials(HttpHeaderCredentials::class) {
                name = CI_TOKEN_VAR
                value = CI_TOKEN_VAL
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }

    }

    val local = {
        mavenLocal()
    }

    when (CI == "true") {
        true -> {
            println("Gradle is running on GitLab CI.")
            println("Gradle build is going to use Gitlab Repository.")
            gitlab()
        }

        false -> {
            println("Gradle is running locally.")
            println("Gradle build is going to use Maven Local Repository.")
            local()
        }
    }

}