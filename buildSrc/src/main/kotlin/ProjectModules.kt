@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
    "ConstPropertyName",
)

object ProjectModules {

    /* __________________________________________________ */
    // Platform
    /* __________________________________________________ */

    const val NAME__PLATFORM_DEPENDENCIES_BOM = "platform-dependencies-bom"
    const val MODULE__PLATFORM_DEPENDENCIES_BOM = "adapthenics:platform-dependencies-bom"
    const val PLATFORM_DEPENDENCIES_BOM = ":$MODULE__PLATFORM_DEPENDENCIES_BOM"

    const val NAME__PLATFORM_CATALOG = "platform-catalog"
    const val MODULE__PLATFORM_CATALOG = "adapthenics:platform-catalog"
    const val PLATFORM_CATALOG = ":$MODULE__PLATFORM_CATALOG"

    const val NAME__PLATFORM_BOM = "platform-bom"
    const val MODULE__PLATFORM_BOM = "adapthenics:platform-bom"
    const val PLATFORM_BOM = ":$MODULE__PLATFORM_BOM"

    /* __________________________________________________ */

    const val NAME__UNIT = "unit"
    const val MODULE__UNIT = "adapthenics:platform:core:unit"
    const val UNIT = ":$MODULE__UNIT"

    const val NAME__LOG = "log"
    const val MODULE__LOG = "adapthenics:platform:log"
    const val LOG = ":$MODULE__LOG"

    const val NAME__BASE = "base"
    const val MODULE__BASE = "adapthenics:platform:base"
    const val BASE = ":$MODULE__BASE"

    /* __________________________________________________ */
    // Support
    /* __________________________________________________ */

    const val NAME__JACKSON_SUPPORT = "jackson-support"
    const val MODULE__JACKSON_SUPPORT = "adapthenics:platform:support:jackson-support"
    const val JACKSON_SUPPORT = ":$MODULE__JACKSON_SUPPORT"

    const val NAME__LOGGING_SUPPORT = "logging-support"
    const val MODULE__LOGGING_SUPPORT = "adapthenics:platform:support:logging-support"
    const val LOGGING_SUPPORT = ":$MODULE__LOGGING_SUPPORT"

    const val NAME__REACTOR_SUPPORT = "reactor-support"
    const val MODULE__REACTOR_SUPPORT = "adapthenics:platform:support:reactor-support"
    const val REACTOR_SUPPORT = ":$MODULE__REACTOR_SUPPORT"

    /* __________________________________________________ */
    // Search
    /* __________________________________________________ */

    const val NAME__SEARCH_SERVICE = "search-service"
    const val MODULE__SEARCH_SERVICE = "adapthenics:platform:search:search-service"
    const val SEARCH_SERVICE = ":$MODULE__SEARCH_SERVICE"

    const val NAME__SEARCH_MONGO = "search-mongodb"
    const val MODULE__SEARCH_MONGO = "adapthenics:platform:search:search-mongodb"
    const val SEARCH_MONGO = ":$MODULE__SEARCH_MONGO"

    const val NAME_SEARCH_POSTGRES = "search-postgres"
    const val MODULE__SEARCH_POSTGRES = "adapthenics:platform:search:search-postgres"
    const val SEARCH_POSTGRES = ":$MODULE__SEARCH_POSTGRES"

    /* __________________________________________________ */
    // Domain
    /* __________________________________________________ */

    const val NAME__DOMAIN_MODEL = "domain-model"
    const val MODULE__DOMAIN_MODEL = "adapthenics:platform:domain:domain-model"
    const val DOMAIN_MODEL = ":$MODULE__DOMAIN_MODEL"

    const val NAME__DOMAIN_SERVICE = "domain-service"
    const val MODULE__DOMAIN_SERVICE = "adapthenics:platform:domain:domain-service"
    const val DOMAIN_SERVICE = ":$MODULE__DOMAIN_SERVICE"

    const val NAME_DOMAIN_SEARCH = "domain-search"
    const val MODULE__DOMAIN_SEARCH = "adapthenics:platform:domain:domain-search"
    const val DOMAIN_SEARCH = ":$MODULE__DOMAIN_SEARCH"

    /* __________________________________________________ */
    // API
    /* __________________________________________________ */

    const val NAME__API = "api"
    const val MODULE__API = "adapthenics:platform:api:api:api"
    const val API = ":$MODULE__API"

    const val NAME__API_SPEC = "api-spec"
    const val MODULE__API_SPEC = "adapthenics:platform:api:api-spec"
    const val API_SPEC = ":$MODULE__API_SPEC"

    const val NAME__API_STRUCTS = "api-structs"
    const val MODULE__API_STRUCTS = "adapthenics:platform:api:api:api-structs"
    const val API_STRUCTS = ":$MODULE__API_STRUCTS"

    const val NAME__API_EXCHANGE = "api-exchange"
    const val MODULE__API_EXCHANGE = "adapthenics:platform:api:api:api-exchange"
    const val API_EXCHANGE = ":$MODULE__API_EXCHANGE"

    const val NAME__API_HANDLERS = "api-handlers"
    const val MODULE__API_HANDLERS = "adapthenics:platform:api:api:api-handlers"
    const val API_HANDLERS = ":$MODULE__API_HANDLERS"

    const val NAME__API_JACKSON = "api-jackson"
    const val MODULE__API_JACKSON = "adapthenics:platform:api:api:api-jackson"
    const val API_JACKSON = ":$MODULE__API_JACKSON"

    /* __________________________________________________ */
    // API Search
    /* __________________________________________________ */

    const val NAME__API_SEARCH_JACKSON = "api-search-jackson"
    const val MODULE__API_SEARCH_JACKSON = "adapthenics:platform:api:api-search:api-search-jackson"
    const val API_SEARCH__JACKSON = ":$MODULE__API_SEARCH_JACKSON"

    const val NAME__API_SEARCH_HANDLERS = "api-search-handlers"
    const val MODULE__API_SEARCH_HANDLERS = "adapthenics:platform:api:api-search:api-search-handlers"
    const val API_SEARCH_HANDLERS = ":$MODULE__API_SEARCH_HANDLERS"

    /* __________________________________________________ */
    // API Operations Properties
    /* __________________________________________________ */

    const val NAME__API_OPERATIONS_CORE = "api-operations-core"
    const val MODULE__API_OPERATIONS_CORE = "adapthenics:platform:api:api-operations:api-operations-core"
    const val API_OPERATIONS_CORE = ":$MODULE__API_OPERATIONS_CORE"

    const val NAME__API_OPERATIONS_SEARCH = "api-operations-search"
    const val MODULE__API_OPERATIONS_SEARCH = "adapthenics:platform:api:api-operations:api-operations-search"
    const val API_OPERATIONS_SEARCH = ":$MODULE__API_OPERATIONS_SEARCH"

    const val NAME__API_OPERATIONS_HTTP = "api-operations-http"
    const val MODULE__API_OPERATIONS_HTTP = "adapthenics:platform:api:api-operations:api-operations-http"
    const val API_OPERATIONS_HTTP = ":$MODULE__API_OPERATIONS_HTTP"

    const val NAME__API_OPERATIONS_RSOCKET = "api-operations-rsocket"
    const val MODULE__API_OPERATIONS_RSOCKET = "adapthenics:platform:api:api-operations:api-operations-rsocket"
    const val API_OPERATIONS_RSOCKET = ":$MODULE__API_OPERATIONS_RSOCKET"

    /* __________________________________________________ */
    // ETCD Properties
    /* __________________________________________________ */

    const val NAME__ETCD_CLIENT = "etcd-client"
    const val MODULE__ETCD_CLIENT = "adapthenics:platform:etcd:etcd-client"
    const val ETCD_CLIENT = ":$MODULE__ETCD_CLIENT"

    /* __________________________________________________ */
    // Kafka Properties
    /* __________________________________________________ */

    const val NAME__KAFKA_ADAPTER_CORE = "kafka-adapter-core"
    const val MODULE__KAFKA_ADAPTER_CORE = "adapthenics:platform:kafka:kafka-adapter-core"
    const val KAFKA_ADAPTER_CORE = ":$MODULE__KAFKA_ADAPTER_CORE"

    const val NAME__KAFKA_INFRASTRUCTURE = "kafka-infrastructure"
    const val MODULE__KAFKA_INFRASTRUCTURE = "adapthenics:platform:kafka:kafka-infrastructure"
    const val KAFKA_INFRASTRUCTURE = ":$MODULE__KAFKA_INFRASTRUCTURE"

    /* __________________________________________________ */
    // jOOQ
    /* __________________________________________________ */

    const val NAME__JOOQX = "jooqx"
    const val MODULE__JOOQX = "adapthenics:platform:jooq:jooqx"
    const val JOOQX = ":$MODULE__JOOQX"

    /* __________________________________________________ */
    // RTC Properties
    /* __________________________________________________ */

    const val NAME__RTC_DOMAIN = "rtc-domain"
    const val MODULE__RTC_DOMAIN = "adapthenics:platform:rtc:rtc-domain"
    const val RTC_DOMAIN = ":$MODULE__RTC_DOMAIN"

    const val NAME__RTC_ETCD_SCHEMA = "rtc-etcd-schema"
    const val MODULE__RTC_ETCD_SCHEMA = "adapthenics:platform:rtc:rtc-etcd-schema"
    const val RTC_ETCD_SCHEMA = ":$MODULE__RTC_ETCD_SCHEMA"

    const val NAME__RTC_ETCD_ADAPTER = "rtc-etcd-adapter"
    const val MODULE__RTC_ETCD_ADAPTER = "adapthenics:platform:rtc:rtc-etcd-adapter"
    const val RTC_ETCD_ADAPTER = ":$MODULE__RTC_ETCD_ADAPTER"

    const val NAME__RTC_ETCD_INFRA = "rtc-etcd-infra"
    const val MODULE__RTC_ETCD_INFRA = "adapthenics:platform:rtc:rtc-etcd-infra"
    const val RTC_ETCD_INFRA = ":$MODULE__RTC_ETCD_INFRA"

    /* __________________________________________________ */
    // Schema
    /* __________________________________________________ */

    const val NAME__SCHEMA_MAPPER = "schema-mapper"
    const val MODULE__SCHEMA_MAPPER = "adapthenics:platform:schema:schema-mapper"
    const val SCHEMA_MAPPER = ":$MODULE__SCHEMA_MAPPER"

    /* __________________________________________________ */
    // Mongo DB
    /* __________________________________________________ */

    const val NAME__MONGODB_ADAPTER = "mongodb-adapter"
    const val MODULE__MONGODB_ADAPTER = "adapthenics:platform:mongodb:mongodb-adapter"
    const val MONGODB_ADAPTER = ":$MODULE__MONGODB_ADAPTER"

    const val NAME__MONGODB_INFRASTRUCTURE = "mongodb-infrastructure"
    const val MODULE__MONGODB_INFRASTRUCTURE = "adapthenics:platform:mongodb:mongodb-infrastructure"
    const val MONGODB_INFRASTRUCTURE = ":$MODULE__MONGODB_INFRASTRUCTURE"

    /* __________________________________________________ */
    // PostgreSQL
    /* __________________________________________________ */

    const val NAME__POSTGRES_ADAPTER = "postgres-adapter"
    const val MODULE__POSTGRES_ADAPTER = "adapthenics:platform:postgres:postgres-adapter"
    const val POSTGRES_ADAPTER = ":$MODULE__POSTGRES_ADAPTER"

    const val NAME__POSTGRES_INFRASTRUCTURE = "postgres-infrastructure"
    const val MODULE__POSTGRES_INFRASTRUCTURE = "adapthenics:platform:postgres:postgres-infrastructure"
    const val POSTGRES_INFRASTRUCTURE = ":$MODULE__POSTGRES_INFRASTRUCTURE"

    const val NAME__POSTGRES_INFRASTRUCTURE_ALL_TEST = "postgres-infrastructure-all-test"
    const val MODULE__POSTGRES_INFRASTRUCTURE_ALL_TEST = "adapthenics:platform:postgres:postgres-infrastructure-all-test"
    const val POSTGRES_INFRASTRUCTURE_ALL_TEST = ":$MODULE__POSTGRES_INFRASTRUCTURE_ALL_TEST"

    /* __________________________________________________ */
    // Spring
    /* __________________________________________________ */

    const val NAME__SPRING_CORE = "spring-core"
    const val MODULE__SPRING_CORE = "adapthenics:platform:spring:spring-core"
    const val SPRING_CORE = ":$MODULE__SPRING_CORE"

    const val NAME__SPRING_WEBFLUX = "spring-webflux"
    const val MODULE__SPRING_WEBFLUX = "adapthenics:platform:spring:spring-webflux"
    const val SPRING_WEBFLUX = ":$MODULE__SPRING_WEBFLUX"

    const val NAME__SPRING_STARTER_CORE = "spring-starter-core"
    const val MODULE__SPRING_STARTER_CORE = "adapthenics:platform:spring:spring-starter-core"
    const val SPRING_STARTER_CORE = ":$MODULE__SPRING_STARTER_CORE"

    const val NAME__SPRING_STARTER_LOGGING = "spring-starter-logging"
    const val MODULE__SPRING_STARTER_LOGGING = "adapthenics:platform:spring:spring-starter-logging"
    const val SPRING_STARTER_LOGGING = ":$MODULE__SPRING_STARTER_LOGGING"

    const val NAME__SPRING_STARTER_MONGO = "spring-starter-mongo"
    const val MODULE__SPRING_STARTER_MONGO = "adapthenics:platform:spring:spring-starter-mongo"
    const val SPRING_STARTER_MONGO = ":$MODULE__SPRING_STARTER_MONGO"

    const val NAME__SPRING_STARTER_POSTGRES = "spring-starter-postgres"
    const val MODULE__SPRING_STARTER_POSTGRES = "adapthenics:platform:spring:spring-starter-postgres"
    const val SPRING_STARTER_POSTGRES = ":$MODULE__SPRING_STARTER_POSTGRES"

    const val NAME__SPRING_STARTER_JOOQ = "spring-starter-jooq"
    const val MODULE__SPRING_STARTER_JOOQ = "adapthenics:platform:spring:spring-starter-jooq"
    const val SPRING_STARTER_JOOQ = ":$MODULE__SPRING_STARTER_JOOQ"

    const val NAME__SPRING_STARTER_KAFKA = "spring-starter-kafka"
    const val MODULE__SPRING_STARTER_KAFKA = "adapthenics:platform:spring:spring-starter-kafka"
    const val SPRING_STARTER_KAFKA = ":$MODULE__SPRING_STARTER_KAFKA"

    const val NAME__SPRING_STARTER_ETCD = "spring-starter-etcd"
    const val MODULE__SPRING_STARTER_ETCD = "adapthenics:platform:spring:spring-starter-etcd"
    const val SPRING_STARTER_ETCD = ":$MODULE__SPRING_STARTER_ETCD"

    const val NAME__SPRING_RTC_ADAPTER = "spring-rtc-adapter"
    const val MODULE__SPRING_RTC_ADAPTER = "adapthenics:platform:spring:spring-rtc-adapter"
    const val SPRING_RTC_ADAPTER = ":$MODULE__SPRING_RTC_ADAPTER"

    const val NAME__SPRING_RTC_INFRA = "spring-rtc-infra"
    const val MODULE__SPRING_RTC_INFRA = "adapthenics:platform:spring:spring-rtc-infra"
    const val SPRING_RTC_INFRA = ":$MODULE__SPRING_RTC_INFRA"

    const val NAME__SPRING_STARTER_RTC = "spring-starter-rtc"
    const val MODULE__SPRING_STARTER_RTC = "adapthenics:platform:spring:spring-starter-rtc"
    const val SPRING_STARTER_RTC = ":$MODULE__SPRING_STARTER_RTC"

    const val NAME__SPRING_STARTER_SERVER_HTTP = "spring-starter-server-http"
    const val MODULE__SPRING_STARTER_SERVER_HTTP = "adapthenics:platform:spring:spring-starter-server-http"
    const val SPRING_STARTER_SERVER_HTTP = ":$MODULE__SPRING_STARTER_SERVER_HTTP"

    const val NAME__SPRING_STARTER_SERVER_RSOCKET = "spring-starter-server-rsocket"
    const val MODULE__SPRING_STARTER_SERVER_RSOCKET = "adapthenics:platform:spring:spring-starter-server-rsocket"
    const val SPRING_STARTER_SERVER_RSOCKET = ":$MODULE__SPRING_STARTER_SERVER_RSOCKET"

}