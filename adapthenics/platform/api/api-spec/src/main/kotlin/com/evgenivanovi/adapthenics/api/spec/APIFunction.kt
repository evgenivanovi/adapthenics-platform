package com.evgenivanovi.adapthenics.api.spec

interface APIFunction {

    fun resource(): String

    fun operation(): FunctionOperation

    companion object {

        const val OPERATIONS = "operations"

    }

}