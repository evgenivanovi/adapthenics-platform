package com.evgenivanovi.adapthenics.api.spec

object WellknownFunctionOperations {

    /* CRUD Operations */
    const val GET_OPERATION = "get"

    const val SAVE_OPERATION = "save"
    const val CREATE_OPERATION = "create"

    const val UPDATE_OPERATION = "update"
    const val UPSERT_OPERATION = "upsert"
    const val MERGE_OPERATION = "merge"

    const val DELETE_OPERATION = "delete"
    const val DELETE_BULK_OPERATION = "delete+bulk"

    const val ARCHIVE_OPERATION = "archive"
    const val ARCHIVE_BULK_OPERATION = "archive+bulk"

    const val UNARCHIVE_OPERATION = "unarchive"
    const val UNARCHIVE_BULK_OPERATION = "unarchive+bulk"

    /* Search Operations */
    const val SEARCH_COUNT_OPERATION = "search+count"
    const val SEARCH_LIST_OPERATION = "search+list"
    const val SEARCH_STREAM_OPERATION = "search+stream"
    const val SEARCH_PAGED_OPERATION = "search+paged"
    const val SEARCH_CHUNKED_OPERATION = "search+chunked"

    val GET = object : FunctionOperation {
        override fun name(): String {
            return GET_OPERATION
        }
    }

    val SAVE = object : FunctionOperation {
        override fun name(): String {
            return SAVE_OPERATION
        }
    }

    val CREATE = object : FunctionOperation {
        override fun name(): String {
            return CREATE_OPERATION
        }
    }

    val UPDATE = object : FunctionOperation {
        override fun name(): String {
            return UPDATE_OPERATION
        }
    }

    val UPSERT = object : FunctionOperation {
        override fun name(): String {
            return UPSERT_OPERATION
        }
    }

    val MERGE = object : FunctionOperation {
        override fun name(): String {
            return MERGE_OPERATION
        }
    }

    val DELETE = object : FunctionOperation {
        override fun name(): String {
            return DELETE_OPERATION
        }
    }

    val DELETE_BULK = object : FunctionOperation {
        override fun name(): String {
            return DELETE_BULK_OPERATION
        }
    }

    val ARCHIVE = object : FunctionOperation {
        override fun name(): String {
            return ARCHIVE_OPERATION
        }
    }

    val ARCHIVE_BULK = object : FunctionOperation {
        override fun name(): String {
            return ARCHIVE_BULK_OPERATION
        }
    }

    val UNARCHIVE = object : FunctionOperation {
        override fun name(): String {
            return UNARCHIVE_OPERATION
        }
    }

    val UNARCHIVE_BULK = object : FunctionOperation {
        override fun name(): String {
            return UNARCHIVE_BULK_OPERATION
        }
    }

    val SEARCH_COUNT = object : FunctionOperation {
        override fun name(): String {
            return SEARCH_COUNT_OPERATION
        }
    }

    val SEARCH_LIST = object : FunctionOperation {
        override fun name(): String {
            return SEARCH_LIST_OPERATION
        }
    }

    val SEARCH_STREAM = object : FunctionOperation {
        override fun name(): String {
            return SEARCH_STREAM_OPERATION
        }
    }

    val SEARCH_PAGED = object : FunctionOperation {
        override fun name(): String {
            return SEARCH_PAGED_OPERATION
        }
    }

    val SEARCH_CHUNKED = object : FunctionOperation {
        override fun name(): String {
            return SEARCH_CHUNKED_OPERATION
        }
    }

}