package com.evgenivanovi.adapthenics.api.spec

interface FunctionOperation {

    fun name(): String

}