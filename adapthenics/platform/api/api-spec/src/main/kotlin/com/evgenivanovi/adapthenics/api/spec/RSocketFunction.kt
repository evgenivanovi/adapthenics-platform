package com.evgenivanovi.adapthenics.api.spec

import com.evgenivanovi.kt.string.Strings.DOT
import com.evgenivanovi.kt.string.build

interface RSocketFunction : APIFunction {

    override fun resource(): String

    override fun operation(): FunctionOperation

    fun route(): String {
        return resource().plus(path())
    }

    fun path(): String {
        return StringBuilder()
            .append(SEPARATOR)
            .append(APIFunction.OPERATIONS)
            .append(SEPARATOR)
            .append(operation().name())
            .build()
    }

    companion object {

        const val SEPARATOR = DOT

    }

}