package com.evgenivanovi.adapthenics.api.operations.http

import com.evgenivanovi.adapthenics.api.contract.notification.NotificationMessage
import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.operations.core.APIOperations
import kotlinx.coroutines.flow.Flow
import org.springframework.web.reactive.function.client.WebClient

class HTTPOperations(
    private val client: WebClient,
) : APIOperations {

    override suspend fun send(request: NotificationMessage<*>) {
        TODO("Not yet implemented")
    }

    override suspend fun <R : Any> requestResponse(request: RequestMessage<*>): ResponseMessage<R> {
        TODO("Not yet implemented")
    }

    override fun <R : Any> requestStream(request: RequestMessage<*>): Flow<ResponseMessage<R>> {
        TODO("Not yet implemented")
    }

}