package com.evgenivanovi.adapthenics.api.operations.search

import com.evgenivanovi.adapthenics.api.contract.request.IDRequest
import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage

interface FindingOperations<ID : Any> {

    suspend fun get(
        request: RequestMessage<IDRequest<ID>>,
    ): ResponseMessage<Any>

}