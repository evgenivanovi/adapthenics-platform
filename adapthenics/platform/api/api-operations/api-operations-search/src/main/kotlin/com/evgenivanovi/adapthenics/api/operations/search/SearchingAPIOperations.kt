package com.evgenivanovi.adapthenics.api.operations.search

import com.evgenivanovi.adapthenics.api.contract.response.ListResponse
import com.evgenivanovi.adapthenics.search.api.request.PageSearchRequest
import com.evgenivanovi.adapthenics.search.api.request.SearchRequest
import com.evgenivanovi.adapthenics.search.api.request.ChunkSearchRequest
import com.evgenivanovi.adapthenics.search.api.request.CursorSearchRequest
import com.evgenivanovi.adapthenics.search.api.response.ChunkingResponse
import com.evgenivanovi.adapthenics.search.api.response.CursorResponse
import com.evgenivanovi.adapthenics.search.api.response.PaginationResponse

interface SearchingAPIOperations<T : Any> {

    suspend fun search(
        request: SearchRequest,
    ): ListResponse<T>

    suspend fun searchPaged(
        request: PageSearchRequest,
    ): PaginationResponse<T>

    suspend fun searchChunked(
        request: ChunkSearchRequest,
    ): ChunkingResponse<T>

    suspend fun searchCursored(
        request: CursorSearchRequest,
    ): CursorResponse<T>

}