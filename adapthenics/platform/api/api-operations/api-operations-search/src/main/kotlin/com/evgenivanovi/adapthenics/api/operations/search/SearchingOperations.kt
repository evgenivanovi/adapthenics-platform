package com.evgenivanovi.adapthenics.api.operations.search

import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ListResponse
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.search.api.request.CountRequest
import com.evgenivanovi.adapthenics.search.api.request.PageSearchRequest
import com.evgenivanovi.adapthenics.search.api.request.SearchRequest
import com.evgenivanovi.adapthenics.search.api.request.ChunkSearchRequest
import com.evgenivanovi.adapthenics.search.api.request.CursorSearchRequest
import com.evgenivanovi.adapthenics.search.api.response.ChunkingResponse
import com.evgenivanovi.adapthenics.search.api.response.CountResponse
import com.evgenivanovi.adapthenics.search.api.response.CursorResponse
import com.evgenivanovi.adapthenics.search.api.response.PaginationResponse

interface SearchingOperations {

    suspend fun searchList(
        request: RequestMessage<SearchRequest>,
    ): ResponseMessage<ListResponse<Any>>

    suspend fun searchPaged(
        request: RequestMessage<PageSearchRequest>,
    ): ResponseMessage<PaginationResponse<Any>>

    suspend fun searchChunked(
        request: RequestMessage<ChunkSearchRequest>,
    ): ResponseMessage<ChunkingResponse<Any>>

    suspend fun searchCursored(
        request: RequestMessage<CursorSearchRequest>,
    ): ResponseMessage<CursorResponse<Any>>

    suspend fun searchCount(
        request: RequestMessage<CountRequest>,
    ): ResponseMessage<CountResponse>

}