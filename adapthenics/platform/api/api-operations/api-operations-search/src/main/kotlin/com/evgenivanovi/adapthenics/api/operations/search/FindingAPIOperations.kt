package com.evgenivanovi.adapthenics.api.operations.search

import com.evgenivanovi.adapthenics.api.contract.request.IDRequest

interface FindingAPIOperations<ID : Any, T : Any> {

    suspend fun get(
        request: IDRequest<ID>,
    ): T?

}