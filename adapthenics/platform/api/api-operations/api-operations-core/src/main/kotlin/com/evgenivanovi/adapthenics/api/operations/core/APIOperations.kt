package com.evgenivanovi.adapthenics.api.operations.core

import com.evgenivanovi.adapthenics.api.contract.notification.NotificationMessage
import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import kotlinx.coroutines.flow.Flow

interface APIOperations {

    /**
     * Perform a fireAndForget exchange.
     */
    suspend fun send(request: NotificationMessage<*>)

    /**
     * Perform a requestResponse exchange.
     */
    suspend fun <R : Any> requestResponse(request: RequestMessage<*>): ResponseMessage<R>

    /**
     * Perform an requestStream exchange.
     */
    fun <R : Any> requestStream(request: RequestMessage<*>): Flow<ResponseMessage<R>>

}