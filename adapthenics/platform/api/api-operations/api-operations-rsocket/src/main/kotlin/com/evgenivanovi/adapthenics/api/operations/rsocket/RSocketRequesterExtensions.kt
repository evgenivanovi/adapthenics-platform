package com.evgenivanovi.adapthenics.api.operations.rsocket

import com.evgenivanovi.adapthenics.api.contract.notification.NotificationMessage
import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.retrieveAndAwait
import org.springframework.messaging.rsocket.retrieveFlow
import org.springframework.messaging.rsocket.sendAndAwait

suspend inline fun RSocketRequester.send(
    request: NotificationMessage<*>,
) = coroutineScope {
    withContext(coroutineContext) {
        doSend(request)
    }
}

suspend inline fun RSocketRequester.doSend(
    request: NotificationMessage<*>,
) {
    return route(request.metadata.getMethod())
        .data(request)
        .sendAndAwait()
}

suspend inline fun <reified T : Any> RSocketRequester.requestResponse(
    request: RequestMessage<*>,
): ResponseMessage<T> = coroutineScope {
    withContext(coroutineContext) {
        doRequestResponse(request)
    }
}

suspend inline fun <reified T : Any> RSocketRequester.doRequestResponse(
    request: RequestMessage<*>,
): ResponseMessage<T> {
    return route(request.metadata.getMethod())
        .data(request)
        .retrieveAndAwait()
}

inline fun <reified T : Any> RSocketRequester.requestStream(
    request: RequestMessage<*>,
): Flow<ResponseMessage<T>> {
    return doRequestStream<T>(request)
}

inline fun <reified T : Any> RSocketRequester.doRequestStream(
    request: RequestMessage<*>,
): Flow<ResponseMessage<T>> {
    return route(request.metadata.getMethod())
        .data(request)
        .retrieveFlow()
}