package com.evgenivanovi.adapthenics.api.operations.rsocket

import com.evgenivanovi.adapthenics.api.contract.notification.NotificationMessage
import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.operations.core.APIOperations
import kotlinx.coroutines.flow.Flow
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.retrieveAndAwait
import org.springframework.messaging.rsocket.retrieveFlow
import org.springframework.messaging.rsocket.sendAndAwait

class RSocketOperations(
    private val client: RSocketRequester,
) : APIOperations {

    override suspend fun send(request: NotificationMessage<*>) {
        return client
            .route(request.metadata.getMethod())
            .data(request)
            .sendAndAwait()
    }

    override suspend fun <R : Any> requestResponse(request: RequestMessage<*>): ResponseMessage<R> {
        return client
            .route(request.metadata.getMethod())
            .data(request)
            .retrieveAndAwait()
    }

    override fun <R : Any> requestStream(request: RequestMessage<*>): Flow<ResponseMessage<R>> {
        return client
            .route(request.metadata.getMethod())
            .data(request)
            .retrieveFlow()
    }

}