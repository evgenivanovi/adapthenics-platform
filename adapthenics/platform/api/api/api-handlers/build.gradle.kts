project(ProjectModules.API_HANDLERS) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(project(ProjectModules.BASE))
        testImplementation(project(ProjectModules.BASE))

        implementation(project(ProjectModules.API))
        testImplementation(project(ProjectModules.API))

        implementation(project(ProjectModules.API_EXCHANGE))
        testImplementation(project(ProjectModules.API_EXCHANGE))

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)
}