package com.evgenivanovi.adapthenics.api.contract.handlers

import com.evgenivanovi.adapthenics.api.contract.exchanger.AdviceHandler
import com.evgenivanovi.adapthenics.api.contract.exchanger.ExceptionContext
import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.meta.CallMetadata
import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.adapthenics.api.contract.response.*
import com.evgenivanovi.adapthenics.base.errx.DuplicateErrorex
import com.evgenivanovi.kt.lang.asOf
import kotlin.reflect.KClass

class DuplicateErrorexAdviceHandler(
    private val svc: ResponseAPIService,
) : AdviceHandler {

    override fun target(): KClass<DuplicateErrorex> {
        return DuplicateErrorex::class
    }

    override fun canHandle(ex: Throwable): Boolean {
        return target().java
            .isAssignableFrom(ex::class.java)
    }

    override fun <R : Any> handle(ctx: ExceptionContext<*>): ResponseMessage<R> {
        return toProblemResponse(
            ctx.ex.asOf(target()),
            ctx.meta
        ).asOf()
    }

    private fun toProblemResponse(ex: DuplicateErrorex, meta: CallMetadata?): ProblemResponseMessage {

        val problem = Problem.from(
            ErrorCodes.CONFLICT__DUPLICATE,
            ErrorMessages.CONFLICT__DUPLICATE,
            ex,
        )

        if (meta is RequestMetadata) {
            return svc.problem(
                meta,
                problem,
                ResponseStatusCodes.CONFLICT
            )
        }

        return svc.problem(
            problem,
            ResponseStatusCodes.CONFLICT
        )

    }

}