package com.evgenivanovi.adapthenics.api.contract.notification

import com.evgenivanovi.adapthenics.api.contract.meta.NotificationMetadata
import kotlinx.serialization.Serializable

@Serializable
class NotificationMessage<T>(
    val data: T?,
    val metadata: NotificationMetadata,
)