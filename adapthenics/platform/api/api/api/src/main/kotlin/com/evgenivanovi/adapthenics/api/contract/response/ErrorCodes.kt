package com.evgenivanovi.adapthenics.api.contract.response

object ErrorCodes {

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.TIMEOUT]
    /* __________________________________________________ */

    const val TIMEOUT = "timeout"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.EXHAUSTED]
    /* __________________________________________________ */

    const val EXHAUSTED = "exhausted"

    // The requested operation requires more resources than the quota allows.
    const val EXHAUSTED__EXCEED_QUOTA = "exceed_quota"

    // Too many requests have been sent within a given time span.
    const val EXHAUSTED__EXCEEDED_RATE_LIMIT = "exceeded_rate_limit"

    // The request failed because a per-user rate limit has been reached.
    const val EXHAUSTED__EXCEEDED_RATE_LIMIT_USER = "exceeded_rate_limit_user"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.INVALID]
    /* __________________________________________________ */

    const val INVALID = "invalid"

    // The request failed because it contained an invalid parameter or parameter value
    const val INVALID__PARAMETER = "invalid_parameter"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.UNAUTHORIZED]
    /* __________________________________________________ */

    const val UNAUTHORIZED = "unauthorized"

    // The API key provided in the request is invalid
    const val UNAUTHORIZED__INVALID = "invalid"

    // The API key provided in the request expired
    const val UNAUTHORIZED__EXPIRED = "expired"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.FORBIDDEN]
    /* __________________________________________________ */

    const val FORBIDDEN = "forbidden"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.ABSENT]
    /* __________________________________________________ */

    const val ABSENT = "absent"

    // The request failed because the resource associated with the request not found
    const val ABSENT__ABSENT = "absent"

    // The request failed because the resource associated with the request has been deleted
    const val ABSENT__DELETED = "deleted"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.CONFLICT]
    /* __________________________________________________ */

    const val CONFLICT = "conflict"

    // The requested operation failed because it tried to create a resource that already exists.
    const val CONFLICT__DUPLICATE = "duplicate"

    // The requested operation failed because it tried to update a resource that already has been modified.
    const val CONFLICT__CONCURRENT = "concurrent"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.INTERNAL]
    /* __________________________________________________ */

    const val INTERNAL = "internal"

    /* __________________________________________________ */
    // Error codes for
    // @code [ResponseStatusCodes.UNAVAILABLE]
    /* __________________________________________________ */

    const val UNAVAILABLE = "unavailable"

}