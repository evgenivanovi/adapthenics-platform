package com.evgenivanovi.adapthenics.api.contract.response

import com.evgenivanovi.adapthenics.api.contract.meta.ResponseMetadata

sealed interface ResponseMessage<T> {

    fun getCode(): Int

    fun getStatus(): ResponseStatus

    fun getData(): T?

    fun getMetadata(): ResponseMetadata?

    companion object {

        const val FIELD_NAME_CODE = "code"
        const val FIELD_NAME_STATUS = "status"

        const val FIELD_NAME_DATA = "data"
        const val FIELD_NAME_METADATA = "metadata"

    }

}