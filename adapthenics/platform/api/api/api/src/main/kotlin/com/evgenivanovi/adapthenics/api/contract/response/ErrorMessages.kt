package com.evgenivanovi.adapthenics.api.contract.response

object ErrorMessages {

    const val TIMEOUT = "Operation timed out."

    const val EXHAUSTED = "Resource exhausted."
    const val EXHAUSTED__EXCEED_QUOTA = "Quota exceeded."
    const val EXHAUSTED__EXCEEDED_RATE_LIMIT = "Rate limit exceeded."
    const val EXHAUSTED__EXCEEDED_RATE_LIMIT_USER = "User rate limit exceeded."

    const val INVALID = "Invalid request."
    const val INVALID__PARAMETER = "Invalid parameter."

    const val UNAUTHORIZED = "Authorization required."
    const val UNAUTHORIZED__INVALID = "Invalid credentials."
    const val UNAUTHORIZED__EXPIRED = "Authorization expired."

    const val FORBIDDEN = "Access forbidden."

    const val ABSENT = "Resource not found."
    const val ABSENT__ABSENT = "Resource not found."
    const val ABSENT__DELETED = "Resource was deleted."

    const val CONFLICT = "Conflict detected."
    const val CONFLICT__DUPLICATE = "Duplicate detected."
    const val CONFLICT__CONCURRENT = "Concurrent modification detected."

    const val INTERNAL = "Internal server error."

    const val UNAVAILABLE = "Service unavailable."
}