package com.evgenivanovi.adapthenics.api.contract.response

interface ResponseStatusCode {

    /**
     * The status code to use for the response.
     */
    fun getCode(): Int

    fun getReason(): String

    fun clientError(): Boolean

    fun serverError(): Boolean

}