package com.evgenivanovi.adapthenics.api.contract.response

import kotlinx.serialization.Serializable

@Serializable
class Problem(
    val code: String,
    val message: String,
    val errors: Collection<Error>,
) {

    companion object {

        fun of(code: String, message: String): Problem {
            return Problem(
                code, message, emptyList()
            )
        }

        fun of(code: String, message: String, error: Error): Problem {
            return Problem(
                code, message, listOf(error)
            )
        }

        fun of(code: String, message: String, errors: Collection<Error>): Problem {
            return Problem(
                code, message, errors
            )
        }

        @Serializable
        class Error {

            private var message: String

            private var properties: MutableMap<String, Any?>?

            constructor(message: String) {
                this.message = message
                this.properties = null
            }

            fun getMessage(): String {
                return this.message
            }

            fun setMessage(message: String) {
                this.message = message
            }

            fun setProperty(name: String, value: Any?) {
                initProperties()
                properties?.put(name, value)
            }

            fun setProperties(properties: MutableMap<String, Any?>) {
                this.properties = properties
            }

            fun getProperties(): MutableMap<String, Any?>? {
                return this.properties
            }

            private fun initProperties() {
                if (this.properties == null) this.properties = LinkedHashMap()
            }

        }

    }

}