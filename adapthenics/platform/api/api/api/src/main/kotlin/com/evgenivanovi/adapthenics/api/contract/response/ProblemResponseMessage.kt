package com.evgenivanovi.adapthenics.api.contract.response

import com.evgenivanovi.adapthenics.api.contract.meta.ResponseMetadata
import kotlinx.serialization.Serializable

/**
 * Failure: When an API call is rejected due to invalid data or call conditions.
 * Error: When an API call fails due to an error on the server.
 */
@Serializable
class ProblemResponseMessage(
    private val code: Int,
    private val status: ResponseStatus,

    private val data: Problem?,
    private val metadata: ResponseMetadata?,
) : ResponseMessage<Problem> {

    override fun getCode(): Int {
        return code
    }

    override fun getStatus(): ResponseStatus {
        return status
    }

    override fun getData(): Problem? {
        return data
    }

    override fun getMetadata(): ResponseMetadata? {
        return metadata
    }

    companion object {

        fun failure(
            code: ResponseStatusCode,
            metadata: ResponseMetadata?,
            data: Problem?,
        ): ProblemResponseMessage {
            require(code.clientError())
            return of(
                status = ResponseStatus.FAILURE,
                code = code.getCode(),
                metadata = metadata,
                data = data
            )
        }

        fun error(
            code: ResponseStatusCode,
            metadata: ResponseMetadata?,
            data: Problem?,
        ): ProblemResponseMessage {
            require(code.serverError())
            return of(
                status = ResponseStatus.ERROR,
                code = code.getCode(),
                metadata = metadata,
                data = data
            )
        }

        private fun of(
            status: ResponseStatus,
            code: Int,
            metadata: ResponseMetadata?,
            data: Problem?,
        ): ProblemResponseMessage {
            return ProblemResponseMessage(
                status = status,
                code = code,
                metadata = metadata,
                data = data
            )
        }

    }

}