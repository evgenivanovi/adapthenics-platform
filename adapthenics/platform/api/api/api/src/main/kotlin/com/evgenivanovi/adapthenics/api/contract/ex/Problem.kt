package com.evgenivanovi.adapthenics.api.contract.ex

import com.evgenivanovi.adapthenics.api.contract.response.Problem
import com.evgenivanovi.kt.errx.Error
import com.evgenivanovi.kt.errx.Errorex
import com.evgenivanovi.kt.errx.Is

class ProblemError : Error, Is {

    val problem: Problem

    internal constructor(problem: Problem) {
        this.problem = problem
    }

    override fun error(): String {
        return "[${problem.code}] ${problem.message}"
    }

    override fun isis(err: Error): Boolean {
        if (err !is ProblemError) {
            return false
        }

        return err.problem.code == problem.code
            && err.problem.message == problem.message
    }

    companion object {

        fun of(problem: Problem): ProblemError {
            return ProblemError(problem)
        }

    }

}

class ProblemException(err: ProblemError) : Errorex(err) {

    companion object {

        fun of(problem: Problem): ProblemException {
            return ProblemException(ProblemError(problem))
        }

        fun of(err: ProblemError): ProblemException {
            return ProblemException(err)
        }

    }

}