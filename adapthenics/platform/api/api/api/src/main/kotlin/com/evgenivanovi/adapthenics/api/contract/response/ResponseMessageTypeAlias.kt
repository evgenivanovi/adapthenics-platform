package com.evgenivanovi.adapthenics.api.contract.response

import com.evgenivanovi.adapthenics.api.contract.APIConstants
import com.evgenivanovi.kt.meta.TypeAlias
import kotlin.reflect.KClass

enum class ResponseMessageTypeAlias(
    val alias: String,
    val type: KClass<*>,
) : TypeAlias {

    SUCCESS_MESSAGE(
        APIConstants.RESPONSE_SUCCESS,
        SuccessResponseMessage::class
    ),

    FAILURE_MESSAGE(
        APIConstants.RESPONSE_FAILURE,
        ProblemResponseMessage::class
    ),

    ERROR_MESSAGE(
        APIConstants.RESPONSE_ERROR,
        ProblemResponseMessage::class
    );

    override fun alias(): String {
        return alias
    }

    override fun type(): KClass<*> {
        return type
    }

    companion object {

        fun names(): List<String> {
            return entries.map { it.alias }
        }

        fun classes(): List<KClass<*>> {
            return entries.map { it.type }
        }

    }

}