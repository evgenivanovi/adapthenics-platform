package com.evgenivanovi.adapthenics.api.contract.meta

import kotlinx.serialization.Serializable

@Serializable
sealed interface CallMetadata {

    /**
     * Where a message came from
     */
    fun getOrigin(): String

    /**
     * Which messaging style it was intended to use. (request, notification, response)
     */
    fun getInteraction(): String

}