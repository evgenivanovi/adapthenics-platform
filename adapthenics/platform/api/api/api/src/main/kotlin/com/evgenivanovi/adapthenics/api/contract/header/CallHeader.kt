package com.evgenivanovi.adapthenics.api.contract.header

import com.evgenivanovi.adapthenics.api.contract.header.CallHeader.Requirement.ABSENCE
import com.evgenivanovi.adapthenics.api.contract.header.CallHeader.Requirement.PRESENCE

enum class CallHeader(
    val value: String,
    private val requestRequirement: Requirement,
    private val responseRequirement: Requirement,
    private val notificationRequirement: Requirement,
) {

    ORIGIN(
        CallHeaderConstants.ORIGIN_HEADER_NAME,
        PRESENCE,
        PRESENCE,
        PRESENCE
    ),

    INTERACTION(
        CallHeaderConstants.INTERACTION_HEADER_NAME,
        PRESENCE,
        PRESENCE,
        PRESENCE
    ),

    METHOD(
        CallHeaderConstants.METHOD_HEADER_NAME,
        PRESENCE,
        ABSENCE,
        PRESENCE,
    );

    companion object {

        private val REQUEST_HEADERS_REQUIREMENT = entries
            .filter { it.requestRequirement == PRESENCE }
            .toSet()

        fun requiredInRequest() = REQUEST_HEADERS_REQUIREMENT

        private val RESPONSE_HEADERS_REQUIREMENT = entries
            .filter { it.responseRequirement == PRESENCE }
            .toSet()

        fun requiredInResponse() = RESPONSE_HEADERS_REQUIREMENT

        private val NOTIFICATION_HEADERS_REQUIREMENT = entries
            .filter { it.notificationRequirement == PRESENCE }
            .toSet()

        fun requiredInNotification() = NOTIFICATION_HEADERS_REQUIREMENT

        private val mappings = entries
            .associateBy { it.value.uppercase() }

        fun resolve(value: String): CallHeader? {
            return mappings[value.uppercase()]
        }

        fun contains(value: String): Boolean {
            return mappings.containsKey(value.uppercase())
        }

    }

    enum class Requirement {
        // Must be present
        PRESENCE,

        // Must be absent
        ABSENCE,

        // May be present or absent
        OPTIONAL;
    }

}