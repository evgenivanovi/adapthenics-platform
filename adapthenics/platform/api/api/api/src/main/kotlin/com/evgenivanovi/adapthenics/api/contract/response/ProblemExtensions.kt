package com.evgenivanovi.adapthenics.api.contract.response

import com.evgenivanovi.kt.errx.Error
import com.evgenivanovi.kt.errx.Errorex

fun Problem.Companion.from(
    status: ResponseStatusCode,
    error: Error,
): Problem {
    return from(
        status.getCode().toString(),
        status.getReason(),
        error
    )
}

fun Problem.Companion.from(
    status: ResponseStatusCode,
    errors: Collection<Error>,
): Problem {
    return from(
        status.getCode().toString(),
        status.getReason(),
        errors
    )
}

fun Problem.Companion.from(
    code: String,
    message: String,
    error: Error,
): Problem {
    val err = Problem.Companion.Error(error.error())
    return Problem(code, message, listOf(err))
}

fun Problem.Companion.from(
    code: String,
    message: String,
    errors: Collection<Error>,
): Problem {
    val errs = errors.map { Problem.Companion.Error(it.error()) }
    return Problem(code, message, errs)
}

fun Problem.Companion.from(code: String, message: String, error: Errorex): Problem {
    val errs = error.errs().map { Problem.Companion.Error(it.error()) }
    return Problem(code, message, errs)
}