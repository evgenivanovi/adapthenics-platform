package com.evgenivanovi.adapthenics.api.contract.header

import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.CONTAINS_ALL_FOR_NOTIFICATION_FILTER
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.CONTAINS_ALL_FOR_REQUEST_FILTER
import com.evgenivanovi.adapthenics.api.contract.meta.CallMetadata
import com.evgenivanovi.adapthenics.api.contract.meta.NotificationMetadata
import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.kt.lang.asOf

object CallMetadataFactory {

    fun from(headers: Map<String, String>): CallMetadata? {

        val collection = headers.toList()
        val builder = mutableMapOf<String, String>()

        CallHeaderResolver.resolveOriginHeader(collection, builder)
        CallHeaderResolver.resolveIncomeInteractionHeader(collection, builder)
        CallHeaderResolver.resolveMethodHeader(collection, builder)

        return from2(builder.toMap())

    }

    private fun from2(headers: Map<String, Any>): CallMetadata? {

        val collection = headers.toList()

        if (collection.let(CONTAINS_ALL_FOR_REQUEST_FILTER))
            return toRequestMetadata(headers)

        if (collection.let(CONTAINS_ALL_FOR_NOTIFICATION_FILTER))
            return toNotificationMetadata(headers)

        return null

    }

    private fun toRequestMetadata(headers: Map<String, Any>): RequestMetadata? {

        if (headers.toList().let(CONTAINS_ALL_FOR_REQUEST_FILTER).not())
            return null

        val origin = headers
            .getValue(CallHeaderConstants.ORIGIN_HEADER_NAME)
            .asOf<String>()

        val method = headers
            .getValue(CallHeaderConstants.METHOD_HEADER_NAME)
            .asOf<String>()

        return RequestMetadata(
            origin = origin,
            method = method,
        )

    }

    private fun toNotificationMetadata(headers: Map<String, Any>): NotificationMetadata? {

        if (headers.toList().let(CONTAINS_ALL_FOR_NOTIFICATION_FILTER).not())
            return null

        val origin = headers
            .getValue(CallHeaderConstants.ORIGIN_HEADER_NAME)
            .asOf<String>()

        val method = headers
            .getValue(CallHeaderConstants.METHOD_HEADER_NAME)
            .asOf<String>()

        return NotificationMetadata(
            origin = origin,
            method = method
        )

    }

}