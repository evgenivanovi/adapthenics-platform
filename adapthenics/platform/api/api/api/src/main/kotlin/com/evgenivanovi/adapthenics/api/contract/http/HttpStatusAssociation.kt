package com.evgenivanovi.adapthenics.api.contract.http

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCodes
import com.evgenivanovi.kt.coll.putAll
import com.google.common.collect.ImmutableBiMap

object HttpStatusAssociation {

    private val PAIRS: List<Pair<ResponseStatusCode, Int>> = listOf(
        ResponseStatusCodes.OK to 200,
        ResponseStatusCodes.TIMEOUT to 408,
        ResponseStatusCodes.EXHAUSTED to 429,
        ResponseStatusCodes.INVALID to 400,
        ResponseStatusCodes.UNAUTHORIZED to 401,
        ResponseStatusCodes.FORBIDDEN to 403,
        ResponseStatusCodes.ABSENT to 404,
        ResponseStatusCodes.CONFLICT to 409,
        ResponseStatusCodes.INTERNAL to 500,
        ResponseStatusCodes.UNAVAILABLE to 503
    )

    private val MAPPINGS: ImmutableBiMap<ResponseStatusCode, Int> = ImmutableBiMap
        .builder<ResponseStatusCode, Int>()
        .putAll(PAIRS)
        .build()

    private val INVERSE_MAPPINGS: ImmutableBiMap<Int, ResponseStatusCode> =
        MAPPINGS.inverse()

    fun resolve(status: ResponseStatusCode): Int {
        return MAPPINGS.getValue(status)
    }

    fun resolve(status: Int): ResponseStatusCode? {
        return INVERSE_MAPPINGS[status]
    }

}