package com.evgenivanovi.adapthenics.api.contract.exchange

import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatus

class RequestExchange<out REQ: Any, out RES: Any> : CallExchange {

    var req: RequestMessage<*>

    var res: ResponseMessage<*>

    constructor(req: RequestMessage<REQ>, res: ResponseMessage<RES>) {
        this.req = req
        this.res = res
    }

    fun isSuccess(): Boolean {
        return res.getStatus() == ResponseStatus.SUCCESS
    }

    fun isFailure(): Boolean {
        return res.getStatus() == ResponseStatus.FAILURE
    }

    fun isError(): Boolean {
        return res.getStatus() == ResponseStatus.ERROR
    }

}