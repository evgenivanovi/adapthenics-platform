package com.evgenivanovi.adapthenics.api.contract

object APIConstants {

    const val RESPONSE_SUCCESS = "success"
    const val RESPONSE_FAILURE = "failure"
    const val RESPONSE_ERROR = "error"

    const val INTERACTION_REQUEST = "request"
    const val INTERACTION_RESPONSE = "response"
    const val INTERACTION_NOTIFICATION = "notification"

    val INCOME_INTERACTION_TYPES: Set<String> = setOf(
        INTERACTION_NOTIFICATION,
        INTERACTION_REQUEST
    )

    val OUTCOME_INTERACTION_TYPES: Set<String> = setOf(
        INTERACTION_RESPONSE
    )

    const val CALL_METHOD = "method"
    const val CALL_INTERACTION = "interaction"

}