@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.api.contract.response

import com.evgenivanovi.kt.math.Numbers.HUNDRED_I

enum class ResponseStatusCodes(
    private val code: Int,
    private val series: Series,
    private val reason: String,
) : ResponseStatusCode {

    // Not an error. Returned on success.
    // gRPC: [0 OK]
    // HTTP: [200 OK]
    OK(0, Series.SUCCESSFUL, "Success"),

    // The deadline expired before the operation could complete.
    // For operations that change the state of the system, this error may be returned even if the operation has completed successfully.
    // For example, a successful response from a server could have been delayed long enough for the deadline to expire.
    // gRPC: [4 DEADLINE_EXCEEDED]
    // HTTP: [408 Request Timeout] [504 Gateway Timeout]
    TIMEOUT(101, Series.CLIENT_SERVER_ERROR, "Timeout"),

    // Some resource has been exhausted, perhaps a per-user quota, or perhaps the entire file system is out of space.
    // gRPC: [8 RESOURCE_EXHAUSTED]
    // HTTP: [429 Too Many Requests] [507 Insufficient Storage]
    EXHAUSTED(102, Series.CLIENT_SERVER_ERROR, "Exhausted"),

    // The client specified an invalid argument.
    // gRPC: [3 INVALID_ARGUMENT]
    // HTTP: [400 Bad Request]
    INVALID(201, Series.CLIENT_ERROR, "Invalid"),

    // The request does not have valid authentication credentials for the operation.
    // gRPC: [16 UNAUTHENTICATED]
    // HTTP: [401 Unauthorized]
    UNAUTHORIZED(202, Series.CLIENT_ERROR, "Unauthorized"),

    // The caller does not have permission to execute the specified operation.
    // Must not be used for rejections caused by exhausting some resource.
    // Use EXHAUSTED instead for those errors.
    // FORBIDDEN must not be used if the caller can not be identified (use UNAUTHORIZED instead for those errors).
    // gRPC: [7 PERMISSION_DENIED]
    // HTTP: [403 Forbidden]
    FORBIDDEN(203, Series.CLIENT_ERROR, "Forbidden"),

    // Some requested entity (e.g., file or directory) was not found.
    // gRPC: [5 NOT_FOUND]
    // HTTP: [404 Not Found]
    ABSENT(204, Series.CLIENT_ERROR, "Absent"),

    // The entity that a client attempted to create (e.g., file or directory) already exists.
    // gRPC: [6 ALREADY_EXISTS]
    // HTTP: [409 Conflict]
    CONFLICT(205, Series.CLIENT_ERROR, "Conflict"),

    // Internal errors.
    // This means that some invariants expected by the underlying system have been broken.
    // This error code is reserved for serious errors.
    // gRPC: [13 INTERNAL]
    // HTTP: [500 Internal Server Error]
    INTERNAL(301, Series.SERVER_ERROR, "Internal"),

    // The service is currently unavailable.
    // This is most likely a transient condition, which can be corrected by retrying with a backoff.
    // Note that it is not always safe to retry non-idempotent operations.
    // gRPC: [14 UNAVAILABLE]
    // HTTP: [503 Service Unavailable]
    UNAVAILABLE(302, Series.SERVER_ERROR, "Unavailable");

    override fun getCode(): Int {
        return code
    }

    override fun getReason(): String {
        return reason
    }

    override fun clientError(): Boolean {
        return series == Series.CLIENT_ERROR
            || series == Series.CLIENT_SERVER_ERROR
    }

    override fun serverError(): Boolean {
        return series == Series.SERVER_ERROR
            || series == Series.CLIENT_SERVER_ERROR
    }

    override fun toString(): String {
        return "$code $reason"
    }

    enum class Series(
        private val value: Int,
    ) {

        SUCCESSFUL(0),

        CLIENT_SERVER_ERROR(1),

        CLIENT_ERROR(2),

        SERVER_ERROR(3);

        fun value(): Int {
            return value
        }

        companion object {

            fun of(code: Int): Series {
                return resolve(code) ?: throw IllegalArgumentException(
                    "There is no known series for code ($code)."
                )
            }

            fun resolve(code: Int): Series? {
                val series = code.div(HUNDRED_I)
                return entries.find { it.value == series }
            }

        }

    }

    companion object {

        private val names = entries
            .associateBy(ResponseStatusCodes::name)

        private val codes = entries
            .associateBy(ResponseStatusCodes::code)

        fun resolve(code: Int): ResponseStatusCodes? {
            return codes[code]
        }

        fun of(code: Int): ResponseStatusCodes {
            return codes[code] ?: throw IllegalArgumentException(
                "There is no known status for this code ($code)."
            )
        }

    }

}
