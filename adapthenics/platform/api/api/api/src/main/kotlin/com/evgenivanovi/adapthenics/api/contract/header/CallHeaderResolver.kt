package com.evgenivanovi.adapthenics.api.contract.header

object CallHeaderResolver {

    fun resolveOriginHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.ORIGIN_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.ORIGIN_VALUE_FILTER(it.second) }
            ?.let { CallHeaderConstants.ORIGIN_HEADER_NAME to it.second }
    }

    fun resolveOriginHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveOriginHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    fun resolveInvalidOriginHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.ORIGIN_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.ORIGIN_VALUE_FILTER(it.second).not() }
            ?.let { CallHeaderConstants.ORIGIN_HEADER_NAME to it.second }
    }

    fun resolveInvalidOriginHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveInvalidOriginHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    /* __________________________________________________ */

    fun resolveIncomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.INTERACTION_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.INCOME_INTERACTION_VALUE_FILTER(it.second) }
            ?.let { CallHeaderConstants.INTERACTION_HEADER_NAME to it.second }
    }

    fun resolveIncomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveIncomeInteractionHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    fun resolveInvalidIncomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.INTERACTION_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.INCOME_INTERACTION_VALUE_FILTER(it.second).not() }
            ?.let { CallHeaderConstants.INTERACTION_HEADER_NAME to it.second }
    }

    fun resolveInvalidIncomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveInvalidIncomeInteractionHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    /* __________________________________________________ */

    fun resolveOutcomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.INTERACTION_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.OUTCOME_INTERACTION_VALUE_FILTER(it.second) }
            ?.let { CallHeaderConstants.INTERACTION_HEADER_NAME to it.second }
    }

    fun resolveOutcomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveOutcomeInteractionHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    fun resolveInvalidOutcomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.INTERACTION_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.OUTCOME_INTERACTION_VALUE_FILTER(it.second).not() }
            ?.let { CallHeaderConstants.INTERACTION_HEADER_NAME to it.second }
    }

    fun resolveInvalidOutcomeInteractionHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveInvalidOutcomeInteractionHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    /* __________________________________________________ */

    fun resolveMethodHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.METHOD_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.METHOD_VALUE_FILTER(it.second) }
            ?.let { CallHeaderConstants.METHOD_HEADER_NAME to it.second }
    }

    fun resolveMethodHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveMethodHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    fun resolveInvalidMethodHeader(
        headers: Collection<Pair<String, String>>,
    ): Pair<String, String>? {
        return headers
            .firstOrNull { CallHeaderFilters.METHOD_KEY_FILTER(it.first) }
            ?.takeIf { CallHeaderFilters.METHOD_VALUE_FILTER(it.second).not() }
            ?.let { CallHeaderConstants.METHOD_HEADER_NAME to it.second }
    }

    fun resolveInvalidMethodHeader(
        headers: Collection<Pair<String, String>>,
        builder: MutableMap<String, String>,
    ) {
        resolveInvalidMethodHeader(headers)
            ?.also { builder[it.first] = it.second }
    }

    /* __________________________________________________ */

}