package com.evgenivanovi.adapthenics.api.contract.response

enum class ResponseStatus {

    /**
     * Success: When an API call is successful.
     */
    SUCCESS,

    /**
     * Failure: When an API call is rejected due to invalid data or call conditions.
     */
    FAILURE,

    /**
     * Error: When an API call fails due to an error on the server.
     */
    ERROR;

    companion object {

        private val ordinals = entries
            .map(ResponseStatus::ordinal)

        private val names = entries
            .associateBy(ResponseStatus::name)

        private val indices = entries
            .associateBy(ResponseStatus::ordinal)

        fun resolve(value: String): ResponseStatus? {
            return names[value.uppercase()]
        }

        fun from(value: String): ResponseStatus {
            return resolve(value) ?: throw IllegalArgumentException(
                "Invalid value: '${value}' for ${ResponseStatus::class.simpleName}!" +
                    " Has to be one of the following: ${entries.joinToString()}."
            )
        }

        fun resolve(value: Int): ResponseStatus? {
            return indices[value]
        }

        fun from(value: Int): ResponseStatus {
            return resolve(value) ?: throw IllegalArgumentException(
                "Invalid value: '${value}' for ${ResponseStatus::class.simpleName}!" +
                    " Has to be one of the following: ${ResponseStatus.ordinals.joinToString()}."
            )
        }

    }

}