package com.evgenivanovi.adapthenics.api.contract.meta

import com.evgenivanovi.adapthenics.api.contract.APIConstants
import kotlinx.serialization.Serializable

@Serializable
class ResponseMetadata(
    private val origin: String,
) : CallMetadata {

    private val interaction: String = APIConstants.INTERACTION_RESPONSE

    override fun getOrigin(): String {
        return origin
    }

    override fun getInteraction(): String {
        return interaction
    }

    companion object {

        fun of(
            origin: String,
        ): ResponseMetadata {
            return ResponseMetadata(origin)
        }

    }

}