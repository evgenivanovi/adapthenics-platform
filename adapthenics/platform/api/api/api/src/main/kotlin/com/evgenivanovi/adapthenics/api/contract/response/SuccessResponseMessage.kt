package com.evgenivanovi.adapthenics.api.contract.response

import com.evgenivanovi.adapthenics.api.contract.meta.ResponseMetadata
import kotlinx.serialization.Serializable

/**
 * When an API call is successful
 */
@Serializable
class SuccessResponseMessage<T>(
    private val data: T?,
    private val metadata: ResponseMetadata,
) : ResponseMessage<T> {

    private val code: Int = ResponseStatusCodes.OK.getCode()

    override fun getCode(): Int {
        return code
    }

    private val status: ResponseStatus = ResponseStatus.SUCCESS

    override fun getStatus(): ResponseStatus {
        return status
    }

    override fun getData(): T? {
        return data
    }

    override fun getMetadata(): ResponseMetadata {
        return metadata
    }

    companion object {

        fun <T> of(
            data: T?,
            metadata: ResponseMetadata,
        ): SuccessResponseMessage<T> {
            return SuccessResponseMessage(data, metadata)
        }

    }

}