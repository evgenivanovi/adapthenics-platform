package com.evgenivanovi.adapthenics.api.contract.request

import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import kotlinx.serialization.Serializable

@Serializable
class RequestMessage<T>(
    val data: T?,
    val metadata: RequestMetadata,
)