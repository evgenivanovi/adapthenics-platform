package com.evgenivanovi.adapthenics.api.contract.exchange

import com.evgenivanovi.adapthenics.api.contract.response.Problem

object RequestExchangeOps {

    // Check RequestExchange.isSuccess() before invoking
    fun success(ex: RequestExchange<*, *>): Any? {
        if (ex.isSuccess().not()) {
            throw IllegalArgumentException(
                "Check RequestExchange.isSuccess() before invoking"
            )
        }
        return ex.res.getData()
    }

    // Check RequestExchange.isFailure() or RequestExchange.isError() before invoking
    fun problem(ex: RequestExchange<*, *>): Problem? {
        if (ex.isSuccess()) {
            throw IllegalArgumentException(
                "Check RequestExchange.isFailure() or RequestExchange.isError() before invoking"
            )
        }
        return ex.res.getData() as Problem?
    }

}