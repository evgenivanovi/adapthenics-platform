package com.evgenivanovi.adapthenics.api.contract.header

object CallHeaderConstants {

    const val ORIGIN_HEADER_NAME = "Origin"

    const val INTERACTION_HEADER_NAME = "Interaction"

    const val METHOD_HEADER_NAME = "Method"

}