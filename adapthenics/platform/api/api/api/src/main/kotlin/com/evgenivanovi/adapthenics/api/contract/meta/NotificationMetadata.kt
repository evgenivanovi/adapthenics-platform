package com.evgenivanovi.adapthenics.api.contract.meta

import com.evgenivanovi.adapthenics.api.contract.APIConstants
import com.evgenivanovi.adapthenics.api.contract.APIConstants.CALL_INTERACTION
import com.evgenivanovi.adapthenics.api.contract.APIConstants.CALL_METHOD
import kotlinx.serialization.Serializable

@Serializable
class NotificationMetadata(
    private val origin: String,
    private val method: String,
) : CallMetadata {

    private val interaction: String = APIConstants.INTERACTION_NOTIFICATION

    override fun getOrigin(): String {
        return origin
    }

    override fun getInteraction(): String {
        return interaction
    }

    fun getMethod(): String {
        return method
    }

    companion object {

        fun of(
            origin: String,
            method: String,
        ): NotificationMetadata {
            return NotificationMetadata(
                origin = origin,
                method = method
            )
        }

        fun asMDC(
            meta: NotificationMetadata,
        ): Map<String, () -> String?> = mapOf(
            CALL_METHOD to meta::method,
            CALL_INTERACTION to meta::interaction,
        )

    }

}