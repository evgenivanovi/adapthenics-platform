package com.evgenivanovi.adapthenics.api.contract.header

import com.evgenivanovi.adapthenics.api.contract.APIConstants
import com.evgenivanovi.kt.lang.eq

object CallHeaderFilters {

    val API_KEY_FILTER: (String) -> Boolean = {
        CallHeader.contains(it)
    }

    val API_FILTER: (Pair<String, String>) -> Boolean = {
        API_KEY_FILTER(it.first)
    }

    /* __________________________________________________ */

    val ORIGIN_KEY_FILTER: (String) -> Boolean = {
        it.eq(CallHeaderConstants.ORIGIN_HEADER_NAME)
    }

    val ORIGIN_VALUE_FILTER: (String) -> Boolean = {
        true
    }

    val ORIGIN_FILTER: (Pair<String, String>) -> Boolean = {
        ORIGIN_KEY_FILTER(it.first)
            && ORIGIN_VALUE_FILTER(it.second)
    }

    /* __________________________________________________ */

    val INCOME_INTERACTION_FILTER: (Pair<String, String>) -> Boolean = {
        INTERACTION_KEY_FILTER(it.first)
            && INCOME_INTERACTION_VALUE_FILTER(it.second)
    }

    val OUTCOME_INTERACTION_FILTER: (Pair<String, String>) -> Boolean = {
        INTERACTION_KEY_FILTER(it.first)
            && OUTCOME_INTERACTION_VALUE_FILTER(it.second)
    }

    /* __________________________________________________ */

    val INTERACTION_KEY_FILTER: (String) -> Boolean = {
        it.eq(CallHeaderConstants.INTERACTION_HEADER_NAME)
    }

    val INCOME_INTERACTION_VALUE_FILTER: (String) -> Boolean = {
        APIConstants.INCOME_INTERACTION_TYPES.contains(it)
    }

    val OUTCOME_INTERACTION_VALUE_FILTER: (String) -> Boolean = {
        APIConstants.OUTCOME_INTERACTION_TYPES.contains(it)
    }

    /* __________________________________________________ */

    val METHOD_KEY_FILTER: (String) -> Boolean = {
        it.eq(CallHeaderConstants.METHOD_HEADER_NAME)
    }

    val METHOD_VALUE_FILTER: (String) -> Boolean = {
        true
    }

    val METHOD_FILTER: (Pair<String, String>) -> Boolean = {
        METHOD_KEY_FILTER(it.first)
            && METHOD_VALUE_FILTER(it.second)
    }

    /* __________________________________________________ */

    val IS_INCOME_INTERACTION_FILTER: (Pair<String, String>) -> Boolean = {
        IS_REQUEST_FILTER(it)
            || IS_NOTIFICATION_FILTER(it)
    }

    val IS_OUTCOME_INTERACTION_FILTER: (Pair<String, String>) -> Boolean = {
        IS_RESPONSE_FILTER(it)
    }

    /* __________________________________________________ */

    val IS_REQUEST_FILTER: (Pair<String, String>) -> Boolean = {
        CallHeaderConstants.INTERACTION_HEADER_NAME.eq(it.first)
            && APIConstants.INTERACTION_REQUEST.eq(it.second)
    }

    val IS_NOTIFICATION_FILTER: (Pair<String, String>) -> Boolean = {
        CallHeaderConstants.INTERACTION_HEADER_NAME.eq(it.first)
            && APIConstants.INTERACTION_NOTIFICATION.eq(it.second)
    }

    val IS_RESPONSE_FILTER: (Pair<String, String>) -> Boolean = {
        CallHeaderConstants.INTERACTION_HEADER_NAME.eq(it.first)
            && APIConstants.INTERACTION_RESPONSE.eq(it.second)
    }

    /* __________________________________________________ */

    val CONTAINS_ALL_FOR_REQUEST_FILTER: (Collection<Pair<String, Any>>) -> Boolean = {
        it
            .mapNotNull { pair -> CallHeader.resolve(pair.first) }
            .containsAll(CallHeader.requiredInRequest())
    }

    val CONTAINS_ALL_FOR_NOTIFICATION_FILTER: (Collection<Pair<String, Any>>) -> Boolean = {
        it
            .mapNotNull { pair -> CallHeader.resolve(pair.first) }
            .containsAll(CallHeader.requiredInNotification())
    }

    val CONTAINS_ALL_FOR_RESPONSE_FILTER: (Collection<Pair<String, Any>>) -> Boolean = {
        it
            .mapNotNull { pair -> CallHeader.resolve(pair.first) }
            .containsAll(CallHeader.requiredInResponse())
    }

}