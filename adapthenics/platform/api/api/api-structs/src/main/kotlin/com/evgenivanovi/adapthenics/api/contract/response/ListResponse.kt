package com.evgenivanovi.adapthenics.api.contract.response

import com.evgenivanovi.kt.lang.cast
import kotlinx.serialization.Serializable

@Serializable
class ListResponse<T : Any>(
    val items: List<T>,
) {

    companion object {

        private val EMPTY_INSTANCE = ListResponse(emptyList())

        private fun <T : Any> emptyInstance(): ListResponse<T> {
            return EMPTY_INSTANCE.cast()
        }

        fun <T : Any> empty(): ListResponse<T> {
            return emptyInstance()
        }

    }

}