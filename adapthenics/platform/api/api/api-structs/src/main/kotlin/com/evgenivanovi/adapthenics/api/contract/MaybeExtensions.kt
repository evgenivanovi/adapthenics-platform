package com.evgenivanovi.adapthenics.api.contract

import com.evgenivanovi.kt.lang.Keeper

fun <T : Any> toKeeper(value: Maybe<T>?): Keeper<T> {
    val optional = value ?: return Keeper.unset()
    return when (optional.set) {
        true -> {
            Keeper.set(optional.value)
        }

        false -> {
            Keeper.unset()
        }
    }
}

fun <T : Any> fromKeeper(value: Keeper<T>?): Maybe<T>? {
    val optional = value ?: return null
    return when (optional.set()) {
        true -> {
            Maybe.set(optional.get())
        }

        false -> {
            null
        }
    }
}