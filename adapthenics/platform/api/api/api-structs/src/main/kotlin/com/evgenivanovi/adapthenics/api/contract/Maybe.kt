package com.evgenivanovi.adapthenics.api.contract

import kotlinx.serialization.Serializable

@Serializable
class Maybe<T : Any?>(
    val set: Boolean,
    val value: T,
) {

    fun get(): T {
        return when (set) {
            true -> value
            else -> throw NoSuchElementException("No value present")
        }
    }

    fun or(other: T): Maybe<T> {
        return when (set) {
            true -> this
            else -> set(other)
        }
    }

    fun or(other: () -> T): Maybe<T> {
        return when (set) {
            true -> this
            else -> set(other())
        }
    }

    fun or(other: Maybe<T>): Maybe<T> {
        return when (set) {
            true -> this
            else -> other
        }
    }

    fun orEmpty(): Maybe<T> {
        return when (set) {
            true -> this
            else -> set()
        }
    }

    fun orElse(other: T): T {
        return when (set) {
            true -> value
            else -> other
        }
    }

    fun orElse(other: () -> T): T {
        return when (set) {
            true -> value
            else -> other()
        }
    }

    fun <E : Throwable> orThrow(ex: E): Maybe<T> {
        return when (set) {
            true -> this
            else -> throw ex
        }
    }

    fun <E : Throwable> orThrow(ex: () -> E): Maybe<T> {
        return when (set) {
            true -> this
            else -> throw ex()
        }
    }

    fun orNull(): T? {
        return when (set) {
            true -> get()
            false -> null
        }
    }

    /* __________________________________________________ */

    fun <R : Any?> map(mapper: (T) -> R): Maybe<R> {
        return when (set) {
            true -> set(mapper(value))
            false -> unset()
        }
    }

    fun filter(predicate: (T) -> Boolean): Maybe<T> {
        return when (set) {
            true -> {
                return when (predicate(value)) {
                    true -> this
                    else -> unset()
                }
            }

            false -> this
        }
    }

    companion object {

        private val SET: Maybe<Any?> =
            Maybe(true, null)

        private val UNSET: Maybe<Any?> =
            Maybe(false, null)

        fun <T : Any?> set(): Maybe<T> {
            @Suppress("UNCHECKED_CAST")
            return SET as Maybe<T>
        }

        fun <T : Any?> set(value: T): Maybe<T> {
            return Maybe(true, value)
        }

        fun <T : Any?> unset(): Maybe<T> {
            @Suppress("UNCHECKED_CAST")
            return UNSET as Maybe<T>
        }

    }

}