package com.evgenivanovi.adapthenics.api.contract.request

import kotlinx.serialization.Serializable

@Serializable
class IDRequest<T : Any>(
    val id: T,
)
