package com.evgenivanovi.adapthenics.api.contract.jackson

import com.evgenivanovi.adapthenics.api.contract.meta.ResponseMetadata
import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatus
import com.evgenivanovi.adapthenics.api.contract.response.SuccessResponseMessage
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class ApiContractModuleTest {

    @ParameterizedTest
    @MethodSource("statusSource")
    fun shouldDeserializeStatus(
        input: String,
        expected: TesteeResponse,
    ) {

        // when
        val actual = mapper().readValue(input, TesteeResponse::class.java)

        // then
        Assertions.assertEquals(expected.status, actual.status)

    }

    @ParameterizedTest
    @MethodSource("statusSource")
    fun shouldSerializeStatus(
        expected: String,
        input: TesteeResponse,
    ) {

        // when
        val actual = mapper().writeValueAsString(input)

        // then
        Assertions.assertEquals(
            mapper().readTree(expected), mapper().readTree(actual)
        )

    }

    @ParameterizedTest
    @MethodSource("successSource")
    fun shouldDeserializeSuccessResponseMessage(
        input: String,
        expected: SuccessResponseMessage<String>,
    ) {

        // when
        val actual = mapper().readValue(
            input,
            SuccessResponseMessage::class.java
        )

        // then
        Assertions.assertAll(
            {
                assertEquals(
                    expected.getStatus(),
                    actual.getStatus()
                )
            },
            {
                assertEquals(
                    expected.getCode(),
                    actual.getCode()
                )
            },
            {
                assertEquals(
                    expected.getMetadata().getOrigin(),
                    actual.getMetadata().getOrigin()
                )
            },
            {
                assertEquals(
                    expected.getMetadata().getInteraction(),
                    actual.getMetadata().getInteraction()
                )
            },
            {
                assertEquals(
                    expected.getData(),
                    actual.getData()
                )
            }
        )

    }

    @ParameterizedTest
    @MethodSource("successSource")
    fun shouldSerializeSuccessResponseMessage(
        expected: String,
        input: SuccessResponseMessage<String>,
    ) {

        // when
        val actual = mapper().writeValueAsString(input)

        // then
        Assertions.assertEquals(
            mapper().readTree(expected), mapper().readTree(actual)
        )

    }

    @ParameterizedTest
    @MethodSource("problemSource")
    fun shouldDeserializeProblemResponseMessage(
        input: String,
        expected: ProblemResponseMessage,
    ) {

        // when
        val actual = mapper().readValue(
            input,
            ProblemResponseMessage::class.java
        )

        // then
        Assertions.assertAll(
            {
                assertEquals(
                    expected.getStatus(),
                    actual.getStatus()
                )
            },
            {
                assertEquals(
                    expected.getCode(),
                    actual.getCode()
                )
            },
            {
                assertEquals(
                    expected.getMetadata()?.getOrigin(),
                    actual.getMetadata()?.getOrigin()
                )
            },
            {
                assertEquals(
                    expected.getMetadata()?.getInteraction(),
                    actual.getMetadata()?.getInteraction()
                )
            }
        )

    }

    @ParameterizedTest
    @MethodSource("problemSource")
    fun shouldSerializeProblemResponseMessage(
        expected: String,
        input: ProblemResponseMessage,
    ) {

        // when
        val actual = mapper().writeValueAsString(input)

        // then
        Assertions.assertEquals(
            mapper().readTree(expected), mapper().readTree(actual)
        )

    }

    @ParameterizedTest
    @MethodSource("source")
    fun shouldDeserializeResponseMessage(
        input: String,
        expected: ResponseMessage<out Any>,
    ) {

        // when
        val actual = mapper().readValue(
            input,
            ResponseMessage::class.java
        )

        // then
        Assertions.assertAll(
            {
                assertEquals(
                    expected.getStatus(),
                    actual.getStatus()
                )
            },
            {
                assertEquals(
                    expected.getCode(),
                    actual.getCode()
                )
            },
            {
                if (expected is SuccessResponseMessage<*> && actual is SuccessResponseMessage<*>) {
                    assertEquals(
                        expected.getMetadata().getOrigin(),
                        actual.getMetadata().getOrigin()
                    )
                }
            },
            {
                if (expected is SuccessResponseMessage<*> && actual is SuccessResponseMessage<*>) {
                    assertEquals(
                        expected.getMetadata().getInteraction(),
                        actual.getMetadata().getInteraction()
                    )
                }
            },
            {
                if (expected is ProblemResponseMessage && actual is ProblemResponseMessage) {
                    assertEquals(
                        expected.getMetadata()?.getOrigin(),
                        actual.getMetadata()?.getOrigin()
                    )
                }
            },
            {
                if (expected is ProblemResponseMessage && actual is ProblemResponseMessage) {
                    assertEquals(
                        expected.getMetadata()?.getInteraction(),
                        actual.getMetadata()?.getInteraction()
                    )
                }
            }
        )

    }

    @ParameterizedTest
    @MethodSource("source")
    fun shouldSerializeResponseMessage(
        expected: String,
        input: ResponseMessage<out Any>,
    ) {

        // when
        val actual = mapper().writeValueAsString(input)

        // then
        Assertions.assertEquals(
            mapper().readTree(expected), mapper().readTree(actual)
        )

    }

    class TesteeResponse(
        val status: ResponseStatus,
    )

    fun mapper(): JsonMapper {
        return JsonMapper.builder()
            .addModules(ApiContractModule())
            .addModules(KotlinModule.Builder().build())
            .build()
    }

    companion object {

        @JvmStatic
        fun statusSource(): Stream<Arguments> = Stream.of(
            Arguments.of(
                """{ "status": "success" }""",
                TesteeResponse(ResponseStatus.SUCCESS)
            ),
            Arguments.of(
                """{ "status": "failure" }""",
                TesteeResponse(ResponseStatus.FAILURE)
            ),
            Arguments.of(
                """{ "status": "error" }""",
                TesteeResponse(ResponseStatus.ERROR)
            ),
        )

        @JvmStatic
        fun successSource(): Stream<Arguments> = Stream.of(
            // success
            Arguments.of(
                """
                    {
                        "code": 0,
                        "status": "success",
                        "metadata": {
                            "origin": "TEST_ORIGIN",
                            "interaction": "response"
                        },
                        "data": null
                    }
                """.trimIndent(),
                SuccessResponseMessage(
                    null,
                    ResponseMetadata(
                        "TEST_ORIGIN",
                    ),
                )
            ),
            Arguments.of(
                """
                    {
                        "code": 0,
                        "status": "success",
                        "metadata": {
                            "origin": "TEST_ORIGIN",
                            "interaction": "response"
                        },
                        "data": "TEST_DATA"
                    }
                """.trimIndent(),
                SuccessResponseMessage(
                    "TEST_DATA",
                    ResponseMetadata(
                        "TEST_ORIGIN",
                    ),
                )
            ),
        )

        @JvmStatic
        fun problemSource(): Stream<Arguments> = Stream.of(
            // failure
            Arguments.of(
                """
                    {
                        "status": "failure",
                        "code": 999,
                        "metadata": null,
                        "data": null
                    }
                """.trimIndent(),
                ProblemResponseMessage(
                    999,
                    ResponseStatus.FAILURE,
                    null,
                    null,
                )
            ),
            // error
            Arguments.of(
                """
                    {
                        "status": "error",
                        "code": 999,
                        "metadata": null,
                        "data": null
                    }
                """.trimIndent(),
                ProblemResponseMessage(
                    999,
                    ResponseStatus.ERROR,
                    null,
                    null,
                )
            ),
        )

        @JvmStatic
        fun source(): Stream<Arguments> = Stream.of(
            // success
            Arguments.of(
                """
                    {
                        "code": 0,
                        "status": "success",
                        "metadata": {
                            "origin": "TEST_ORIGIN",
                            "interaction": "response"
                        },
                        "data": null
                    }
                """.trimIndent(),
                SuccessResponseMessage(
                    null,
                    ResponseMetadata(
                        "TEST_ORIGIN",
                    ),
                )
            ),
            Arguments.of(
                """
                    {
                        "code": 0,
                        "status": "success",
                        "metadata": {
                            "origin": "TEST_ORIGIN",
                            "interaction": "response"
                        },
                        "data": "TEST_DATA"
                    }
                """.trimIndent(),
                SuccessResponseMessage(
                    "TEST_DATA",
                    ResponseMetadata(
                        "TEST_ORIGIN",
                    ),
                )
            ),
            // failure
            Arguments.of(
                """
                    {
                        "status": "failure",
                        "code": 999,
                        "metadata": null,
                        "data": null
                    }
                """.trimIndent(),
                ProblemResponseMessage(
                    999,
                    ResponseStatus.FAILURE,
                    null,
                    null,
                )
            ),
            // error
            Arguments.of(
                """
                    {
                        "status": "error",
                        "code": 999,
                        "metadata": null,
                        "data": null
                    }
                """.trimIndent(),
                ProblemResponseMessage(
                    999,
                    ResponseStatus.ERROR,
                    null,
                    null,
                )
            ),
        )

    }

}