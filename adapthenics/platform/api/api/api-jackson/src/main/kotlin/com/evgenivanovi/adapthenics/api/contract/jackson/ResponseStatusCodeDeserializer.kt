package com.evgenivanovi.adapthenics.api.contract.jackson

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCodes
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class ResponseStatusCodeDeserializer : JsonDeserializer<ResponseStatusCode>() {

    override fun deserialize(
        parser: JsonParser, ctx: DeserializationContext,
    ): ResponseStatusCode? {
        return ResponseStatusCodes.resolve(parser.valueAsInt)
    }

}