package com.evgenivanovi.adapthenics.api.contract.jackson

import com.evgenivanovi.adapthenics.api.contract.APIConstants
import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.SuccessResponseMessage
import com.evgenivanovi.kt.lang.Booleans
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = ResponseMessage.FIELD_NAME_STATUS,
    visible = Booleans.TRUE
)
@JsonSubTypes(
    value = [
        JsonSubTypes.Type(
            value = SuccessResponseMessage::class,
            name = APIConstants.RESPONSE_SUCCESS
        ),
        JsonSubTypes.Type(
            value = ProblemResponseMessage::class,
            name = APIConstants.RESPONSE_FAILURE
        ),
        JsonSubTypes.Type(
            value = ProblemResponseMessage::class,
            name = APIConstants.RESPONSE_ERROR
        )
    ]
)
interface ResponseMessageMixin