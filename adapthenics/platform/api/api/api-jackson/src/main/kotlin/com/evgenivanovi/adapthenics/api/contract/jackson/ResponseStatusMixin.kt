package com.evgenivanovi.adapthenics.api.contract.jackson

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize(
    using = ResponseStatusSerializer::class
)
@JsonDeserialize(
    using = ResponseStatusDeserializer::class
)
interface ResponseStatusMixin