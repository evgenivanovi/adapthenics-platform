package com.evgenivanovi.adapthenics.api.contract.jackson

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatus
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class ResponseStatusDeserializer : JsonDeserializer<ResponseStatus>() {

    override fun deserialize(
        parser: JsonParser, ctx: DeserializationContext
    ): ResponseStatus? {
        return ResponseStatus.resolve(parser.valueAsString)
    }

}