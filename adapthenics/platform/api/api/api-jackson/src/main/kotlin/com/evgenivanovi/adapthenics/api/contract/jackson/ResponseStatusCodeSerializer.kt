package com.evgenivanovi.adapthenics.api.contract.jackson

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class ResponseStatusCodeSerializer : StdSerializer<ResponseStatusCode>(ResponseStatusCode::class.java) {

    override fun serialize(
        value: ResponseStatusCode, gen: JsonGenerator, ser: SerializerProvider,
    ) {
        gen.writeNumber(value.getCode())
    }

}