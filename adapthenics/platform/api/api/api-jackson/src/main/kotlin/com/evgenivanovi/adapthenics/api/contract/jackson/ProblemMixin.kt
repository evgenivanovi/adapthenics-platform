package com.evgenivanovi.adapthenics.api.contract.jackson

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonInclude

interface ProblemMixins {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    interface Error {

        @JsonAnySetter
        fun setProperty(name: String, value: Any?)

        @JsonAnyGetter
        fun getProperties(): MutableMap<String, Any?>

    }

}