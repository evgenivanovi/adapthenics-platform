package com.evgenivanovi.adapthenics.api.contract.jackson

import com.evgenivanovi.adapthenics.api.contract.response.Problem
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatus
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.fasterxml.jackson.databind.module.SimpleModule

class ApiContractModule : SimpleModule() {

    init {
        mixins()
        ser()
    }

    private fun mixins() {
        setMixInAnnotation(ResponseStatus::class.java, ResponseStatusMixin::class.java)
        setMixInAnnotation(ResponseStatusCode::class.java, ResponseStatusCodeMixin::class.java)
        setMixInAnnotation(ResponseMessage::class.java, ResponseMessageMixin::class.java)
        setMixInAnnotation(Problem.Companion.Error::class.java, ProblemMixins.Error::class.java)
    }

    private fun ser() {
        addSerializer(ResponseStatus::class.java, ResponseStatusSerializer())
        addDeserializer(ResponseStatus::class.java, ResponseStatusDeserializer())

        addSerializer(ResponseStatusCode::class.java, ResponseStatusCodeSerializer())
        addDeserializer(ResponseStatusCode::class.java, ResponseStatusCodeDeserializer())
    }

}