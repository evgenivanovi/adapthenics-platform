package com.evgenivanovi.adapthenics.api.contract.jackson

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatus
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class ResponseStatusSerializer : StdSerializer<ResponseStatus>(ResponseStatus::class.java) {

    override fun serialize(
        value: ResponseStatus, gen: JsonGenerator, ser: SerializerProvider
    ) {
        gen.writeString(value.name.lowercase())
    }

}