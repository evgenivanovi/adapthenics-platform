package com.evgenivanovi.adapthenics.api.contract.jackson

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize(
    using = ResponseStatusCodeSerializer::class
)
@JsonDeserialize(
    using = ResponseStatusCodeDeserializer::class
)
interface ResponseStatusCodeMixin