package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.adapthenics.api.contract.response.Problem
import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.api.contract.response.SuccessResponseMessage
import com.evgenivanovi.adapthenics.base.app.ApplicationMetadata
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactor.mono
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class ResponseAPIService(
    private val app: ApplicationMetadata,
) {

    fun <R : Any> success(
        metadata: RequestMetadata,
        data: R,
    ): SuccessResponseMessage<R> {
        return ResponseAPI.toSuccessResponse(
            this.app.id, data
        )
    }

    fun <R : Any> successMono(
        metadata: RequestMetadata,
        data: R,
    ): Mono<SuccessResponseMessage<R>> {
        return mono { success(metadata, data) }
    }

    fun <R : Any> success(
        metadata: RequestMetadata,
        data: Flow<R>,
    ): Flow<SuccessResponseMessage<R>> {
        return data.map { success(metadata, it) }
    }

    fun <R : Any> success(
        metadata: RequestMetadata,
        data: Flux<R>,
    ): Flux<SuccessResponseMessage<R>> {
        return data.map { success(metadata, it) }
    }

    fun <R : Any> emptySuccess(
        metadata: RequestMetadata,
    ): SuccessResponseMessage<R> {
        return ResponseAPI.toEmptySuccessResponse(
            this.app.id
        )
    }

    fun <R : Any> emptySuccessMono(
        metadata: RequestMetadata,
    ): Mono<SuccessResponseMessage<R>> {
        return mono { emptySuccess(metadata) }
    }

    fun problem(
        data: Problem?,
        status: ResponseStatusCode,
    ): ProblemResponseMessage {
        return ResponseAPI
            .toProblemResponse(data, status)
    }

    fun problemMono(
        data: Problem?,
        status: ResponseStatusCode,
    ): Mono<ProblemResponseMessage> {
        return mono { problem(data, status) }
    }

    fun problem(
        metadata: RequestMetadata,
        data: Problem?,
        status: ResponseStatusCode,
    ): ProblemResponseMessage {
        return ResponseAPI.toProblemResponse(
            this.app.id,
            data,
            status
        )
    }

    fun problemMono(
        metadata: RequestMetadata,
        data: Problem?,
        status: ResponseStatusCode,
    ): Mono<ProblemResponseMessage> {
        return mono {
            problem(metadata, data, status)
        }
    }

}
