package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import kotlinx.coroutines.CoroutineScope

fun CoroutineScope.requestContext(): RequestMetadataContext {
    return coroutineContext[RequestMetadataContext]
        ?: throw IllegalStateException(
            "No ${RequestMetadataContext::class.simpleName} present."
        )
}

fun CoroutineScope.requestMetadata(): RequestMetadata {
    return coroutineContext[RequestMetadataContext]
        ?.metadata
        ?: throw IllegalStateException(
            "No ${RequestMetadata::class.simpleName} present."
        )
}
