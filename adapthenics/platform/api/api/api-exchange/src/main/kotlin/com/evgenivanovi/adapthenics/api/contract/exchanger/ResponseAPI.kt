package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.ResponseMetadata
import com.evgenivanovi.adapthenics.api.contract.response.Problem
import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.api.contract.response.SuccessResponseMessage

object ResponseAPI {

    fun <R : Any> toSuccessResponse(
        origin: String,
        data: R,
    ): SuccessResponseMessage<R> {
        val meta = ResponseMetadata.of(
            origin = origin,
        )
        return SuccessResponseMessage.of(data, meta)
    }

    fun <R : Any> toEmptySuccessResponse(
        origin: String,
    ): SuccessResponseMessage<R> {
        val meta = ResponseMetadata.of(
            origin = origin,
        )
        return SuccessResponseMessage.of(null, meta)
    }

    fun toProblemResponse(
        origin: String,
        data: Problem?,
        status: ResponseStatusCode,
    ): ProblemResponseMessage {

        if (status.clientError()) {
            return toFailureResponse(
                origin, data, status
            )
        }

        if (status.serverError()) {
            return toErrorResponse(
                origin, data, status
            )
        }

        throw IllegalArgumentException(
            "There is no "
                + ProblemResponseMessage::class.simpleName
                + " for this problem status ( " + status + " )."
        )

    }

    fun toProblemResponse(
        data: Problem?,
        status: ResponseStatusCode,
    ): ProblemResponseMessage {

        if (status.clientError()) {
            return ProblemResponseMessage.failure(
                status, null, data
            )
        }

        if (status.serverError()) {
            return ProblemResponseMessage.error(
                status, null, data
            )
        }

        throw IllegalArgumentException(
            "There is no "
                + ProblemResponseMessage::class.simpleName
                + " for this problem status ( " + status + " )."
        )

    }

    fun toFailureResponse(
        origin: String,
        data: Problem?,
        status: ResponseStatusCode,
    ): ProblemResponseMessage {

        val response = ResponseMetadata.of(
            origin = origin,
        )

        return ProblemResponseMessage.failure(
            status,
            response,
            data
        )

    }

    fun toErrorResponse(
        origin: String,
        data: Problem?,
        status: ResponseStatusCode,
    ): ProblemResponseMessage {

        val response = ResponseMetadata(
            origin = origin,
        )

        return ProblemResponseMessage.error(
            status,
            response,
            data
        )

    }

}