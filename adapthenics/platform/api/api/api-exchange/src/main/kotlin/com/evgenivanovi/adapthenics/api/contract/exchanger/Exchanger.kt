package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.adapthenics.api.contract.notification.NotificationMessage
import com.evgenivanovi.adapthenics.api.contract.request.RequestMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.support.logging.asyncDebug
import com.evgenivanovi.adapthenics.support.logging.mdc.cleanup
import com.evgenivanovi.adapthenics.support.logging.mdc.toMDCEntriesSupplied
import com.evgenivanovi.adapthenics.support.logging.syncDebug
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.slf4j.MDCContext
import kotlinx.coroutines.withContext

class Exchanger(
    private val responser: ResponseInterceptor,
    private val exceptioner: ExceptionInterceptor,
) {

    private val log = KotlinLogging.logger {}

    suspend fun <R : Any> exchange(
        request: RequestMessage<*>,
        action: () -> ResponseMessage<R>,
    ): ResponseMessage<R> = coroutineScope {
        withContext(request) { doExchange(request, action) }
    }

    private fun <R : Any> doExchange(
        request: RequestMessage<*>,
        action: () -> ResponseMessage<R>,
    ): ResponseMessage<R> {
        // @formatter:off
        return try {
            action.invoke()
        } catch (ex: Exception) {
            return exceptioner
                .handle(ex, request.metadata)
        }
        // @formatter:on
    }

    suspend fun <R : Any> exchangeR(
        request: RequestMessage<*>,
        action: () -> R,
    ): ResponseMessage<R> = coroutineScope {
        withContext(request) { doExchangeR(request, action) }
    }

    private fun <R : Any> doExchangeR(
        request: RequestMessage<*>,
        action: () -> R,
    ): ResponseMessage<R> {
        // @formatter:off
        return try {
            responser
                .handle(action.invoke(), request.metadata)
        } catch (ex: Exception) {
            return exceptioner
                .handle(ex, request.metadata)
        }
        // @formatter:on
    }

    suspend fun <R : Any> exchangeSuspending(
        request: RequestMessage<*>,
        action: suspend () -> ResponseMessage<R>,
    ): ResponseMessage<R> = coroutineScope {
        withContext(request) { doExchangeSuspending(request, action) }
    }

    private suspend fun <R : Any> doExchangeSuspending(
        request: RequestMessage<*>,
        action: suspend () -> ResponseMessage<R>,
    ): ResponseMessage<R> {
        // @formatter:off
        return try {
            action.invoke()
        } catch (ex: Exception) {
            return exceptioner
                .handle(ex, request.metadata)
        }
        // @formatter:on
    }

    /* __________________________________________________ */

    suspend fun <R : Any> exchangeSuspendingR(
        request: RequestMessage<*>,
        action: suspend () -> R,
    ): ResponseMessage<R> = coroutineScope {
        withContext(request) { doExchangeSuspendingR(request, action) }
    }

    private suspend fun <R : Any> doExchangeSuspendingR(
        request: RequestMessage<*>,
        action: suspend () -> R,
    ): ResponseMessage<R> {
        // @formatter:off
        return try {
            responser
                .handle(action.invoke(), request.metadata)
        } catch (ex: Exception) {
            return exceptioner
                .handle(ex, request.metadata)
        }
        // @formatter:on
    }

    fun <R : Any> exchangeFlow(
        request: RequestMessage<*>,
        action: () -> Flow<ResponseMessage<R>>,
    ): Flow<ResponseMessage<R>> = flow {
        withContext(request) { doExchangeFlow(request, action) }
    }

    private fun <R : Any> doExchangeFlow(
        request: RequestMessage<*>,
        action: () -> Flow<ResponseMessage<R>>,
    ): Flow<ResponseMessage<R>> {
        // @formatter:off
        return try {
            action.invoke()
        } catch (ex: Exception) {
            return flowOf(
                exceptioner
                    .handle(ex, request.metadata)
            )
        }
        // @formatter:on
    }

    fun <R : Any> exchangeFlowR(
        request: RequestMessage<*>,
        action: () -> Flow<R>,
    ): Flow<ResponseMessage<R>> = flow {
        withContext(request) { doExchangeFlowR(request, action) }
    }

    private fun <R : Any> doExchangeFlowR(
        request: RequestMessage<*>,
        action: () -> Flow<R>,
    ): Flow<ResponseMessage<R>> {
        // @formatter:off
        return try {
            action
                .invoke()
                .map {
                    responser
                        .handle(it, request.metadata)
                }
        } catch (ex: Exception) {
            return flowOf(
                exceptioner
                    .handle(ex, request.metadata)
            )
        }
        // @formatter:on
    }

    /* __________________________________________________ */

    suspend fun exchange(
        request: NotificationMessage<*>,
        action: () -> Unit,
    ) = coroutineScope {
        // @formatter:off
        try {
            action.invoke()
        } catch (ex: Exception) {
            exceptioner
                .handle<Any>(ex, request.metadata)
                .let { log.asyncDebug { ex }  }
        }
        // @formatter:on
    }

    suspend fun exchangeSuspending(
        request: NotificationMessage<*>,
        action: suspend () -> Unit,
    ) {
        // @formatter:off
        return try {
            action.invoke()
        } catch (ex: Exception) {
            exceptioner
                .handle<Any>(ex, request.metadata)
                .let { log.asyncDebug { ex } }
        }
        // @formatter:on
    }

    fun exchangeFlow(
        request: NotificationMessage<*>,
        action: () -> Flow<Unit>,
    ): Flow<Unit> {
        // @formatter:off
        return try {
            action.invoke()
        } catch (ex: Exception) {
            return flowOf(
                exceptioner
                    .handle<Any>(ex, request.metadata)
                    .let { log.syncDebug { ex } }
            )
        }
        // @formatter:on
    }

    /* __________________________________________________ */

    suspend fun <T> withContext(
        request: RequestMessage<*>,
        block: suspend CoroutineScope.() -> T,
    ): T {

        val mdc = RequestMetadata
            .asMDC(request.metadata)
            .toMDCEntriesSupplied()

        val mdcContext = MDCContext(mdc.newMap())

        val requestContext = RequestMetadataContext(request.metadata)

        return withContext(requestContext.plus(mdcContext)) {
            try {
                block()
            } finally {
                mdc.cleanup()
            }
        }

    }

}