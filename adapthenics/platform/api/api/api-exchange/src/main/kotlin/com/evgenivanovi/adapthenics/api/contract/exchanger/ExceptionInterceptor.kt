package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.CallMetadata
import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCodes
import com.evgenivanovi.kt.ex.Exceptions
import com.evgenivanovi.kt.lang.asOf

class ExceptionInterceptor {

    private val svc: ResponseAPIService

    private val handlers: List<AdviceHandler>

    constructor(svc: ResponseAPIService, handlers: List<AdviceHandler>) {
        this.svc = svc
        this.handlers = handlers.sortedWith(COMPARATOR)
    }

    fun <R : Any> handle(
        exception: Throwable,
        metadata: CallMetadata,
    ): ResponseMessage<R> {

        val handler = findHandler(exception)
            ?: return fallback(metadata).asOf()

        return handler.handle(
            ExceptionContext(exception, metadata)
        )

    }

    private fun findHandler(exception: Throwable): AdviceHandler? {
        return handlers
            .firstOrNull { it.canHandle(exception) }
    }

    private fun fallback(metadata: CallMetadata): ProblemResponseMessage {

        if (metadata is RequestMetadata) {
            return svc.problem(
                metadata,
                null,
                ResponseStatusCodes.INTERNAL
            )
        }

        return svc.problem(
            null,
            ResponseStatusCodes.INTERNAL,
        )

    }

    companion object {

        private val COMPARATOR = Comparator<AdviceHandler> { adv1, adv2 ->
            Exceptions.compare(
                adv1.target().java, adv2.target().java
            )
        }

    }

}