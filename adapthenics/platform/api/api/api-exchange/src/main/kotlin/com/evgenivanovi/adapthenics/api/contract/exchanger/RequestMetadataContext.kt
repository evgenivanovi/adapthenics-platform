package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext

class RequestMetadataContext(
    val metadata: RequestMetadata,
) : AbstractCoroutineContextElement(Key) {

    companion object Key : CoroutineContext.Key<RequestMetadataContext>

}