package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.CallMetadata

class ExceptionContext<E : Throwable>(
    val ex: E,
    val meta: CallMetadata?,
)