package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import kotlin.reflect.KClass

interface AdviceHandler {

    fun canHandle(ex: Throwable): Boolean

    fun <R : Any> handle(ctx: ExceptionContext<*>): ResponseMessage<R>

    fun target(): KClass<out Throwable>

}