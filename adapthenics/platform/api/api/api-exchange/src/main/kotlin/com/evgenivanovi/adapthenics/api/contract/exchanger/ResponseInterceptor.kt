package com.evgenivanovi.adapthenics.api.contract.exchanger

import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.lang.isOf

class ResponseInterceptor(
    private val svc: ResponseAPIService,
) {

    fun <R : Any> handle(
        result: R,
        metadata: RequestMetadata,
    ): ResponseMessage<R> {
        if (result.isOf<ResponseMessage<*>>())
            return result.asOf()
        return svc.success(metadata, result)
    }

}