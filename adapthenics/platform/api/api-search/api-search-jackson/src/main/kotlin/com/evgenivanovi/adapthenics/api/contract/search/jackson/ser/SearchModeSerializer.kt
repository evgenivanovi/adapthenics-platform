package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.adapthenics.search.api.request.SearchRequestOptions
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class SearchModeSerializer : StdSerializer<SearchRequestOptions.SearchMode>(
    SearchRequestOptions.SearchMode::class.java
) {

    override fun serialize(
        value: SearchRequestOptions.SearchMode,
        gen: JsonGenerator,
        ser: SerializerProvider,
    ) {
        gen.writeString(value.name.lowercase())
    }

}