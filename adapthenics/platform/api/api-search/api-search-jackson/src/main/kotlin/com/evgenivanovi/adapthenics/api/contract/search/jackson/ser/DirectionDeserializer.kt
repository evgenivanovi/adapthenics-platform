package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.search.util.Direction
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class DirectionDeserializer : JsonDeserializer<Direction>() {

    override fun deserialize(
        parser: JsonParser, ctx: DeserializationContext
    ): Direction? {
        return Direction.resolve(parser.text.uppercase())
    }

}