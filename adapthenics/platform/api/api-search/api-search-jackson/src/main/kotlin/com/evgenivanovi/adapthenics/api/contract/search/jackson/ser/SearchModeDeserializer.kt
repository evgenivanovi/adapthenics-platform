package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.adapthenics.search.api.request.SearchRequestOptions
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class SearchModeDeserializer : JsonDeserializer<SearchRequestOptions.SearchMode>() {

    override fun deserialize(
        parser: JsonParser, ctx: DeserializationContext
    ): SearchRequestOptions.SearchMode? {
        return SearchRequestOptions
            .SearchMode
            .resolve(parser.text.uppercase())
    }

}