package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.search.util.Direction
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class DirectionSerializer : StdSerializer<Direction>(Direction::class.java) {

    override fun serialize(
        value: Direction, gen: JsonGenerator, ser: SerializerProvider
    ) {
        gen.writeString(value.name.lowercase())
    }

}