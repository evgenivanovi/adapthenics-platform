package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.adapthenics.search.api.request.SearchRequestOperator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class SearchOperatorDeserializer : JsonDeserializer<SearchRequestOperator>() {

    override fun deserialize(
        parser: JsonParser, ctx: DeserializationContext
    ): SearchRequestOperator? {
        return SearchRequestOperator
            .resolve(parser.text.uppercase())
    }

}