package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.adapthenics.search.api.request.SearchRequestOperator
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class SearchOperatorSerializer : StdSerializer<SearchRequestOperator>(
    SearchRequestOperator::class.java
) {

    override fun serialize(
        value: SearchRequestOperator, gen: JsonGenerator, ser: SerializerProvider
    ) {
        gen.writeString(value.operationCharacter())
    }

}