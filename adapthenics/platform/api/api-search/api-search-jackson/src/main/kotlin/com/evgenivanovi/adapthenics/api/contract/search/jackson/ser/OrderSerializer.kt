package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.search.util.Order
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class OrderSerializer : StdSerializer<Order>(Order::class.java) {

    override fun serialize(
        value: Order, gen: JsonGenerator, ser: SerializerProvider,
    ) {
        gen.writeString(value.name.lowercase())
    }

}