package com.evgenivanovi.adapthenics.api.contract.search.jackson

import com.evgenivanovi.adapthenics.api.contract.search.jackson.ser.*
import com.evgenivanovi.adapthenics.search.api.request.SearchRequestOperator
import com.evgenivanovi.adapthenics.search.api.request.SearchRequestOptions
import com.evgenivanovi.search.util.Direction
import com.evgenivanovi.search.util.Order
import com.fasterxml.jackson.databind.module.SimpleModule

class ApiContractSearchModule : SimpleModule() {

    init {
        mixins()
        ser()
    }

    private fun mixins() {
        //
    }

    private fun ser() {
        addSerializer(SearchRequestOptions.SearchMode::class.java, SearchModeSerializer())
        addDeserializer(SearchRequestOptions.SearchMode::class.java, SearchModeDeserializer())

        addSerializer(SearchRequestOperator::class.java, SearchOperatorSerializer())
        addDeserializer(SearchRequestOperator::class.java, SearchOperatorDeserializer())

        addSerializer(Order::class.java, OrderSerializer())
        addDeserializer(Order::class.java, OrderDeserializer())

        addSerializer(Direction::class.java, DirectionSerializer())
        addDeserializer(Direction::class.java, DirectionDeserializer())
    }

}