package com.evgenivanovi.adapthenics.api.contract.search.jackson.ser

import com.evgenivanovi.search.util.Order
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class OrderDeserializer : JsonDeserializer<Order>() {

    override fun deserialize(
        parser: JsonParser, ctx: DeserializationContext
    ): Order? {
        return Order.resolve(parser.text.uppercase())
    }

}