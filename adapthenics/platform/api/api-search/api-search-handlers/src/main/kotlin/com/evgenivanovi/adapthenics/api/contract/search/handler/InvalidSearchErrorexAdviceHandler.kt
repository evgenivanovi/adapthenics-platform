package com.evgenivanovi.adapthenics.api.contract.search.handler

import com.evgenivanovi.adapthenics.api.contract.exchanger.AdviceHandler
import com.evgenivanovi.adapthenics.api.contract.exchanger.ExceptionContext
import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.meta.CallMetadata
import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.adapthenics.api.contract.response.*
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.search.InvalidSearchErrorex
import kotlin.reflect.KClass

class InvalidSearchErrorexAdviceHandler(
    private val svc: ResponseAPIService,
) : AdviceHandler {

    override fun target(): KClass<InvalidSearchErrorex> {
        return InvalidSearchErrorex::class
    }

    override fun canHandle(ex: Throwable): Boolean {
        return target().java
            .isAssignableFrom(ex::class.java)
    }

    override fun <R : Any> handle(ctx: ExceptionContext<*>): ResponseMessage<R> {
        return toProblemResponse(
            ctx.ex.asOf(target()),
            ctx.meta
        ).asOf()
    }

    private fun toProblemResponse(ex: InvalidSearchErrorex, meta: CallMetadata?): ProblemResponseMessage {

        val problem = Problem.from(
            ErrorCodes.INVALID,
            ErrorMessages.INVALID,
            ex,
        )

        if (meta is RequestMetadata) {
            return svc.problem(
                meta,
                problem,
                ResponseStatusCodes.INVALID
            )
        }

        return svc.problem(
            problem,
            ResponseStatusCodes.INVALID
        )

    }

}