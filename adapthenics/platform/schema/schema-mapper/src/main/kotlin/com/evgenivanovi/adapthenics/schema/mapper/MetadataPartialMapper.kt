package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.kt.lang.Keeper
import kotlinx.datetime.Instant
import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant
import com.evgenivanovi.adapthenics.schema.model.`object`.PartialMetadata as DocumentMetadata
import com.evgenivanovi.domain.model.meta.PartialMetadata as DomainMetadata

object MetadataPartialMapper {

    fun to(value: DocumentMetadata): DomainMetadata {

        val createdAt: Keeper<Instant> = value
            .createdAt
            ?.map { it.toKotlinInstant() }
            ?: Keeper.unset()

        val updatedAt: Keeper<Instant?> = value
            .updatedAt
            ?.map { it.toKotlinInstant() }
            ?: Keeper.unset()

        val deletedAt: Keeper<Instant?> = value
            .deletedAt
            ?.map { it.toKotlinInstant() }
            ?: Keeper.unset()

        return DomainMetadata.of(
            createdAt, updatedAt, deletedAt
        )

    }

    fun from(value: DomainMetadata): DocumentMetadata {

        val createdAt = value
            .createdAt
            .map { it.toJavaInstant() }

        val updatedAt = value
            .updatedAt
            .map { it?.toJavaInstant() }

        val deletedAt = value
            .deletedAt
            .map { it?.toJavaInstant() }

        return DocumentMetadata(
            createdAt, updatedAt, deletedAt,
        )

    }

}