package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.adapthenics.schema.model.document.MetadataDocument
import com.evgenivanovi.domain.model.meta.Metadata
import kotlinx.datetime.toJavaInstant

object MetadataDocumentMapper : DocumentDataMapper<String, Metadata, MetadataDocument> {

    override fun from(value: Metadata): MetadataDocument {

        val id = null

        val createdAt = value
            .createdAt
            .toJavaInstant()

        val updatedAt = value
            .updatedAt
            ?.toJavaInstant()

        val deletedAt = value
            .deletedAt
            ?.toJavaInstant()

        return MetadataDocument(
            id,
            createdAt, updatedAt, deletedAt
        )

    }

}