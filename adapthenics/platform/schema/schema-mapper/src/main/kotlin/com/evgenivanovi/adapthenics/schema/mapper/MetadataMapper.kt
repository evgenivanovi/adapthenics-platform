package com.evgenivanovi.adapthenics.schema.mapper

import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant
import com.evgenivanovi.adapthenics.schema.model.`object`.Metadata as DocumentMetadata
import com.evgenivanovi.domain.model.meta.Metadata as DomainMetadata

object MetadataMapper {

    fun to(value: DocumentMetadata): DomainMetadata {
        return DomainMetadata.of(
            value.createdAt.toKotlinInstant(),
            value.updatedAt?.toKotlinInstant(),
            value.deletedAt?.toKotlinInstant()
        )
    }

    fun from(value: DomainMetadata): DocumentMetadata {
        return DocumentMetadata(
            value.createdAt.toJavaInstant(),
            value.updatedAt?.toJavaInstant(),
            value.deletedAt?.toJavaInstant()
        )
    }

}