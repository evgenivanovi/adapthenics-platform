package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.adapthenics.schema.model.api.PartialDocument
import com.evgenivanovi.domain.model.entity.PartialEntity

// @formatter:off
interface PartialDocumentMapper
<
    ID : Any,
    ENTITY : PartialEntity<ID>,
    DOCUMENT : PartialDocument<ID>
>
{
// @formatter:on

    fun to(value: DOCUMENT): ENTITY

    fun from(value: ENTITY): DOCUMENT

}