package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.adapthenics.schema.model.document.MetadataPartialDocument
import com.evgenivanovi.domain.model.meta.PartialMeta
import com.evgenivanovi.domain.model.meta.PartialMetadata
import com.evgenivanovi.kt.lang.asNotNull
import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant

object MetaPartialMapper : PartialDocumentMapper<String, PartialMeta, MetadataPartialDocument> {

    override fun to(value: MetadataPartialDocument): PartialMeta {

        val id = value.id.asNotNull()

        val createdAt = value
            .createdAt
            .map { it.toKotlinInstant() }

        val updatedAt = value
            .updatedAt
            .map { it?.toKotlinInstant() }

        val deletedAt = value
            .deletedAt
            .map { it?.toKotlinInstant() }

        val metadata = PartialMetadata.of(
            createdAt, updatedAt, deletedAt,
        )

        return PartialMeta.of(id, metadata)

    }

    override fun from(value: PartialMeta): MetadataPartialDocument {

        val id = value.id()

        val createdAt = value
            .data()
            .createdAt
            .map { it.toJavaInstant() }

        val updatedAt = value
            .data()
            .updatedAt
            .map { it?.toJavaInstant() }

        val deletedAt = value
            .data()
            .deletedAt
            .map { it?.toJavaInstant() }

        return MetadataPartialDocument(
            id,
            createdAt, updatedAt, deletedAt
        )

    }

}