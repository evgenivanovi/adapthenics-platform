package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.domain.model.entity.Entity

// @formatter:off
interface DocumentMapper
<
    ID : Any,
    ENTITY : Entity<ID>,
    DOCUMENT : Document<ID>
>
{
// @formatter:on

    fun to(value: DOCUMENT): ENTITY

    fun from(value: ENTITY): DOCUMENT

}