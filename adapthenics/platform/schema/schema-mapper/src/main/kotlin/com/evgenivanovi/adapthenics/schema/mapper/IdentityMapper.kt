package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.adapthenics.schema.model.api.Doc

object IdentityMapper {

    fun <ID : Any> get(doc: Doc<ID>): ID? {
        return doc.id
    }

    fun <ID : Any> obtain(doc: Doc<ID>): ID {
        return get(doc) ?: throw IllegalStateException(
            "Identity for ${doc::class} is empty."
        )
    }

}