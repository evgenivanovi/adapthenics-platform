package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.domain.model.entity.EntityData

// @formatter:off
interface DocumentDataMapper
<
    ID : Any,
    ENTITY_DATA : EntityData,
    DOCUMENT : Document<ID>
>
{
// @formatter:on

    fun from(value: ENTITY_DATA): DOCUMENT

}