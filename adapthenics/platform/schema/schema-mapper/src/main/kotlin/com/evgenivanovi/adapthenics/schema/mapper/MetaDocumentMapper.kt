package com.evgenivanovi.adapthenics.schema.mapper

import com.evgenivanovi.adapthenics.schema.model.document.MetadataDocument
import com.evgenivanovi.domain.model.meta.Meta
import com.evgenivanovi.domain.model.meta.Metadata
import kotlinx.datetime.toKotlinInstant

object MetaDocumentMapper : DocumentMapper<String, Meta, MetadataDocument> {

    override fun to(value: MetadataDocument): Meta {

        val identity = IdentityMapper
            .obtain(value)

        val createdAt = value
            .createdAt
            .toKotlinInstant()

        val updatedAt = value
            .updatedAt
            ?.toKotlinInstant()

        val deletedAt = value
            .deletedAt
            ?.toKotlinInstant()

        val metadata = Metadata.of(
            createdAt, updatedAt, deletedAt
        )

        return Meta.of(identity, metadata)

    }

    override fun from(value: Meta): MetadataDocument {

        val id = value.id()

        val metadata = MetadataMapper
            .from(value.data())

        return MetadataDocument(
            id,
            metadata.createdAt, metadata.updatedAt, metadata.deletedAt
        )

    }

}