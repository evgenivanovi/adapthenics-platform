package com.evgenivanovi.search.mongodb.visitor

import com.evgenivanovi.search.mongodb.ChunkRequest
import com.evgenivanovi.search.query.chunk.ChunkingQueries
import com.evgenivanovi.search.query.chunk.ChunkingQuery
import com.evgenivanovi.search.query.chunk.ChunkingQueryVisitor
import org.springframework.data.domain.Pageable

internal object ChunkingBuildingVisitor
    : ChunkingQueryVisitor<Pageable> {

    override fun visit(
        query: ChunkingQuery,
    ): Pageable {
        return when (query) {
            is ChunkingQueries.Chunked ->
                visitChunked(query)

            is ChunkingQueries.Unchunked ->
                visitUnchunked(query)
        }
    }

    override fun visitChunked(
        query: ChunkingQueries.Chunked,
    ): Pageable {
        return ChunkRequest(
            query.offset(),
            query.limit().toInt()
        )
    }

    override fun visitUnchunked(
        query: ChunkingQueries.Unchunked,
    ): Pageable {
        return Pageable.unpaged()
    }

}