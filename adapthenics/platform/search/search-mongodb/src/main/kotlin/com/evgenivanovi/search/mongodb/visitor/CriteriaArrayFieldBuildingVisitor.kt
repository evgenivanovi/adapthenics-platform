package com.evgenivanovi.search.mongodb.visitor

import com.evgenivanovi.kt.ex.UnknownTypeException
import com.evgenivanovi.search.query.field.ArrayFieldQueries
import com.evgenivanovi.search.query.field.ArrayFieldQuery
import com.evgenivanovi.search.query.field.ArrayFieldQueryVisitor
import org.springframework.data.mongodb.core.query.Criteria

internal object CriteriaArrayFieldBuildingVisitor :
    ArrayFieldQueryVisitor<Criteria> {

    override fun visit(
        query: ArrayFieldQuery,
    ): Criteria {
        return when (query) {
            is ArrayFieldQueries.Equality<*> -> {
                visitEquality(query)
            }

            is ArrayFieldQueries.Inequality<*> -> {
                visitInequality(query)
            }

            is ArrayFieldQueries.ContainsAll<*> -> {
                visitContainsAll(query)
            }

            is ArrayFieldQueries.NotContainAll<*> -> {
                visitNotContainAll(query)
            }

            is ArrayFieldQueries.ContainsAny<*> -> {
                visitContainsAny(query)
            }

            else -> throw UnknownTypeException(query)
        }
    }

    override fun visitEquality(
        query: ArrayFieldQueries.Equality<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .`is`(query.values)
    }

    override fun visitInequality(
        query: ArrayFieldQueries.Inequality<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .ne(query.values)
    }

    override fun visitContainsAll(
        query: ArrayFieldQueries.ContainsAll<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .all(query.values)
    }

    override fun visitNotContainAll(
        query: ArrayFieldQueries.NotContainAll<*>,
    ): Criteria {
        return query.values
            .map { Criteria.where(query.property()).ne(it) }
            .let { Criteria().andOperator(it) }
    }

    override fun visitContainsAny(
        query: ArrayFieldQueries.ContainsAny<*>,
    ): Criteria {
        return query.values
            .map { Criteria.where(query.property()).`is`(it) }
            .let { Criteria().orOperator(it) }
    }

    override fun visitContainsNone(
        query: ArrayFieldQueries.ContainsNone<*>,
    ): Criteria {
        return query.values
            .map { Criteria.where(query.property()).ne(it) }
            .let { Criteria().andOperator(it) }
    }

}