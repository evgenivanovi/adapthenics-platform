package com.evgenivanovi.search.mongodb.visitor

import com.evgenivanovi.kt.ex.UnknownTypeException
import com.evgenivanovi.kt.regex.escape
import com.evgenivanovi.kt.string.surround
import com.evgenivanovi.search.query.field.SingleFieldQueries
import com.evgenivanovi.search.query.field.SingleFieldQuery
import com.evgenivanovi.search.query.field.SingleFieldQueryVisitor
import org.springframework.data.mongodb.core.query.Criteria

internal object CriteriaSingleFieldBuildingVisitor :
    SingleFieldQueryVisitor<Criteria> {

    override fun visit(
        query: SingleFieldQuery,
    ): Criteria {
        return when (query) {
            is SingleFieldQueries.Equality<*> -> {
                visitEquality(query)
            }

            is SingleFieldQueries.Inequality<*> -> {
                visitInequality(query)
            }

            is SingleFieldQueries.GreaterThan<*> -> {
                visitGreaterThan(query)
            }

            is SingleFieldQueries.LessThan<*> -> {
                visitLessThan(query)
            }

            is SingleFieldQueries.GreaterThanEquality<*> -> {
                visitGreaterThanEquality(query)
            }

            is SingleFieldQueries.LessThanEquality<*> -> {
                visitLessThanEquality(query)
            }

            is SingleFieldQueries.StartsWith<*> -> {
                visitStartsWith(query)
            }

            is SingleFieldQueries.NotStartWith<*> -> {
                visitNotStartWith(query)
            }

            is SingleFieldQueries.EndsWith<*> -> {
                visitEndsWith(query)
            }

            is SingleFieldQueries.NotEndWith<*> -> {
                visitNotEndWith(query)
            }

            else -> throw UnknownTypeException(query)
        }
    }

    override fun visitEquality(
        query: SingleFieldQueries.Equality<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .`is`(query.value)
    }

    override fun visitInequality(
        query: SingleFieldQueries.Inequality<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .ne(query.value)
    }

    override fun visitGreaterThan(
        query: SingleFieldQueries.GreaterThan<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .gt(query.value)
    }

    override fun visitLessThan(
        query: SingleFieldQueries.LessThan<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .lt(query.value)
    }

    override fun visitGreaterThanEquality(
        query: SingleFieldQueries.GreaterThanEquality<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .gte(query.value)
    }

    override fun visitLessThanEquality(
        query: SingleFieldQueries.LessThanEquality<*>,
    ): Criteria {
        return Criteria
            .where(query.property())
            .lte(query.value)
    }

    override fun visitStartsWith(
        query: SingleFieldQueries.StartsWith<*>,
    ): Criteria {

        val regex = query
            .value
            .toString()
            .escape()
            .surround(
                pre = MONGO_STARTS_WITH_PREFIX,
                post = MONGO_STARTS_WITH_SUFFIX
            )

        return Criteria
            .where(query.property())
            .regex(regex)

    }

    override fun visitNotStartWith(
        query: SingleFieldQueries.NotStartWith<*>,
    ): Criteria {

        val regex = query
            .value
            .toString()
            .escape()
            .surround(
                pre = MONGO_STARTS_WITH_PREFIX,
                post = MONGO_STARTS_WITH_SUFFIX
            )

        return Criteria
            .where(query.property())
            .not()
            .regex(regex)

    }

    override fun visitEndsWith(
        query: SingleFieldQueries.EndsWith<*>,
    ): Criteria {

        val regex = query
            .value
            .toString()
            .escape()
            .surround(
                pre = MONGO_ENDS_WITH_PREFIX,
                post = MONGO_ENDS_WITH_SUFFIX
            )

        return Criteria
            .where(query.property())
            .regex(regex)

    }

    override fun visitNotEndWith(
        query: SingleFieldQueries.NotEndWith<*>,
    ): Criteria {

        val regex = query
            .value
            .toString()
            .escape()
            .surround(
                pre = MONGO_ENDS_WITH_PREFIX,
                post = MONGO_ENDS_WITH_SUFFIX
            )

        return Criteria
            .where(query.property())
            .not()
            .regex(regex)

    }

    private const val MONGO_STARTS_WITH_PREFIX = "^"
    private const val MONGO_STARTS_WITH_SUFFIX = ".*"

    private const val MONGO_ENDS_WITH_PREFIX = "^.*"
    private const val MONGO_ENDS_WITH_SUFFIX = "$"

}