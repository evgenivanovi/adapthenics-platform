package com.evgenivanovi.search.mongodb

import com.evgenivanovi.kt.ex.UnknownTypeException
import com.evgenivanovi.search.mongodb.visitor.*
import com.evgenivanovi.search.query.chunk.ChunkingQuery
import com.evgenivanovi.search.query.Queries
import com.evgenivanovi.search.query.Query
import com.evgenivanovi.search.query.SliceQueries
import com.evgenivanovi.search.query.SliceQuery
import com.evgenivanovi.search.query.cursor.CursoringQuery
import com.evgenivanovi.search.query.field.ArrayFieldQuery
import com.evgenivanovi.search.query.field.SingleFieldQuery
import com.evgenivanovi.search.query.page.PageableQuery
import com.evgenivanovi.search.query.sort.StringSortQuery
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query as MongoQuery

object QueriesMapper {

    fun mapToQuery(queries: Queries): MongoQuery {

        val criterias = queries.fields
            .map(::criteriaForQuery)

        val orders = queries.sorts
            .map(::orderForQuery)

        return mapToQuery(criterias, orders)

    }

    fun mapToPageQuery(queries: SliceQueries): MongoQuery {

        val criterias = queries.queries.fields
            .map(::criteriaForQuery)

        val orders = queries.queries.sorts
            .map(::orderForQuery)

        val slice = sliceForQuery(queries.slice)

        return mapToQuery(criterias, orders, slice)

    }

    private fun mapToQuery(
        criterias: List<Criteria>,
        orders: List<Sort.Order>,
        pageable: Pageable = Pageable.unpaged(),
    ): MongoQuery {
        return MongoQuery()
            // property
            .also { query ->
                criterias.forEach { criteria ->
                    query.addCriteria(criteria)
                }
            }
            // sort
            .also { query ->
                Sort.by(orders)
                    .let { query.with(it) }
            }
            // page
            .also { query ->
                query.with(pageable)
            }
    }

    private fun criteriaForQuery(query: Query): Criteria {

        return when (query) {
            is SingleFieldQuery -> {
                CriteriaSingleFieldBuildingVisitor
                    .visit(query)
            }

            is ArrayFieldQuery -> {
                CriteriaArrayFieldBuildingVisitor
                    .visit(query)
            }

            else -> throw UnknownTypeException(query)
        }

    }

    private fun orderForQuery(query: StringSortQuery): Sort.Order {
        return SortOrderBuildingVisitor.visit(query)
    }

    private fun sliceForQuery(query: SliceQuery): Pageable {
        return when (query) {
            is PageableQuery -> PageableBuildingVisitor.visit(query)
            is ChunkingQuery -> ChunkingBuildingVisitor.visit(query)
            is CursoringQuery -> CursoringBuildingVisitor.visit(query)
            else -> throw UnknownTypeException(query)
        }
    }

}