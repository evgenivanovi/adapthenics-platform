package com.evgenivanovi.search.mongodb.visitor

import com.evgenivanovi.search.mongodb.ChunkRequest
import com.evgenivanovi.search.query.cursor.CursoringQueries
import com.evgenivanovi.search.query.cursor.CursoringQuery
import com.evgenivanovi.search.query.cursor.CursoringQueryVisitor
import org.springframework.data.domain.Pageable

internal object CursoringBuildingVisitor
    : CursoringQueryVisitor<Pageable> {

    override fun visit(
        query: CursoringQuery,
    ): Pageable {
        return when (query) {
            is CursoringQueries.Cursored ->
                visitCursored(query)

            is CursoringQueries.Uncursored ->
                visitUncursored(query)
        }
    }

    override fun visitCursored(
        query: CursoringQueries.Cursored,
    ): Pageable {
        return ChunkRequest(query.limit().toInt())
    }

    override fun visitUncursored(
        query: CursoringQueries.Uncursored,
    ): Pageable {
        return Pageable.unpaged()
    }

}