package com.evgenivanovi.search.mongodb.visitor

import com.evgenivanovi.search.query.page.PageableQueries
import com.evgenivanovi.search.query.page.PageableQuery
import com.evgenivanovi.search.query.page.PageableQueryVisitor
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable

internal object PageableBuildingVisitor
    : PageableQueryVisitor<Pageable> {

    override fun visit(
        query: PageableQuery,
    ): Pageable {
        return when (query) {
            is PageableQueries.Paged ->
                visitPaged(query)

            is PageableQueries.Unpaged ->
                visitUnpaged(query)
        }
    }

    override fun visitPaged(
        query: PageableQueries.Paged,
    ): Pageable {
        return PageRequest
            .of(query.page().toInt(), query.size().toInt())
    }

    override fun visitUnpaged(
        query: PageableQueries.Unpaged,
    ): Pageable {
        return Pageable.unpaged()
    }

}