package com.evgenivanovi.search.mongodb.visitor

import com.evgenivanovi.kt.ex.UnknownTypeException
import com.evgenivanovi.search.query.sort.SortQueries
import com.evgenivanovi.search.query.sort.SortQueryVisitor
import com.evgenivanovi.search.query.sort.StringSortQuery
import org.springframework.data.domain.Sort

internal object SortOrderBuildingVisitor
    : SortQueryVisitor<Sort.Order> {

    override fun visit(
        query: StringSortQuery,
    ): Sort.Order {
        return when (query) {
            is SortQueries.Ascending -> {
                visitAscending(query)
            }

            is SortQueries.Descending -> {
                visitDescending(query)
            }

            else -> throw UnknownTypeException(query)
        }
    }

    override fun visitAscending(
        query: SortQueries.Ascending,
    ): Sort.Order {
        return Sort.Order(
            Sort.Direction.ASC,
            query.property()
        )
    }

    override fun visitDescending(
        query: SortQueries.Descending,
    ): Sort.Order {
        return Sort.Order(
            Sort.Direction.DESC,
            query.property()
        )
    }

}