package com.evgenivanovi.search.mongodb

import com.evgenivanovi.kt.ex.UnreachableExceptionObject
import com.evgenivanovi.kt.math.Numbers.ONE_I
import com.evgenivanovi.kt.math.Numbers.ZERO
import com.evgenivanovi.kt.math.Numbers.ZERO_I
import com.evgenivanovi.kt.math.eq
import com.evgenivanovi.kt.math.gt
import com.evgenivanovi.kt.math.gte
import com.evgenivanovi.kt.math.lt
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import java.io.Serializable

class ChunkRequest : Pageable, Serializable {

    private val offset: Long

    private val limit: Int

    private val sort: Sort

    constructor(
        offset: Long,
        limit: Int,
        sort: Sort,
    ) {

        require(offset.gte(ZERO_I)) {
            "Page index must not be less than zero!"
        }

        require(limit.gte(ONE_I)) {
            "Page size must not be less than one!"
        }

        this.offset = offset
        this.limit = limit
        this.sort = sort

    }

    constructor(
        offset: Long,
        limit: Int,
    ) {

        require(offset.gte(ZERO_I)) {
            "Offset must not be less than zero!"
        }

        require(limit.gte(ONE_I)) {
            "Limit must not be less than one!"
        }

        this.offset = offset
        this.limit = limit
        this.sort = Sort.unsorted()

    }

    constructor(
        limit: Int,
    ) {

        require(limit.gte(ONE_I)) {
            "Limit must not be less than one!"
        }

        this.offset = ZERO
        this.limit = limit
        this.sort = Sort.unsorted()

    }

    override fun getPageNumber(): Int {
        return Math.toIntExact(getOffset().div(limit))
    }

    override fun getPageSize(): Int {
        return limit
    }

    override fun getOffset(): Long {
        return offset
    }

    override fun getSort(): Sort {
        return sort
    }

    override fun next(): ChunkRequest {
        return next(ONE_I)
    }

    private fun next(times: Int): ChunkRequest {
        val nextOffset = getOffset()
            .plus(pageSize.times(times))
        return ChunkRequest(nextOffset, pageSize)
    }

    override fun hasPrevious(): Boolean {
        return getOffset().gt(limit)
    }

    private fun hasPrevious(times: Int): Boolean {
        return getOffset().gt(limit.times(times))
    }

    override fun previousOrFirst(): ChunkRequest {
        return if (hasPrevious()) previous()
        else first()
    }

    private fun previous(): ChunkRequest {
        return if (hasPrevious()) previous(ONE_I)
        else this
    }

    private fun previous(times: Int): ChunkRequest {
        return if (hasPrevious(times)) {
            val offset = getOffset().minus(pageSize.times(times))
            ChunkRequest(offset, pageSize)
        } else this
    }

    override fun first(): ChunkRequest {
        return ChunkRequest(ZERO, pageSize)
    }

    override fun withPage(pageNumber: Int): ChunkRequest {

        require(pageNumber.gte(ZERO_I)) {
            "Page index must not be less than zero!"
        }

        val comparison = getPageNumber().compareTo(pageNumber)

        if (comparison.eq(ZERO_I)) {
            return this
        }

        if (comparison.lt(ZERO_I)) {
            val diff = pageNumber.minus(getPageNumber())
            return previous(diff)
        }

        if (comparison.gt(ZERO_I)) {
            val diff = getPageNumber().minus(pageNumber)
            return next(diff)
        }

        throw UnreachableExceptionObject

    }

}