project(ProjectModules.SEARCH_MONGO) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(libs.bundles.mongo.client.impl)
    implementation(libs.bundles.mongo.client.spring.impl)
    testImplementation(ktDependenciesCatalog.bundles.testcontainers.mongo.impl)

    implementation(searchCatalog.searchQuery)
    testImplementation(searchCatalog.searchQuery)
}