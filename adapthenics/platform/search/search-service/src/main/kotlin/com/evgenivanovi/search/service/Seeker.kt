package com.evgenivanovi.search.service

import com.evgenivanovi.search.model.*

interface Seeker<ENTITY : Any> {

    suspend fun count(command: CountCommand): Long

    suspend fun search(command: SearchCommand): Collection<ENTITY>

    suspend fun search(command: PageSearchCommand): PageableResult<ENTITY>

    suspend fun search(command: ChunkSearchCommand): ChunkingResult<ENTITY>

    suspend fun search(command: CursorSearchCommand): CursorResult<ENTITY>

}