package com.evgenivanovi.search.service

import com.evgenivanovi.kt.func.slet
import com.evgenivanovi.search.model.*
import com.evgenivanovi.search.spec.*
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

abstract class BaseSeekerService<ID : Any, T : Any>(
    private val pk: PrimaryKeyVisitor<T, String>,
    private val search: SearchConfiguration,
    private val sort: SortableConfiguration,
    private val factory: SpecificationFactory,
    private val repo: SpecificationRepository<ID, T>,
) : Seeker<T> {

    override suspend fun count(command: CountCommand): Long {
        SearchValidator(search, sort).also { it.validate(command) }
        return doCount(command.distinct())
    }

    override suspend fun search(
        command: SearchCommand,
    ): Collection<T> {
        SearchValidator(search, sort).also { it.validate(command) }
        return doSearch(command.distinct())
    }

    override suspend fun search(
        command: PageSearchCommand,
    ): PageableResult<T> {
        SearchValidator(search, sort).also { it.validate(command) }
        return doSearch(command.distinct())
    }

    override suspend fun search(
        command: ChunkSearchCommand,
    ): ChunkingResult<T> {
        SearchValidator(search, sort).also { it.validate(command) }
        return doSearch(command.distinct())
    }

    override suspend fun search(
        command: CursorSearchCommand,
    ): CursorResult<T> {
        SearchValidator(search, sort).also { it.validate(command) }
        return doSearch(command.distinct())
    }

    private suspend fun doCount(
        command: CountCommand,
    ): Long {
        val specification = factory.from(command)
        return repo.count(specification)
    }

    private suspend fun doSearch(
        command: SearchCommand,
    ): Collection<T> {
        return factory.from(command).slet(repo::find)
    }

    private suspend fun doSearch(
        command: PageSearchCommand,
    ): PageableResult<T> = coroutineScope {

        /**
         * The preferable way how you can avoid the crash is by wrapping async with coroutineScope.
         * Now when the exception occurs inside async it will cancel all other coroutines created in this scope,
         * without touching outer scope.
         */

        val specification = factory.from(command)

        // All the async coroutines become the children of this scope and,
        // if the scope fails with an exception or is cancelled,
        // all the children are cancelled, too.
        val count: Deferred<Long> =
            async(coroutineContext) {
                repo.count(specification.countSpecification())
            }

        // All the async coroutines become the children of this scope and,
        // if the scope fails with an exception or is cancelled,
        // all the children are cancelled, too.
        val items: Deferred<Collection<T>> =
            async(coroutineContext) {
                repo.find(specification)
            }

        PageableResult.of(
            specification.page(),
            count.await(),
            items.await()
        )

    }

    private suspend fun doSearch(
        command: ChunkSearchCommand,
    ): ChunkingResult<T> = coroutineScope {

        /**
         * The preferable way how you can avoid the crash is by wrapping async with coroutineScope.
         * Now when the exception occurs inside async it will cancel all other coroutines created in this scope,
         * without touching outer scope.
         */

        val specification = factory.from(command)

        // All the async coroutines become the children of this scope and,
        // if the scope fails with an exception or is cancelled,
        // all the children are cancelled, too.
        val countResult: Deferred<Long> =
            async(coroutineContext) {
                repo.count(specification.countSpecification())
            }

        // All the async coroutines become the children of this scope and,
        // if the scope fails with an exception or is cancelled,
        // all the children are cancelled, too.
        val searchResult: Deferred<Collection<T>> =
            async(coroutineContext) {
                repo.find(specification)
            }

        ChunkingResult.of(
            specification.chunk(),
            countResult.await(),
            searchResult.await()
        )

    }

    private suspend fun doSearch(
        command: CursorSearchCommand,
    ): CursorResult<T> {

        val specification = factory.from(command)

        val searchResult = repo.find(specification)

        val meta = CursorMeta.of(
            searchResult.makeCursors(),
            specification.cursor()
        )

        return CursorResult.of(meta, searchResult)

    }

    private fun Collection<T>.makeCursors(): Cursors {

        val before = firstOrNull()
            ?.let(pk::visitPrimaryKey)

        val after = lastOrNull()
            ?.let(pk::visitPrimaryKey)

        return Cursors.of(before, after)

    }

}