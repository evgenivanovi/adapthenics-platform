package com.evgenivanovi.search.service

import com.evgenivanovi.kt.coll.anyOrTrue
import com.evgenivanovi.kt.errx.ErrorFormats
import com.evgenivanovi.kt.errx.Errors
import com.evgenivanovi.kt.errx.InvalidError
import com.evgenivanovi.kt.errx.InvalidErrorex
import com.evgenivanovi.kt.lang.eq
import com.evgenivanovi.kt.string.Strings.COLON
import com.evgenivanovi.kt.string.Strings.DOT
import com.evgenivanovi.kt.string.Strings.SINGLE_QUOTE
import com.evgenivanovi.kt.string.Strings.SPACE
import com.evgenivanovi.kt.string.build
import com.evgenivanovi.search.model.*
import com.evgenivanovi.search.spec.SearchConfiguration
import com.evgenivanovi.search.spec.SortableConfiguration

internal class SearchValidator(
    private val searchConfiguration: SearchConfiguration,
    private val sortableConfiguration: SortableConfiguration,
) {

    fun validate(command: CountableCommand) {

        when (command.options().mode) {
            SearchOptions.SearchMode.ELASTIC ->
                validateElasticly(command)

            SearchOptions.SearchMode.STRICT ->
                validateStrictly(command)
        }

    }

    fun validate(command: SearchableCommand) {

        when (command.options().mode) {
            SearchOptions.SearchMode.ELASTIC ->
                validateElasticly(command)

            SearchOptions.SearchMode.STRICT ->
                validateStrictly(command)
        }

    }

    /* __________________________________________________ */

    private fun validateStrictly(command: CountableCommand) {

        val err = Errors
            .collect()
            .add(allSearchKeysExistCheck(command.criterias()))
            .add(differentCriteriasCheck(command.criterias()))

        if (err.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), err.errors())
            throw InvalidErrorex.of(invalid)
        }

    }

    private fun validateElasticly(command: CountableCommand) {

        val err = Errors
            .collect()
            .add(anySearchKeyExistCheck(command.criterias()))

        if (err.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), err.errors())
            throw InvalidErrorex.of(invalid)
        }

    }

    /* __________________________________________________ */

    private fun validateStrictly(command: SearchableCommand) {

        val err = Errors
            .collect()
            .add(allSearchKeysExistCheck(command.criterias()))
            .add(allSortKeysExistCheck(command.orders()))
            .add(differentCriteriasCheck(command.criterias()))
            .add(differentOrdersCheck(command.orders()))

        if (err.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), err.errors())
            throw InvalidErrorex.of(invalid)
        }

    }

    private fun validateElasticly(command: SearchableCommand) {
        val err = Errors
            .collect()
            .add(anySearchKeyExistCheck(command.criterias()))
            .add(anySortKeyExistCheck(command.orders()))

        if (err.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), err.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    /* __________________________________________________ */

    private fun allSearchKeysExist(
        criterias: List<SearchCriteria>,
    ): Boolean {
        return criterias
            .map(SearchCriteria::key)
            .all(searchConfiguration::containsSearchKey)
    }

    private fun allSearchKeysExistCheck(
        criterias: List<SearchCriteria>,
    ): String? {

        if (allSearchKeysExist(criterias))
            return null

        return SearchExceptionMessages
            .invalidSearchKeysMessage(invalidSearchKeys(criterias))

    }

    private fun invalidSearchKeys(
        criterias: List<SearchCriteria>,
    ): List<String> {
        return criterias
            .map(SearchCriteria::key)
            .filterNot(searchConfiguration::containsSearchKey)
    }

    /* __________________________________________________ */

    private fun allSortKeysExist(
        orders: List<SearchOrder>,
    ): Boolean {
        return orders
            .map(SearchOrder::key)
            .all(sortableConfiguration::containsSortableKey)
    }

    private fun allSortKeysExistCheck(
        orders: List<SearchOrder>,
    ): String? {

        if (allSortKeysExist(orders))
            return null

        return SearchExceptionMessages
            .invalidSortKeysMessage(invalidSortKeys(orders))

    }

    private fun invalidSortKeys(
        orders: List<SearchOrder>,
    ): List<String> {
        return orders
            .map(SearchOrder::key)
            .filterNot(sortableConfiguration::containsSortableKey)
    }

    /* __________________________________________________ */

    private fun anySearchKeyExist(
        criterias: List<SearchCriteria>,
    ): Boolean {
        return criterias
            .map(SearchCriteria::key)
            .anyOrTrue(searchConfiguration::containsSearchKey)
    }

    private fun anySearchKeyExistCheck(
        criterias: List<SearchCriteria>,
    ): String? {

        if (anySearchKeyExist(criterias))
            return null

        return SearchExceptionMessages
            .invalidSearchKeysMessage(invalidSearchKeys(criterias))

    }

    /* __________________________________________________ */

    private fun anySortKeyExist(
        orders: List<SearchOrder>,
    ): Boolean {
        return orders
            .map { it.key }
            .anyOrTrue { sortableConfiguration.containsSortableKey(it) }
    }

    private fun anySortKeyExistCheck(
        orders: List<SearchOrder>,
    ): String? {

        if (anySortKeyExist(orders))
            return null

        return SearchExceptionMessages
            .invalidSortKeysMessage(invalidSortKeys(orders))

    }

    /* __________________________________________________ */

    private fun differentCriterias(
        criterias: List<SearchCriteria>,
    ): Boolean {
        return criterias.distinct().eq(criterias)
    }

    private fun differentCriteriasCheck(
        criterias: List<SearchCriteria>,
    ): String? {

        if (differentCriterias(criterias))
            return null

        return SearchExceptionMessages.duplicateCriteriasMessage()

    }

    /* __________________________________________________ */

    private fun differentOrders(
        orders: List<SearchOrder>,
    ): Boolean {
        return orders.distinct().eq(orders)
    }

    private fun differentOrdersCheck(
        orders: List<SearchOrder>,
    ): String? {

        if (differentOrders(orders))
            return null

        return SearchExceptionMessages.duplicateOrdersMessage()

    }

    /* __________________________________________________ */

    internal object SearchExceptionMessages {

        fun unknownSearchModeMessage(): String {
            return "Search mode is unknown."
        }

        fun invalidSearchKeysMessage(
            keys: List<String>,
        ): String {

            return StringBuilder()
                .append("Search command contains invalid search keys")
                .append(COLON)
                .append(SPACE)
                .append(SINGLE_QUOTE)
                .append(keys.joinToString())
                .append(SINGLE_QUOTE)
                .append(DOT)
                .build()

        }

        fun invalidSortKeysMessage(
            keys: List<String>,
        ): String {

            return StringBuilder()
                .append("Search command contains invalid sort keys")
                .append(COLON)
                .append(SPACE)
                .append(SINGLE_QUOTE)
                .append(keys.joinToString())
                .append(SINGLE_QUOTE)
                .append(DOT)
                .build()

        }

        fun duplicateCriteriasMessage(): String {
            return "Search command contains duplicate search criterias."
        }

        fun duplicateOrdersMessage(): String {
            return "Search command contains duplicate search orders."
        }

    }

}