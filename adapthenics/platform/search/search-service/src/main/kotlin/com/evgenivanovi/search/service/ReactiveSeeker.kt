package com.evgenivanovi.search.service

import com.evgenivanovi.search.model.*
import kotlinx.coroutines.flow.Flow

interface ReactiveSeeker<ENTITY : Any> {

    suspend fun count(command: CountCommand): Long

    fun searchR(command: SearchCommand): Flow<ENTITY>

    suspend fun search(command: PageSearchCommand): PageableResult<ENTITY>

    suspend fun search(command: ChunkSearchCommand): ChunkingResult<ENTITY>

    suspend fun search(command: CursorSearchCommand): CursorResult<ENTITY>

}