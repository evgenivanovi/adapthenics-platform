package com.evgenivanovi.search.postgres

import com.evgenivanovi.kt.ex.UnknownTypeException
import com.evgenivanovi.search.query.field.SingleFieldQueries
import com.evgenivanovi.search.query.field.SingleFieldQuery
import com.evgenivanovi.search.query.field.SingleFieldQueryVisitor
import org.jooq.Condition
import org.jooq.impl.DSL

internal object CriteriaSingleFieldBuildingVisitor : SingleFieldQueryVisitor<Condition> {

    override fun visit(
        query: SingleFieldQuery,
    ): Condition {
        return when (query) {
            is SingleFieldQueries.Equality<*> -> {
                visitEquality(query)
            }

            is SingleFieldQueries.Inequality<*> -> {
                visitInequality(query)
            }

            is SingleFieldQueries.GreaterThan<*> -> {
                visitGreaterThan(query)
            }

            is SingleFieldQueries.LessThan<*> -> {
                visitLessThan(query)
            }

            is SingleFieldQueries.GreaterThanEquality<*> -> {
                visitGreaterThanEquality(query)
            }

            is SingleFieldQueries.LessThanEquality<*> -> {
                visitLessThanEquality(query)
            }

            is SingleFieldQueries.StartsWith<*> -> {
                visitStartsWith(query)
            }

            is SingleFieldQueries.NotStartWith<*> -> {
                visitNotStartWith(query)
            }

            is SingleFieldQueries.EndsWith<*> -> {
                visitEndsWith(query)
            }

            is SingleFieldQueries.NotEndWith<*> -> {
                visitNotEndWith(query)
            }

            else -> throw UnknownTypeException(query)
        }
    }

    override fun visitEquality(
        query: SingleFieldQueries.Equality<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitInequality(
        query: SingleFieldQueries.Inequality<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitGreaterThan(
        query: SingleFieldQueries.GreaterThan<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitLessThan(
        query: SingleFieldQueries.LessThan<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitGreaterThanEquality(
        query: SingleFieldQueries.GreaterThanEquality<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitLessThanEquality(
        query: SingleFieldQueries.LessThanEquality<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitStartsWith(
        query: SingleFieldQueries.StartsWith<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitNotStartWith(
        query: SingleFieldQueries.NotStartWith<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitEndsWith(
        query: SingleFieldQueries.EndsWith<*>,
    ): Condition {
        return DSL.trueCondition()
    }

    override fun visitNotEndWith(
        query: SingleFieldQueries.NotEndWith<*>,
    ): Condition {
        return DSL.trueCondition()
    }

}