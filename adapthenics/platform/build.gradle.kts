@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

import com.evgenivanovi.adapthenics.lib.Platform.JAVA_LANG_VERSION
import com.evgenivanovi.adapthenics.lib.Platform.JAVA_LANG_VERSION_VALUE
import com.evgenivanovi.adapthenics.lib.Platform.KOTLIN_LANG_VERSION_VALUE
import com.evgenivanovi.adapthenics.lib.tool.onlyLibraries

plugins {

    /* Gradle Plugins */
    id("idea")
    id("org.gradle.base")
    id("org.gradle.java")
    id("org.gradle.java-library")
    id("org.gradle.distribution")
    id("org.gradle.maven-publish")

    /* Gradle Extended Plugins */
    id("net.saliman.properties")

    /* Kotlin Plugins */
    id("org.jetbrains.kotlin.jvm")
    id("org.jetbrains.kotlin.plugin.serialization")

    /* Adapthenics Plugins */
    id("com.evgenivanovi.adapthenics.lib")

}

configure(project.subprojects.onlyLibraries()) {

    /* Gradle Plugins */
    apply(plugin = "idea")
    apply(plugin = "org.gradle.base")
    apply(plugin = "org.gradle.java")
    apply(plugin = "org.gradle.java-library")
    apply(plugin = "org.gradle.distribution")
    apply(plugin = "org.gradle.maven-publish")
    apply(plugin = "org.gradle.version-catalog")

    /* Gradle Extended Plugins */
    apply(plugin = "net.saliman.properties")

    /* Kotlin Plugins */
    apply(plugin = "org.jetbrains.kotlin.jvm")
    /* https://docs.spring.io/spring-framework/docs/current/reference/html/languages.html#kotlin-multiplatform-serialization */
    // apply(plugin = "org.jetbrains.kotlin.plugin.serialization")

    /* Adapthenics Plugins */
    apply(plugin = "com.evgenivanovi.adapthenics.lib")

    /* __________________________________________________ */

    dependencies {
        api(platform(project(ProjectModules.PLATFORM_DEPENDENCIES_BOM)))
        annotationProcessor(platform(project(ProjectModules.PLATFORM_DEPENDENCIES_BOM)))
    }

    /* __________________________________________________ */

    tasks {

        val javaArgs = listOf(
            "--add-opens=java.base/java.net=ALL-UNNAMED",
            "--add-opens=java.base/java.nio=ALL-UNNAMED",
            "--add-opens=java.base/java.time=ALL-UNNAMED",
            "--add-opens=java.base/java.text=ALL-UNNAMED",

            "--add-opens=java.base/java.lang=ALL-UNNAMED",
            "--add-opens=java.base/java.lang.invoke=ALL-UNNAMED",
            "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED",

            "--add-opens=java.base/java.util=ALL-UNNAMED",
            "--add-opens=java.base/java.util.concurrent=ALL-UNNAMED",
        )

        val kotlinArgs = listOf(
            "-Xemit-jvm-type-annotations"
        )

        java {

            withSourcesJar()

            sourceCompatibility = JAVA_LANG_VERSION
            targetCompatibility = JAVA_LANG_VERSION

            compileJava {
                sourceCompatibility = JAVA_LANG_VERSION.toString()
                targetCompatibility = JAVA_LANG_VERSION.toString()
                options.compilerArgs = options.compilerArgs.apply {
                    addAll(javaArgs)
                }
            }

            compileTestJava {
                sourceCompatibility = JAVA_LANG_VERSION.toString()
                targetCompatibility = JAVA_LANG_VERSION.toString()
                options.compilerArgs = options.compilerArgs.apply {
                    addAll(javaArgs)
                }
            }

        }

        kotlin {

            compileKotlin {
                kotlinOptions.apiVersion = KOTLIN_LANG_VERSION_VALUE
                kotlinOptions.languageVersion = KOTLIN_LANG_VERSION_VALUE
                kotlinOptions.jvmTarget = JAVA_LANG_VERSION_VALUE
                kotlinOptions.freeCompilerArgs = kotlinArgs
            }

            compileTestKotlin {
                kotlinOptions.apiVersion = KOTLIN_LANG_VERSION_VALUE
                kotlinOptions.languageVersion = KOTLIN_LANG_VERSION_VALUE
                kotlinOptions.jvmTarget = JAVA_LANG_VERSION_VALUE
                kotlinOptions.freeCompilerArgs = kotlinArgs
            }

        }

        test {
            useJUnitPlatform()
            jvmArgs = javaArgs
        }

    }

    /* __________________________________________________ */

    tasks.withType<Jar> {
        // due to https://youtrack.jetbrains.com/issue/KT-46165
        duplicatesStrategy = DuplicatesStrategy.WARN
    }

    /* __________________________________________________ */

    afterEvaluate {

        publishing {

            publications {

                register("mavenJava", MavenPublication::class) {
                    from(components["java"])
                }

                repositories {
                    gitlab()
                }

            }

        }

    }

}
