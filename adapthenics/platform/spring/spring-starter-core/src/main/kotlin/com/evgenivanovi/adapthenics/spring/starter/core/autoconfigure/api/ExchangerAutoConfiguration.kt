package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.api

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        ExchangerConfiguration::class
    ]
)
open class ExchangerAutoConfiguration