package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.cloud

import com.evgenivanovi.kt.lang.Booleans
import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.autoconfigure.RefreshAutoConfiguration.RefreshProperties
import org.springframework.cloud.context.refresh.ConfigDataContextRefresher
import org.springframework.cloud.context.scope.refresh.RefreshScope
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

/**
 * Autoconfiguration for the refresh scope and associated features to do with changes in
 * the Environment (e.g. rebinding logger levels).
 */
@Configuration(
    proxyBeanMethods = Booleans.FALSE
)
@ConditionalOnClass(
    value = [
        RefreshScope::class
    ]
)
@ConditionalOnProperty(
    name = [
        org.springframework.cloud.autoconfigure.RefreshAutoConfiguration.REFRESH_SCOPE_ENABLED
    ],
    matchIfMissing = Booleans.TRUE
)
@AutoConfigureBefore(
    org.springframework.cloud.autoconfigure.RefreshAutoConfiguration::class,
)
@EnableConfigurationProperties(
    value = [
        RefreshProperties::class
    ]
)
open class RefreshAutoConfiguration {

    @Bean
    @Primary
    @ConditionalOnMissingBean
    open fun configDataContextRefresher(
        context: ConfigurableApplicationContext,
        scope: RefreshScope,
        properties: RefreshProperties,
    ): ConfigDataContextRefresher {
        return ConfigDataContextRefresher(context, scope, properties)
    }

}