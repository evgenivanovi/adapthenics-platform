package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.actuator

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource(
    name = PlatformActuatorModuleProperties.PROPERTY_SOURCE_NAME,
    value = [PlatformActuatorModuleProperties.PROPERTY_SOURCE_VALUE],
)
open class PlatformActuatorModuleProperties {

    companion object {
        const val PROPERTY_SOURCE_NAME = "adapthenics/platform/actuator"
        const val PROPERTY_SOURCE_VALUE = "classpath:/adapthenics/platform/actuator/module.properties"
    }

}