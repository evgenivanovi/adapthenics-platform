package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.api

import com.evgenivanovi.adapthenics.api.contract.exchanger.*
import com.evgenivanovi.adapthenics.base.app.ApplicationMetadata
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ExchangerConfiguration {

    @Bean
    open fun exchanger(
        responseInterceptor: ResponseInterceptor,
        exceptionInterceptor: ExceptionInterceptor,
    ): Exchanger {
        return Exchanger(
            responseInterceptor,
            exceptionInterceptor
        )
    }

    @Bean
    open fun responseAPIService(
        applicationMetadata: ApplicationMetadata,
    ): ResponseAPIService {
        return ResponseAPIService(applicationMetadata)
    }

    @Bean
    open fun responseInterceptor(
        responseAPIService: ResponseAPIService,
    ): ResponseInterceptor {
        return ResponseInterceptor(responseAPIService)
    }

    @Bean
    open fun exceptionInterceptor(
        responseAPIService: ResponseAPIService,
        adviceHandlers: List<AdviceHandler>,
    ): ExceptionInterceptor {
        return ExceptionInterceptor(
            responseAPIService,
            adviceHandlers
        )
    }

}