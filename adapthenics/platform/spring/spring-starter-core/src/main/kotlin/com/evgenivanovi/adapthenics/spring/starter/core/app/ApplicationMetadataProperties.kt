package com.evgenivanovi.adapthenics.spring.starter.core.app

class ApplicationMetadataProperties {

    lateinit var id: String

    lateinit var name: String

    lateinit var version: String

}