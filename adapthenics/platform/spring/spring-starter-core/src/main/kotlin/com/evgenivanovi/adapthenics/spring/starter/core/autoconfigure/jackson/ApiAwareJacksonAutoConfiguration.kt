package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.jackson

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration(
    before = [
        JacksonAutoConfiguration::class
    ]
)
@Import(
    value = [
        ApiAwareJacksonConfiguration::class
    ]
)
open class ApiAwareJacksonAutoConfiguration