package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.app

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        ApplicationMetadataConfiguration::class
    ]
)
open class ApplicationMetadataAutoConfiguration