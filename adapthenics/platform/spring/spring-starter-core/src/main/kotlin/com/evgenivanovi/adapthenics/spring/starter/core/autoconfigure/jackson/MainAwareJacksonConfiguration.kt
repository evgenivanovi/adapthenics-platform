@file:Suppress(
    "unused",
    "ClassName",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.jackson

import com.evgenivanovi.adapthenics.api.contract.jackson.ApiContractModule
import com.evgenivanovi.adapthenics.api.contract.search.jackson.ApiContractSearchModule
import com.evgenivanovi.adapthenics.support.jackson.ObjectMapperCustomizer
import com.evgenivanovi.adapthenics.support.jackson.ser.KotlinxDateTimeModule
import com.evgenivanovi.kt.jackson.AdapthenicsModule
import com.evgenivanovi.kt.jackson.KeeperSerializationConfiguration
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.guava.GuavaModule
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@Configuration
open class MainAwareJacksonConfiguration {

    companion object {

        const val BEAN_NAME = "objectMapper"

        private val CONFIG = KeeperSerializationConfiguration.of(
            "set",
            "value",
            false,
            true,
            false,
            false,
            true,
            false,
            true
        )

    }

    @Bean
    @Qualifier(BEAN_NAME)
    open fun objectMapper(
        customizers: List<ObjectMapperCustomizer<ObjectMapper>>,
        context: ApplicationContext,
    ): ObjectMapper {

        val mapper: ObjectMapper = Jackson2ObjectMapperBuilder()
            .createXmlMapper(false)
            .applicationContext(context)
            .build()

        customizers.forEach { it.customize(mapper) }

        return mapper

    }

    @Bean
    open fun mainObjectMapperCustomizer(): ObjectMapperCustomizer<ObjectMapper> {

        return ObjectMapperCustomizer { mapper ->
            mapper.registerModules(
                Jdk8Module(),
                JavaTimeModule(),
                GuavaModule(),
                AdapthenicsModule.create(CONFIG),

                KotlinModule.Builder().build(),
                KotlinxDateTimeModule(),

                ApiContractModule(),
                ApiContractSearchModule(),
            )

            mapper.enable(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES
            )

            mapper.disable(
                MapperFeature.DEFAULT_VIEW_INCLUSION,
            )

            mapper.disable(
                SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
            )
        }
    }

    @Bean
    open fun mainObjectMapperBuilderCustomizer(): Jackson2ObjectMapperBuilderCustomizer {
        return Jackson2ObjectMapperBuilderCustomizer { mapper ->
            mapper.modulesToInstall(
                Jdk8Module(),
                JavaTimeModule(),
                GuavaModule(),
                AdapthenicsModule.create(CONFIG),

                KotlinModule.Builder().build(),
                KotlinxDateTimeModule(),

                ApiContractModule(),
                ApiContractSearchModule(),
            )

            mapper.featuresToEnable(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES
            )

            mapper.featuresToDisable(
                MapperFeature.DEFAULT_VIEW_INCLUSION,
                SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
            )
        }
    }

}