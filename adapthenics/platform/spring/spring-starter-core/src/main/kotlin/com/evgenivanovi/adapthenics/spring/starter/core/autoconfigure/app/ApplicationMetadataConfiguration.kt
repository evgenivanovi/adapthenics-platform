package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.app

import com.evgenivanovi.adapthenics.base.app.ApplicationMetadata
import com.evgenivanovi.adapthenics.spring.starter.core.app.ApplicationMetadataProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ApplicationMetadataConfiguration {

    @Bean
    @ConfigurationProperties("adapthenics.app.metadata")
    open fun applicationMetadataProperties(): ApplicationMetadataProperties {
        return ApplicationMetadataProperties()
    }

    @Bean
    open fun applicationMetadata(
        applicationMetadataProperties: ApplicationMetadataProperties,
    ): ApplicationMetadata {
        return ApplicationMetadata(
            id = applicationMetadataProperties.id,
            name = applicationMetadataProperties.name,
            version = applicationMetadataProperties.version
        )
    }

}