package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.api

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        ExceptionHandlingConfiguration::class
    ]
)
open class ExceptionHandlingAutoConfiguration