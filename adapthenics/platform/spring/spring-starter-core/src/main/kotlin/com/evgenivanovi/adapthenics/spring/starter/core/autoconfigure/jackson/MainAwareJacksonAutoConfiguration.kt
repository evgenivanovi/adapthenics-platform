@file:Suppress(
    "unused",
    "ClassName",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.jackson

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration(
    before = [
        JacksonAutoConfiguration::class
    ]
)
@Import(
    value = [
        MainAwareJacksonConfiguration::class
    ]
)
open class MainAwareJacksonAutoConfiguration