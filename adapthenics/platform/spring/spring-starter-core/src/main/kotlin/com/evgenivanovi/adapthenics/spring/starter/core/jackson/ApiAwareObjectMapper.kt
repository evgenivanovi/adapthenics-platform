package com.evgenivanovi.adapthenics.spring.starter.core.jackson

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper

class ApiAwareObjectMapper : ObjectMapper {

    constructor()
        : super()

    constructor(factory: JsonFactory)
        : super(factory)

    constructor(mapper: ObjectMapper)
        : super(mapper)

}