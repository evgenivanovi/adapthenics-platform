package com.evgenivanovi.adapthenics.spring.starter.core.autoconfigure.api

import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.handlers.*
import com.evgenivanovi.adapthenics.api.contract.search.handler.InvalidSearchErrorexAdviceHandler
import com.evgenivanovi.adapthenics.api.contract.search.handler.SearchErrorexAdviceHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(
    value = [
        ExceptionHandlingConfiguration.CoreExceptionHandlingConfiguration::class,
        ExceptionHandlingConfiguration.PersistenceExceptionHandlingConfiguration::class,
        ExceptionHandlingConfiguration.SearchExceptionHandlingConfiguration::class,
        ExceptionHandlingConfiguration.ValidationExceptionHandlingConfiguration::class
    ]
)
open class ExceptionHandlingConfiguration {

    @Configuration
    open class CoreExceptionHandlingConfiguration {

        @Bean
        open fun invalidErrorexAdviceHandler(
            responseAPIService: ResponseAPIService,
        ): InvalidErrorexAdviceHandler {
            return InvalidErrorexAdviceHandler(responseAPIService)
        }

        @Bean
        open fun invalidEntityErrorexAdviceHandler(
            responseAPIService: ResponseAPIService,
        ): InvalidEntityErrorexAdviceHandler {
            return InvalidEntityErrorexAdviceHandler(responseAPIService)
        }

    }

    @Configuration
    open class PersistenceExceptionHandlingConfiguration {

        @Bean
        open fun duplicateErrorexAdviceHandler(
            responseAPIService: ResponseAPIService,
        ): DuplicateErrorexAdviceHandler {
            return DuplicateErrorexAdviceHandler(responseAPIService)
        }

        @Bean
        open fun optimisticLockErrorexAdviceHandler(
            responseAPIService: ResponseAPIService,
        ): OptimisticLockErrorexAdviceHandler {
            return OptimisticLockErrorexAdviceHandler(responseAPIService)
        }

        @Bean
        open fun pessimisticLockErrorexAdviceHandler(
            responseAPIService: ResponseAPIService,
        ): PessimisticLockErrorexAdviceHandler {
            return PessimisticLockErrorexAdviceHandler(responseAPIService)
        }

    }

    @Configuration
    open class SearchExceptionHandlingConfiguration {

        @Bean
        open fun searchExceptionAdviceHandler(
            responseAPIService: ResponseAPIService,
        ): SearchErrorexAdviceHandler {
            return SearchErrorexAdviceHandler(responseAPIService)
        }

        @Bean
        open fun invalidSearchExceptionAdviceHandler(
            responseAPIService: ResponseAPIService,
        ): InvalidSearchErrorexAdviceHandler {
            return InvalidSearchErrorexAdviceHandler(responseAPIService)
        }

    }

    @Configuration
    open class ValidationExceptionHandlingConfiguration {

    }

}