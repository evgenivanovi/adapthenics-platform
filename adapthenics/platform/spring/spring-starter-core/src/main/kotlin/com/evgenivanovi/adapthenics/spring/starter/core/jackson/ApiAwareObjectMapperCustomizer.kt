package com.evgenivanovi.adapthenics.spring.starter.core.jackson

import com.evgenivanovi.adapthenics.support.jackson.ObjectMapperCustomizer

fun interface ApiAwareObjectMapperCustomizer : ObjectMapperCustomizer<ApiAwareObjectMapper> {

    override fun customize(mapper: ApiAwareObjectMapper)

}