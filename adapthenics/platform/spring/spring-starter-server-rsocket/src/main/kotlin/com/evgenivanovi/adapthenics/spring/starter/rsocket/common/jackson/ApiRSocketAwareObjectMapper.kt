package com.evgenivanovi.adapthenics.spring.starter.rsocket.common.jackson

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper

class ApiRSocketAwareObjectMapper : ObjectMapper {

    constructor() : super()

    constructor(factory: JsonFactory) : super(factory)

    constructor(mapper: ObjectMapper) : super(mapper)

}