package com.evgenivanovi.adapthenics.spring.starter.rsocket.autoconfigure.rsocket

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        RSocketStrategiesConfiguration::class
    ]
)
open class RSocketStrategiesAutoConfiguration