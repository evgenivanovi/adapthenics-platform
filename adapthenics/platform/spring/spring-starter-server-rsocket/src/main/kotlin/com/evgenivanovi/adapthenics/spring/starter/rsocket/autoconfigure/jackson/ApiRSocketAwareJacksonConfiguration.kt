package com.evgenivanovi.adapthenics.spring.starter.rsocket.autoconfigure.jackson

import com.evgenivanovi.adapthenics.api.contract.jackson.ApiContractModule
import com.evgenivanovi.adapthenics.api.contract.search.jackson.ApiContractSearchModule
import com.evgenivanovi.adapthenics.spring.starter.rsocket.common.jackson.ApiRSocketAwareObjectMapper
import com.evgenivanovi.adapthenics.spring.starter.rsocket.common.jackson.ApiRSocketAwareObjectMapperCustomizer
import com.evgenivanovi.adapthenics.support.jackson.ser.KotlinxDateTimeModule
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.cbor.CBORFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.AutoConfigureAfter
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@Configuration
@AutoConfigureAfter(
    value = [
        JacksonAutoConfiguration::class
    ]
)
open class ApiRSocketAwareJacksonConfiguration {

    companion object {

        const val API_RSOCKET_OBJECT_MAPPER_BEAN = "apiRSocketObjectMapper"

        private val MAPPER_CUSTOMIZER = ApiRSocketAwareObjectMapperCustomizer { mapper ->
            mapper.registerModules(
                KotlinModule.Builder().build(),
                KotlinxDateTimeModule(),
                ApiContractModule(),
                ApiContractSearchModule(),
            )

            mapper.disable(
                MapperFeature.DEFAULT_VIEW_INCLUSION,
            )

            mapper.disable(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES
            )
        }

        private val MAPPER_BUILDER_CUSTOMIZER = Jackson2ObjectMapperBuilderCustomizer { builder ->
            builder.modulesToInstall(
                KotlinxDateTimeModule(),
                ApiContractModule(),
                ApiContractSearchModule(),
            )

            builder.featuresToEnable(
                //
            )

            builder.featuresToDisable(
                MapperFeature.DEFAULT_VIEW_INCLUSION,
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES
            )
        }

    }

    @Bean
    @Qualifier(API_RSOCKET_OBJECT_MAPPER_BEAN)
    open fun apiRSocketObjectMapper(
        objectMapperCustomizers: List<ApiRSocketAwareObjectMapperCustomizer>,
        applicationContext: ApplicationContext,
    ): ApiRSocketAwareObjectMapper {

        val mapper = Jackson2ObjectMapperBuilder()
            .createXmlMapper(false)
            .factory(CBORFactory())
            .applicationContext(applicationContext)
            .apply { MAPPER_BUILDER_CUSTOMIZER.customize(this) }
            .build<ObjectMapper>()
            .let(::ApiRSocketAwareObjectMapper)

        objectMapperCustomizers.forEach { it.customize(mapper) }

        return mapper

    }

    @Bean
    open fun apiRSocketAwareObjectMapperCustomizer(): ApiRSocketAwareObjectMapperCustomizer {
        return MAPPER_CUSTOMIZER
    }

    @Bean
    open fun apiRSocketAwareObjectMapperBuilderCustomizer(): Jackson2ObjectMapperBuilderCustomizer {
        return MAPPER_BUILDER_CUSTOMIZER
    }

}