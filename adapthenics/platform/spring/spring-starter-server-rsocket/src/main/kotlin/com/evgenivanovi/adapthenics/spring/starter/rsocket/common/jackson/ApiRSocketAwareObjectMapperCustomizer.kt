package com.evgenivanovi.adapthenics.spring.starter.rsocket.common.jackson

import com.evgenivanovi.adapthenics.support.jackson.ObjectMapperCustomizer

fun interface ApiRSocketAwareObjectMapperCustomizer : ObjectMapperCustomizer<ApiRSocketAwareObjectMapper> {

    override fun customize(mapper: ApiRSocketAwareObjectMapper)

}