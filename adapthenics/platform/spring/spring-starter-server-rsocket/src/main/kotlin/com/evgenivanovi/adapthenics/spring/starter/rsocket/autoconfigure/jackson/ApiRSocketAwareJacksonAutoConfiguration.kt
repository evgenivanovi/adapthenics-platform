package com.evgenivanovi.adapthenics.spring.starter.rsocket.autoconfigure.jackson

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        ApiRSocketAwareJacksonConfiguration::class
    ]
)
open class ApiRSocketAwareJacksonAutoConfiguration