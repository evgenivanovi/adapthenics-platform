package com.evgenivanovi.adapthenics.spring.starter.rsocket.autoconfigure.rsocket

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        RSocketConfiguration::class
    ]
)
open class RSocketAutoConfiguration