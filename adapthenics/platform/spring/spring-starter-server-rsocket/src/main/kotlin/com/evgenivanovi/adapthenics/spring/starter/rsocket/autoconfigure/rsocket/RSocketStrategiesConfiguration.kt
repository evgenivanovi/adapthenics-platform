package com.evgenivanovi.adapthenics.spring.starter.rsocket.autoconfigure.rsocket

import com.evgenivanovi.adapthenics.spring.starter.rsocket.autoconfigure.jackson.ApiRSocketAwareJacksonConfiguration.Companion.API_RSOCKET_OBJECT_MAPPER_BEAN
import com.evgenivanovi.adapthenics.spring.starter.rsocket.common.jackson.ApiRSocketAwareObjectMapper
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.rsocket.messaging.RSocketStrategiesCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.http.codec.cbor.Jackson2CborDecoder
import org.springframework.http.codec.cbor.Jackson2CborEncoder
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.web.util.pattern.PathPatternRouteMatcher

@Configuration
open class RSocketStrategiesConfiguration {

    @Bean
    @Qualifier(API_RSOCKET_STRATEGIES_BEAN)
    open fun apiRSocketStrategies(
        @Qualifier(API_RSOCKET_OBJECT_MAPPER_BEAN)
        apiRSocketObjectMapper: ApiRSocketAwareObjectMapper,
    ): RSocketStrategies {
        return RSocketStrategies.builder()
            .apply { routeMatcher(PathPatternRouteMatcher()) }
            .apply { jacksonCborRSocketStrategyCustomizer(apiRSocketObjectMapper).customize(this) }
            .apply { jacksonJsonRSocketStrategyCustomizer(apiRSocketObjectMapper).customize(this) }
            .build()
    }

    private fun jacksonCborRSocketStrategyCustomizer(
        objectMapper: ObjectMapper,
    ): RSocketStrategiesCustomizer {

        return RSocketStrategiesCustomizer { strategy: RSocketStrategies.Builder ->

            strategy.decoder(
                Jackson2CborDecoder(
                    objectMapper,
                    *JSON_SUPPORTED_TYPES
                )
            )

            strategy.encoder(
                Jackson2CborEncoder(
                    objectMapper,
                    *JSON_SUPPORTED_TYPES
                )
            )

        }

    }

    private fun jacksonJsonRSocketStrategyCustomizer(
        objectMapper: ObjectMapper,
    ): RSocketStrategiesCustomizer {

        return RSocketStrategiesCustomizer { strategy: RSocketStrategies.Builder ->

            strategy.decoder(
                Jackson2JsonDecoder(
                    objectMapper,
                    *CBOR_SUPPORTED_TYPES
                )
            )

            strategy.encoder(
                Jackson2JsonEncoder(
                    objectMapper,
                    *CBOR_SUPPORTED_TYPES
                )
            )

        }

    }

    companion object {

        const val API_RSOCKET_STRATEGIES_BEAN = "apiRSocketStrategies"

        private val CBOR_SUPPORTED_TYPES = arrayOf(
            MediaType.APPLICATION_CBOR
        )

        private val JSON_SUPPORTED_TYPES = arrayOf(
            MediaType.APPLICATION_JSON,
            MediaType("application", "*+json")
        )

    }

}