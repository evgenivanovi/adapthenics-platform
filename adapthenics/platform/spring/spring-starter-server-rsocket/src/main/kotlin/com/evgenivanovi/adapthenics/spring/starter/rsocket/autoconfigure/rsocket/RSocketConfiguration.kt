package com.evgenivanovi.adapthenics.spring.starter.rsocket.autoconfigure.rsocket

import com.evgenivanovi.adapthenics.spring.starter.rsocket.ControllerAdviceWrapper
import org.springframework.boot.autoconfigure.rsocket.RSocketMessageHandlerCustomizer
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler
import org.springframework.web.method.ControllerAdviceBean

@Configuration
open class RSocketConfiguration {

    @Bean
    open fun rsocketMessageHandlerCustomizer(
        context: ApplicationContext,
    ): RSocketMessageHandlerCustomizer {
        return RSocketMessageHandlerCustomizer { handler: RSocketMessageHandler ->
            ControllerAdviceBean
                .findAnnotatedBeans(context)
                .forEach { bean ->
                    ControllerAdviceWrapper(bean)
                        .also(handler::registerMessagingAdvice)
                }
        }
    }

}