package com.evgenivanovi.adapthenics.spring.starter.rsocket

import org.springframework.messaging.handler.MessagingAdviceBean
import org.springframework.web.method.ControllerAdviceBean

/*
    Issue: https://github.com/spring-projects/spring-framework/issues/26636
    Commit: https://github.com/spring-projects/spring-framework/commit/6214ff153fd92e9fd37c5d7dee83f2cc9c9e89ce
*/
class ControllerAdviceWrapper(
    private val delegate: ControllerAdviceBean,
) : MessagingAdviceBean {

    override fun getOrder(): Int {
        return delegate.order
    }

    override fun getBeanType(): Class<*>? {
        return delegate.beanType
    }

    override fun resolveBean(): Any {
        return delegate.resolveBean()
    }

    override fun isApplicableToBeanType(beanType: Class<*>): Boolean {
        return delegate.isApplicableToBeanType(beanType)
    }

}