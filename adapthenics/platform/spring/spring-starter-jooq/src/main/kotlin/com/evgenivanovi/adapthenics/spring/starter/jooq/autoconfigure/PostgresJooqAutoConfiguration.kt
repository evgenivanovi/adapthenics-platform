package com.evgenivanovi.adapthenics.spring.starter.jooq.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        PostgresJooqConfiguration::class
    ]
)
open class PostgresJooqAutoConfiguration