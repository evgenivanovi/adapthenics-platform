package com.evgenivanovi.adapthenics.spring.starter.jooq.autoconfigure

import org.jooq.ExecuteContext
import org.jooq.ExecuteListener
import org.springframework.boot.autoconfigure.jooq.ExceptionTranslatorExecuteListener

object ExceptionListener : ExecuteListener {

    private val translator: ExceptionTranslatorExecuteListener =
        ExceptionTranslatorExecuteListener.DEFAULT

    private fun readResolve(): Any = ExceptionListener

    override fun exception(ctx: ExecuteContext) {
        translator.exception(ctx)
        val ex = ctx.exception() ?: return
        ctx.exception(ExceptionTranslator.translate(ex))
    }

}