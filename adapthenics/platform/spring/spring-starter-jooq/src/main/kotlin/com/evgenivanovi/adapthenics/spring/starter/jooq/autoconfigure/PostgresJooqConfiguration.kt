package com.evgenivanovi.adapthenics.spring.starter.jooq.autoconfigure

import com.evgenivanovi.adapthenics.spring.starter.postgres.autoconfigure.PostgresConditional
import org.jooq.*
import org.jooq.conf.Settings
import org.jooq.impl.DefaultConfiguration
import org.jooq.impl.DefaultDSLContext
import org.jooq.impl.DefaultExecuteListenerProvider
import org.jooq.impl.ThreadLocalTransactionProvider
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.autoconfigure.jooq.DefaultConfigurationCustomizer
import org.springframework.context.annotation.Bean
import javax.sql.DataSource

@org.springframework.context.annotation.Configuration
@org.springframework.context.annotation.Conditional(
    value = [
        PostgresConditional::class
    ]
)
open class PostgresJooqConfiguration {

    companion object {

        val DIALECT = SQLDialect.POSTGRES

    }

    @Bean
    open fun dslContext(
        configuration: Configuration,
    ): DSLContext {
        return DefaultDSLContext(configuration)
    }

    @Bean
    open fun transactionProvider(
        connectionProvider: ConnectionProvider,
    ): TransactionProvider {
        return ThreadLocalTransactionProvider(connectionProvider, true)
    }

    @Bean
    open fun configurationCustomizer(): DefaultConfigurationCustomizer {
        return DefaultConfigurationCustomizer { configuration ->
            // overrides DefaultExecuteListenerProvider(new JooqExceptionTranslator())
            configuration.set(DefaultExecuteListenerProvider(ExceptionListener))
        }
    }

    @Bean
    open fun configuration(
        datasource: DataSource,
        connectionProvider: ConnectionProvider,
        transactionProvider: TransactionProvider,
        executeListenerProviders: ObjectProvider<ExecuteListenerProvider>,
        configurationCustomizers: ObjectProvider<DefaultConfigurationCustomizer>,
    ): Configuration {

        val settings = Settings()
            // Whether UpdatableRecord instances should modify the record version prior to storing the record.
            // This feature is independent of, but related to optimistic locking.
            .withUpdateRecordVersion(false)
            // Whether UpdatableRecord instances should modify the record timestamp prior to storing the record.
            // This feature is independent of, but related to optimistic locking.
            .withUpdateRecordTimestamp(false)
            // This allows for turning off the feature entirely.
            .withExecuteWithOptimisticLocking(false)
            // This allows for turning off the feature for updatable records who are not explicitly versioned.
            .withExecuteWithOptimisticLockingExcludeUnversioned(false)

        val configuration = DefaultConfiguration()
            .apply { setSQLDialect(DIALECT) }
            .apply { setConnectionProvider(connectionProvider) }
            .apply { setTransactionProvider(transactionProvider) }
            .apply { setSettings(settings) }
            .apply { set(*executeListenerProviders.toList().toTypedArray()) }

        configurationCustomizers.forEach { customizer ->
            customizer.customize(configuration)
        }

        return configuration

    }

}