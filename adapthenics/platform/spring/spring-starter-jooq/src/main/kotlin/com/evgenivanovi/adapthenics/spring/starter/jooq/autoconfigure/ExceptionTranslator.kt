package com.evgenivanovi.adapthenics.spring.starter.jooq.autoconfigure

import com.evgenivanovi.adapthenics.base.errx.*
import org.springframework.dao.DuplicateKeyException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.dao.PessimisticLockingFailureException

object ExceptionTranslator {

    fun translate(ex: RuntimeException): PersistenceErrorex {
        return when (ex) {
            is DuplicateKeyException -> {
                DuplicateErrorex.of(ex)
            }

            is EmptyResultDataAccessException -> {
                AbsenceErrorex.of(ex)
            }

            is OptimisticLockingFailureException -> {
                OptimisticLockErrorex.of(ex)
            }

            is PessimisticLockingFailureException -> {
                PessimisticLockErrorex.of(ex)
            }

            else -> UncategorizedErrorex.of(ex)
        }
    }

}