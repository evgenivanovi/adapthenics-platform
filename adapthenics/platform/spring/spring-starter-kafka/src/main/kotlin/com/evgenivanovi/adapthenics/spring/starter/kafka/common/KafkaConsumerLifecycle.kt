package com.evgenivanovi.adapthenics.spring.starter.kafka.common

import com.evgenivanovi.adapthenics.base.Lifecycle

interface KafkaConsumerLifecycle : Lifecycle