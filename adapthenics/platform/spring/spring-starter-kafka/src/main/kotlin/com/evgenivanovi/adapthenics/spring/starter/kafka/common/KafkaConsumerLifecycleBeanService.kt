package com.evgenivanovi.adapthenics.spring.starter.kafka.common

import com.evgenivanovi.adapthenics.base.Lifecycle
import org.springframework.boot.context.event.ApplicationReadyEvent

class KafkaConsumerLifecycleBeanService(
    private val lifecycle: Lifecycle,
) : KafkaLifecycleBean {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        start()
    }

    override fun destroy() {
        stop()
    }

    override fun close() {
        stop()
    }

    private fun start() {
        lifecycle.start()
    }

    private fun stop() {
        lifecycle.stop()
    }

}