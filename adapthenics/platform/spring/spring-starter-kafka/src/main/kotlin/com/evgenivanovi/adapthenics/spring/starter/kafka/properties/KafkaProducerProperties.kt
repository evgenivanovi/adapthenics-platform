@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.spring.starter.kafka.properties

import com.evgenivanovi.kt.coll.`in`
import com.evgenivanovi.kt.coll.put
import org.apache.kafka.clients.CommonClientConfigs
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.boot.context.properties.PropertyMapper

class KafkaProducerProperties(

    /**
     * ID to pass to the server when making requests. Used for server-side logging.
     */
    var clientId: String? = null,

    /**
     * Comma-delimited list of host:port pairs to use for establishing the initial connections to the Kafka cluster.
     * Overrides the [KafkaCommonProperties] property for producers.
     */
    var servers: List<String> = emptyList(),

    /**
     * Number of acknowledgments the producer requires the leader to have received before considering a request complete.
     */
    var acks: String? = null,

    /**
     * Serializer class for keys.
     */
    var keySerializer: Class<*> = StringSerializer::class.java,

    /**
     * Serializer class for values.
     */
    var valueSerializer: Class<*> = StringSerializer::class.java,

    /**
     * Additional properties for producers used to configure the client.
     * Overrides the [KafkaCommonProperties] property for producers.
     */
    var properties: MutableMap<String, Any?> = mutableMapOf(),

    ) {

    fun buildProducerProperties(
        common: KafkaCommonProperties,
    ): Map<String, Any?> {
        val cluster = common
            .buildClusterProperties()
            .toMutableMap()
        return doBuildProducerProperties(cluster)
    }

    fun buildProducerProperties(): Map<String, Any?> {
        return doBuildProducerProperties(mutableMapOf())
    }

    private fun doBuildProducerProperties(
        props: MutableMap<String, Any?>,
    ): Map<String, Any?> {
        val mapper = PropertyMapper
            .get()
            .alwaysApplyingWhenNonNull()

        mapper
            .from(this::clientId)
            .to(props.`in`(CommonClientConfigs.CLIENT_ID_CONFIG))

        mapper
            .from(this::servers)
            .to(props.`in`(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG))

        mapper
            .from(this::acks)
            .to(props.`in`(ProducerConfig.ACKS_CONFIG))

        mapper
            .from(this::keySerializer)
            .to(props.`in`(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG))

        mapper
            .from(this::valueSerializer)
            .to(props.`in`(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG))

        return props.put(properties)
    }

}