package com.evgenivanovi.adapthenics.spring.starter.kafka.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        KafkaClientConfiguration::class
    ]
)
@Conditional(
    value = [
        KafkaConditional::class
    ]
)
open class KafkaClientAutoConfiguration