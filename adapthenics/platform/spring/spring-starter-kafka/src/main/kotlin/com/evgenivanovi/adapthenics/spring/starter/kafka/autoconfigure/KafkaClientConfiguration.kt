package com.evgenivanovi.adapthenics.spring.starter.kafka.autoconfigure

import com.evgenivanovi.adapthenics.spring.starter.kafka.properties.ConsumingProperties
import com.evgenivanovi.adapthenics.spring.starter.kafka.properties.KafkaCommonProperties
import com.evgenivanovi.adapthenics.spring.starter.kafka.properties.KafkaConsumerProperties
import com.evgenivanovi.adapthenics.spring.starter.kafka.properties.KafkaProducerProperties
import com.evgenivanovi.kt.coll.asList
import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Configuration
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions

@Configuration
@Conditional(
    value = [
        KafkaConditional::class
    ]
)
open class KafkaClientConfiguration {

    @Bean
    @ConfigurationProperties("adapthenics.app.kafka.common")
    open fun kafkaCommonProperties(): KafkaCommonProperties {
        return KafkaCommonProperties()
    }

    @Bean
    @ConfigurationProperties("adapthenics.app.kafka.producer")
    open fun kafkaProducerProperties(): KafkaProducerProperties {
        return KafkaProducerProperties()
    }

    @Bean
    @ConfigurationProperties("adapthenics.app.kafka.consumer")
    open fun kafkaConsumerProperties(): KafkaConsumerProperties {
        return KafkaConsumerProperties()
    }

    @Bean
    @ConditionalOnBean(
        value = [
            KafkaCommonProperties::class,
            KafkaProducerProperties::class
        ]
    )
    open fun kafkaSender(
        kafkaCommonProperties: KafkaCommonProperties,
        kafkaProducerProperties: KafkaProducerProperties,
    ): KafkaSender<String, String> {

        val properties = kafkaProducerProperties
            .buildProducerProperties(kafkaCommonProperties)

        val senderOptions = SenderOptions
            .create<String, String>(properties)
            .maxInFlight(1024)

        return KafkaSender.create(senderOptions)

    }

    @Bean
    @ConditionalOnBean(
        value = [
            KafkaCommonProperties::class,
            KafkaProducerProperties::class,
            ConsumingProperties::class
        ]
    )
    open fun kafkaReceiver(
        kafkaCommonProperties: KafkaCommonProperties,
        kafkaConsumerProperties: KafkaConsumerProperties,
        consumingProperties: ConsumingProperties,
    ): KafkaReceiver<String, String> {

        val log = KotlinLogging.logger {}

        val properties = kafkaConsumerProperties
            .buildConsumerProperties(kafkaCommonProperties)

        val receiverOptions = ReceiverOptions
            .create<String, String>(properties)
            .addAssignListener {
                log.info { "Group ${kafkaConsumerProperties.groupId} partitions assigned $it" }
            }
            .addRevokeListener {
                log.info { "Group ${kafkaConsumerProperties.groupId} partitions assigned $it" }
            }
            .subscription(consumingProperties.topic.asList())

        return KafkaReceiver.create(receiverOptions)

    }

}