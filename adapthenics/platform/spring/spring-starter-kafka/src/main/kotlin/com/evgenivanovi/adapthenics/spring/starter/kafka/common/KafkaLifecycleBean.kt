package com.evgenivanovi.adapthenics.spring.starter.kafka.common

import org.springframework.beans.factory.DisposableBean
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener

interface KafkaLifecycleBean :
    ApplicationListener<ApplicationReadyEvent>, DisposableBean, AutoCloseable