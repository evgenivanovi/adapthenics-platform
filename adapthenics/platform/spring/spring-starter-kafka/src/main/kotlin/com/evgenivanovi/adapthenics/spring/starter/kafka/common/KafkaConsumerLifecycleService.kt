package com.evgenivanovi.adapthenics.spring.starter.kafka.common

import com.evgenivanovi.adapthenics.kafka.infrastructure.KafkaConsumer

class KafkaConsumerLifecycleService(
    private val consumer: KafkaConsumer,
) : KafkaConsumerLifecycle {

    override fun start() {
        consumer.start()
    }

    override fun stop() {
        consumer.stop()
    }

}