@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.spring.starter.kafka.properties

import com.evgenivanovi.kt.coll.`in`
import com.evgenivanovi.kt.coll.put
import org.apache.kafka.clients.CommonClientConfigs
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.boot.context.properties.PropertyMapper

class KafkaConsumerProperties(

    /**
     * ID to pass to the server when making requests. Used for server-side logging.
     */
    var clientId: String? = null,

    /**
     * Unique string that identifies the consumer group to which this consumer belongs.
     */
    var groupId: String? = null,

    /**
     * Comma-delimited list of host:port pairs to use for establishing the initial connections to the Kafka cluster.
     * Overrides the [KafkaCommonProperties] property for consumers.
     */
    var servers: List<String> = emptyList(),

    /**
     * Deserializer class for keys.
     */
    var keyDeserializer: Class<*> = StringDeserializer::class.java,

    /**
     * Deserializer class for values.
     */
    var valueDeserializer: Class<*> = StringDeserializer::class.java,

    /**
     * Additional properties for producers used to configure the client.
     * Overrides the [KafkaCommonProperties] property for producers.
     */
    var properties: MutableMap<String, Any?> = mutableMapOf(),

    ) {

    fun buildConsumerProperties(
        common: KafkaCommonProperties,
    ): Map<String, Any?> {
        val cluster = common
            .buildClusterProperties()
            .toMutableMap()
        return doBuildConsumerProperties(cluster)
    }

    fun buildConsumerProperties(): Map<String, Any?> {
        return doBuildConsumerProperties(mutableMapOf())
    }

    private fun doBuildConsumerProperties(
        props: MutableMap<String, Any?>,
    ): Map<String, Any?> {
        val mapper = PropertyMapper
            .get()
            .alwaysApplyingWhenNonNull()

        mapper
            .from(this::clientId)
            .to(props.`in`(CommonClientConfigs.CLIENT_ID_CONFIG))

        mapper
            .from(this::servers)
            .to(props.`in`(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG))

        mapper
            .from(this::groupId)
            .to(props.`in`(CommonClientConfigs.GROUP_ID_CONFIG))

        mapper
            .from(this::keyDeserializer)
            .to(props.`in`(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG))

        mapper
            .from(this::valueDeserializer)
            .to(props.`in`(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG))

        return props.put(properties)
    }

}