package com.evgenivanovi.adapthenics.spring.starter.kafka.autoconfigure

import com.evgenivanovi.adapthenics.base.app.ServiceModeConstants
import com.evgenivanovi.kt.lang.Booleans
import org.springframework.boot.autoconfigure.condition.AllNestedConditions
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.ConfigurationCondition.ConfigurationPhase.PARSE_CONFIGURATION

object KafkaConditional : AllNestedConditions(PARSE_CONFIGURATION) {

    @ConditionalOnProperty(
        name = [
            "adapthenics.app.kafka.enabled"
        ],
        havingValue = ServiceModeConstants.ON,
        matchIfMissing = Booleans.FALSE
    )
    class EnabledConditional

}