@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.spring.starter.kafka.properties

import com.evgenivanovi.kt.coll.`in`
import com.evgenivanovi.kt.coll.put
import org.apache.kafka.clients.CommonClientConfigs
import org.springframework.boot.context.properties.PropertyMapper

class KafkaCommonProperties(

    /**
     * Comma-delimited list of host:port pairs to use for establishing the initial connections to the Kafka cluster.
     * Applies to all components unless overridden.
     */
    var servers: List<String> = emptyList(),

    /**
     * Additional properties, common to producers and consumers, used to configure the client.
     * Check [KafkaConsumerProperties.buildConsumerProperties]
     * Check [KafkaProducerProperties.buildProducerProperties]
     */
    var properties: MutableMap<String, Any> = mutableMapOf(),

    ) {

    fun buildClusterProperties(): Map<String, Any?> {
        val props = HashMap<String, Any?>()

        val mapper = PropertyMapper
            .get()
            .alwaysApplyingWhenNonNull()

        mapper
            .from(this::servers)
            .to(props.`in`(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG))

        return props.put(properties)
    }

}