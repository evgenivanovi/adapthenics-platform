package com.evgenivanovi.adapthenics.spring.webflux.http

import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCodes
import org.springframework.http.ResponseEntity
import org.springframework.web.reactive.function.server.ServerResponse

fun statusForServerResponse(response: ResponseMessage<*>): ServerResponse.BodyBuilder {
    if (response is ProblemResponseMessage) {
        return buildStatusForServerResponse(response.getCode())
    }
    return buildStatusForServerResponse(ResponseStatusCodes.OK)
}

private fun buildStatusForServerResponse(code: Int): ServerResponse.BodyBuilder {
    return buildStatusForServerResponse(
        ResponseStatusCodes.of(code)
    )
}

private fun buildStatusForServerResponse(code: ResponseStatusCode): ServerResponse.BodyBuilder {
    return ServerResponse.status(
        HttpStatusAssociation.resolve(code)
    )
}

fun statusForResponseEntity(response: ResponseMessage<*>): ResponseEntity.BodyBuilder {
    if (response is ProblemResponseMessage) {
        return buildStatusForResponseEntity(response.getCode())
    }
    return buildStatusForResponseEntity(ResponseStatusCodes.OK)
}

private fun buildStatusForResponseEntity(code: Int): ResponseEntity.BodyBuilder {
    return buildStatusForResponseEntity(
        ResponseStatusCodes.of(code)
    )
}

private fun buildStatusForResponseEntity(code: ResponseStatusCode): ResponseEntity.BodyBuilder {
    return ResponseEntity.status(
        HttpStatusAssociation.resolve(code)
    )
}