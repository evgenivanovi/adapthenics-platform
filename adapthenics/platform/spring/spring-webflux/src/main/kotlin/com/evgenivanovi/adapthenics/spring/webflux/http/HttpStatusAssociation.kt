package com.evgenivanovi.adapthenics.spring.webflux.http

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCodes
import com.evgenivanovi.kt.coll.putAll
import com.google.common.collect.ImmutableBiMap
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode

object HttpStatusAssociation {

    private val PAIRS: List<Pair<ResponseStatusCode, HttpStatus>> = listOf(
        ResponseStatusCodes.OK to HttpStatus.OK,
        ResponseStatusCodes.TIMEOUT to HttpStatus.REQUEST_TIMEOUT,
        ResponseStatusCodes.EXHAUSTED to HttpStatus.TOO_MANY_REQUESTS,
        ResponseStatusCodes.INVALID to HttpStatus.BAD_REQUEST,
        ResponseStatusCodes.UNAUTHORIZED to HttpStatus.UNAUTHORIZED,
        ResponseStatusCodes.FORBIDDEN to HttpStatus.FORBIDDEN,
        ResponseStatusCodes.ABSENT to HttpStatus.NOT_FOUND,
        ResponseStatusCodes.CONFLICT to HttpStatus.CONFLICT,
        ResponseStatusCodes.INTERNAL to HttpStatus.INTERNAL_SERVER_ERROR,
        ResponseStatusCodes.UNAVAILABLE to HttpStatus.SERVICE_UNAVAILABLE
    )

    private val MAPPINGS: ImmutableBiMap<ResponseStatusCode, HttpStatusCode> = ImmutableBiMap
        .builder<ResponseStatusCode, HttpStatusCode>()
        .putAll(PAIRS)
        .build()

    private val INVERSE_MAPPINGS: ImmutableBiMap<HttpStatusCode, ResponseStatusCode> =
        MAPPINGS.inverse()

    fun resolve(status: ResponseStatusCode): HttpStatusCode {
        return MAPPINGS.getValue(status)
    }

    fun resolve(status: HttpStatusCode): ResponseStatusCode? {
        return INVERSE_MAPPINGS[status]
    }

}