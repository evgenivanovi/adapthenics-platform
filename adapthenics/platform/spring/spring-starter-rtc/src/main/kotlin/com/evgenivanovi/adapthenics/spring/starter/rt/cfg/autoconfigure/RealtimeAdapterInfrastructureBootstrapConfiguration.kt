package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import com.evgenivanovi.adapthenics.spring.rtc.adapter.InMemoryPropertyRepository
import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertyRepository
import com.evgenivanovi.adapthenics.spring.rtc.infra.RealtimePropertySourceLocator
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class RealtimeAdapterInfrastructureBootstrapConfiguration {

    @Bean
    @ConditionalOnMissingBean
    open fun propertyRepository(): PropertyRepository {
        return InMemoryPropertyRepository()
    }

    @Bean
    @ConditionalOnMissingBean
    open fun propertySourceLocator(
        propertyRepository: PropertyRepository,
    ): RealtimePropertySourceLocator {
        return RealtimePropertySourceLocator(
            name = "realtime",
            repository = propertyRepository
        )
    }

}