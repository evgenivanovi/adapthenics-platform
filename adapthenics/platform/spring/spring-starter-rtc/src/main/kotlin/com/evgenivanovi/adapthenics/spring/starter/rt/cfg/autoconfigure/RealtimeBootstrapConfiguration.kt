package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(
    value = [
        RealtimeDomainConfiguration::class,
        RealtimeAdapterBootstrapConfiguration::class,
        RealtimeAdapterInfrastructureBootstrapConfiguration::class,
    ]
)
open class RealtimeBootstrapConfiguration
