package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import com.evgenivanovi.adapthenics.etcd.client.reactive.PlatformReactiveClient
import com.evgenivanovi.adapthenics.rtc.etcd.adapter.model.ETCDListener
import com.evgenivanovi.adapthenics.rtc.etcd.adapter.service.PropertyNameResolver
import com.evgenivanovi.adapthenics.rtc.etcd.infra.ETCDWatcher
import com.evgenivanovi.adapthenics.rtc.etcd.infra.ETCDWatcherService
import com.evgenivanovi.adapthenics.rtc.schema.conv.Converter
import com.evgenivanovi.adapthenics.rtc.schema.conv.ConverterFactory
import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel
import com.evgenivanovi.adapthenics.spring.starter.etcd.autoconfigure.ETCDConditional
import com.evgenivanovi.adapthenics.spring.starter.rt.cfg.common.ETCDWatcherLifecycleBean
import com.evgenivanovi.adapthenics.spring.starter.rt.cfg.common.ETCDWatcherLifecycleBeanService
import io.etcd.jetcd.reactive.ReactiveWatch
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Configuration
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers

@Configuration
@Conditional(
    value = [
        ETCDConditional::class
    ]
)
open class RealtimePortInfrastructureConfiguration {

    @Bean(
        destroyMethod = "dispose"
    )
    @Qualifier(
        value = "etcdScheduler"
    )
    @ConditionalOnMissingBean
    open fun etcdScheduler(): Scheduler {
        return Schedulers.newParallel("etcd-scheduler")
    }

    @Bean
    @ConditionalOnMissingBean
    open fun etcdWatcher(
        propertyNameResolver: PropertyNameResolver,
        etcdListener: ETCDListener,
        @Qualifier("etcdScheduler")
        etcdScheduler: Scheduler,
        platformReactiveClient: PlatformReactiveClient,
    ): ETCDWatcher {

        val key: String = propertyNameResolver
            .prefix

        val watch: ReactiveWatch = platformReactiveClient
            .delegate
            .watchClient

        val converter: Converter<ByteArray, ParameterModel<Any>> = ConverterFactory
            .createJsonConverter()

        return ETCDWatcherService(
            key = key,
            watch = watch,
            scheduler = etcdScheduler,
            converter = converter,
            listener = etcdListener
        )

    }

    @Bean
    @ConditionalOnMissingBean
    open fun etcdWatcherLifecycleBean(
        etcdWatcher: ETCDWatcher,
    ): ETCDWatcherLifecycleBean {
        return ETCDWatcherLifecycleBeanService(etcdWatcher)
    }

}