package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        RealtimeDomainConfiguration::class,
        RealtimeAdapterConfiguration::class,
        RealtimeAdapterInfrastructureConfiguration::class,
        RealtimePortConfiguration::class,
        RealtimePortInfrastructureConfiguration::class,
    ]
)
open class RealtimeAutoConfiguration
