package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import com.evgenivanovi.adapthenics.spring.rtc.infra.PropertySourceSignal
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class RealtimeAdapterInfrastructureConfiguration {

    @Bean
    @ConditionalOnMissingBean
    open fun propertySignal(
        applicationEventPublisher: ApplicationEventPublisher,
    ): PropertySourceSignal {
        return PropertySourceSignal()
            .apply { setApplicationEventPublisher(applicationEventPublisher) }
    }

}