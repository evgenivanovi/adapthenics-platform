package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import com.evgenivanovi.adapthenics.rtc.domain.service.EventCourier
import kotlinx.coroutines.ExecutorCoroutineDispatcher
import kotlinx.coroutines.asCoroutineDispatcher
import org.apache.commons.lang3.concurrent.BasicThreadFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.Executors

@Configuration
open class RealtimeDomainConfiguration {

    @Bean(
        destroyMethod = "close"
    )
    @Qualifier(
        value = "rtcDispatcher"
    )
    @ConditionalOnMissingBean
    open fun rtcDispatcher(): ExecutorCoroutineDispatcher {

        val threads = 8

        val factory = BasicThreadFactory
            .Builder()
            .daemon(true)
            .build()

        return Executors
            .newFixedThreadPool(threads, factory)
            .asCoroutineDispatcher()

    }

    @Bean
    @ConditionalOnMissingBean
    open fun eventCourier(
        rtcDispatcher: ExecutorCoroutineDispatcher,
    ): EventCourier {
        return EventCourier(rtcDispatcher)
    }

}