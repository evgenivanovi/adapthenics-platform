package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import com.evgenivanovi.adapthenics.base.app.ApplicationMetadata
import com.evgenivanovi.adapthenics.rtc.domain.service.EventCourier
import com.evgenivanovi.adapthenics.rtc.etcd.adapter.model.ETCDListener
import com.evgenivanovi.adapthenics.rtc.etcd.adapter.service.ETCDListenerService
import com.evgenivanovi.adapthenics.rtc.etcd.adapter.service.PropertyNameResolver
import com.evgenivanovi.adapthenics.spring.starter.etcd.autoconfigure.ETCDConditional
import com.evgenivanovi.kt.string.Strings
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Configuration

@Configuration
@Conditional(
    value = [
        ETCDConditional::class
    ]
)
open class RealtimePortConfiguration {

    @Bean
    @ConditionalOnMissingBean
    open fun propertyNameResolver(
        applicationMetadata: ApplicationMetadata,
    ): PropertyNameResolver {
        return PropertyNameResolver.of(
            prefix = "config",
            delimiter = Strings.SLASH,
            applicationMetadata.id,
            applicationMetadata.version
        )
    }

    @Bean
    @ConditionalOnMissingBean
    open fun etcdListener(
        propertyNameResolver: PropertyNameResolver,
        eventCourier: EventCourier,
    ): ETCDListener {
        return ETCDListenerService(
            resolver = propertyNameResolver,
            courier = eventCourier
        )
    }

}