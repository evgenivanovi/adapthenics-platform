package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import com.evgenivanovi.adapthenics.rtc.domain.service.EventCourier
import com.evgenivanovi.adapthenics.rtc.domain.service.EventListener
import com.evgenivanovi.adapthenics.rtc.domain.service.filter.KeyFilter
import com.evgenivanovi.adapthenics.spring.rtc.adapter.InMemoryPropertyRepository
import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertyEventListenerService
import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertyRepository
import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertySignal
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class RealtimeAdapterConfiguration {

    @Bean
    @ConditionalOnMissingBean
    open fun propertyRepository(): PropertyRepository {
        return InMemoryPropertyRepository()
    }

    @Bean
    @ConditionalOnMissingBean
    open fun propertyEventListener(
        eventCourier: EventCourier,
        propertySignal: PropertySignal,
        propertyRepository: PropertyRepository,
    ): EventListener {

        val key = KeyFilter
            .configure()
            .build()

        val listener = PropertyEventListenerService(
            propertySignal, propertyRepository
        )
        
        eventCourier.register(listener, key)
        return listener

    }

}