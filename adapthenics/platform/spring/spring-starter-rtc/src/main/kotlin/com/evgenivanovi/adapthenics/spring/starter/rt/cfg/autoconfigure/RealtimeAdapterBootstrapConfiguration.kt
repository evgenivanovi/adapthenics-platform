package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import com.evgenivanovi.adapthenics.spring.rtc.adapter.InMemoryPropertyRepository
import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertyRepository
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class RealtimeAdapterBootstrapConfiguration {

    @Bean
    @ConditionalOnMissingBean
    open fun propertyRepository(): PropertyRepository {
        return InMemoryPropertyRepository()
    }

}