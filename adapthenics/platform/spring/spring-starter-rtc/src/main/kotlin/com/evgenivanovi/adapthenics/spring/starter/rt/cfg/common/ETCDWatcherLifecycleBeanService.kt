package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.common

import com.evgenivanovi.adapthenics.rtc.etcd.infra.ETCDWatcher
import org.springframework.boot.context.event.ApplicationReadyEvent

class ETCDWatcherLifecycleBeanService(
    private val watcher: ETCDWatcher,
) : ETCDWatcherLifecycleBean {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        start()
    }

    override fun destroy() {
        stop()
    }

    override fun close() {
        stop()
    }

    private fun start() {
        watcher.start()
    }

    private fun stop() {
        watcher.stop()
    }

}