package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.autoconfigure

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource(
    name = RealtimeConfigModuleProperties.PROPERTY_SOURCE_NAME,
    value = [RealtimeConfigModuleProperties.PROPERTY_SOURCE_VALUE]
)
open class RealtimeConfigModuleProperties {

    companion object {

        const val PROPERTY_SOURCE_NAME = "adapthenics/platform/realtimeconfig"
        const val PROPERTY_SOURCE_VALUE = "classpath:/adapthenics/platform/realtimeconfig/module.properties"

    }

}