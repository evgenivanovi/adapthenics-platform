package com.evgenivanovi.adapthenics.spring.starter.rt.cfg.common

import org.springframework.beans.factory.DisposableBean
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener

interface ETCDWatcherLifecycleBean :
    ApplicationListener<ApplicationReadyEvent>, DisposableBean, AutoCloseable