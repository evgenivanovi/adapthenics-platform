package com.evgenivanovi.adapthenics.spring.starter.mongo.autoconfigure

import com.evgenivanovi.kt.lang.Booleans
import org.springframework.boot.autoconfigure.condition.AllNestedConditions
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.ConfigurationCondition.ConfigurationPhase.PARSE_CONFIGURATION

object MongoConditional : AllNestedConditions(PARSE_CONFIGURATION) {

    @ConditionalOnProperty(
        name = [
            "adapthenics.app.persistence.mode"
        ],
        havingValue = "mongo",
        matchIfMissing = Booleans.FALSE
    )
    class EnabledConditional

}