package com.evgenivanovi.adapthenics.spring.starter.mongo.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        MongoClientConfiguration::class
    ]
)
open class MongoClientAutoConfiguration