package com.evgenivanovi.adapthenics.spring.starter.mongo.autoconfigure

import com.evgenivanovi.adapthenics.mongodb.client.MongoDocumentOps
import com.evgenivanovi.adapthenics.mongodb.client.cfg.*
import com.evgenivanovi.adapthenics.mongodb.client.ops.MongoDocumentReactiveOpsTemplate
import com.mongodb.ConnectionString
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.ReactiveMongoDatabaseFactory
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import com.mongodb.reactivestreams.client.MongoClient as ReactiveMongoClient

@Configuration
@Conditional(
    value = [
        MongoConditional::class
    ]
)
open class MongoClientConfiguration {

    companion object {

        const val MONGO_PROPERTIES_PREFIX = "adapthenics.app.persistence.mongo"

    }

    @Bean
    @ConfigurationProperties(MONGO_PROPERTIES_PREFIX)
    open fun mongoProperties(): MongoProperties {
        return MongoProperties()
    }

    @Bean
    @ConditionalOnMissingBean
    open fun mongoSchemaSettings(): MongoSchemaSettings {
        return schema {
            documentPackages = mutableListOf()
            definitionPackages = mutableListOf()
        }
    }

    @Bean
    open fun mongoConnectionString(
        mongoProperties: MongoProperties,
    ): ConnectionString {
        return MongoPropertiesConnectionFactory
            .create(mongoProperties)
    }

    @Bean(destroyMethod = "close")
    open fun reactiveMongoClient(
        mongoConnectionString: ConnectionString,
    ): ReactiveMongoClient {
        return MongoClientConfigurer
            .createReactive(mongoConnectionString)
    }

    @Bean
    open fun reactiveMongoDatabaseFactory(
        reactiveMongoClient: ReactiveMongoClient,
        mongoProperties: MongoProperties,
    ): ReactiveMongoDatabaseFactory {
        return MongoDatabaseConfigurer.createReactiveDatabaseFactory(
            reactiveMongoClient,
            mongoProperties.database
        )
    }

    @Bean
    open fun reactiveMongoTemplate(
        reactiveMongoDatabaseFactory: ReactiveMongoDatabaseFactory,
        mongoSchemaSettings: MongoSchemaSettings,
    ): ReactiveMongoTemplate {
        return MongoTemplateConfigurer.createReactiveTemplate(
            reactiveMongoDatabaseFactory,
            mongoSchemaSettings.documentPackages
        )
    }

    @Bean
    open fun mongoDocumentOps(
        reactiveMongoTemplate: ReactiveMongoTemplate,
    ): MongoDocumentOps {
        return MongoDocumentReactiveOpsTemplate(
            reactiveMongoTemplate
        )
    }

}