package com.evgenivanovi.adapthenics.spring.starter.mongo.autoconfigure

import com.evgenivanovi.adapthenics.mongodb.client.cfg.MongoConnectionConfigurer
import com.evgenivanovi.adapthenics.mongodb.client.cfg.MongoConnectionFactory
import com.evgenivanovi.kt.string.Strings
import com.evgenivanovi.kt.string.Strings.AT
import com.evgenivanovi.kt.string.Strings.COLON
import com.evgenivanovi.kt.string.Strings.SLASH
import com.evgenivanovi.kt.string.build
import com.mongodb.ConnectionString
import org.springframework.boot.autoconfigure.mongo.MongoProperties

object MongoPropertiesConnectionFactory : MongoConnectionFactory<MongoProperties> {

    override fun create(params: MongoProperties): ConnectionString {

        val connection = determineConnectionFromUri(params)
            ?: determineConnectionFromProperties(params)

        return ConnectionString(connection)

    }

    private fun determineConnectionFromUri(params: MongoProperties): String? {
        return params.uri
    }

    private fun determineConnectionFromProperties(params: MongoProperties): String {
        return MongoConnectionConfigurer.PREFIX +
            determineCredentialString(params) +
            determineHostPortString(params) +
            determineDatabaseString(params)
    }

    private fun determineCredentialString(params: MongoProperties): String {

        val usernameSet = params.username
            ?.toString()
            ?.isNotBlank()
            ?: false

        val passwordSet = params.password
            ?.toString()
            ?.isNotBlank()
            ?: false

        return when (usernameSet && passwordSet) {
            true -> buildCredentialString(
                username = params.username,
                password = params.password.toString()
            )

            false -> Strings.EMPTY
        }

    }

    private fun buildCredentialString(username: String, password: String): String {
        return StringBuilder()
            .append(username)
            .append(COLON)
            .append(password)
            .append(AT)
            .build()
    }

    private fun determineHostPortString(params: MongoProperties): String {
        return StringBuilder()
            .append(params.host)
            .append(COLON)
            .append(params.port)
            .append(SLASH)
            .build()
    }

    private fun determineDatabaseString(params: MongoProperties): String {
        return params.database
    }

}