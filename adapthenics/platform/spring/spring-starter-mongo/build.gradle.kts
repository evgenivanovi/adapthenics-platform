project(ProjectModules.SPRING_STARTER_MONGO) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(mongodbCatalog.mongodbClient)
        testImplementation(mongodbCatalog.mongodbClient)

        implementation(mongodbCatalog.mongodbClientSpring)
        testImplementation(mongodbCatalog.mongodbClientSpring)

        testImplementation(mongodbCatalog.mongodbEnvotur)
        testImplementation(mongodbCatalog.mongodbEnvoturJunit)

        implementation(project(ProjectModules.BASE))
        testImplementation(project(ProjectModules.BASE))

        implementation(project(ProjectModules.SPRING_CORE))
        testImplementation(project(ProjectModules.SPRING_CORE))

        implementation(project(ProjectModules.SPRING_STARTER_CORE))
        testImplementation(project(ProjectModules.SPRING_STARTER_CORE))

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.JACKSON_SUPPORT))
        testImplementation(project(ProjectModules.JACKSON_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(libs.bundles.mongo.client.impl)
    implementation(libs.bundles.mongo.client.spring.impl)
    testImplementation(ktDependenciesCatalog.bundles.testcontainers.mongo.impl)

    implementation(libs.bundles.mongo.spring.impl)

    implementation(libs.bundles.springboot.impl)
    testImplementation(libs.bundles.springboot.testimpl)
}
