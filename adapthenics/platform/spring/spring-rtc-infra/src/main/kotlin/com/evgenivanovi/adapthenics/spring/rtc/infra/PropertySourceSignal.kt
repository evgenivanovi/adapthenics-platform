package com.evgenivanovi.adapthenics.spring.rtc.infra

import com.evgenivanovi.adapthenics.rtc.domain.model.Property
import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertySignal
import org.springframework.cloud.endpoint.event.RefreshEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.ApplicationEventPublisherAware

class PropertySourceSignal : ApplicationEventPublisherAware, PropertySignal {

    lateinit var eventPublisher: ApplicationEventPublisher

    override fun setApplicationEventPublisher(
        applicationEventPublisher: ApplicationEventPublisher,
    ) {
        this.eventPublisher = applicationEventPublisher
    }

    override fun signal(event: Property, source: () -> Any) {
        createEvent(event, source).let(eventPublisher::publishEvent)
    }

    /**
     * [RefreshEvent] via [org.springframework.cloud.context.refresh.ContextRefresher]
     * will trigger [org.springframework.cloud.context.environment.EnvironmentChangeEvent].
     */
    private fun createEvent(signal: Property, source: () -> Any): RefreshEvent {
        val description = String.format(
            "PropertyModel source has been changed. Modified keys: [%s]",
            signal.key()
        )
        return RefreshEvent(source(), signal, description)
    }

}