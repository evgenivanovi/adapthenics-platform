package com.evgenivanovi.adapthenics.spring.rtc.infra

import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage
import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertyRepository
import org.springframework.core.env.EnumerablePropertySource

class PropertySourceRepository(
    name: String,
    repository: PropertyRepository,
) : EnumerablePropertySource<PropertyRepository>(name, repository) {

    override fun getProperty(name: String): Any? {
        return source.read(name)?.value?.value()
    }

    override fun getPropertyNames(): Array<String> {
        return source
            .readAll()
            .map(PropertyMessage::key)
            .toTypedArray()
    }

}