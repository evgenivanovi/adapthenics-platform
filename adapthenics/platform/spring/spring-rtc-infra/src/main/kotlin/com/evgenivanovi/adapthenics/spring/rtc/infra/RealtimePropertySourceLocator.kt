package com.evgenivanovi.adapthenics.spring.rtc.infra

import com.evgenivanovi.adapthenics.spring.rtc.adapter.PropertyRepository
import com.evgenivanovi.kt.math.Numbers
import org.springframework.cloud.bootstrap.config.PropertySourceLocator
import org.springframework.core.PriorityOrdered
import org.springframework.core.env.CompositePropertySource
import org.springframework.core.env.ConfigurableEnvironment
import org.springframework.core.env.Environment
import org.springframework.core.env.PropertySource

class RealtimePropertySourceLocator(
    private val name: String,
    private val repository: PropertyRepository,
) : PropertySourceLocator, PriorityOrdered {

    override fun locate(environment: Environment): PropertySource<*>? {

        if (environment !is ConfigurableEnvironment)
            return null

        val source = CompositePropertySource(name)
        val repository = PropertySourceRepository(name, repository)

        source.addPropertySource(repository)
        return source

    }

    override fun getOrder(): Int {
        return Numbers.ZERO_I
    }

}