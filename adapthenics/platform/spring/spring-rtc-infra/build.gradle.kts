project(ProjectModules.SPRING_RTC_INFRA) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(project(ProjectModules.SPRING_CORE))
        testImplementation(project(ProjectModules.SPRING_CORE))

        implementation(project(ProjectModules.RTC_DOMAIN))
        testImplementation(project(ProjectModules.RTC_DOMAIN))

        implementation(project(ProjectModules.RTC_ETCD_SCHEMA))
        testImplementation(project(ProjectModules.RTC_ETCD_SCHEMA))

        implementation(project(ProjectModules.RTC_ETCD_ADAPTER))
        testImplementation(project(ProjectModules.RTC_ETCD_ADAPTER))

        implementation(project(ProjectModules.SPRING_RTC_ADAPTER))
        testImplementation(project(ProjectModules.SPRING_RTC_ADAPTER))

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(libs.bundles.spring.core.impl)
}