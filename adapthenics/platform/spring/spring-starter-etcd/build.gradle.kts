project(ProjectModules.SPRING_STARTER_ETCD) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(project(ProjectModules.BASE))
        testImplementation(project(ProjectModules.BASE))

        implementation(project(ProjectModules.SPRING_CORE))
        testImplementation(project(ProjectModules.SPRING_CORE))

        implementation(project(ProjectModules.SPRING_STARTER_CORE))
        testImplementation(project(ProjectModules.SPRING_STARTER_CORE))

        implementation(project(ProjectModules.ETCD_CLIENT))
        testImplementation(project(ProjectModules.ETCD_CLIENT))

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.JACKSON_SUPPORT))
        testImplementation(project(ProjectModules.JACKSON_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(ktDependenciesCatalog.bundles.jackson.json.impl)

    implementation(libs.spring.web)
    implementation(libs.bundles.spring.core.impl)
    implementation(libs.bundles.springboot.impl)
    testImplementation(libs.bundles.springboot.testimpl)

    annotationProcessor(libs.bundles.springboot.cfg.annotation)
}
