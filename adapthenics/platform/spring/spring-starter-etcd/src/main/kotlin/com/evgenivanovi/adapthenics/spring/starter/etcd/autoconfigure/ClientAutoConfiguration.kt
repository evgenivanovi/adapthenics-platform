package com.evgenivanovi.adapthenics.spring.starter.etcd.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        ClientConfiguration::class,
    ]
)
@Conditional(
    value = [
        ETCDConditional::class
    ]
)
open class ClientAutoConfiguration