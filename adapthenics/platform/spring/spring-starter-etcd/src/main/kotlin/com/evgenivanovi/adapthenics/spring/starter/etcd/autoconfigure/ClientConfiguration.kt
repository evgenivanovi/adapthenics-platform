package com.evgenivanovi.adapthenics.spring.starter.etcd.autoconfigure

import com.evgenivanovi.adapthenics.base.app.ApplicationMetadata
import com.evgenivanovi.adapthenics.etcd.client.ClientParameters
import com.evgenivanovi.adapthenics.etcd.client.PlatformClientParameters
import com.evgenivanovi.adapthenics.etcd.client.reactive.PlatformReactiveClient
import com.evgenivanovi.adapthenics.etcd.client.reactive.PlatformReactiveClientFactory
import com.evgenivanovi.adapthenics.etcd.client.reactive.ReactiveClientCustomizer
import com.evgenivanovi.adapthenics.etcd.client.sync.ClientCustomizer
import com.evgenivanovi.adapthenics.etcd.client.sync.PlatformClient
import com.evgenivanovi.adapthenics.etcd.client.sync.PlatformClientFactory
import io.etcd.jetcd.Client
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Configuration

@Configuration
@Conditional(
    value = [
        ETCDConditional::class
    ]
)
open class ClientConfiguration {

    @Bean
    @ConfigurationProperties("adapthenics.app.etcd")
    open fun clientProperties(): ClientProperties {
        return ClientProperties()
    }

    @Bean
    open fun platformClientParameters(
        clientProperties: ClientProperties,
        applicationMetadata: ApplicationMetadata,
    ): PlatformClientParameters {

        return PlatformClientParameters(

            project = applicationMetadata.id,
            version = applicationMetadata.version,

            endpoints = clientProperties.buildEndpoints(),
            clientPrivateKeyPem = clientProperties.buildClientPrivateKeyPem(),
            clientCertificatePem = clientProperties.buildClientCertificatePem(),
            trustedCertificatesPem = clientProperties.buildTrustedCertificatesPem()

        )

    }

    @Bean
    open fun platformClientFactory(
        clientParameters: ClientParameters,
        clientCustomizers: ObjectProvider<ClientCustomizer>,
    ): PlatformClientFactory {
        return PlatformClientFactory(
            clientParameters,
            clientCustomizers.orderedStream().toList()
        )
    }

    @Bean
    open fun platformClient(
        platformClientFactory: PlatformClientFactory,
        clientParameters: ClientParameters,
    ): PlatformClient {
        return PlatformClient(
            platformClientFactory.create(), clientParameters
        )
    }

    @Bean
    open fun platformReactiveClientFactory(
        client: Client,
        clientCustomizers: ObjectProvider<ReactiveClientCustomizer>,
    ): PlatformReactiveClientFactory {
        return PlatformReactiveClientFactory(
            client, clientCustomizers.orderedStream().toList()
        )
    }

    @Bean
    open fun platformReactiveClient(
        platformClientFactory: PlatformReactiveClientFactory,
        clientParameters: ClientParameters,
    ): PlatformReactiveClient {
        return PlatformReactiveClient(
            platformClientFactory.create(), clientParameters
        )
    }

}