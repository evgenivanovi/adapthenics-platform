package com.evgenivanovi.adapthenics.spring.starter.etcd.autoconfigure

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource(
    name = ClientModuleProperties.PROPERTY_SOURCE_NAME,
    value = [ClientModuleProperties.PROPERTY_SOURCE_VALUE]
)
open class ClientModuleProperties {

    companion object {

        const val PROPERTY_SOURCE_NAME = "adapthenics/platform/etcd"
        const val PROPERTY_SOURCE_VALUE = "classpath:/adapthenics/platform/etcd/module.properties"

        /**
         * Comma-separated ETCD addresses;
         */
        const val ETCD_ENDPOINTS_ENV = "ETCD_ENDPOINTS"

        /**
         * Chain of CA certificates;
         */
        const val ETCD_CA_PEM_ENV = "ETCD_CA_PEM"

        /**
         * Private client key SSL/TLS;
         */
        const val ETCD_CLIENT_KEY_ENV = "ETCD_CLIENT_KEY"

        /**
         * Public client certificate SSL/TLS;
         */
        const val ETCD_CLIENT_PEM_ENV = "ETCD_CLIENT_PEM"

    }

}