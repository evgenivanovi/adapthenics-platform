package com.evgenivanovi.adapthenics.spring.starter.etcd.autoconfigure

import com.evgenivanovi.kt.contract.requireNotEmpty
import com.evgenivanovi.kt.string.nullIfBlank

class ClientProperties(

    var enabled: Boolean = true,

    /**
     * [ClientModuleProperties.ETCD_ENDPOINTS_ENV]
     */
    var endpoints: MutableList<String> = mutableListOf(),

    /**
     * [ClientModuleProperties.ETCD_CA_PEM_ENV]
     */
    var clientPrivateKeyPem: String? = null,

    /**
     * [ClientModuleProperties.ETCD_CLIENT_KEY_ENV]
     */
    var clientCertificatePem: String? = null,

    /**
     * [ClientModuleProperties.ETCD_CLIENT_PEM_ENV]
     */
    var trustedCertificatesPem: String? = null,

    ) {

    fun buildEndpoints(): MutableList<String> {

        endpoints.requireNotEmpty("ETCD endpoints must not be empty")

        return endpoints
            .map(String::trim)
            .map(::attachScheme)
            .toMutableList()

    }

    private fun attachScheme(endpoint: String): String {

        if (endpoint.startsWith(ETCD_ENDPOINT_SCHEME))
            return endpoint

        return ETCD_ENDPOINT_SCHEME + endpoint

    }

    fun buildClientPrivateKeyPem(): String? {
        return clientPrivateKeyPem.nullIfBlank()
    }

    fun buildClientCertificatePem(): String? {
        return clientCertificatePem.nullIfBlank()
    }

    fun buildTrustedCertificatesPem(): String? {
        return trustedCertificatesPem.nullIfBlank()
    }

    companion object {

        const val ETCD_ENDPOINT_SCHEME = "http://"

    }

}