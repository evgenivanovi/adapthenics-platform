project(ProjectModules.SPRING_RTC_ADAPTER) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.JACKSON_SUPPORT))
        testImplementation(project(ProjectModules.JACKSON_SUPPORT))

        // ETCD
        implementation(project(ProjectModules.RTC_DOMAIN))
        testImplementation(project(ProjectModules.RTC_DOMAIN))

        implementation(project(ProjectModules.RTC_ETCD_ADAPTER))
        testImplementation(project(ProjectModules.RTC_ETCD_ADAPTER))

        implementation(project(ProjectModules.RTC_ETCD_SCHEMA))
        testImplementation(project(ProjectModules.RTC_ETCD_SCHEMA))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)
}