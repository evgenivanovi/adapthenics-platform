package com.evgenivanovi.adapthenics.spring.rtc.adapter

import com.evgenivanovi.adapthenics.rtc.domain.model.Property

interface PropertySignal {

    /**
     * @param source – the object on which the event initially occurred
     * or with which the event is associated
     */
    fun signal(event: Property, source: () -> Any)

}