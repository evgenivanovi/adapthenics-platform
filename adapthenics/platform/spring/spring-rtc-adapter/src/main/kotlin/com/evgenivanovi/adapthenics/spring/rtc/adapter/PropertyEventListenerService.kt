package com.evgenivanovi.adapthenics.spring.rtc.adapter

import com.evgenivanovi.adapthenics.rtc.domain.model.Property
import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage
import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyType
import com.evgenivanovi.adapthenics.rtc.domain.service.EventListener

class PropertyEventListenerService(
    private val signal: PropertySignal,
    private val repository: PropertyRepository,
) : EventListener {

    override fun listen(event: Property) {
        onPropertyEvent(event)
        signal.signal(event) { this }
    }

    private fun onPropertyEvent(
        event: Property,
    ) {
        when (event.type) {
            PropertyType.CREATED -> {
                onCreated(event.msg)
            }

            PropertyType.UPDATED -> {
                onUpdated(event.msg)
            }

            PropertyType.DELETED -> {
                onDeleted(event.msg)
            }

            PropertyType.UNKNOWN -> {
                onUnknown(event.msg)
            }
        }
    }

    private fun onCreated(msg: PropertyMessage) {
        repository.write(msg.key, msg)
    }

    private fun onUpdated(msg: PropertyMessage) {
        repository.write(msg.key, msg)
    }

    private fun onDeleted(msg: PropertyMessage) {
        repository.remove(msg.key)
    }

    private fun onUnknown(msg: PropertyMessage) {
        //
    }

}