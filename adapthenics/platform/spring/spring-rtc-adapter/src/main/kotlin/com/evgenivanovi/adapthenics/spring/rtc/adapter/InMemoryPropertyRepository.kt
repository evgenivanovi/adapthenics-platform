package com.evgenivanovi.adapthenics.spring.rtc.adapter

import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage
import com.google.common.collect.Maps

class InMemoryPropertyRepository : PropertyRepository {

    private val cache: MutableMap<String, PropertyMessage> = Maps.newConcurrentMap()

    override fun read(key: String): PropertyMessage? {
        return cache[key]
    }

    override fun readAll(): List<PropertyMessage> {
        return cache.values.toList()
    }

    override fun write(key: String, value: PropertyMessage) {
        cache[key] = value
    }

    override fun writeAll(values: List<PropertyMessage>) {
        return values
            .associateBy(PropertyMessage::key)
            .let(cache::putAll)
    }

    override fun remove(key: String): PropertyMessage? {
        return cache.remove(key)
    }

    override fun removeAll(keys: List<String>): List<PropertyMessage> {
        return keys.mapNotNull(cache::remove)
    }

}