package com.evgenivanovi.adapthenics.spring.rtc.adapter

import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage

interface PropertyRepository {

    fun read(key: String): PropertyMessage?

    fun readAll(): List<PropertyMessage>

    fun write(key: String, value: PropertyMessage)

    fun writeAll(values: List<PropertyMessage>)

    fun remove(key: String): PropertyMessage?

    fun removeAll(keys: List<String>): List<PropertyMessage>

}