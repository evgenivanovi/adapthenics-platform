package com.evgenivanovi.adapthenics.spring.starter.logging.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        PlatformLoggingConfiguration::class,
    ]
)
open class PlatformLoggingAutoConfiguration
