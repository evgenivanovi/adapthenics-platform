package com.evgenivanovi.adapthenics.spring.starter.logging.autoconfigure

import com.evgenivanovi.adapthenics.spring.starter.logging.common.PlatformLoggingRebinder
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.ConfigurableEnvironment

@Configuration
open class PlatformLoggingConfiguration {

    @Bean
    @ConditionalOnMissingBean
    open fun platformLoggingRebinder(
        configurableEnvironment: ConfigurableEnvironment,
    ): PlatformLoggingRebinder {
        return PlatformLoggingRebinder(configurableEnvironment)
    }

}
