package com.evgenivanovi.adapthenics.spring.starter.logging.common

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.boot.logging.LogFile
import org.springframework.boot.logging.LoggingInitializationContext
import org.springframework.boot.logging.LoggingSystem
import org.springframework.cloud.context.environment.EnvironmentChangeEvent
import org.springframework.cloud.logging.LoggingRebinder
import org.springframework.core.env.ConfigurableEnvironment

class PlatformLoggingRebinder(
    private val env: ConfigurableEnvironment,
) : LoggingRebinder() {

    private val log = KotlinLogging.logger { }

    override fun onApplicationEvent(event: EnvironmentChangeEvent) {
        // class loader is used as in org.springframework.cloud.logging.LoggingRebinder#onApplicationEvent
        val system = LoggingSystem.get(LoggingSystem::class.java.classLoader)
        reinitialize(system)
        super.onApplicationEvent(event)
    }

    /**
     * Platform Logback configuration depends on environment properties.
     * It means that if environment is changed (e.g. realtime configuration is changed)
     * whole logging system should be reinitialized to be refreshed by updated environment-based configuration.
     *
     * @see [org.springframework.cloud.bootstrap.config.PropertySourceBootstrapConfiguration.reinitializeLoggingSystem]
     */
    private fun reinitialize(system: LoggingSystem) {

        val config = env.resolvePlaceholders("\${logging.config:}")

        val file = LogFile.get(env)

        // Three-step initialization that accounts for the cleanup of the logging context before initialization.
        // Spring Boot doesn't initialize a logging system that hasn't had this sequence applied (since 1.4.1).
        try {
            system.cleanUp()
            system.beforeInitialize()
            system.initialize(LoggingInitializationContext(env), config, file)
        } catch (ex: Exception) {
            log.warn { "Error on logback rebind by logging config file $config: $ex " }
        }

    }

}