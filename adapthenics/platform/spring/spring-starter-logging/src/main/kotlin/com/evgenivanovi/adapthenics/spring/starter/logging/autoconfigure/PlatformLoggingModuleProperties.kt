package com.evgenivanovi.adapthenics.spring.starter.logging.autoconfigure

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource(
    name = PlatformLoggingModuleProperties.PROPERTY_SOURCE_NAME,
    value = [
        PlatformLoggingModuleProperties.PROPERTY_SOURCE_VALUE
    ]
)
open class PlatformLoggingModuleProperties {

    companion object {

        const val PROPERTY_SOURCE_NAME = "adapthenics/platform/logging"
        const val PROPERTY_SOURCE_VALUE = "classpath:/adapthenics/platform/logging/module.properties"

        const val PLATFORM_LOGGING_FORMAT_ENV_KEY = "PLATFORM_LOGGING_FORMAT"
        const val PLATFORM_LOGGING_LEVEL_ENV_KEY = "PLATFORM_LOGGING_LEVEL"

    }

}