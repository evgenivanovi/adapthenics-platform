package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.handlers.InvalidEntityErrorexAdviceHandler
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.base.errx.InvalidEntityErrorex
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

class InvalidEntityErrorexHandlerAdvice(
    private val delegate: InvalidEntityErrorexAdviceHandler,
) : ResponseHandlerAdvice {

    override fun target(): KClass<InvalidEntityErrorex> {
        return delegate.target()
    }

    override fun canHandle(ex: Throwable): Boolean {
        return delegate.canHandle(ex)
    }

    @ExceptionHandler(InvalidEntityErrorex::class)
    override fun <R : Any> handle(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): ResponseMessage<R> {
        return ResponseHandlerAdvice
            .retrieveExceptionContext(ex, exchange)
            .let(delegate::handle)
    }

}