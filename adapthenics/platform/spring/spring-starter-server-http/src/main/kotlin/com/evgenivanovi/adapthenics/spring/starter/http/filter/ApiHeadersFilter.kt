package com.evgenivanovi.adapthenics.spring.starter.http.filter

import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

class ApiHeadersFilter : WebFilter {

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        return InteractionApiHeaderFilter
            .process(exchange)
            .flatMap(MethodApiHeaderFilter::process)
            .flatMap(RequestApiHeaderFilter::process)
            .flatMap(NotificationApiHeaderFilter::process)
            .flatMap(chain::filter)
    }

}