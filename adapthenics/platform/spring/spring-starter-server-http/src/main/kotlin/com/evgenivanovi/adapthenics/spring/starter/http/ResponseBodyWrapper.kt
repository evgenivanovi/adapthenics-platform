package com.evgenivanovi.adapthenics.spring.starter.http

import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.header.CallMetadataFactory
import com.evgenivanovi.adapthenics.api.contract.meta.RequestMetadata
import com.evgenivanovi.adapthenics.api.contract.response.SuccessResponseMessage
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.math.Numbers.HUNDRED_I
import com.evgenivanovi.kt.math.Numbers.ONE_I
import org.springframework.http.HttpStatus
import org.springframework.http.codec.HttpMessageWriter
import org.springframework.web.reactive.HandlerResult
import org.springframework.web.reactive.accept.RequestedContentTypeResolver
import org.springframework.web.reactive.result.method.annotation.ResponseBodyResultHandler
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.toMono

class ResponseBodyWrapper : ResponseBodyResultHandler {

    private val svc: ResponseAPIService

    constructor(
        svc: ResponseAPIService,
        writers: List<HttpMessageWriter<*>>,
        resolver: RequestedContentTypeResolver,
    ) : super(writers, resolver) {
        this.svc = svc
        this.order = HUNDRED_I.minus(ONE_I)
    }

    override fun handleResult(
        exchange: ServerWebExchange,
        result: HandlerResult,
    ): Mono<Void> {

        val metadata = exchange
            .request
            .headers
            .toSingleValueMap()
            .let(CallMetadataFactory::from)

        if (metadata is RequestMetadata) {
            return successResponse(metadata, result)
                .switchIfEmpty { emptyResponse(metadata) }
                .flatMap { writeBody(it, result.returnTypeSource, exchange) }
        }

        return ResponseStatusException(
            HttpStatus.INTERNAL_SERVER_ERROR
        ).toMono()

    }

    private fun successResponse(
        metadata: RequestMetadata,
        result: HandlerResult,
    ): Mono<SuccessResponseMessage<Any>> {
        return result.returnValue.asMono()
            .map { svc.success(metadata, it) }
    }

    private fun <R : Any> emptyResponse(
        metadata: RequestMetadata,
    ): Mono<SuccessResponseMessage<R>> {
        return svc.emptySuccessMono(metadata)
    }

    companion object {

        private fun <T : Any> T?.asMono(): Mono<T> {
            if (this is Mono<*>)
                return this.asOf()
            return this.toMono()
        }

    }

}