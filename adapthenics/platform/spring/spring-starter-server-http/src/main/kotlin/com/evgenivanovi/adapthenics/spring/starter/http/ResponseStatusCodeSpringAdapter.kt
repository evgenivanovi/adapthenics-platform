package com.evgenivanovi.adapthenics.spring.starter.http

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.spring.webflux.http.HttpStatusAssociation
import org.springframework.web.bind.annotation.ResponseStatus

class ResponseStatusCodeSpringAdapter(
    private val status: ResponseStatus,
) : ResponseStatusCode {

    private val resolved: ResponseStatusCode? =
        HttpStatusAssociation.resolve(status.code)

    override fun getCode(): Int {
        return resolved?.getCode()
            ?: status.code.value()
    }

    override fun getReason(): String {
        return resolved?.getReason()
            ?: status.code.reasonPhrase
    }

    override fun clientError(): Boolean {
        return resolved?.clientError()
            ?: status.code.is4xxClientError
    }

    override fun serverError(): Boolean {
        return resolved?.serverError()
            ?: status.code.is5xxServerError
    }

}