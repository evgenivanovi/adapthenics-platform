package com.evgenivanovi.adapthenics.spring.starter.http.filter

import com.evgenivanovi.adapthenics.api.contract.header.CallHeader
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.API_FILTER
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.CONTAINS_ALL_FOR_NOTIFICATION_FILTER
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.IS_NOTIFICATION_FILTER
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderResolver
import com.evgenivanovi.adapthenics.base.errx.HeaderError
import com.evgenivanovi.adapthenics.base.errx.HeaderErrorex
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

internal object NotificationApiHeaderFilter {

    fun process(exchange: ServerWebExchange): Mono<ServerWebExchange> {

        val headers = exchange
            .request
            .headers
            .toSingleValueMap()
            .toList()

        if (headers.any(IS_NOTIFICATION_FILTER).not())
            return processEmpty(exchange)

        val apiHeaders = headers
            .filter(API_FILTER)

        val containsAllHeaders = apiHeaders
            .let(CONTAINS_ALL_FOR_NOTIFICATION_FILTER)

        return when (containsAllHeaders) {
            true -> processEmpty(exchange)
            false -> processApiHeaders(apiHeaders)
        }

    }

    private fun processApiHeaders(
        headers: Collection<Pair<String, String>>
    ): Mono<ServerWebExchange> {

        val missing = calculateMissingApiHeaders(headers)
            .map(CallHeader::value)

        val invalid = calculateInvalidApiHeaders(headers)

        return HeaderError
            .of(missing, invalid)
            .let(HeaderErrorex.Companion::of)
            .toMono()

    }

    private fun calculateMissingApiHeaders(
        headers: Collection<Pair<String, String>>
    ): Set<CallHeader> {
        return headers
            .mapNotNull { CallHeader.resolve(it.first) }
            .let { CallHeader.requiredInNotification().subtract(it.toSet()) }
    }

    private fun calculateInvalidApiHeaders(
        headers: Collection<Pair<String, String>>
    ): Map<String, String> {
        val builder = mutableMapOf<String, String>()
        CallHeaderResolver.resolveInvalidOriginHeader(headers, builder)
        CallHeaderResolver.resolveInvalidIncomeInteractionHeader(headers, builder)
        CallHeaderResolver.resolveInvalidMethodHeader(headers, builder)
        return builder.toMap()
    }

    private fun processEmpty(exchange: ServerWebExchange) = Mono.just(exchange)

}