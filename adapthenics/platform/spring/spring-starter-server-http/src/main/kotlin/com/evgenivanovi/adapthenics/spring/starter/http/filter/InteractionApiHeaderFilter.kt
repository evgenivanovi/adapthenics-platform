package com.evgenivanovi.adapthenics.spring.starter.http.filter

import com.evgenivanovi.adapthenics.api.contract.header.CallHeader
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.API_FILTER
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.IS_INCOME_INTERACTION_FILTER
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderResolver
import com.evgenivanovi.adapthenics.base.errx.HeaderError
import com.evgenivanovi.adapthenics.base.errx.HeaderErrorex
import com.evgenivanovi.kt.coll.asSet
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

internal object InteractionApiHeaderFilter {

    fun process(exchange: ServerWebExchange): Mono<ServerWebExchange> {

        val headers = exchange
            .request
            .headers
            .toSingleValueMap()
            .toList()

        val apiHeaders = headers
            .filter(API_FILTER)

        val containsInteractionHeader = apiHeaders
            .any(IS_INCOME_INTERACTION_FILTER)

        return when (containsInteractionHeader) {
            true -> processEmpty(exchange)
            false -> processApiHeaders(apiHeaders)
        }

    }

    private fun processApiHeaders(
        headers: Collection<Pair<String, String>>
    ): Mono<ServerWebExchange> {

        val missing = calculateMissingApiHeaders(headers)
            .map(CallHeader::value)

        val invalid = calculateInvalidApiHeaders(headers)

        return HeaderError
            .of(missing, invalid)
            .let(HeaderErrorex.Companion::of)
            .toMono()

    }

    private fun calculateMissingApiHeaders(
        headers: Collection<Pair<String, String>>
    ): Set<CallHeader> {
        return headers
            .mapNotNull { CallHeader.resolve(it.first) }
            .let { CallHeader.INTERACTION.asSet().subtract(it.toSet()) }
    }

    private fun calculateInvalidApiHeaders(
        headers: Collection<Pair<String, String>>
    ): Map<String, String> {
        return CallHeaderResolver
            .resolveInvalidIncomeInteractionHeader(headers)
            ?.let(::mapOf)
            ?: emptyMap()
    }

    private fun processEmpty(exchange: ServerWebExchange) = Mono.just(exchange)

}