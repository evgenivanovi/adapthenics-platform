package com.evgenivanovi.adapthenics.spring.starter.http.filter

import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderConstants
import com.evgenivanovi.adapthenics.api.contract.header.CallHeaderFilters.METHOD_FILTER
import com.evgenivanovi.adapthenics.base.errx.HeaderError
import com.evgenivanovi.adapthenics.base.errx.HeaderErrorex
import com.evgenivanovi.kt.lang.eq
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

internal object MethodApiHeaderFilter {

    fun process(exchange: ServerWebExchange): Mono<ServerWebExchange> {

        val headers = exchange
            .request
            .headers
            .toSingleValueMap()
            .toList()

        return headers
            .firstOrNull(METHOD_FILTER)
            ?.let { processExistingMethodHeader(it, exchange) }
            ?: processNonExistingMethodHeader(exchange)

    }

    private fun processNonExistingMethodHeader(exchange: ServerWebExchange): Mono<ServerWebExchange> {

        val mutatedRequest = exchange
            .request
            .mutate()
            .header(CallHeaderConstants.METHOD_HEADER_NAME, exchange.request.path.value())
            .build()

        return exchange
            .mutate()
            .request(mutatedRequest)
            .build()
            .toMono()

    }

    private fun processExistingMethodHeader(
        header: Pair<String, String>,
        exchange: ServerWebExchange,
    ): Mono<ServerWebExchange> {

        val equals = header
            .second
            .eq(exchange.request.path.value())

        if (equals) return Mono.just(exchange)

        return HeaderError
            .of(emptyList(), mapOf(header))
            .let(HeaderErrorex.Companion::of)
            .toMono()

    }

    private fun processEmpty(exchange: ServerWebExchange) = Mono.just(exchange)

}