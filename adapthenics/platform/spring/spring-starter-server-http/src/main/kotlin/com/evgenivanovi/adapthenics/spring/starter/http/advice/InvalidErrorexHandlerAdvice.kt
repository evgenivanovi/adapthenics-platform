package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.handlers.InvalidErrorexAdviceHandler
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.kt.errx.InvalidErrorex
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

class InvalidErrorexHandlerAdvice(
    private val delegate: InvalidErrorexAdviceHandler,
) : ResponseHandlerAdvice {

    override fun target(): KClass<InvalidErrorex> {
        return delegate.target()
    }

    override fun canHandle(ex: Throwable): Boolean {
        return delegate.canHandle(ex)
    }

    @ExceptionHandler(InvalidErrorex::class)
    override fun <R : Any> handle(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): ResponseMessage<R> {
        return ResponseHandlerAdvice
            .retrieveExceptionContext(ex, exchange)
            .let(delegate::handle)
    }

}