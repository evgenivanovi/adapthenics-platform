package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.response.Problem
import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.from
import com.evgenivanovi.adapthenics.spring.starter.http.ResponseStatusCodeSpringHttpAdapter
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.lang.cast
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

class ResponseStatusExceptionHandlerAdvice(
    private val svc: ResponseAPIService,
) : ResponseHandlerAdvice {

    override fun target(): KClass<ResponseStatusException> {
        return ResponseStatusException::class
    }

    override fun canHandle(ex: Throwable): Boolean {
        return target().java
            .isAssignableFrom(ex::class.java)
    }

    @ExceptionHandler(ResponseStatusException::class)
    override fun <R : Any> handle(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): ResponseMessage<R> {
        return toProblemResponse(
            ex.cast()
        ).asOf()
    }

    private fun toProblemResponse(ex: ResponseStatusException): ProblemResponseMessage {
        val resolved = ResponseStatusCodeSpringHttpAdapter(ex.statusCode.asOf())
        val problem = Problem.from(resolved, emptyList())
        return svc.problem(problem, resolved)
    }

}