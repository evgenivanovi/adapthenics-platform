package com.evgenivanovi.adapthenics.spring.starter.http.autoconfigure

import com.evgenivanovi.adapthenics.spring.starter.http.filter.ApiHeadersFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.server.WebFilter

/**
 * Not used due to ApiExchanger configuration
 */
@Configuration
open class RequestConfiguration {

    @Bean
    open fun apiHeadersFilter(): WebFilter {
        return ApiHeadersFilter()
    }

}