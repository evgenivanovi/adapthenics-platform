package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.search.handler.InvalidSearchErrorexAdviceHandler
import com.evgenivanovi.search.InvalidSearchErrorex
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

class InvalidSearchErrorexHandlerAdvice(
    private val delegate: InvalidSearchErrorexAdviceHandler,
) : ResponseHandlerAdvice {

    override fun target(): KClass<InvalidSearchErrorex> {
        return delegate.target()
    }

    override fun canHandle(ex: Throwable): Boolean {
        return delegate.canHandle(ex)
    }

    @ExceptionHandler(InvalidSearchErrorex::class)
    override fun <R : Any> handle(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): ResponseMessage<R> {
        return ResponseHandlerAdvice
            .retrieveExceptionContext(ex, exchange)
            .let(delegate::handle)
    }

}