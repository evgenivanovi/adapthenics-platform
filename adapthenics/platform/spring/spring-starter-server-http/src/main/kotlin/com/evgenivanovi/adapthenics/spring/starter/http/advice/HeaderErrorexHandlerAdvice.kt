package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.response.*
import com.evgenivanovi.adapthenics.base.errx.HeaderErrorex
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.lang.cast
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

class HeaderErrorexHandlerAdvice(
    private val svc: ResponseAPIService,
) : ResponseHandlerAdvice {

    override fun target(): KClass<HeaderErrorex> {
        return HeaderErrorex::class
    }

    override fun canHandle(ex: Throwable): Boolean {
        return target().java
            .isAssignableFrom(ex::class.java)
    }

    @ExceptionHandler(HeaderErrorex::class)
    override fun <R : Any> handle(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): ResponseMessage<R> {
        return toProblemResponse(ex.cast()).asOf()
    }

    private fun toProblemResponse(ex: HeaderErrorex): ProblemResponseMessage {
        val problem = Problem.from(ResponseStatusCodes.INVALID, ex.err)
        return svc.problem(problem, ResponseStatusCodes.INVALID)
    }

}