package com.evgenivanovi.adapthenics.spring.starter.http

import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCode
import com.evgenivanovi.adapthenics.spring.webflux.http.HttpStatusAssociation
import org.springframework.http.HttpStatus

class ResponseStatusCodeSpringHttpAdapter(
    private val status: HttpStatus,
) : ResponseStatusCode {

    private val resolved: ResponseStatusCode? =
        HttpStatusAssociation.resolve(status)

    override fun getCode(): Int {
        return resolved?.getCode()
            ?: status.value()
    }

    override fun getReason(): String {
        return resolved?.getReason()
            ?: status.reasonPhrase
    }

    override fun clientError(): Boolean {
        return resolved?.clientError()
            ?: status.is4xxClientError
    }

    override fun serverError(): Boolean {
        return resolved?.serverError()
            ?: status.is5xxServerError
    }

}