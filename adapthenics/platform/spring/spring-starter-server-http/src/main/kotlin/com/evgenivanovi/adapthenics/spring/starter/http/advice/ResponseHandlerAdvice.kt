package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.exchanger.ExceptionContext
import com.evgenivanovi.adapthenics.api.contract.header.CallMetadataFactory
import com.evgenivanovi.adapthenics.api.contract.meta.CallMetadata
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.spring.webflux.http.statusForResponseEntity
import com.evgenivanovi.adapthenics.spring.webflux.http.statusForServerResponse
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

interface ResponseHandlerAdvice : HandlerAdvice {

    fun <R : Any> handleResponseEntity(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): Mono<ResponseEntity<ResponseMessage<R>>> {
        return handle<R>(ex, exchange)
            .let(Companion::toResponseEntity)
            .toMono()
    }

    fun handleServerResponse(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): Mono<ServerResponse> {
        return handle<Any>(ex, exchange)
            .let(Companion::toServerResponse)
    }

    companion object {

        fun <R : Any> toResponseEntity(
            message: ResponseMessage<R>,
        ): ResponseEntity<ResponseMessage<R>> {
            return statusForResponseEntity(message)
                .contentType(MediaType.APPLICATION_JSON)
                .body(message)
        }

        fun <R : Any> toServerResponse(
            message: ResponseMessage<R>,
        ): Mono<ServerResponse> {
            return statusForServerResponse(message)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(message)
        }

        fun retrieveExceptionContext(
            ex: Throwable,
            exchange: ServerWebExchange,
        ): ExceptionContext<*> {
            return ExceptionContext(
                ex = ex,
                meta = retrieveMetadata(exchange)
            )
        }

        private fun retrieveMetadata(exchange: ServerWebExchange): CallMetadata? {
            return exchange
                .request
                .headers
                .toSingleValueMap()
                .let(CallMetadataFactory::from)
        }

    }

}