package com.evgenivanovi.adapthenics.spring.starter.http.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        ExceptionConfiguration::class
    ]
)
open class ExceptionAutoConfiguration