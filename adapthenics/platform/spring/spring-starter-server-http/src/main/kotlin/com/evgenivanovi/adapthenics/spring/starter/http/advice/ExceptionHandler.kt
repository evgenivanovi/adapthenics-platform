package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.response.ProblemResponseMessage
import com.evgenivanovi.adapthenics.api.contract.response.ResponseStatusCodes
import com.evgenivanovi.kt.ex.Exceptions
import org.springframework.http.codec.HttpMessageReader
import org.springframework.http.codec.HttpMessageWriter
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.result.view.ViewResolver
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebExceptionHandler
import reactor.core.publisher.Mono

class ExceptionHandler : WebExceptionHandler {

    private val readers: List<HttpMessageReader<*>>
    private val writers: List<HttpMessageWriter<*>>
    private val handlers: List<ResponseHandlerAdvice>

    private val svc: ResponseAPIService

    constructor(
        readers: List<HttpMessageReader<*>>,
        writers: List<HttpMessageWriter<*>>,
        handlers: List<ResponseHandlerAdvice>,
        svc: ResponseAPIService,
    ) {
        this.readers = readers
        this.writers = writers
        this.handlers = handlers.sortedWith(COMPARATOR)

        this.svc = svc
    }

    override fun handle(
        exchange: ServerWebExchange,
        exception: Throwable,
    ): Mono<Void> {

        val handler = findHandler(exception)
            ?: return fallbackResponse(exchange)

        return handler
            .handleServerResponse(exception, exchange)
            .flatMap { it.writeTo(exchange, ServerResponseContext(writers)) }

    }

    private fun findHandler(exception: Throwable): ResponseHandlerAdvice? {
        return handlers
            .firstOrNull { it.canHandle(exception) }
    }

    private fun fallbackResponse(exchange: ServerWebExchange): Mono<Void> {
        return fallbackProblemMessage()
            .let(ResponseHandlerAdvice.Companion::toServerResponse)
            .flatMap { it.writeTo(exchange, ServerResponseContext(writers)) }
    }

    private fun fallbackProblemMessage(): ProblemResponseMessage {
        return svc.problem(null, ResponseStatusCodes.INTERNAL)
    }

    private inner class ServerResponseContext(
        private val writers: List<HttpMessageWriter<*>>,
    ) : ServerResponse.Context {

        override fun messageWriters(): List<HttpMessageWriter<*>> {
            return writers
        }

        override fun viewResolvers(): List<ViewResolver> {
            return emptyList()
        }

    }

    companion object {

        private val COMPARATOR = Comparator<HandlerAdvice> { adv1, adv2 ->
            Exceptions.compare(
                adv1.target().java, adv2.target().java
            )
        }

    }

}

