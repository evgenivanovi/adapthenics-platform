package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.api.contract.search.handler.SearchErrorexAdviceHandler
import com.evgenivanovi.search.SearchErrorex
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

class SearchErrorexHandlerAdvice(
    private val delegate: SearchErrorexAdviceHandler,
) : ResponseHandlerAdvice {

    override fun target(): KClass<SearchErrorex> {
        return delegate.target()
    }

    override fun canHandle(ex: Throwable): Boolean {
        return delegate.canHandle(ex)
    }

    @ExceptionHandler(SearchErrorex::class)
    override fun <R : Any> handle(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): ResponseMessage<R> {
        return ResponseHandlerAdvice
            .retrieveExceptionContext(ex, exchange)
            .let(delegate::handle)
    }

}