package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.handlers.DuplicateErrorexAdviceHandler
import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import com.evgenivanovi.adapthenics.base.errx.DuplicateErrorex
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

class DuplicateErrorexHandlerAdvice(
    private val delegate: DuplicateErrorexAdviceHandler,
) : ResponseHandlerAdvice {

    override fun target(): KClass<DuplicateErrorex> {
        return delegate.target()
    }

    override fun canHandle(ex: Throwable): Boolean {
        return delegate.canHandle(ex)
    }

    @ExceptionHandler(DuplicateErrorex::class)
    override fun <R : Any> handle(
        ex: Throwable,
        exchange: ServerWebExchange,
    ): ResponseMessage<R> {
        return ResponseHandlerAdvice
            .retrieveExceptionContext(ex, exchange)
            .let(delegate::handle)
    }

}