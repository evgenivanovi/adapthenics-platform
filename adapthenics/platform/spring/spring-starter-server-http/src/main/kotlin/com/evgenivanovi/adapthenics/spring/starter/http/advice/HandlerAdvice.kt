package com.evgenivanovi.adapthenics.spring.starter.http.advice

import com.evgenivanovi.adapthenics.api.contract.response.ResponseMessage
import org.springframework.web.server.ServerWebExchange
import kotlin.reflect.KClass

interface HandlerAdvice {

    fun canHandle(ex: Throwable): Boolean

    fun <R : Any> handle(ex: Throwable, exchange: ServerWebExchange): ResponseMessage<R>

    fun target(): KClass<out Throwable>

}