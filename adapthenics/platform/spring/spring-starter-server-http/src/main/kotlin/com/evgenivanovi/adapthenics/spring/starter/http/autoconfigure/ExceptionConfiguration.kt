package com.evgenivanovi.adapthenics.spring.starter.http.autoconfigure

import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.api.contract.handlers.*
import com.evgenivanovi.adapthenics.api.contract.search.handler.InvalidSearchErrorexAdviceHandler
import com.evgenivanovi.adapthenics.api.contract.search.handler.SearchErrorexAdviceHandler
import com.evgenivanovi.adapthenics.spring.starter.http.advice.*
import com.evgenivanovi.kt.math.Numbers
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.core.annotation.Order
import org.springframework.http.codec.ServerCodecConfigurer

@Configuration
@Import(
    value = [
        ExceptionConfiguration.CoreExceptionConfiguration::class,
        ExceptionConfiguration.ApiExceptionConfiguration::class,
        ExceptionConfiguration.SpringExceptionConfiguration::class,
        ExceptionConfiguration.PersistenceExceptionConfiguration::class,
    ]
)
open class ExceptionConfiguration {

    @Bean
    @Order(Numbers.TWO_I.unaryMinus())
    open fun exceptionHandlerStarter(
        serverCodecConfigurer: ServerCodecConfigurer,
        handlerAdvices: List<ResponseHandlerAdvice>,
        responseAPIService: ResponseAPIService,
    ): ExceptionHandler {
        return ExceptionHandler(
            serverCodecConfigurer.readers,
            serverCodecConfigurer.writers,
            handlerAdvices,
            responseAPIService
        )
    }

    @Configuration
    open class CoreExceptionConfiguration {

        @Bean
        @ConditionalOnBean(InvalidErrorexAdviceHandler::class)
        open fun invalidErrorexHandlerAdvice(
            invalidErrorexAdviceHandler: InvalidErrorexAdviceHandler,
        ): InvalidErrorexHandlerAdvice {
            return InvalidErrorexHandlerAdvice(invalidErrorexAdviceHandler)
        }

        @Bean
        @ConditionalOnBean(InvalidEntityErrorexAdviceHandler::class)
        open fun invalidEntityErrorexHandlerAdvice(
            invalidEntityErrorexAdviceHandler: InvalidEntityErrorexAdviceHandler,
        ): InvalidEntityErrorexHandlerAdvice {
            return InvalidEntityErrorexHandlerAdvice(invalidEntityErrorexAdviceHandler)
        }

        @Bean
        @ConditionalOnBean(SearchErrorexAdviceHandler::class)
        open fun searchErrorexHandlerAdvice(
            searchErrorexAdviceHandler: SearchErrorexAdviceHandler,
        ): SearchErrorexHandlerAdvice {
            return SearchErrorexHandlerAdvice(searchErrorexAdviceHandler)
        }

        @Bean
        @ConditionalOnBean(InvalidSearchErrorexAdviceHandler::class)
        open fun invalidSearchErrorexHandlerAdvice(
            invalidSearchErrorexAdviceHandler: InvalidSearchErrorexAdviceHandler,
        ): InvalidSearchErrorexHandlerAdvice {
            return InvalidSearchErrorexHandlerAdvice(invalidSearchErrorexAdviceHandler)
        }

    }

    @Configuration
    open class ApiExceptionConfiguration {

        @Bean
        open fun headerErrorexHandlerAdvice(
            responseAPIService: ResponseAPIService,
        ): HeaderErrorexHandlerAdvice {
            return HeaderErrorexHandlerAdvice(responseAPIService)
        }

    }

    @Configuration
    open class SpringExceptionConfiguration {

        @Bean
        open fun responseStatusExceptionHandlerAdvice(
            responseAPIService: ResponseAPIService,
        ): ResponseStatusExceptionHandlerAdvice {
            return ResponseStatusExceptionHandlerAdvice(responseAPIService)
        }

    }

    @Configuration
    open class ValidationExceptionConfiguration {

    }

    @Configuration
    open class PersistenceExceptionConfiguration {

        @Bean
        @ConditionalOnBean(DuplicateErrorexAdviceHandler::class)
        open fun duplicateErrorexHandlerAdvice(
            duplicateErrorexAdviceHandler: DuplicateErrorexAdviceHandler,
        ): DuplicateErrorexHandlerAdvice {
            return DuplicateErrorexHandlerAdvice(duplicateErrorexAdviceHandler)
        }

        @Bean
        @ConditionalOnBean(OptimisticLockErrorexAdviceHandler::class)
        open fun optimisticLockErrorexHandlerAdvice(
            optimisticLockErrorexAdviceHandler: OptimisticLockErrorexAdviceHandler,
        ): OptimisticLockErrorexHandlerAdvice {
            return OptimisticLockErrorexHandlerAdvice(optimisticLockErrorexAdviceHandler)
        }

        @Bean
        @ConditionalOnBean(PessimisticLockErrorexAdviceHandler::class)
        open fun pessimisticLockErrorexHandlerAdvice(
            pessimisticLockErrorexHandlerAdvice: PessimisticLockErrorexAdviceHandler,
        ): PessimisticLockErrorexHandlerAdvice {
            return PessimisticLockErrorexHandlerAdvice(pessimisticLockErrorexHandlerAdvice)
        }

    }

}