package com.evgenivanovi.adapthenics.spring.starter.http.autoconfigure

import com.evgenivanovi.adapthenics.api.contract.exchanger.ResponseAPIService
import com.evgenivanovi.adapthenics.spring.starter.http.ResponseBodyWrapper
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.web.reactive.accept.RequestedContentTypeResolver

/**
 * Not used due to ApiExchanger configuration
 */
@Configuration
open class ResponseConfiguration {

    @Bean
    @ConditionalOnBean(
        value = [
            ResponseAPIService::class,
            ServerCodecConfigurer::class,
            RequestedContentTypeResolver::class,
        ]
    )
    open fun responseBodyWrapper(
        responseAPIService: ResponseAPIService,
        serverCodecConfigurer: ServerCodecConfigurer,
        resolver: RequestedContentTypeResolver,
    ): ResponseBodyWrapper {
        return ResponseBodyWrapper(
            responseAPIService,
            serverCodecConfigurer.writers,
            resolver
        )
    }

}