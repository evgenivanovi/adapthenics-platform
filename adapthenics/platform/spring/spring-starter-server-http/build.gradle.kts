project(ProjectModules.SPRING_STARTER_SERVER_HTTP) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(project(ProjectModules.BASE))
        testImplementation(project(ProjectModules.BASE))

        implementation(project(ProjectModules.API))
        testImplementation(project(ProjectModules.API))

        implementation(project(ProjectModules.API_EXCHANGE))
        testImplementation(project(ProjectModules.API_EXCHANGE))

        implementation(project(ProjectModules.API_HANDLERS))
        testImplementation(project(ProjectModules.API_HANDLERS))

        implementation(project(ProjectModules.API_JACKSON))
        testImplementation(project(ProjectModules.API_JACKSON))

        implementation(project(ProjectModules.API_SEARCH_HANDLERS))
        testImplementation(project(ProjectModules.API_SEARCH_HANDLERS))

        implementation(project(ProjectModules.API_SEARCH__JACKSON))
        testImplementation(project(ProjectModules.API_SEARCH__JACKSON))

        implementation(project(ProjectModules.SPRING_CORE))
        testImplementation(project(ProjectModules.SPRING_CORE))

        implementation(project(ProjectModules.SPRING_WEBFLUX))
        testImplementation(project(ProjectModules.SPRING_WEBFLUX))

        implementation(project(ProjectModules.SPRING_STARTER_CORE))
        testImplementation(project(ProjectModules.SPRING_STARTER_CORE))

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.JACKSON_SUPPORT))
        testImplementation(project(ProjectModules.JACKSON_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(ktDependenciesCatalog.bundles.jackson.json.impl)

    implementation(libs.bundles.spring.core.impl)
    implementation(libs.bundles.spring.webflux.impl)

    implementation(searchCatalog.search)
    testImplementation(searchCatalog.search)
}
