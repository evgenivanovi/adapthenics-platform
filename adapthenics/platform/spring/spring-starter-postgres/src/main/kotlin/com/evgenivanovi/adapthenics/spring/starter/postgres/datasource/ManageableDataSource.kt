package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

/**
 * Datasource that should be started before usage and should be closed after.
 * @see javax.sql.DataSource
 */
interface ManageableDataSource : StartableDataSource, CloseableDataSource