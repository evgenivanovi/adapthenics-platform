package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import com.evgenivanovi.adapthenics.spring.starter.postgres.properties.GracefulShutdownProperties
import com.zaxxer.hikari.HikariConfig

class PlatformHikariConfig : HikariConfig {

    var shutdownProperties: GracefulShutdownProperties

    constructor(shutdownProperties: GracefulShutdownProperties) : super() {
        this.shutdownProperties = shutdownProperties
    }

}