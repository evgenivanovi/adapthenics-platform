package com.evgenivanovi.adapthenics.spring.starter.postgres.autoconfigure

import com.evgenivanovi.adapthenics.spring.starter.postgres.datasource.PlatformDataSource
import com.evgenivanovi.adapthenics.spring.starter.postgres.datasource.PlatformDataSourceFactory
import com.evgenivanovi.adapthenics.spring.starter.postgres.datasource.PlatformHikariConfigFactory
import com.evgenivanovi.adapthenics.spring.starter.postgres.datasource.PlatformHikariDataSourceFactory
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Conditional
import org.springframework.context.annotation.Configuration

@Configuration
@Conditional(
    value = [
        PostgresConditional::class
    ]
)
open class PostgresClientConfiguration {

    companion object {

        const val POSTGRES_PROPERTIES_PREFIX = "adapthenics.app.persistence.postgres"

    }

    @Bean
    @ConfigurationProperties(prefix = POSTGRES_PROPERTIES_PREFIX)
    open fun dataSourceProperties(): DataSourceProperties {
        return DataSourceProperties()
    }

    @Bean
    open fun platformHikariConfigFactory(): PlatformHikariConfigFactory {
        return PlatformHikariConfigFactory()
    }

    @Bean
    open fun platformDataSourceFactory(
        platformHikariConfigFactory: PlatformHikariConfigFactory,
    ): PlatformDataSourceFactory {
        return PlatformHikariDataSourceFactory(platformHikariConfigFactory)
    }

    @Bean(destroyMethod = "close")
    open fun platformDataSource(
        dataSourceProperties: DataSourceProperties,
        platformDataSourceFactory: PlatformDataSourceFactory,
    ): PlatformDataSource {
        return platformDataSourceFactory.create(dataSourceProperties)
    }

}