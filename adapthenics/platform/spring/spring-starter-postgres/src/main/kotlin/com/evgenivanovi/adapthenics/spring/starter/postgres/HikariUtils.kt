package com.evgenivanovi.adapthenics.spring.starter.postgres

import com.zaxxer.hikari.HikariDataSource
import io.github.oshai.kotlinlogging.KotlinLogging
import java.time.Duration
import java.util.concurrent.TimeUnit
import kotlin.math.min

object HikariUtils {

    private val log = KotlinLogging.logger {}

    @Throws(InterruptedException::class)
    fun awaitPoolFilled(
        datasource: HikariDataSource,
        timeout: Duration,
        backoff: Duration,
    ): Boolean {

        val pool = datasource.hikariPoolMXBean
        if (pool == null) {
            log.debug { "$datasource - cannot obtain HikariPoolMXBean" }
            return true
        }

        val config = datasource.hikariConfigMXBean
        if (config == null) {
            log.debug { "$datasource - cannot obtain HikariConfigMXBean" }
            return true
        }

        var remaining = timeout.toNanos()
        var period = backoff.toNanos()

        while (pool.totalConnections < config.minimumIdle && remaining > 0) {
            val start = System.nanoTime()

            period = min(period + backoff.toNanos(), remaining)
            log.debug { "$datasource - waiting for connections to be added..." }
            TimeUnit.NANOSECONDS.sleep(period)

            val end = System.nanoTime()
            remaining -= end - start
        }

        return pool.totalConnections >= config.minimumIdle

    }

    @Throws(InterruptedException::class)
    fun awaitConnectionIdle(
        datasource: HikariDataSource,
        timeout: Duration,
        backoff: Duration,
    ): Boolean {

        val pool = datasource.hikariPoolMXBean
        if (pool == null) {
            log.debug { "${"{} - cannot obtain HikariPoolMXBean"} $datasource" }
            return true
        }

        var remaining = timeout.toNanos()
        var period = backoff.toNanos()

        datasource.minimumIdle = 0
        while (pool.activeConnections > 0 && remaining > 0) {
            val start = System.nanoTime()

            pool.softEvictConnections()

            period = min(period + backoff.toNanos(), remaining)
            log.debug { "$datasource - waiting for connections to shutdown..." }
            TimeUnit.NANOSECONDS.sleep(period)

            val end = System.nanoTime()
            remaining -= end - start
        }

        return pool.activeConnections == 0

    }

    fun softEvictPool(datasource: HikariDataSource) {
        val pool = datasource.hikariPoolMXBean
        if (pool == null) {
            log.debug { "{} - cannot obtain HikariPoolMXBean $datasource" }
            return
        }
        pool.softEvictConnections()
    }

}