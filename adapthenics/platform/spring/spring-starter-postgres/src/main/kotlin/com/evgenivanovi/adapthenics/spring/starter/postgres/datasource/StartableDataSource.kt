package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import javax.sql.DataSource

/**
 * DataSource that should be started before usage.
 * @see javax.sql.DataSource
 */
interface StartableDataSource : DataSource {

    /**
     * Start datasource.
     * @throws DatasourceInitializationException if datasource cannot be started.
     */
    @Throws(DatasourceInitializationException::class)
    fun start()

    class DatasourceInitializationException : RuntimeException {

        constructor(message: String) : super(message)

        /**
         * Construct an exception, possibly wrapping the provided Throwable as the cause.
         * @param throwable the Throwable to wrap
         */
        constructor(throwable: Throwable)
            : super("Failed to initialize datasource: ${throwable.message}", throwable)

    }

}