package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import com.evgenivanovi.adapthenics.spring.starter.postgres.properties.PlainGracefulShutdownProperties
import com.zaxxer.hikari.HikariConfig
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import java.time.Duration

class PlatformHikariConfigFactory : HikariConfigFactory {

    override fun create(
        properties: DataSourceProperties,
    ): PlatformHikariConfig {
        val props = PlainGracefulShutdownProperties(
            true,
            Duration.ofSeconds(10),
            Duration.ofSeconds(2)
        )
        val config = PlatformHikariConfig(props)
        datasourceProperties(config, properties)
        hikariProperties(config, properties)
        return config
    }

    private fun datasourceProperties(config: HikariConfig, properties: DataSourceProperties) {
        config.username = properties.username
        config.password = properties.password

        config.jdbcUrl = properties.determineUrl()
        config.driverClassName = properties.determineDriverClassName()
    }

    private fun hikariProperties(config: HikariConfig, properties: DataSourceProperties) {
        config.maximumPoolSize = 8
    }

}