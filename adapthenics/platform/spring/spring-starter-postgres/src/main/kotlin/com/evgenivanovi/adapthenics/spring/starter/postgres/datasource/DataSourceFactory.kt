package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import javax.sql.DataSource

@FunctionalInterface
interface DataSourceFactory<T> {

    fun create(params: T): DataSource

}