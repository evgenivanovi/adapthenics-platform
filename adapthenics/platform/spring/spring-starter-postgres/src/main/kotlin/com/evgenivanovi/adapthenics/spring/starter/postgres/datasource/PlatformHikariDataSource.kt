package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import com.evgenivanovi.adapthenics.spring.starter.postgres.HikariUtils
import com.zaxxer.hikari.HikariDataSource
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging

class PlatformHikariDataSource : HikariDataSource, PlatformDataSource {

    private val config: PlatformHikariConfig

    private val log: KLogger = KotlinLogging.logger { }

    constructor(config: PlatformHikariConfig) : super(config) {
        this.config = config
    }

    override fun start() {
        //
    }

    override fun close() {
        gracefulClose()
        super.close()
    }

    private fun gracefulClose() {

        if (!config.shutdownProperties.isEnabled) {
            return
        }

        try {
            log.debug { "$poolName - gracefully shutting down..." }
            HikariUtils.awaitConnectionIdle(
                this,
                config.shutdownProperties.timeoutDuration,
                config.shutdownProperties.backoffDuration,
            )
            log.debug { "$poolName - gracefully shut down" }
        } catch (ex: InterruptedException) {
            log.warn { "$poolName - graceful shutdown was interrupted with exception $ex" }
            Thread.currentThread().interrupt()
        }

    }

}