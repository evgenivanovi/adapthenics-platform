package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties

class PlatformHikariDataSourceFactory(
    private val factory: PlatformHikariConfigFactory,
) : PlatformDataSourceFactory {

    override fun create(params: DataSourceProperties): PlatformDataSource {
        return PlatformHikariDataSource(
            factory.create(params)
        )
    }

}