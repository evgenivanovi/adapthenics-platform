package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import javax.sql.DataSource

interface CloseableDataSource : DataSource, AutoCloseable {

    override fun close()

}