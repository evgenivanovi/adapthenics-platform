package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties

@FunctionalInterface
interface PlatformDataSourceFactory : DataSourceFactory<DataSourceProperties> {

    override fun create(params: DataSourceProperties): PlatformDataSource

}