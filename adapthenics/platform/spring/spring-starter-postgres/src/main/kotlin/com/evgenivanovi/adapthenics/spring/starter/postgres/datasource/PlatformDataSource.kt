package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import javax.sql.DataSource

interface PlatformDataSource : DataSource, ManageableDataSource