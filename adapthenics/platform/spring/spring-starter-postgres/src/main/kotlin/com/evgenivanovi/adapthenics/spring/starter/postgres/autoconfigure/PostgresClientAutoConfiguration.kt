package com.evgenivanovi.adapthenics.spring.starter.postgres.autoconfigure

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(
    value = [
        PostgresClientConfiguration::class
    ]
)
open class PostgresClientAutoConfiguration