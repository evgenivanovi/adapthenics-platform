package com.evgenivanovi.adapthenics.spring.starter.postgres.datasource

import com.zaxxer.hikari.HikariConfig
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties

@FunctionalInterface
interface HikariConfigFactory {

    fun create(properties: DataSourceProperties): HikariConfig

}