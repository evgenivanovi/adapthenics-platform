package com.evgenivanovi.adapthenics.spring.starter.postgres.autoconfigure

import com.evgenivanovi.kt.lang.Booleans
import org.springframework.boot.autoconfigure.condition.AllNestedConditions
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.ConfigurationCondition.ConfigurationPhase.PARSE_CONFIGURATION

object PostgresConditional : AllNestedConditions(PARSE_CONFIGURATION) {

    @ConditionalOnProperty(
        name = [
            "adapthenics.app.persistence.mode"
        ],
        havingValue = "postgres",
        matchIfMissing = Booleans.FALSE
    )
    class EnabledConditional

}