package com.evgenivanovi.adapthenics.spring.starter.postgres.properties;

import java.time.Duration;

public final class PlainGracefulShutdownProperties implements GracefulShutdownProperties {

    private final boolean isEnabled;

    // Nullable
    private final Duration timeoutDuration;

    // Nullable
    private final Duration backoffDuration;

    public PlainGracefulShutdownProperties() {
        this(false, null, null);
    }

    public PlainGracefulShutdownProperties(final GracefulShutdownProperties props) {
        this(props.isEnabled(), props.getTimeoutDuration(), props.getBackoffDuration());
    }

    public PlainGracefulShutdownProperties(
        final boolean isEnabled,
        final Duration shutdownDuration,
        final Duration backoffDuration
    ) {

        if (isEnabled
            && !validateDuration(shutdownDuration)
            && !validateDuration(backoffDuration)
        ) {
            throw new IllegalArgumentException(
                "Shutdown is enabled but timeout or backoff is null or negative."
            );
        }

        this.isEnabled = isEnabled;
        this.timeoutDuration = shutdownDuration;
        this.backoffDuration = backoffDuration;

    }

    private boolean validateDuration(final Duration time) {
        return time != null && !time.isNegative();
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public Duration getTimeoutDuration() {
        return timeoutDuration;
    }

    @Override
    public Duration getBackoffDuration() {
        return backoffDuration;
    }

    public static PlainGracefulShutdownProperties empty() {
        return new PlainGracefulShutdownProperties(false, null, null);
    }

}
