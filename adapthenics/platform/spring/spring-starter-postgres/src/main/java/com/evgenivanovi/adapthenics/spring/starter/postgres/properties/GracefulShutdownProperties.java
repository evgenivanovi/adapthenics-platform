package com.evgenivanovi.adapthenics.spring.starter.postgres.properties;

import java.time.Duration;

public interface GracefulShutdownProperties {

    /**
     * Returns true if datasource should be shut down gracefully, otherwise false.
     */
    boolean isEnabled();

    /**
     * Returns the duration of time of waiting for datasource to be shut down gracefully.
     */
    Duration getTimeoutDuration();

    /**
     * Returns the duration of time of datasource graceful shutdown backoff.
     */
    Duration getBackoffDuration();

}
