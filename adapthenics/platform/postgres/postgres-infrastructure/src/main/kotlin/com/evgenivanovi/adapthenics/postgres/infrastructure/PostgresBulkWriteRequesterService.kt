package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.jooqx.JooqQuerier
import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresBulkWriteRequester
import kotlinx.coroutines.coroutineScope
import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.TableRecord
import org.jooq.UpdatableRecord

class PostgresBulkWriteRequesterService : PostgresBulkWriteRequester {

    override suspend fun softInsert(
        ctx: Configuration,
        records: Collection<TableRecord<*>>,
    ) = coroutineScope {
        return@coroutineScope doExecuteBulkSoftInsert(ctx.dsl(), records)
    }

    private fun doExecuteBulkSoftInsert(
        dsl: DSLContext,
        records: Collection<TableRecord<*>>,
    ) {

        val queries = records
            .map { record -> JooqQuerier.createSoftInsertQuery(dsl, record) }
            .map { query -> query.attach(dsl.configuration()); query }

        dsl
            .batch(queries)
            .execute()

        return

    }

    override suspend fun forceInsert(
        ctx: Configuration,
        records: Collection<TableRecord<*>>,
    ) = coroutineScope {
        return@coroutineScope doExecuteBulkForceInsert(ctx.dsl(), records)
    }

    private fun doExecuteBulkForceInsert(
        dsl: DSLContext,
        records: Collection<TableRecord<*>>,
    ) {

        records.map { it.attach(dsl.configuration()) }

        dsl
            .batchInsert(records)
            .execute()

        return

    }

    override suspend fun softUpdate(
        ctx: Configuration,
        records: Collection<UpdatableRecord<*>>,
    ) = coroutineScope {
        return@coroutineScope doExecuteBulkSoftUpdate(ctx.dsl(), records)
    }

    private fun doExecuteBulkSoftUpdate(
        dsl: DSLContext,
        records: Collection<UpdatableRecord<*>>,
    ) {

        val queries = records
            .map { record -> JooqQuerier.createSoftUpdateQuery(dsl, record) }
            .map { query -> query.attach(dsl.configuration()); query }

        dsl
            .batch(queries)
            .execute()

        return

    }

    override suspend fun forceUpdate(
        ctx: Configuration,
        records: Collection<UpdatableRecord<*>>,
    ) = coroutineScope {
        return@coroutineScope doExecuteBulkForceUpdate(ctx.dsl(), records)
    }

    private fun doExecuteBulkForceUpdate(
        dsl: DSLContext,
        records: Collection<UpdatableRecord<*>>,
    ) {

        val queries = records
            .map { record -> JooqQuerier.createForceUpdateQuery(record) }
            .map { query -> query.attach(dsl.configuration()); query }

        dsl
            .batch(queries)
            .execute()

        return

    }

}