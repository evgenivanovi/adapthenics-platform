package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.kt.coll.allOrFalse
import com.evgenivanovi.kt.lang.asNotNull
import com.evgenivanovi.kt.lang.cast
import org.jooq.Identity
import org.jooq.Record
import org.jooq.Table

internal fun <ID : Any> compatibleID(
    id: ID, table: Table<*>,
): Identity<out Record, ID> {
    checkCompatibility(id, table)
    return table.identity
        .asNotNull()
        .cast()
}

internal fun <ID : Any> compatibleID(
    ids: Collection<ID>, table: Table<*>,
): Identity<out Record, ID> {
    checkCompatibility(ids, table)
    return table.identity
        .asNotNull()
        .cast()
}

private fun <ID : Any> checkCompatibility(
    id: ID,
    table: Table<*>,
) {

    if (isTableCompatible(table).not())
        throw tableCompatibleException(table)

    val identity = table.identity.asNotNull()

    if (isIdentityCompatible(id, identity).not())
        throw identityCompatibleException(id, identity)

}

private fun <ID : Any> checkCompatibility(
    ids: Collection<ID>,
    table: Table<*>,
) {

    if (isTableCompatible(table).not())
        throw tableCompatibleException(table)

    val identity = table.identity.asNotNull()

    if (isIdentityCompatible(ids, identity).not())
        throw identityCompatibleException(ids, identity)

}

private fun isTableCompatible(
    table: Table<*>,
): Boolean {
    return table.identity != null
}

private fun tableCompatibleException(
    table: Table<*>,
): Exception {

    return IllegalArgumentException(
        "The table: '${table.name}' does not have a primary key."
    )

}

private fun <ID : Any> isIdentityCompatible(
    id: ID,
    identity: Identity<out Record, *>,
): Boolean {

    return identity
        .field
        .type
        .isInstance(id)

}

private fun <ID : Any> isIdentityCompatible(
    ids: Collection<ID>,
    identity: Identity<out Record, *>,
): Boolean {
    return ids.allOrFalse { id -> isIdentityCompatible(id, identity) }
}

private fun <ID : Any> identityCompatibleException(
    id: ID,
    identity: Identity<out Record, *>,
): Exception {

    return IllegalArgumentException(
        "The ID column with type: '${identity.field.dataType}'" +
            "does not respect provided id: '$id'."
    )

}

private fun <ID : Any> identityCompatibleException(
    id: Collection<ID>,
    identity: Identity<out Record, *>,
): Exception {

    return IllegalArgumentException(
        "The ID column with type: '${identity.field.dataType}'" +
            " does not respect provided ids: '${id.joinToString()}'."
    )

}