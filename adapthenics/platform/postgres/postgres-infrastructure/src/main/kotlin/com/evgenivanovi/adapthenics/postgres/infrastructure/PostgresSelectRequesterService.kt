package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresSelectRequester
import com.evgenivanovi.adapthenics.schema.model.api.Document
import io.github.oshai.kotlinlogging.KotlinLogging
import org.jooq.*
import org.jooq.conf.ParamType

class PostgresSelectRequesterService(
    private val dsl: DSLContext,
) : PostgresSelectRequester {

    private val log = KotlinLogging.logger {}

    private val configuration: Configuration = dsl.configuration()

    override suspend fun <T : Document<*>> executeSingle(
        query: SelectQuery<Record>,
        map: (Result<Record>) -> T,
    ): T? {
        return execute(query)
            .takeIf { it.isNotEmpty }
            ?.let(map)
            ?: return null
    }

    override suspend fun <T : Document<*>> executeMulti(
        query: SelectQuery<Record>,
        map: (Result<Record>) -> Collection<T>,
    ): Collection<T> {
        return execute(query)
            .takeIf { it.isNotEmpty }
            ?.let(map)
            ?: return emptyList()
    }

    override suspend fun execute(
        query: SelectQuery<Record>,
    ): Result<Record> {
        return doExecute(query)
    }

    private fun doExecute(query: SelectQuery<Record>): Result<Record> {
        query.attach(configuration)
        val sql = query.getSQL(ParamType.INLINED)
        log.debug { "Executing query: $sql" }
        return dsl.fetch(query)
    }

}