package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.jooqx.JooqQuerier
import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresWriteRequester
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.lang.isOf
import kotlinx.coroutines.coroutineScope
import org.jooq.*

class PostgresWriteRequesterService : PostgresWriteRequester {

    override suspend fun softInsert(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record = coroutineScope {
        if (record.isOf<UpdatableRecord<*>>()) {
            return@coroutineScope doExecuteSoftInsert(ctx.dsl(), record.asOf())
        }
        return@coroutineScope doExecuteSoftInsert(ctx.dsl(), record)
    }

    private fun doExecuteSoftInsert(
        dsl: DSLContext,
        record: TableRecord<*>,
    ): Record {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createSoftInsertQuery(dsl, record)
            .returning()
            .fetchSingle()

    }

    override suspend fun softInsert(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record = coroutineScope {
        return@coroutineScope doExecuteSoftInsert(ctx.dsl(), record)
    }

    private fun doExecuteSoftInsert(
        dsl: DSLContext,
        record: UpdatableRecord<*>,
    ): UpdatableRecord<*> {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createSoftInsertQuery(dsl, record)
            .returning()
            .fetchSingle()

    }

    override suspend fun forceInsert(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record = coroutineScope {
        if (record.isOf<UpdatableRecord<*>>()) {
            return@coroutineScope forceInsert(ctx, record.asOf())
        }
        return@coroutineScope doExecuteForceInsert(ctx.dsl(), record)
    }

    private fun doExecuteForceInsert(
        dsl: DSLContext,
        record: TableRecord<*>,
    ): Record {

        record.attach(dsl.configuration())

        return dsl
            .insertInto(record.table)
            .set(record)
            .returning()
            .fetchSingle()

    }

    override suspend fun forceInsert(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record = coroutineScope {
        return@coroutineScope doExecuteForceInsert(ctx.dsl(), record)
    }

    private fun doExecuteForceInsert(
        dsl: DSLContext,
        record: UpdatableRecord<*>,
    ): UpdatableRecord<*> {

        record.attach(dsl.configuration())

        return dsl
            .insertInto(record.table)
            .set(record)
            .returning()
            .fetchSingle()

    }

    override suspend fun softUpdate(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record? = coroutineScope {
        if (record.isOf<UpdatableRecord<*>>()) {
            return@coroutineScope softUpdate(ctx, record.asOf())
        }
        return@coroutineScope doExecuteSoftUpdate(ctx.dsl(), record)
    }

    private fun doExecuteSoftUpdate(
        dsl: DSLContext,
        record: TableRecord<*>,
    ): TableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createSoftUpdateQuery(dsl, record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun softUpdate(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record? = coroutineScope {
        return@coroutineScope doExecuteSoftUpdate(ctx.dsl(), record)
    }

    private fun doExecuteSoftUpdate(
        dsl: DSLContext,
        record: UpdatableRecord<*>,
    ): UpdatableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createSoftUpdateQuery(dsl, record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun forceUpdate(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record? = coroutineScope {
        if (record.isOf<UpdatableRecord<*>>()) {
            return@coroutineScope forceUpdate(ctx, record.asOf())
        }
        return@coroutineScope doExecuteForceUpdate(ctx.dsl(), record)
    }

    private fun doExecuteForceUpdate(
        dsl: DSLContext,
        record: TableRecord<*>,
    ): TableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createForceUpdateQuery(record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun forceUpdate(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record? = coroutineScope {
        return@coroutineScope doExecuteForceUpdate(ctx.dsl(), record)
    }

    private fun doExecuteForceUpdate(
        dsl: DSLContext,
        record: UpdatableRecord<*>,
    ): UpdatableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createForceUpdateQuery(record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun softMerge(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record? = coroutineScope {
        if (record.isOf<UpdatableRecord<*>>()) {
            return@coroutineScope softMerge(ctx, record.asOf())
        }
        return@coroutineScope doExecuteSoftMerge(ctx.dsl(), record)
    }

    private fun doExecuteSoftMerge(
        dsl: DSLContext,
        record: TableRecord<*>,
    ): TableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createSoftMergeQuery(dsl, record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun softMerge(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record? = coroutineScope {
        return@coroutineScope doExecuteSoftMerge(ctx.dsl(), record)
    }

    private fun doExecuteSoftMerge(
        dsl: DSLContext,
        record: UpdatableRecord<*>,
    ): UpdatableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createSoftMergeQuery(dsl, record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun forceMerge(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record? = coroutineScope {
        if (record.isOf<UpdatableRecord<*>>()) {
            return@coroutineScope forceMerge(ctx, record.asOf())
        }
        return@coroutineScope doExecuteForceMerge(ctx.dsl(), record)
    }

    private fun doExecuteForceMerge(
        dsl: DSLContext,
        record: TableRecord<*>,
    ): TableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createForceMergeQuery(record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun forceMerge(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record? = coroutineScope {
        return@coroutineScope doExecuteForceMerge(ctx.dsl(), record)
    }

    private fun doExecuteForceMerge(
        dsl: DSLContext,
        record: UpdatableRecord<*>,
    ): UpdatableRecord<*>? {

        record.attach(dsl.configuration())

        return JooqQuerier
            .createForceMergeQuery(record)
            .apply { attach(dsl.configuration()) }
            .returning()
            .fetchOne()

    }

    override suspend fun softMerge(
        ctx: Configuration,
        table: Table<*>,
        fields: Map<Field<*>, Any?>,
    ): Record? = coroutineScope {
        return@coroutineScope JooqQuerier
            .createSoftMergeQuery(ctx.dsl(), table, fields)
            .apply { attach(ctx.dsl().configuration()) }
            .returning()
            .fetchOne()
    }

    override suspend fun forceMerge(
        ctx: Configuration,
        table: Table<*>,
        fields: Map<Field<*>, Any?>,
    ): Record? = coroutineScope {
        return@coroutineScope JooqQuerier
            .createForceMergeQuery(table, fields)
            .apply { attach(ctx.dsl().configuration()) }
            .returning()
            .fetchOne()
    }

}