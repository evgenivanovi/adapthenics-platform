package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_FIELD
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_IS_NOT_NULL_CONDITION
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_IS_NULL_CONDITION
import com.evgenivanovi.adapthenics.jooqx.JooqTool
import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresBulkDeleteRequester
import com.evgenivanovi.kt.time.OffsetUTC
import org.jooq.*
import java.time.OffsetDateTime

class PostgresBulkDeleteRequesterService : PostgresBulkDeleteRequester {

    override suspend fun <ID : Any> delete(
        ctx: Configuration,
        ids: Collection<ID>,
        table: Table<*>,
    ): Result<out Record> {
        return doDelete(ctx.dsl(), table, ids)
    }

    override suspend fun delete(
        ctx: Configuration,
        condition: Condition,
        table: Table<*>,
    ): Result<out Record> {
        return doDelete(ctx.dsl(), table, condition)
    }

    override suspend fun <ID : Any> archive(
        ctx: Configuration,
        ids: Collection<ID>,
        table: Table<*>,
    ): Result<out Record> {
        return doArchive(ctx.dsl(), table, ids)
    }

    override suspend fun archive(
        ctx: Configuration,
        condition: Condition,
        table: Table<*>,
    ): Result<out Record> {
        return doArchive(ctx.dsl(), table, condition)
    }

    override suspend fun <ID : Any> unarchive(
        ctx: Configuration,
        ids: Collection<ID>,
        table: Table<*>,
    ): Result<out Record> {
        return doUnarchive(ctx.dsl(), table, ids)
    }

    override suspend fun unarchive(
        ctx: Configuration,
        condition: Condition,
        table: Table<*>,
    ): Result<out Record> {
        return doUnarchive(ctx.dsl(), table, condition)
    }

    /* __________________________________________________ */

    private fun <ID : Any> doDelete(
        dsl: DSLContext,
        table: Table<*>,
        ids: Collection<ID>,
    ): Result<out Record> {

        val identity = compatibleID(ids, table)

        val condition = JooqTool
            .identityCondition(ids, identity)

        return dsl
            .delete(table)
            .where(condition)
            .returning()
            .fetch()

    }

    private fun doDelete(
        dsl: DSLContext,
        table: Table<*>,
        condition: Condition,
    ): Result<out Record> {

        return dsl
            .delete(table)
            .where(condition)
            .returning()
            .fetch()

    }

    private fun <ID : Any> doArchive(
        dsl: DSLContext,
        table: Table<*>,
        ids: Collection<ID>,
    ): Result<out Record> {

        val identity = compatibleID(ids, table)

        val condition = JooqTool
            .identityCondition(ids, identity)
            .and(DELETED_AT_IS_NULL_CONDITION)

        return dsl
            .update(table)
            .set(DELETED_AT_FIELD, OffsetUTC.utc())
            .where(condition)
            .returning()
            .fetch()

    }

    private fun doArchive(
        dsl: DSLContext,
        table: Table<*>,
        condition: Condition,
    ): Result<out Record> {

        val conditions = condition
            .and(DELETED_AT_IS_NULL_CONDITION)

        return dsl
            .update(table)
            .set(DELETED_AT_FIELD, OffsetUTC.utc())
            .where(conditions)
            .returning()
            .fetch()

    }

    private fun <ID : Any> doUnarchive(
        dsl: DSLContext,
        table: Table<*>,
        ids: Collection<ID>,
    ): Result<out Record> {

        val identity = compatibleID(ids, table)

        val condition = JooqTool
            .identityCondition(ids, identity)
            .and(DELETED_AT_IS_NOT_NULL_CONDITION)

        val deleted: OffsetDateTime? = null

        return dsl
            .update(table)
            .set(DELETED_AT_FIELD, deleted)
            .where(condition)
            .returning()
            .fetch()

    }

    private fun doUnarchive(
        dsl: DSLContext,
        table: Table<*>,
        condition: Condition,
    ): Result<out Record> {

        val conditions = condition
            .and(DELETED_AT_IS_NOT_NULL_CONDITION)

        val deleted: OffsetDateTime? = null

        return dsl
            .update(table)
            .set(DELETED_AT_FIELD, deleted)
            .where(conditions)
            .returning()
            .fetch()

    }

}