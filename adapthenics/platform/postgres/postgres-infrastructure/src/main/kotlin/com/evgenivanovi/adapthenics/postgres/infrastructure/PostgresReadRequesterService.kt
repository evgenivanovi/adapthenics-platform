package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresReadRequester
import io.github.oshai.kotlinlogging.KotlinLogging
import org.jooq.*
import org.jooq.conf.ParamType

class PostgresReadRequesterService(
    private val dsl: DSLContext,
) : PostgresReadRequester {

    private val log = KotlinLogging.logger {}

    private val configuration: Configuration = dsl.configuration()

    override suspend fun findMany(
        query: SelectQuery<*>,
    ): Result<Record> {
        return doExecuteMany(dsl, query)
    }

    private fun doExecuteMany(
        dsl: DSLContext,
        query: SelectQuery<*>,
    ): Result<Record> {
        query.attach(configuration)
        val sql = query.getSQL(ParamType.INLINED)
        log.debug { "Executing query: $sql" }
        return dsl.fetch(sql)
    }

    override suspend fun findOne(
        query: SelectQuery<*>,
    ): Result<Record> {
        return doExecuteOne(dsl, query)
    }

    private fun doExecuteOne(
        dsl: DSLContext,
        query: SelectQuery<*>,
    ): Result<Record> {
        query.attach(configuration)
        val sql = query.getSQL(ParamType.INLINED)
        log.debug { "Executing query: $sql" }
        return dsl.fetch(sql)
    }

    override suspend fun findCount(
        query: SelectQuery<*>,
    ): Long {
        return doExecuteCount(dsl, query)
    }

    private fun doExecuteCount(
        dsl: DSLContext,
        query: SelectQuery<*>,
    ): Long {
        query.attach(configuration)
        return dsl
            .fetchCount(query)
            .toLong()
    }

}