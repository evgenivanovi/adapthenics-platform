package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_FIELD
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_IS_NOT_NULL_CONDITION
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_IS_NULL_CONDITION
import com.evgenivanovi.adapthenics.jooqx.JooqTool
import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresDeleteRequester
import com.evgenivanovi.kt.time.OffsetUTC
import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.Table
import java.time.OffsetDateTime

class PostgresDeleteRequesterService : PostgresDeleteRequester {

    override suspend fun <ID : Any> delete(
        ctx: Configuration,
        id: ID,
        table: Table<*>,
    ): Record? {
        return doDelete(ctx.dsl(), table, id)
    }

    override suspend fun <ID : Any> archive(
        ctx: Configuration,
        id: ID,
        table: Table<*>,
    ): Record? {
        return doArchive(ctx.dsl(), table, id)
    }

    override suspend fun <ID : Any> unarchive(
        ctx: Configuration, id: ID, table: Table<*>
    ): Record? {
        return doUnarchive(ctx.dsl(), table, id)
    }

    private fun <ID : Any> doDelete(
        dsl: DSLContext,
        table: Table<*>,
        id: ID,
    ): Record? {

        val identity = compatibleID(id, table)

        val condition = JooqTool
            .identityCondition(id, identity)

        return dsl
            .delete(table)
            .where(condition)
            .returning()
            .fetchOne()

    }

    private fun <ID : Any> doArchive(
        dsl: DSLContext,
        table: Table<*>,
        id: ID,
    ): Record? {

        val identity = compatibleID(id, table)

        val condition = JooqTool
            .identityCondition(id, identity)
            .and(DELETED_AT_IS_NULL_CONDITION)

        return dsl
            .update(table)
            .set(DELETED_AT_FIELD, OffsetUTC.utc())
            .where(condition)
            .returning()
            .fetchOne()

    }

    private fun <ID : Any> doUnarchive(
        dsl: DSLContext,
        table: Table<*>,
        id: ID,
    ): Record? {

        val identity = compatibleID(id, table)

        val condition = JooqTool
            .identityCondition(id, identity)
            .and(DELETED_AT_IS_NOT_NULL_CONDITION)

        val deleted: OffsetDateTime? = null

        return dsl
            .update(table)
            .set(DELETED_AT_FIELD, deleted)
            .where(condition)
            .returning()
            .fetchOne()

    }

}