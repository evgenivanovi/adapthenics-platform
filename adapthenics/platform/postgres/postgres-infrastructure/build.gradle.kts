project(ProjectModules.POSTGRES_INFRASTRUCTURE) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(jsCatalog.jsApi)
        testImplementation(jsCatalog.jsApi)

        implementation(jsCatalog.jsSpec)
        testImplementation(jsCatalog.jsSpec)

        implementation(searchCatalog.searchQuery)
        testImplementation(searchCatalog.searchQuery)

        implementation(project(ProjectModules.BASE))
        testImplementation(project(ProjectModules.BASE))

        implementation(project(ProjectModules.JOOQX))
        testImplementation(project(ProjectModules.JOOQX))

        implementation(project(ProjectModules.POSTGRES_ADAPTER))
        testImplementation(project(ProjectModules.POSTGRES_ADAPTER))

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.LOGGING_SUPPORT))
        testImplementation(project(ProjectModules.LOGGING_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(ktDependenciesCatalog.bundles.jooq.impl)

    runtimeOnly(libs.bundles.postgres.runtime)
    testImplementation(ktDependenciesCatalog.bundles.testcontainers.postgres.impl)

    testImplementation(tools.envotur.impl)
    testImplementation(tools.envotur.junit.impl)
    testImplementation(tools.envotur.postgres.impl)
    testImplementation(tools.envotur.postgres.junit.impl)
}