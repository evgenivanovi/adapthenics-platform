CREATE TABLE IF NOT EXISTS "notes"
(
    "id"          BIGSERIAL                NOT NULL,
    "version"     BIGINT                   NOT NULL,
    "name"        VARCHAR                  NOT NULL,
    "description" VARCHAR                  NOT NULL,
    "created_at"  TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_at"  TIMESTAMP WITH TIME ZONE NULL,
    "deleted_at"  TIMESTAMP WITH TIME ZONE NULL
);

ALTER TABLE IF EXISTS "notes"
    ADD CONSTRAINT "PK_T_NOTES_C_ID"
        PRIMARY KEY ("id");

-- __________________________________________________ --

CREATE TABLE IF NOT EXISTS "notes_no_meta"
(
    "id"          BIGSERIAL NOT NULL,
    "name"        VARCHAR   NOT NULL,
    "description" VARCHAR   NOT NULL
);

ALTER TABLE IF EXISTS "notes_no_meta"
    ADD CONSTRAINT "PK_T_NOTES_NO_META_C_ID"
        PRIMARY KEY ("id");

-- __________________________________________________ --

CREATE TABLE IF NOT EXISTS "notes_no_key_no_meta"
(
    "name"        VARCHAR NOT NULL,
    "description" VARCHAR NOT NULL
);