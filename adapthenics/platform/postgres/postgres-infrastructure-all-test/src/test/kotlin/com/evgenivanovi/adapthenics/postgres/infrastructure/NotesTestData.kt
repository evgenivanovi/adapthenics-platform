@file:Suppress(
    "TestFunctionName"
)

package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesNoMetaRecord
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesRecord
import com.evgenivanovi.kt.time.OffsetUTC

object NotesTestData {

    const val NOTE_1_ID = 1L

    fun NOTE_1_WITH_ID(): NotesRecord = NotesRecord()
        .setId(NOTE_1_ID)
        .setVersion(0L)
        .setName("NOTE_1")
        .setDescription("NOTE_1_DESCRIPTION")
        .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setDeletedAt(null)

    fun NOTE_1_WITHOUT_ID(): NotesRecord = NotesRecord()
        .setVersion(0L)
        .setName("NOTE_1")
        .setDescription("NOTE_1_DESCRIPTION")
        .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setDeletedAt(null)

    const val NOTE_2_ID = 2L

    fun NOTE_2_WITH_ID() = NotesRecord()
        .setId(NOTE_2_ID)
        .setVersion(0L)
        .setName("NOTE_2")
        .setDescription("NOTE_2_DESCRIPTION")
        .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setDeletedAt(null)

    fun NOTE_2_WITHOUT_ID() = NotesRecord()
        .setVersion(0L)
        .setName("NOTE_2")
        .setDescription("NOTE_2_DESCRIPTION")
        .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
        .setDeletedAt(null)

    const val NOTE_NO_META_1_ID = 1L

    fun NOTE_NO_META_1() = NotesNoMetaRecord()
        .setId(NOTE_NO_META_1_ID)
        .setName("NOTE_1")
        .setDescription("NOTE_1_DESCRIPTION")

    const val NOTE_NO_META_2_ID = 2L

    fun NOTE_NO_META_2() = NotesNoMetaRecord()
        .setId(NOTE_NO_META_2_ID)
        .setName("NOTE_2")
        .setDescription("NOTE_2_DESCRIPTION")

}