@file:Suppress(
    "LocalVariableName"
)

package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.envotur.pg.extension.PostgresGlobalExtension
import com.evgenivanovi.adapthenics.postgres.infrastructure.NotesTestData.NOTE_1_ID
import com.evgenivanovi.adapthenics.postgres.infrastructure.NotesTestData.NOTE_2_ID
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.Notes
import kotlinx.coroutines.runBlocking
import org.jooq.DSLContext
import org.jooq.tools.jdbc.SingleConnectionDataSource
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import java.sql.Connection
import javax.sql.DataSource

class PostgresBulkDeleteRequesterTest {

    companion object {

        private val POSTGRES: Envo = Envo("postgres", 5432)

        private val SCHEMA_PATHS: List<String> = listOf(
            "classpath:migration"
        )

        @RegisterExtension
        val extension = PostgresGlobalExtension.extension()
            .withOptions { options ->
                options
                    .withGitlabLocation("../../../../.gitlab-ci.yml")
                    .withGitlabJob("test")
                    .withEnvo(POSTGRES)
            }
            .withSchemaLocations(SCHEMA_PATHS)
            .build()

    }

    private lateinit var dsl: DSLContext

    private lateinit var bulkDelete: PostgresBulkDeleteRequesterService

    private lateinit var connection: Connection

    private lateinit var datasource: DataSource

    /* __________________________________________________ */

    @BeforeEach
    fun beforeAll() {
        initDatasource()
        initDSL()
        initRequesters()
    }

    @AfterEach
    fun afterAll() {
        dsl.clearDatabase()
        connection.close()
    }

    /* __________________________________________________ */

    private fun initDatasource() {

        connection = DatabaseTool
            .containerConnection(extension.runtime())

        datasource =
            SingleConnectionDataSource(connection)

    }

    private fun initDSL() {
        dsl = initDSLContext()
    }

    private fun initDSLContext(): DSLContext {
        return DatabaseTool.dsl(datasource)
    }

    private fun initRequesters() {
        bulkDelete = PostgresBulkDeleteRequesterService()
    }

    /* __________________________________________________ */

    @Test
    fun `should delete by ids`() = runBlocking {

        // given
        val RECORD_TABLE = Notes.NOTES

        val RECORD_1_ID = NOTE_1_ID
        val RECORD_1 = NotesTestData.NOTE_1_WITH_ID()

        val RECORD_2_ID = NOTE_2_ID
        val RECORD_2 = NotesTestData.NOTE_2_WITH_ID()

        val RECORD_IDS = listOf(RECORD_1_ID, RECORD_2_ID)

        // init
        insertRecords(dsl, RECORD_1, RECORD_2)

        // when
        val RESULT = bulkDelete
            .delete(dsl.configuration(), RECORD_IDS, RECORD_TABLE)

        val QUERY_AFTER = dsl
            .select()
            .from(RECORD_TABLE)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val RESULT_AFTER = dsl.fetch(QUERY_AFTER)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT.isNotEmpty)
            },
            {
                assertTrue(RESULT.size == 2)
            },
            {
                assertTrue(RESULT_AFTER.isEmpty())
            }
        )

    }

    @Test
    fun `should delete by condition`() = runBlocking {

        // given
        val RECORD_TABLE = Notes.NOTES

        val RECORD_1_ID = NOTE_1_ID
        val RECORD_1 = NotesTestData.NOTE_1_WITH_ID()

        val RECORD_2_ID = NOTE_2_ID
        val RECORD_2 = NotesTestData.NOTE_2_WITH_ID()

        val RECORD_IDS = listOf(RECORD_1_ID, RECORD_2_ID)

        // init
        insertRecords(dsl, RECORD_1, RECORD_2)

        // when
        val CONDITION = Notes.NOTES.ID.`in`(RECORD_IDS)

        val RESULT = bulkDelete
            .delete(dsl.configuration(), CONDITION, RECORD_TABLE)

        val QUERY_AFTER = dsl
            .select()
            .from(RECORD_TABLE)
            .where(CONDITION)
            .query

        val RESULT_AFTER = dsl.fetch(QUERY_AFTER)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT.isNotEmpty)
            },
            {
                assertTrue(RESULT.size == 2)
            },
            {
                assertTrue(RESULT_AFTER.isEmpty())
            }
        )

    }

    @Test
    fun `should archive by ids`() = runBlocking {

        // given
        val RECORD_TABLE = Notes.NOTES

        val RECORD_1_ID = NOTE_1_ID
        val RECORD_1 = NotesTestData.NOTE_1_WITH_ID()

        val RECORD_2_ID = NOTE_2_ID
        val RECORD_2 = NotesTestData.NOTE_2_WITH_ID()

        val RECORDS = listOf(RECORD_1_ID, RECORD_2_ID)

        // init
        insertRecords(dsl, RECORD_1, RECORD_2)

        // when
        val RESULT = bulkDelete
            .archive(dsl.configuration(), RECORDS, RECORD_TABLE)

        val QUERY_AFTER = dsl
            .select()
            .from(RECORD_TABLE)
            .where(Notes.NOTES.ID.`in`(RECORDS))
            .query

        val RESULT_AFTER = dsl.fetch(QUERY_AFTER)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT.isNotEmpty)
            },
            {
                assertTrue(RESULT.size == 2)
            },
            {
                assertTrue(RESULT_AFTER.isNotEmpty)
            },
            {
                assertTrue(RESULT_AFTER.size == 2)
            }
        )

    }

    @Test
    fun `should archive by condition`() = runBlocking {

        // given
        val RECORD_TABLE = Notes.NOTES

        val RECORD_1_ID = NOTE_1_ID
        val RECORD_1 = NotesTestData.NOTE_1_WITH_ID()

        val RECORD_2_ID = NOTE_2_ID
        val RECORD_2 = NotesTestData.NOTE_2_WITH_ID()

        val RECORDS = listOf(RECORD_1_ID, RECORD_2_ID)

        // init
        insertRecords(dsl, RECORD_1, RECORD_2)

        // when
        val CONDITION = Notes.NOTES.ID.`in`(RECORDS)

        val RESULT = bulkDelete
            .archive(dsl.configuration(), CONDITION, RECORD_TABLE)

        val QUERY_AFTER = dsl
            .select()
            .from(RECORD_TABLE)
            .where(CONDITION)
            .query

        val RESULT_AFTER = dsl.fetch(QUERY_AFTER)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT.isNotEmpty)
            },
            {
                assertTrue(RESULT.size == 2)
            },
            {
                assertTrue(RESULT_AFTER.isNotEmpty)
            },
            {
                assertTrue(RESULT_AFTER.size == 2)
            }
        )

    }

    @Test
    fun `given incompatible id, when delete, should throw exception`() = runBlocking {

        // given
        val IDS = listOf("TEST_INVALID_ID")

        // when
        val ACTION = {
            runBlocking {
                bulkDelete.delete(dsl.configuration(), IDS, Notes.NOTES)
            }
        }

        // then
        Assertions.assertAll(
            {
                assertThrows(IllegalArgumentException::class.java) { ACTION.invoke() }
            }
        )

    }

    @Test
    fun `given incompatible id, when archive, should throw exception`() = runBlocking {

        // given
        val IDS = listOf("TEST_INVALID_ID")

        // when
        val ACTION = {
            runBlocking {
                bulkDelete.archive(dsl.configuration(), IDS, Notes.NOTES)
            }
        }

        // then
        Assertions.assertAll(
            {
                assertThrows(IllegalArgumentException::class.java) { ACTION.invoke() }
            }
        )

    }

}