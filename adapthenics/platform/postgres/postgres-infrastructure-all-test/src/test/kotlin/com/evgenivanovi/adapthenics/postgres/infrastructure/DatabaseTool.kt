package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.envotur.pg.Runtime
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.conf.Settings
import org.jooq.impl.DSL
import org.jooq.impl.DefaultConfiguration
import org.jooq.impl.DefaultConnectionProvider
import org.jooq.impl.DefaultTransactionProvider
import org.jooq.tools.jdbc.SingleConnectionDataSource
import org.testcontainers.containers.PostgreSQLContainer
import java.sql.Connection
import java.sql.DriverManager
import javax.sql.DataSource

object DatabaseTool {

    fun createDatasource(
        url: String,
        username: String,
        password: String,
    ): DataSource {
        val connection: Connection = openConnection(url, username, password)
        return SingleConnectionDataSource(connection)
    }

    fun openConnection(
        url: String,
        username: String,
        password: String,
    ): Connection {
        return DriverManager
            .getConnection(url, username, password)
    }

    fun closeConnection(connection: Connection) {
        connection.close()
    }

    fun containerConnection(cfg: Runtime): Connection {
        return openConnection(
            cfg.url(),
            cfg.user(),
            cfg.password()
        )
    }

    fun containerConnection(container: PostgreSQLContainer<*>): Connection {
        return openConnection(
            container.jdbcUrl,
            container.username,
            container.password
        )
    }

    fun containerDatasource(runtime: Runtime): SingleConnectionDataSource {
        val connection: Connection = containerConnection(runtime)
        return SingleConnectionDataSource(connection)
    }

    fun containerDatasource(container: PostgreSQLContainer<*>): SingleConnectionDataSource {
        val connection: Connection = containerConnection(container)
        return SingleConnectionDataSource(connection)
    }

    fun dsl(connection: Connection): DSLContext {
        return DSL.using(connection)
    }

    fun dsl(datasource: DataSource): DSLContext {

        val connection = DefaultConnectionProvider(datasource.connection)
        val transaction = DefaultTransactionProvider(connection)

        val settings = Settings()
            // Whether UpdatableRecord instances should modify the record version prior to storing the record.
            // This feature is independent of, but related to optimistic locking.
            .withUpdateRecordVersion(false)
            // Whether UpdatableRecord instances should modify the record timestamp prior to storing the record.
            // This feature is independent of, but related to optimistic locking.
            .withUpdateRecordTimestamp(false)
            // This allows for turning off the feature entirely.
            .withExecuteWithOptimisticLocking(false)
            // This allows for turning off the feature for updatable records who are not explicitly versioned.
            .withExecuteWithOptimisticLockingExcludeUnversioned(false)

        val configuration = DefaultConfiguration()
            .apply { setSQLDialect(SQLDialect.POSTGRES) }
            .apply { setConnection(datasource.connection) }
            .apply { setConnectionProvider(connection) }
            .apply { setDataSource(datasource) }
            .apply { setTransactionProvider(transaction) }
            .apply { setSettings(settings) }

        return DSL.using(configuration)

    }

}