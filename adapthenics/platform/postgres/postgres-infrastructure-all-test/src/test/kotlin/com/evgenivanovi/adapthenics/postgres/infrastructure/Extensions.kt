package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.jooqx.JooqTool
import com.evgenivanovi.kt.coll.pairs
import com.evgenivanovi.kt.time.LocalUTC
import com.evgenivanovi.kt.time.OffsetUTC
import org.jooq.*
import java.time.LocalDateTime
import java.time.OffsetDateTime

/* __________________________________________________ */

fun Record.intoMap(
    vararg exclude: String,
): Map<String, Any?> {
    return JooqTool.intoMap(this, *exclude)
}

fun Record.intoFieldValuesMap(
    vararg exclude: String,
): Map<Field<*>, Any?> {
    return JooqTool.intoFieldValuesMap(this, *exclude)
}

fun Record.intoChangedFieldValuesMap(
    vararg exclude: String,
): Map<Field<*>, Any?> {
    return JooqTool.intoChangedFieldValuesMap(this, *exclude)
}

/* __________________________________________________ */

fun Map<String, Any?>.utcMap(): Map<String, Any?> {
    return pairs().associate(::inUtc)
}

fun Map<Field<*>, Any?>.utcFieldMap(): Map<Field<*>, Any?> {
    return pairs().associate(::inFieldUtc)
}

private fun inUtc(entry: Pair<String, Any?>): Pair<String, Any?> {
    return entry.first to inValueUtc(entry.second)
}

private fun inFieldUtc(entry: Pair<Field<*>, Any?>): Pair<Field<*>, Any?> {
    return entry.first to inValueUtc(entry.second)
}

private fun inValueUtc(value: Any?): Any? {

    var result = value

    if (value != null && value is OffsetDateTime) {
        result = OffsetUTC.utc(value)
    }

    if (value != null && value is LocalDateTime) {
        result = LocalUTC.utc(value)
    }

    return result

}

/* __________________________________________________ */

fun <R : TableRecord<out R>> insertRecords(
    dsl: DSLContext,
    records: Collection<R>,
) {
    records.forEach { insertRecord(dsl, it) }
}

fun <R : TableRecord<out R>> insertRecords(
    dsl: DSLContext,
    vararg records: R,
) {
    records.forEach { insertRecord(dsl, it) }
}

fun <R : TableRecord<out R>> insertRecord(
    dsl: DSLContext,
    record: R,
) {
    dsl
        .insertInto(record.table)
        .set(record)
        .returning()
        .fetchOne()
}

/* __________________________________________________ */

fun DSLContext.clearDatabase() {

    val exclude = listOf(
        "information_schema",
        "pg_catalog",
        "flyway_schema_history",
    )

    val thiz = this

    thiz
        .meta()
        .tables
        .filter { table -> table.tableType.isTable }
        .filterNot { table -> table.qualifiedName.name.any { name -> exclude.contains(name) } }
        .forEach { tryCatch { thiz.clearTable(it) } }

}

fun DSLContext.clearTable(table: Table<*>) {
    this
        .truncate(table)
        .restartIdentity()
        .cascade()
        .execute()
}

/* __________________________________________________ */

fun tryCatch(
    block: () -> Unit,
) {
    try {
        block()
    } catch (e: Exception) {
        // ignore
    }
}