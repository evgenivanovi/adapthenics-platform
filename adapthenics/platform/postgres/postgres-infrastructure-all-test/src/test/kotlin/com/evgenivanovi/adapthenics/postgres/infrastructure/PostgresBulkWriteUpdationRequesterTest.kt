@file:Suppress(
    "LocalVariableName"
)

package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.envotur.pg.extension.PostgresGlobalExtension
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.Notes
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesRecord
import com.evgenivanovi.kt.time.OffsetUTC
import kotlinx.coroutines.runBlocking
import org.jooq.DSLContext
import org.jooq.tools.jdbc.SingleConnectionDataSource
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.RegisterExtension
import java.sql.Connection
import javax.sql.DataSource

class PostgresBulkWriteUpdationRequesterTest {

    companion object {

        private val POSTGRES: Envo = Envo("postgres", 5432)

        private val SCHEMA_PATHS: List<String> = listOf(
            "classpath:migration"
        )

        @RegisterExtension
        val extension = PostgresGlobalExtension.extension()
            .withOptions { options ->
                options
                    .withGitlabLocation("../../../../.gitlab-ci.yml")
                    .withGitlabJob("test")
                    .withEnvo(POSTGRES)
            }
            .withSchemaLocations(SCHEMA_PATHS)
            .build()

    }

    private lateinit var dsl: DSLContext

    private lateinit var bulkWrite: PostgresBulkWriteRequesterService

    private lateinit var connection: Connection

    private lateinit var datasource: DataSource

    /* __________________________________________________ */

    @BeforeEach
    fun beforeEach() {
        initDatasource()
        initDSL()
        initRequesters()
    }

    @AfterEach
    fun afterEach() {
        dsl.clearDatabase()
        connection.close()
    }

    /* __________________________________________________ */

    private fun initDatasource() {

        connection = DatabaseTool
            .containerConnection(extension.runtime())

        datasource =
            SingleConnectionDataSource(connection)

    }

    private fun initDSL() {
        dsl = initDSLContext()
    }

    private fun initDSLContext(): DSLContext {
        return DatabaseTool.dsl(datasource)
    }

    private fun initRequesters() {
        bulkWrite = PostgresBulkWriteRequesterService()
    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing records to update;" +
            " when soft update;" +
            " should not update;"
    )
    fun test1() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_TO_UPDATE_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_ID_2 = 1L
        val RECORD_VERSION_2 = 0L

        val RECORD_TO_UPDATE_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2,
        )

        val RECORDS_TO_UPDATE = listOf(
            RECORD_TO_UPDATE_1,
            RECORD_TO_UPDATE_2,
        )

        // when
        bulkWrite.softUpdate(dsl.configuration(), RECORDS_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isEmpty())
            }
        )

    }

    @Test
    @DisplayName(
        "given existing record, record to update with different version;" +
            " when soft update;" +
            " should not update;"
    )
    fun test2() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_ID_2 = 2L
        val RECORD_VERSION_2 = 0L

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2,
        )

        val RECORD_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORDS = listOf(
            RECORD_1,
            RECORD_2,
        )

        val RECORD1_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(999L)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD2_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(999L)
            .setName("NOTE_2_NEW")
            .setDescription("NOTE_2_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORDS_TO_UPDATE = listOf(
            RECORD1_TO_UPDATE,
            RECORD2_TO_UPDATE
        )

        insertRecords(dsl, RECORDS)

        // when
        bulkWrite.softUpdate(dsl.configuration(), RECORDS_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(
                    RECORDS[0].intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORDS[1].intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given existing records with deleted_at, records to update;" +
            " when soft update;" +
            " should not update;"
    )
    fun test3() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_ID_2 = 2L
        val RECORD_VERSION_2 = 0L

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2,
        )

        val RECORD_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))

        val RECORD_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))

        val RECORDS = listOf(
            RECORD_1,
            RECORD_2,
        )

        val RECORD1_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD2_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2_NEW")
            .setDescription("NOTE_2_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORDS_TO_UPDATE = listOf(
            RECORD1_TO_UPDATE,
            RECORD2_TO_UPDATE
        )

        insertRecords(dsl, RECORDS)

        // when
        bulkWrite.softUpdate(dsl.configuration(), RECORDS_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(
                    RECORDS[0].intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORDS[1].intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given existing records, records to update with unneeded meta fields;" +
            " when soft update;" +
            " should update without unneeded meta fields;"
    )
    fun test4() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_ID_2 = 2L
        val RECORD_VERSION_2 = 0L

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2,
        )

        val RECORD_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORDS = listOf(
            RECORD_1,
            RECORD_2,
        )

        val RECORD1_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            // unneeded fields
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD2_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2_NEW")
            .setDescription("NOTE_2_DESCRIPTION_NEW")
            // unneeded fields
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORDS_TO_UPDATE = listOf(
            RECORD1_TO_UPDATE,
            RECORD2_TO_UPDATE
        )

        insertRecords(dsl, RECORDS)

        // when
        bulkWrite.softUpdate(dsl.configuration(), RECORDS_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertNotEquals(
                    RECORDS[0].intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORDS[1].intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {

                val expectedVersion = RECORDS[0]
                    .getValue(Notes.NOTES.VERSION)
                    .inc()

                val actualVersion = QUERY_RESULT[0].component1()
                    ?.getValue(Notes.NOTES.VERSION)

                assertEquals(expectedVersion, actualVersion)

            },
            {

                val expectedVersion = RECORDS[1]
                    .getValue(Notes.NOTES.VERSION)
                    .inc()

                val actualVersion = QUERY_RESULT[1].component1()
                    ?.getValue(Notes.NOTES.VERSION)

                assertEquals(expectedVersion, actualVersion)

            },
            {

                val expectedUpdatedAt = RECORDS[0]
                    .getValue(Notes.NOTES.UPDATED_AT)

                val actualUpdatedAt = QUERY_RESULT[0].component1()
                    ?.getValue(Notes.NOTES.UPDATED_AT)

                val matcher = { actualUpdatedAt?.isAfter(expectedUpdatedAt) ?: false }

                assertTrue(matcher.invoke())

            },
            {

                val expectedUpdatedAt = RECORDS[1]
                    .getValue(Notes.NOTES.UPDATED_AT)

                val actualUpdatedAt = QUERY_RESULT[1].component1()
                    ?.getValue(Notes.NOTES.UPDATED_AT)

                val matcher = { actualUpdatedAt?.isAfter(expectedUpdatedAt) ?: false }

                assertTrue(matcher.invoke())

            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.NAME.name,
                    Notes.NOTES.DESCRIPTION.name,
                )
                assertEquals(
                    RECORDS[0].intoMap(*excludes).utcMap(),
                    QUERY_RESULT[0].component1().intoMap(*excludes).utcMap()
                )
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.NAME.name,
                    Notes.NOTES.DESCRIPTION.name,
                )
                assertEquals(
                    RECORDS[1].intoMap(*excludes).utcMap(),
                    QUERY_RESULT[1].component1().intoMap(*excludes).utcMap()
                )
            }
        )

    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing records to update;" +
            " when force update;" +
            " should not update;"
    )
    fun test5() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_TO_UPDATE_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_ID_2 = 1L
        val RECORD_VERSION_2 = 0L

        val RECORD_TO_UPDATE_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2,
        )

        val RECORDS_TO_UPDATE = listOf(
            RECORD_TO_UPDATE_1,
            RECORD_TO_UPDATE_2,
        )

        // when
        bulkWrite.forceUpdate(dsl.configuration(), RECORDS_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isEmpty())
            }
        )

    }

    @Test
    @DisplayName(
        "given existing records, records to update;" +
            " when force update;" +
            " should update all fields;"
    )
    fun test6() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_ID_2 = 2L
        val RECORD_VERSION_2 = 0L

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2,
        )

        val RECORD_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORDS = listOf(
            RECORD_1,
            RECORD_2,
        )

        val RECORD1_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD2_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2_NEW")
            .setDescription("NOTE_2_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORDS_TO_UPDATE = listOf(
            RECORD1_TO_UPDATE,
            RECORD2_TO_UPDATE
        )

        val RECORD1_TO_UPDATE_COPY = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD2_TO_UPDATE_COPY = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2_NEW")
            .setDescription("NOTE_2_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORDS_TO_UPDATE_COPY = listOf(
            RECORD1_TO_UPDATE_COPY,
            RECORD2_TO_UPDATE_COPY
        )

        insertRecords(dsl, RECORDS)

        // when
        bulkWrite.forceUpdate(dsl.configuration(), RECORDS_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertNotEquals(
                    RECORDS[0].intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORDS[1].intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.NAME.name,
                    Notes.NOTES.DESCRIPTION.name,
                )
                assertEquals(
                    RECORDS_TO_UPDATE_COPY[0].intoMap(*excludes).utcMap(),
                    QUERY_RESULT[0].component1().intoMap(*excludes).utcMap()
                )
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.NAME.name,
                    Notes.NOTES.DESCRIPTION.name,
                )
                assertEquals(
                    RECORDS_TO_UPDATE_COPY[1].intoMap(*excludes).utcMap(),
                    QUERY_RESULT[1].component1().intoMap(*excludes).utcMap()
                )
            }
        )

    }

}