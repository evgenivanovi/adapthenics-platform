@file:Suppress(
    "LocalVariableName"
)

package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.envotur.pg.extension.PostgresGlobalExtension
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.Notes
import kotlinx.coroutines.runBlocking
import org.jooq.DSLContext
import org.jooq.tools.jdbc.SingleConnectionDataSource
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import java.sql.Connection
import javax.sql.DataSource

class PostgresDeleteRequesterTest {

    companion object {

        private val POSTGRES: Envo = Envo("postgres", 5432)

        private val SCHEMA_PATHS: List<String> = listOf(
            "classpath:migration"
        )

        @RegisterExtension
        val extension = PostgresGlobalExtension.extension()
            .withOptions { options ->
                options
                    .withGitlabLocation("../../../../.gitlab-ci.yml")
                    .withGitlabJob("test")
                    .withEnvo(POSTGRES)
            }
            .withSchemaLocations(SCHEMA_PATHS)
            .build()

    }

    private lateinit var dsl: DSLContext

    private lateinit var delete: PostgresDeleteRequesterService

    private lateinit var connection: Connection

    private lateinit var datasource: DataSource

    /* __________________________________________________ */

    @BeforeEach
    fun beforeAll() {
        initDatasource()
        initDSL()
        initRequesters()
    }

    @AfterEach
    fun afterAll() {
        dsl.clearDatabase()
        connection.close()
    }

    /* __________________________________________________ */

    private fun initDatasource() {

        connection = DatabaseTool
            .containerConnection(extension.runtime())

        datasource =
            SingleConnectionDataSource(connection)

    }

    private fun initDSL() {
        dsl = initDSLContext()
    }

    private fun initDSLContext(): DSLContext {
        return DatabaseTool.dsl(datasource)
    }

    private fun initRequesters() {
        delete = PostgresDeleteRequesterService()
    }

    /* __________________________________________________ */

    @Test
    fun `should delete`() = runBlocking {

        // given
        val RECORD = NotesTestData.NOTE_1_WITH_ID()
        val RECORD_ID = NotesTestData.NOTE_1_ID
        val RECORD_TABLE = Notes.NOTES

        // init
        insertRecord(dsl, RECORD)

        // when
        val RESULT = delete
            .delete(dsl.configuration(), RECORD_ID, RECORD_TABLE)

        val QUERY_AFTER = dsl
            .select()
            .from(RECORD_TABLE)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val RESULT_AFTER = dsl.fetchOne(QUERY_AFTER)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT != null)
            },
            {
                assertTrue(RESULT?.get(Notes.NOTES.ID) == RECORD_ID)
            },
            {
                assertTrue(RESULT_AFTER == null)
            }
        )

    }

    @Test
    fun `should archive`() = runBlocking {

        // given
        val RECORD_ID = NotesTestData.NOTE_1_ID
        val RECORD = NotesTestData.NOTE_1_WITH_ID()
        val RECORD_TABLE = Notes.NOTES

        // init
        insertRecord(dsl, RECORD)

        // when
        val RESULT = delete
            .archive(dsl.configuration(), RECORD_ID, RECORD_TABLE)

        val QUERY_AFTER = dsl
            .select()
            .from(RECORD_TABLE)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val RESULT_AFTER = dsl.fetchOne(QUERY_AFTER)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT != null)
            },
            {
                assertTrue(RESULT?.get(Notes.NOTES.ID) == RECORD_ID)
            },
            {
                assertTrue(RESULT?.get(JooqEntityTool.DELETED_AT_FIELD) != null)
            },
            {
                assertTrue(RESULT_AFTER != null)
            },
            {
                assertTrue(RESULT_AFTER?.get(Notes.NOTES.ID) == RECORD_ID)
            },
            {
                assertTrue(RESULT_AFTER?.get(JooqEntityTool.DELETED_AT_FIELD) != null)
            }
        )

    }

    @Test
    fun `given incompatible id, when delete, should throw exception`() = runBlocking {

        // given
        val IDS = listOf("TEST_INVALID_ID")

        // when
        val ACTION = {
            runBlocking {
                delete.delete(dsl.configuration(), IDS, Notes.NOTES)
            }
        }

        // then
        Assertions.assertAll(
            {
                assertThrows(IllegalArgumentException::class.java) { ACTION.invoke() }
            }
        )

    }

    @Test
    fun `given incompatible id, when archive, should throw exception`() = runBlocking {

        // given
        val IDS = listOf("TEST_INVALID_ID")

        // when
        val ACTION = {
            runBlocking {
                delete.archive(dsl.configuration(), IDS, Notes.NOTES)
            }
        }

        // then
        Assertions.assertAll(
            {
                assertThrows(IllegalArgumentException::class.java) { ACTION.invoke() }
            }
        )

    }

}