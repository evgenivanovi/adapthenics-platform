@file:Suppress(
    "LocalVariableName"
)

package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.envotur.pg.extension.PostgresGlobalExtension
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.Notes
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.NotesNoMeta
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesNoMetaRecord
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesRecord
import com.evgenivanovi.kt.time.OffsetUTC
import kotlinx.coroutines.runBlocking
import org.jooq.DSLContext
import org.jooq.tools.jdbc.SingleConnectionDataSource
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.RegisterExtension
import java.sql.Connection
import javax.sql.DataSource

class PostgresWriteInsertionRequesterTest {

    companion object {

        private val POSTGRES: Envo = Envo("postgres", 5432)

        private val SCHEMA_PATHS: List<String> = listOf(
            "classpath:migration"
        )

        @RegisterExtension
        val extension = PostgresGlobalExtension.extension()
            .withOptions { options ->
                options
                    .withGitlabLocation("../../../../.gitlab-ci.yml")
                    .withGitlabJob("test")
                    .withEnvo(POSTGRES)
            }
            .withSchemaLocations(SCHEMA_PATHS)
            .build()

    }

    private lateinit var dsl: DSLContext

    private lateinit var write: PostgresWriteRequesterService

    private lateinit var connection: Connection

    private lateinit var datasource: DataSource

    /* __________________________________________________ */

    @BeforeEach
    fun beforeEach() {
        initDatasource()
        initDSL()
        initRequesters()
    }

    @AfterEach
    fun afterEach() {
        dsl.clearDatabase()
        connection.close()
    }

    /* __________________________________________________ */

    private fun initDatasource() {

        connection = DatabaseTool
            .containerConnection(extension.runtime())

        datasource =
            SingleConnectionDataSource(connection)

    }

    private fun initDSL() {
        dsl = initDSLContext()
    }

    private fun initDSLContext(): DSLContext {
        return DatabaseTool.dsl(datasource)
    }

    private fun initRequesters() {
        write = PostgresWriteRequesterService()
    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing meta record, meta record to insert with PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test1() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD_TO_INSERT = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_EXPECTED = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        // when
        val RESULT = write.forceInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing meta record, meta record to insert without PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test2() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD_TO_INSERT = NotesRecord()
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_EXPECTED = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        // when
        val RESULT = write.forceInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta record, no-meta record to insert with PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test3() = runBlocking {

        // given
        val RECORD_ID = 1L

        val RECORD_TO_INSERT = NotesNoMetaRecord()
            .setId(RECORD_ID)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED = NotesNoMetaRecord()
            .setId(RECORD_ID)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        // when
        val RESULT = write.forceInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta record, no-meta record to insert without PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test4() = runBlocking {

        // given
        val RECORD_ID = 1L

        val RECORD_TO_INSERT = NotesNoMetaRecord()
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED = NotesNoMetaRecord()
            .setId(RECORD_ID)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        // when
        val RESULT = write.forceInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing meta record, meta record to insert with PK && unneeded meta fields;" +
            " when soft insert;" +
            " should insert without unneeded meta fields;"
    )
    fun test5() = runBlocking {

        // given
        val RECORD_ID = 1L

        val RECORD_TO_INSERT = NotesRecord()
            .setId(RECORD_ID)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            // unneeded fields
            .setVersion(999L)
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        // when
        val RESULT = write.softInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.CREATED_AT.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.DELETED_AT.name,
                )
                assertEquals(
                    RECORD_TO_INSERT.intoMap(*excludes).utcMap(),
                    QUERY_RESULT[0].component1().intoMap(*excludes).utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing meta record, meta record to insert without PK && unneeded meta fields;" +
            " when soft insert;" +
            " should insert without unneeded meta fields;"
    )
    fun test6() = runBlocking {

        // given
        val RECORD_ID = 1L

        val RECORD_TO_INSERT = NotesRecord()
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            // unneeded fields
            .setVersion(999L)
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        // when
        val RESULT = write.forceInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.ID.name,
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.CREATED_AT.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.DELETED_AT.name,
                )
                assertEquals(
                    RECORD_TO_INSERT.intoMap(*excludes).utcMap(),
                    QUERY_RESULT[0].component1().intoMap(*excludes).utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta record, no-meta record to insert with PK;" +
            " when soft insert;" +
            " should insert;"
    )
    fun test7() = runBlocking {

        // given
        val RECORD_ID = 1L

        val RECORD_TO_INSERT = NotesNoMetaRecord()
            .setId(RECORD_ID)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED = NotesNoMetaRecord()
            .setId(RECORD_ID)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        // when
        val RESULT = write.softInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta record, no-meta record to insert without PK;" +
            " when soft insert;" +
            " should insert;"
    )
    fun test8() = runBlocking {

        // given
        val RECORD_ID = 1L

        val RECORD_TO_INSERT = NotesNoMetaRecord()
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED = NotesNoMetaRecord()
            .setId(RECORD_ID)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        // when
        val RESULT = write.softInsert(dsl.configuration(), RECORD_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

}