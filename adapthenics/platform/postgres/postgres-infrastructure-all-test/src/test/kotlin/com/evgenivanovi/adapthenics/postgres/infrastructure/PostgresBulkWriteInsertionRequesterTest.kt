@file:Suppress(
    "LocalVariableName"
)

package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.envotur.pg.extension.PostgresGlobalExtension
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.Notes
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.NotesNoMeta
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesNoMetaRecord
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesRecord
import com.evgenivanovi.kt.time.OffsetUTC
import kotlinx.coroutines.runBlocking
import org.jooq.DSLContext
import org.jooq.tools.jdbc.SingleConnectionDataSource
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.RegisterExtension
import java.sql.Connection
import javax.sql.DataSource

class PostgresBulkWriteInsertionRequesterTest {

    companion object {

        private val POSTGRES: Envo = Envo("postgres", 5432)

        private val SCHEMA_PATHS: List<String> = listOf(
            "classpath:migration"
        )

        @RegisterExtension
        val extension = PostgresGlobalExtension.extension()
            .withOptions { options ->
                options
                    .withGitlabLocation("../../../../.gitlab-ci.yml")
                    .withGitlabJob("test")
                    .withEnvo(POSTGRES)
            }
            .withSchemaLocations(SCHEMA_PATHS)
            .build()

    }

    private lateinit var dsl: DSLContext

    private lateinit var bulkWrite: PostgresBulkWriteRequesterService

    private lateinit var connection: Connection

    private lateinit var datasource: DataSource

    /* __________________________________________________ */

    @BeforeEach
    fun beforeEach() {
        initDatasource()
        initDSL()
        initRequesters()
    }

    @AfterEach
    fun afterEach() {
        dsl.clearDatabase()
        connection.close()
    }

    /* __________________________________________________ */

    private fun initDatasource() {

        connection = DatabaseTool
            .containerConnection(extension.runtime())

        datasource =
            SingleConnectionDataSource(connection)

    }

    private fun initDSL() {
        dsl = initDSLContext()
    }

    private fun initDSLContext(): DSLContext {
        return DatabaseTool.dsl(datasource)
    }

    private fun initRequesters() {
        bulkWrite = PostgresBulkWriteRequesterService()
    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing meta records, meta records to insert with PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test1() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_ID_2 = 2L
        val RECORD_VERSION_2 = 0L

        val RECORD_TO_INSERT_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_EXPECTED_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_EXPECTED_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.forceInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing meta record, meta record to insert without PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test2() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_VERSION_1 = 0L

        val RECORD_ID_2 = 2L
        val RECORD_VERSION_2 = 0L

        val RECORD_TO_INSERT_1 = NotesRecord()
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_EXPECTED_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setVersion(RECORD_VERSION_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_2 = NotesRecord()
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_INSERT_EXPECTED_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setVersion(RECORD_VERSION_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.forceInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta records, no-meta records to insert with PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test3() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_ID_2 = 2L

        val RECORD_TO_INSERT_1 = NotesNoMetaRecord()
            .setId(RECORD_ID_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_1 = NotesNoMetaRecord()
            .setId(RECORD_ID_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_2 = NotesNoMetaRecord()
            .setId(RECORD_ID_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_2 = NotesNoMetaRecord()
            .setId(RECORD_ID_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.forceInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta records, no-meta records to insert without PK;" +
            " when force insert;" +
            " should insert;"
    )
    fun test4() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_ID_2 = 2L

        val RECORD_TO_INSERT_1 = NotesNoMetaRecord()
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_1 = NotesNoMetaRecord()
            .setId(RECORD_ID_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_2 = NotesNoMetaRecord()
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_2 = NotesNoMetaRecord()
            .setId(RECORD_ID_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.forceInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing meta records, meta records to insert with PK && unneeded meta fields;" +
            " when soft insert;" +
            " should insert without unneeded meta fields;"
    )
    fun test5() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_ID_2 = 2L

        val RECORD_TO_INSERT_1 = NotesRecord()
            .setId(RECORD_ID_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            // unneeded fields
            .setVersion(999L)
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD_TO_INSERT_2 = NotesRecord()
            .setId(RECORD_ID_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            // unneeded fields
            .setVersion(999L)
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.softInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.CREATED_AT.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.DELETED_AT.name,
                )
                assertEquals(
                    RECORD_TO_INSERT_1.intoMap(*excludes).utcMap(),
                    QUERY_RESULT[0].component1().intoMap(*excludes).utcMap()
                )
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.CREATED_AT.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.DELETED_AT.name,
                )
                assertEquals(
                    RECORD_TO_INSERT_2.intoMap(*excludes).utcMap(),
                    QUERY_RESULT[1].component1().intoMap(*excludes).utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing meta records, meta records to insert without PK && unneeded meta fields;" +
            " when soft insert;" +
            " should insert without unneeded meta fields;"
    )
    fun test6() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_ID_2 = 2L

        val RECORD_TO_INSERT_1 = NotesRecord()
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            // unneeded fields
            .setVersion(999L)
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD_TO_INSERT_2 = NotesRecord()
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")
            // unneeded fields
            .setVersion(999L)
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.softInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.ID.name,
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.CREATED_AT.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.DELETED_AT.name,
                )
                assertEquals(
                    RECORD_TO_INSERT_1.intoMap(*excludes).utcMap(),
                    QUERY_RESULT[0].component1().intoMap(*excludes).utcMap()
                )
            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.ID.name,
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.CREATED_AT.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.DELETED_AT.name,
                )
                assertEquals(
                    RECORD_TO_INSERT_2.intoMap(*excludes).utcMap(),
                    QUERY_RESULT[1].component1().intoMap(*excludes).utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta records, no-meta records to insert with PK;" +
            " when soft insert;" +
            " should insert;"
    )
    fun test7() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_ID_2 = 2L

        val RECORD_TO_INSERT_1 = NotesNoMetaRecord()
            .setId(RECORD_ID_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_1 = NotesNoMetaRecord()
            .setId(RECORD_ID_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_2 = NotesNoMetaRecord()
            .setId(RECORD_ID_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_2 = NotesNoMetaRecord()
            .setId(RECORD_ID_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.softInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given non existing no-meta records, no-meta records to insert without PK;" +
            " when soft insert;" +
            " should insert;"
    )
    fun test8() = runBlocking {

        // given
        val RECORD_ID_1 = 1L
        val RECORD_ID_2 = 2L

        val RECORD_TO_INSERT_1 = NotesNoMetaRecord()
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_1 = NotesNoMetaRecord()
            .setId(RECORD_ID_1)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")

        val RECORD_TO_INSERT_2 = NotesNoMetaRecord()
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_TO_INSERT_EXPECTED_2 = NotesNoMetaRecord()
            .setId(RECORD_ID_2)
            .setName("NOTE_2")
            .setDescription("NOTE_2_DESCRIPTION")

        val RECORD_IDS = listOf(
            RECORD_ID_1,
            RECORD_ID_2
        )

        val RECORDS_TO_INSERT = listOf(
            RECORD_TO_INSERT_1,
            RECORD_TO_INSERT_2,
        )

        // when
        bulkWrite.softInsert(dsl.configuration(), RECORDS_TO_INSERT)

        val QUERY = dsl
            .select(NotesNoMeta.NOTES_NO_META)
            .from(NotesNoMeta.NOTES_NO_META)
            .where(NotesNoMeta.NOTES_NO_META.ID.`in`(RECORD_IDS))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(2, QUERY_RESULT.size)
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_1.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD_TO_INSERT_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_INSERT_EXPECTED_2.intoMap().utcMap(),
                    QUERY_RESULT[1].component1().intoMap().utcMap()
                )
            }
        )

    }

}