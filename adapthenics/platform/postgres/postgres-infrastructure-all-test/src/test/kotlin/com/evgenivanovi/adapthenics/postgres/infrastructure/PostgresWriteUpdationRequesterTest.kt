@file:Suppress(
    "LocalVariableName"
)

package com.evgenivanovi.adapthenics.postgres.infrastructure

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.envotur.pg.extension.PostgresGlobalExtension
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.Notes
import com.evgenivanovi.adapthenics.postgres.infrastructure.tables.records.NotesRecord
import com.evgenivanovi.kt.time.OffsetUTC
import kotlinx.coroutines.runBlocking
import org.jooq.DSLContext
import org.jooq.tools.jdbc.SingleConnectionDataSource
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.RegisterExtension
import java.sql.Connection
import javax.sql.DataSource

class PostgresWriteUpdationRequesterTest {

    companion object {

        private val POSTGRES: Envo = Envo("postgres", 5432)

        private val SCHEMA_PATHS: List<String> = listOf(
            "classpath:migration"
        )

        @RegisterExtension
        val extension = PostgresGlobalExtension.extension()
            .withOptions { options ->
                options
                    .withGitlabLocation("../../../../.gitlab-ci.yml")
                    .withGitlabJob("test")
                    .withEnvo(POSTGRES)
            }
            .withSchemaLocations(SCHEMA_PATHS)
            .build()

    }

    private lateinit var dsl: DSLContext

    private lateinit var write: PostgresWriteRequesterService

    private lateinit var connection: Connection

    private lateinit var datasource: DataSource

    /* __________________________________________________ */

    @BeforeEach
    fun beforeEach() {
        initDatasource()
        initDSL()
        initRequesters()
    }

    @AfterEach
    fun afterEach() {
        dsl.clearDatabase()
        connection.close()
    }

    /* __________________________________________________ */

    private fun initDatasource() {

        connection = DatabaseTool
            .containerConnection(extension.runtime())

        datasource =
            SingleConnectionDataSource(connection)

    }

    private fun initDSL() {
        dsl = initDSLContext()
    }

    private fun initDSLContext(): DSLContext {
        return DatabaseTool.dsl(datasource)
    }

    private fun initRequesters() {
        write = PostgresWriteRequesterService()
    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing record to update;" +
            " when soft update;" +
            " should not update;"
    )
    fun test1() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        // when
        val RESULT = write.softUpdate(dsl.configuration(), RECORD_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT == null)
            },
            {
                assertTrue(QUERY_RESULT.isEmpty())
            }
        )

    }

    @Test
    @DisplayName(
        "given existing record, record to update with different version;" +
            " when soft update;" +
            " should not update;"
    )
    fun test2() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(999L)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        insertRecord(dsl, RECORD)

        // when
        val RESULT = write.softUpdate(dsl.configuration(), RECORD_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT == null)
            },
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(
                    RECORD.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given existing record with deleted_at, record to update;" +
            " when soft update;" +
            " should not update;"
    )
    fun test3() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))

        val RECORD_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        insertRecord(dsl, RECORD)

        // when
        val RESULT = write.softUpdate(dsl.configuration(), RECORD_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT == null)
            },
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(
                    RECORD.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

    @Test
    @DisplayName(
        "given existing record, record to update with unneeded meta fields;" +
            " when soft update;" +
            " should update without unneeded meta fields;"
    )
    fun test4() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            // unneeded fields
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        insertRecord(dsl, RECORD)

        // when
        val RESULT = write.softUpdate(dsl.configuration(), RECORD_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT != null)
            },
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT?.intoMap()?.utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {

                val expectedVersion = RECORD
                    .getValue(Notes.NOTES.VERSION)
                    .inc()

                val actualVersion = QUERY_RESULT[0].component1()
                    ?.getValue(Notes.NOTES.VERSION)

                assertEquals(expectedVersion, actualVersion)

            },
            {

                val expectedUpdatedAt = RECORD
                    .getValue(Notes.NOTES.UPDATED_AT)

                val actualUpdatedAt = QUERY_RESULT[0].component1()
                    ?.getValue(Notes.NOTES.UPDATED_AT)

                val matcher = { actualUpdatedAt?.isAfter(expectedUpdatedAt) ?: false }

                assertTrue(matcher.invoke())

            },
            {
                val excludes = arrayOf(
                    Notes.NOTES.VERSION.name,
                    Notes.NOTES.UPDATED_AT.name,
                    Notes.NOTES.NAME.name,
                    Notes.NOTES.DESCRIPTION.name,
                )
                assertEquals(
                    RECORD.intoMap(*excludes).utcMap(),
                    QUERY_RESULT[0].component1().intoMap(*excludes).utcMap()
                )
            }
        )

    }

    /* __________________________________________________ */

    @Test
    @DisplayName(
        "given non existing record to update;" +
            " when force update;" +
            " should not update;"
    )
    fun test5() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        // when
        val RESULT = write.forceUpdate(dsl.configuration(), RECORD_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT == null)
            },
            {
                assertTrue(QUERY_RESULT.isEmpty())
            }
        )

    }

    @Test
    @DisplayName(
        "given existing record, record to update;" +
            " when force update;" +
            " should update all fields;"
    )
    fun test6() = runBlocking {

        // given
        val RECORD_ID = 1L
        val RECORD_VERSION = 0L

        val RECORD = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(RECORD_VERSION)
            .setName("NOTE_1")
            .setDescription("NOTE_1_DESCRIPTION")
            .setCreatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2022-01-01T00:00:00.00Z"))
            .setDeletedAt(null)

        val RECORD_TO_UPDATE = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(999L)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        val RECORD_TO_UPDATE_COPY = NotesRecord()
            .setId(RECORD_ID)
            .setVersion(999L)
            .setName("NOTE_1_NEW")
            .setDescription("NOTE_1_DESCRIPTION_NEW")
            .setCreatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setUpdatedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))
            .setDeletedAt(OffsetUTC.utc("2099-01-01T00:00:00.00Z"))

        insertRecord(dsl, RECORD)

        // when
        val RESULT = write.forceUpdate(dsl.configuration(), RECORD_TO_UPDATE)

        val QUERY = dsl
            .select(Notes.NOTES)
            .from(Notes.NOTES)
            .where(Notes.NOTES.ID.eq(RECORD_ID))
            .query

        val QUERY_RESULT = dsl.fetch(QUERY)

        // then
        Assertions.assertAll(
            {
                assertTrue(RESULT != null)
            },
            {
                assertTrue(QUERY_RESULT.isNotEmpty)
            },
            {
                assertEquals(1, QUERY_RESULT.size)
            },
            {
                assertEquals(
                    RESULT?.intoMap()?.utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertNotEquals(
                    RECORD.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            },
            {
                assertEquals(
                    RECORD_TO_UPDATE_COPY.intoMap().utcMap(),
                    QUERY_RESULT[0].component1().intoMap().utcMap()
                )
            }
        )

    }

}