@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.lib.tool.AdapthenicsProject
import com.evgenivanovi.adapthenics.lib.util.Jvm
import com.evgenivanovi.adapthenics.lib.util.Strings

plugins {
    id("com.evgenivanovi.adapthenics.gradle.plugin.envotur.pg")
}

project(ProjectModules.POSTGRES_INFRASTRUCTURE_ALL_TEST) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.JOOQX))
        testImplementation(project(ProjectModules.JOOQX))

        implementation(project(ProjectModules.POSTGRES_ADAPTER))
        testImplementation(project(ProjectModules.POSTGRES_ADAPTER))

        implementation(project(ProjectModules.POSTGRES_INFRASTRUCTURE))
        testImplementation(project(ProjectModules.POSTGRES_INFRASTRUCTURE))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(ktDependenciesCatalog.bundles.flyway)
    runtimeOnly(ktDependenciesCatalog.flyway.pg)
    implementation(ktDependenciesCatalog.bundles.jooq.impl)

    runtimeOnly(libs.bundles.postgres.runtime)
    testImplementation(ktDependenciesCatalog.bundles.testcontainers.postgres.impl)

    testImplementation(tools.envotur.impl)
    testImplementation(tools.envotur.junit.impl)
    testImplementation(tools.envotur.postgres.impl)
    testImplementation(tools.envotur.postgres.junit.impl)
}

tasks { test { maxParallelForks = 1 } }

/* __________________________________________________ */

val ENVOTUR_RUNTIME_LOCATION = rootDir.path + "/" + ".gitlab-ci.yml"
val ENVOTUR_RUNTIME_JOB = "build"
val ENVOTUR_RUNTIME_ENVO = Envo("postgres", 5432)

// Java Classpath Migrations: AdapthenicsProject.mainPackageClasspath()
// SQL Classpath Migrations: Jvm.CLASSPATH_PREFIX + "migration"
// SQL Resources Migrations: Jvm.FILESYSTEM_PREFIX + getProjectDir().path + "/src/main/resources/migration"
val ENVOTUR_FLYWAY_LOCATIONS = listOf(
    Jvm.FILESYSTEM_PREFIX + projectDir.path + "/src/test/resources/migration"
)

val ENVOTUR_JOOQ_PACKAGE_NAME = listOf(
    AdapthenicsProject.mainPackage(),
    "postgres",
    "infrastructure",
).joinToString(separator = Strings.DOT)

val ENVOTUR_JOOQ_GENERATE: org.jooq.meta.jaxb.Generate = org.jooq.meta.jaxb.Generate()
    // Useful functionality
    .withDaos(false)
    .withRecords(true)

    // Comments
    .withComments(true)
    .withCommentsOnTables(true)
    .withCommentsOnColumns(true)
    .withCommentsOnKeys(true)

    // Useful programming functionality
    .withJavadoc(true)

    .withNonnullAnnotation(true)
    .withNullableAnnotation(true)

    .withPojos(false)
    .withSerializablePojos(false)
    .withPojosToString(false)
    .withPojosEqualsAndHashCode(false)

    .withFluentSetters(true)
    .withInstanceFields(true)

    .withJavaTimeTypes(true)

    // Other
    .withDeprecated(false)
    .withDeprecationOnUnknownTypes(false)

    .withEmptySchemas(true)
    .withEmptyCatalogs(true)

    .withGlobalSchemaReferences(false)
    .withGlobalCatalogReferences(false)

val ENVOTUR_JOOQ_DATABASE: org.jooq.meta.jaxb.Database = org.jooq.meta.jaxb.Database()
    .withInputSchema("public")
    .withRecordVersionFields("version")

envotur {
    runtime {
        location.set(ENVOTUR_RUNTIME_LOCATION)
        job.set(ENVOTUR_RUNTIME_JOB)
        envo.set(ENVOTUR_RUNTIME_ENVO)
    }
    flyway {
        locations.set(ENVOTUR_FLYWAY_LOCATIONS)
    }
    jooq {
        generate.set(ENVOTUR_JOOQ_GENERATE)
        database.set(ENVOTUR_JOOQ_DATABASE)
        packageName.set(ENVOTUR_JOOQ_PACKAGE_NAME)
    }
}

tasks.register("logInfo") {
    logging.captureStandardOutput(LogLevel.DEBUG)
    doFirst {
        println("A task message which is logged at DEBUG level")
    }
}