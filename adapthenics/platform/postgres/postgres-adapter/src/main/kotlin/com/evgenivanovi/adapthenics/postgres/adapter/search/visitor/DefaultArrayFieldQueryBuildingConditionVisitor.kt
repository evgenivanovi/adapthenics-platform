package com.evgenivanovi.adapthenics.postgres.adapter.search.visitor

import com.evgenivanovi.kt.ex.UnreachableExceptionObject
import com.evgenivanovi.kt.lang.asNotNull
import com.evgenivanovi.search.condition.SearchArrayConditionVisitor
import com.evgenivanovi.search.condition.SearchArrayConditions
import org.jooq.Condition
import org.jooq.Field
import org.jooq.TableField

/**
 * Condition visitor with only one-to-one capabilities.
 */
class DefaultArrayFieldQueryBuildingConditionVisitor<F : Any>(
    private val singleProperty: TableField<*, F>?,
    private val arrayProperty: TableField<*, Array<F>>?,
) : SearchArrayConditionVisitor<F, Condition> {

    init {
        if (isSingleProperty().not() && isArrayProperty().not()) {
            throw IllegalArgumentException("Only one property allowed to set.")
        }

        if (isSingleProperty() && isArrayProperty()) {
            throw IllegalArgumentException("Only one property allowed to set.")
        }
    }

    private fun isSingleProperty(): Boolean {
        return singleProperty != null
    }

    private fun singleProperty(): Field<F> {
        return singleProperty.asNotNull()
    }

    private fun isArrayProperty(): Boolean {
        return arrayProperty != null
    }

    private fun arrayProperty(): Field<Array<F>> {
        return arrayProperty.asNotNull()
    }

    override fun visitEquality(
        condition: SearchArrayConditions.Equality<F>,
    ): Condition {

        if (isArrayProperty()) {
            return ARRAY_EQUALITY_VISITOR(arrayProperty(), condition)
        }

        throw UnreachableExceptionObject

    }

    override fun visitInequality(
        condition: SearchArrayConditions.Inequality<F>,
    ): Condition {

        if (isArrayProperty()) {
            return ARRAY_INEQUALITY_VISITOR(arrayProperty(), condition)
        }

        throw UnreachableExceptionObject

    }

    override fun visitContainsAll(
        condition: SearchArrayConditions.ContainsAll<F>,
    ): Condition {

        if (isArrayProperty()) {
            return ARRAY_CONTAINS_ALL_VISITOR(arrayProperty(), condition)
        }

        throw UnreachableExceptionObject

    }

    override fun visitNotContainAll(
        condition: SearchArrayConditions.NotContainAll<F>,
    ): Condition {

        if (isArrayProperty()) {
            return ARRAY_NOT_CONTAINS_ALL_VISITOR(arrayProperty(), condition)
        }

        throw UnreachableExceptionObject

    }

    override fun visitContainsAny(
        condition: SearchArrayConditions.ContainsAny<F>,
    ): Condition {

        if (isSingleProperty()) {
            return ARRAY_FOR_SINGLE_FIELD_CONTAINS_ANY_VISITOR(singleProperty(), condition)
        }

        if (isArrayProperty()) {
            return ARRAY_FOR_ARRAY_FIELD_CONTAINS_ANY_VISITOR(arrayProperty(), condition)
        }

        throw UnreachableExceptionObject

    }

    override fun visitContainsNone(
        condition: SearchArrayConditions.ContainsNone<F>,
    ): Condition {

        if (isSingleProperty()) {
            return ARRAY_FOR_SINGLE_FIELD_CONTAINS_NONE_VISITOR(singleProperty(), condition)
        }

        if (isArrayProperty()) {
            return ARRAY_FOR_ARRAY_FIELD_CONTAINS_NONE_VISITOR(arrayProperty(), condition)
        }

        throw UnreachableExceptionObject

    }

    companion object {

        fun <F : Any> forSingleProperty(
            property: TableField<*, F>,
        ): DefaultArrayFieldQueryBuildingConditionVisitor<F> {
            return DefaultArrayFieldQueryBuildingConditionVisitor(
                property, null
            )
        }

        fun <F : Any> forArrayProperty(
            property: TableField<*, Array<F>>,
        ): DefaultArrayFieldQueryBuildingConditionVisitor<F> {
            return DefaultArrayFieldQueryBuildingConditionVisitor(
                null, property
            )
        }

    }

}