package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.search.spec.SpecificationRepository

interface DocumentSpecificationRepository<ID : Any, DOCUMENT : Document<ID>>
    : SpecificationRepository<ID, DOCUMENT>