package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresReadRequester
import com.evgenivanovi.adapthenics.schema.mapper.DocumentMapper
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.service.repo.KeyRepository
import org.jooq.Record
import org.jooq.Result

// @formatter:off
interface PostgresKeyRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    DOCUMENT : Document<ID>
> : KeyRepository<ID, ENTITY> {
// @formatter:on

    val requester: PostgresReadRequester
    val querier: QueryBuilder<ID>

    val mapper: DocumentMapper<ID, ENTITY, DOCUMENT>
    val recordMapper: DocumentRecordComplexMapper<DOCUMENT>

    val type: Class<DOCUMENT>

    override suspend fun find(id: ID): ENTITY? {
        return requester
            .findOne(querier.get(id))
            .takeIf(Result<Record>::isNotEmpty)
            ?.let { record -> recordMapper.mapSingle(record) }
            ?.let { doc -> mapper.to(doc) }
    }

}