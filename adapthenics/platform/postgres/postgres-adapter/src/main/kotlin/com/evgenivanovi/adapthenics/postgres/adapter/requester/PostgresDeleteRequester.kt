package com.evgenivanovi.adapthenics.postgres.adapter.requester

import org.jooq.Configuration
import org.jooq.Record
import org.jooq.Table

interface PostgresDeleteRequester {

    suspend fun <ID : Any> delete(
        ctx: Configuration,
        id: ID,
        table: Table<*>,
    ): Record?

    suspend fun <ID : Any> archive(
        ctx: Configuration,
        id: ID,
        table: Table<*>,
    ): Record?

    suspend fun <ID : Any> unarchive(
        ctx: Configuration,
        id: ID,
        table: Table<*>,
    ): Record?

}