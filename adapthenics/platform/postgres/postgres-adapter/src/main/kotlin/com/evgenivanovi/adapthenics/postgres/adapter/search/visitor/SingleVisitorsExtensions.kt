@file:Suppress("FunctionName")

package com.evgenivanovi.adapthenics.postgres.adapter.search.visitor

import com.evgenivanovi.kt.string.surround
import com.evgenivanovi.search.condition.SearchSingleConditions
import org.jooq.Condition
import org.jooq.Field

const val SQL_STARTS_WITH_PREFIX = "^"
const val SQL_STARTS_WITH_SUFFIX = ".*"

const val SQL_ENDS_WITH_PREFIX = "^.*"
const val SQL_ENDS_WITH_SUFFIX = "$"

fun <F : Any> SINGLE_EQUALITY_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.Equality<F>,
): Condition {
    return property
        .equal(condition.value())
}

fun <F : Any> SINGLE_INEQUALITY_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.Inequality<F>,
): Condition {
    return property
        .notEqual(condition.value())
}

fun <F : Any> SINGLE_GREATER_THAN_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.Greater<F>,
): Condition {
    return property
        .greaterThan(condition.value())
}

fun <F : Any> SINGLE_LESS_THAN_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.Less<F>,
): Condition {
    return property
        .lessThan(condition.value())
}

fun <F : Any> SINGLE_GREATER_THAN_EQUALITY_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.GreaterEquality<F>,
): Condition {
    return property
        .greaterOrEqual(condition.value())
}

fun <F : Any> SINGLE_LESS_THAN_EQUALITY_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.LessEquality<F>,
): Condition {
    return property
        .lessOrEqual(condition.value())
}

fun <F : Any> SINGLE_STARTS_WITH_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.StartsWith<F>,
): Condition {

    val regex = condition
        .value()
        .toString()
        .surround(
            pre = SQL_STARTS_WITH_PREFIX,
            post = SQL_STARTS_WITH_SUFFIX
        )

    return property.likeRegex(regex)

}

fun <F : Any> SINGLE_NOT_START_WITH_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.NotStartWith<F>,
): Condition {

    val regex = condition
        .value()
        .toString()
        .surround(
            pre = SQL_STARTS_WITH_PREFIX,
            post = SQL_STARTS_WITH_SUFFIX
        )

    return property.notLikeRegex(regex)

}

fun <F : Any> SINGLE_ENDS_WITH_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.EndsWith<F>,
): Condition {

    val regex = condition
        .value()
        .toString()
        .surround(
            pre = SQL_ENDS_WITH_PREFIX,
            post = SQL_ENDS_WITH_SUFFIX
        )

    return property.likeRegex(regex)

}

fun <F : Any> SINGLE_NOT_END_WITH_VISITOR(
    property: Field<F>,
    condition: SearchSingleConditions.NotEndWith<F>,
): Condition {

    val regex = condition
        .value()
        .toString()
        .surround(
            pre = SQL_ENDS_WITH_PREFIX,
            post = SQL_ENDS_WITH_SUFFIX
        )

    return property.notLikeRegex(regex)

}