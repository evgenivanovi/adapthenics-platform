@file:Suppress("FunctionName")

package com.evgenivanovi.adapthenics.postgres.adapter.search.visitor

import com.evgenivanovi.search.condition.SearchArrayConditions
import org.jooq.Condition
import org.jooq.Field
import org.jooq.impl.DSL

fun <F : Any> ARRAY_EQUALITY_VISITOR(
    property: Field<Array<F>>,
    condition: SearchArrayConditions.Equality<F>,
): Condition {

    val values: Field<Array<F>> = condition
        .values()
        .map { DSL.inline(it) }
        .let { DSL.array(it) }

    return property.equal(values)

}

fun <F : Any> ARRAY_INEQUALITY_VISITOR(
    property: Field<Array<F>>,
    condition: SearchArrayConditions.Inequality<F>,
): Condition {

    val values: Field<Array<F>> = condition
        .values()
        .map { DSL.inline(it) }
        .let { DSL.array(it) }

    return property.notEqual(values)

}

fun <F : Any> ARRAY_CONTAINS_ALL_VISITOR(
    property: Field<Array<F>>,
    condition: SearchArrayConditions.ContainsAll<F>,
): Condition {

    val values: Field<Array<F>> = condition
        .values()
        .map { DSL.inline(it) }
        .let { DSL.array(it) }

    return property.contains(values)

}

fun <F : Any> ARRAY_NOT_CONTAINS_ALL_VISITOR(
    property: Field<Array<F>>,
    condition: SearchArrayConditions.NotContainAll<F>,
): Condition {

    val values: Field<Array<F>> = condition
        .values()
        .map { DSL.inline(it) }
        .let { DSL.array(it) }

    return property.notContains(values)

}

fun <F : Any> ARRAY_FOR_SINGLE_FIELD_CONTAINS_ANY_VISITOR(
    property: Field<F>,
    condition: SearchArrayConditions.ContainsAny<F>,
): Condition {
    val values: Collection<F> = condition.values()
    return property.`in`(values)
}

fun <F : Any> ARRAY_FOR_ARRAY_FIELD_CONTAINS_ANY_VISITOR(
    property: Field<Array<F>>,
    condition: SearchArrayConditions.ContainsAny<F>,
): Condition {

    val values: Field<Array<F>> = condition
        .values()
        .map { DSL.inline(it) }
        .let { DSL.array(it) }

    return property.`in`(values)

}

fun <F : Any> ARRAY_FOR_SINGLE_FIELD_CONTAINS_NONE_VISITOR(
    property: Field<F>,
    condition: SearchArrayConditions.ContainsNone<F>,
): Condition {
    val values: Collection<F> = condition.values()
    return property.notIn(values)
}

fun <F : Any> ARRAY_FOR_ARRAY_FIELD_CONTAINS_NONE_VISITOR(
    property: Field<Array<F>>,
    condition: SearchArrayConditions.ContainsNone<F>,
): Condition {

    val values: Field<Array<F>> = condition
        .values()
        .map { DSL.inline(it) }
        .let { DSL.array(it) }

    return property.notIn(values)

}

fun <F : Any> Field<out F>.isArray(): Boolean {
    return dataType.isArray || dataType.isAssociativeArray
}

fun <F : Any> Field<out F>.isNotArray(): Boolean {
    return isArray().not()
}