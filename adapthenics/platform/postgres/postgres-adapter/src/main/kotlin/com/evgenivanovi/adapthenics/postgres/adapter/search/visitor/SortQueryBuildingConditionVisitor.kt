package com.evgenivanovi.adapthenics.postgres.adapter.search.visitor

import com.evgenivanovi.search.condition.SortConditionVisitor
import com.evgenivanovi.search.condition.SortConditions
import org.jooq.Field
import org.jooq.SortField
import org.jooq.SortOrder

class SortQueryBuildingConditionVisitor(
    private val property: Field<*>,
) : SortConditionVisitor<SortField<*>> {

    override fun visitAscending(
        condition: SortConditions.Ascending,
    ): SortField<*> {
        return property.sort(SortOrder.ASC)
    }

    override fun visitDescending(
        condition: SortConditions.Descending,
    ): SortField<*> {
        return property.sort(SortOrder.DESC)
    }

    companion object {

        fun forProperty(
            property: Field<*>,
        ): SortQueryBuildingConditionVisitor {
            return SortQueryBuildingConditionVisitor(property)
        }

    }

}