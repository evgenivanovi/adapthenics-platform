package com.evgenivanovi.adapthenics.postgres.adapter.requester

import org.jooq.Record
import org.jooq.Result
import org.jooq.SelectQuery

interface PostgresReadRequester {

    suspend fun findMany(query: SelectQuery<*>): Result<Record>

    suspend fun findOne(query: SelectQuery<*>): Result<Record>

    suspend fun findCount(query: SelectQuery<*>): Long

}