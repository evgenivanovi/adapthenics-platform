package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.service.repo.DeleteRepository

// @formatter:off
interface PostgresDeleteRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
> : DeleteRepository<ID, ENTITY>
// @formatter:on
