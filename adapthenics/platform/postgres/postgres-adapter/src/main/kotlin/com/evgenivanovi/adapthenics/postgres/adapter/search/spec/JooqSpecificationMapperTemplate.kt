package com.evgenivanovi.adapthenics.postgres.adapter.search.spec

import com.evgenivanovi.adapthenics.postgres.adapter.search.visitor.SortQueryBuildingConditionVisitor
import com.evgenivanovi.adapthenics.postgres.adapter.search.visitor.Visitors
import com.evgenivanovi.kt.ex.UnreachableExceptionObject
import com.evgenivanovi.kt.lang.cast
import com.evgenivanovi.search.condition.*
import com.evgenivanovi.search.query.adapter.FieldMapper
import com.evgenivanovi.search.query.adapter.SpecificationMapper
import com.evgenivanovi.search.spec.*
import com.evgenivanovi.search.query.chunk.ChunkingQueries
import com.evgenivanovi.search.query.chunk.ChunkingQuery
import com.evgenivanovi.search.query.SliceQuery
import com.evgenivanovi.search.query.cursor.CursoringQueries
import com.evgenivanovi.search.query.cursor.CursoringQuery
import com.evgenivanovi.search.query.page.PageableQueries
import com.evgenivanovi.search.query.page.PageableQuery
import org.jooq.Condition
import org.jooq.Field
import org.jooq.SortField
import org.jooq.TableField

class JooqSpecificationMapperTemplate(
    private val mapper: FieldMapper<TableField<*, out Any>>,
    private val visitors: Visitors,
) : SpecificationMapper<Condition, SortField<out Any>> {

    override fun mapFieldQuery(
        specification: Specification,
    ): List<Condition> {
        return specification
            .searchConditions()
            .mapNotNull(::buildQueryForCondition)
    }

    override fun mapFieldQuery(
        specification: CountSpecification,
    ): List<Condition> {
        return specification
            .searchConditions()
            .mapNotNull(::buildQueryForCondition)
    }

    private inline fun <reified F : Any> buildQueryForCondition(
        condition: SearchCondition<out F>,
    ): Condition? {

        val field: TableField<*, out Any> = mapper
            .get(condition.key())
            ?: return null

        val isAssignable: Boolean = field
            .dataType
            .arrayBaseType
            .isAssignableFrom(condition.valueType())

        if (isAssignable) {

            val castField: TableField<*, F> = field.cast()

            return when (val castCondition: SearchCondition<F> = condition.cast()) {
                is SearchSingleCondition<F> -> {
                    queryForSingleCondition(castField, castCondition)
                }

                is SearchArrayCondition<F> -> {
                    queryForArrayCondition(castField, castCondition)
                }
            }

        }

        throw UnreachableExceptionObject

    }

    private fun <F : Any> queryForSingleCondition(
        property: TableField<*, F>,
        condition: SearchSingleCondition<F>,
    ): Condition {
        return visitors
            .getSingleVisitor(property)
            ?.let(condition::accept)
            ?: throw UnreachableExceptionObject
    }

    private fun <F : Any> queryForArrayCondition(
        property: TableField<*, F>,
        condition: SearchArrayCondition<F>,
    ): Condition {
        return visitors
            .getArrayVisitor(property)
            ?.let(condition::accept)
            ?: throw UnreachableExceptionObject
    }

    override fun mapSortQuery(
        specification: Specification,
    ): List<SortField<out Any>> {
        return specification
            .sortConditions()
            .mapNotNull(::buildSortQueryForCondition)
    }

    private fun buildSortQueryForCondition(
        condition: SortCondition,
    ): SortField<out Any>? {
        return mapper
            .get(condition.key())
            ?.let { sortQuery(it, condition) }
    }

    private fun sortQuery(
        property: Field<out Any>,
        condition: SortCondition,
    ): SortField<*> {

        return when (condition) {
            is SortConditions.Ascending -> {
                SortQueryBuildingConditionVisitor
                    .forProperty(property)
                    .visitAscending(condition)
            }

            is SortConditions.Descending -> {
                SortQueryBuildingConditionVisitor
                    .forProperty(property)
                    .visitDescending(condition)
            }
        }

    }

    override fun mapSliceQuery(
        specification: SlicedSpecification,
    ): SliceQuery {
        return when (specification) {
            is PagedSpecification ->
                mapPageableQuery(specification)

            is ChunkedSpecification ->
                mapChunkingQuery(specification)

            is CursorSpecification ->
                mapCursorQuery(specification)
        }
    }

    private fun mapPageableQuery(
        specification: PagedSpecification,
    ): PageableQuery {
        return PageableQueries
            .paged(specification.page())
    }

    private fun mapChunkingQuery(
        specification: ChunkedSpecification,
    ): ChunkingQuery {
        return ChunkingQueries
            .chunked(specification.chunk())
    }

    private fun mapCursorQuery(
        specification: CursorSpecification,
    ): CursoringQuery {
        return CursoringQueries
            .cursored(specification.cursor())
    }

}
