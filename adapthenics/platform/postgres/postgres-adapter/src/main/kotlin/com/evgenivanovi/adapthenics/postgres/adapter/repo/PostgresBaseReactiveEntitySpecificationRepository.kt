package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqQueries
import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqSliceQueries
import com.evgenivanovi.adapthenics.schema.mapper.DocumentMapper
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.domain.model.entity.Entity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

// @formatter:off
interface PostgresBaseReactiveEntitySpecificationRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    DOCUMENT : Document<ID>,
> : BaseReactiveEntitySpecificationRepository<ID, ENTITY> {
// @formatter:on

    val repo: PostgresBaseReactiveDocumentSpecificationRepository<ID, DOCUMENT>
    val mapper: DocumentMapper<ID, ENTITY, DOCUMENT>

    override fun executeFindManyBy(
        queries: JooqQueries,
    ): Flow<ENTITY> {
        return repo
            .executeFindManyBy(queries)
            .map(mapper::to)
    }

    override suspend fun executeFindManyBy(
        queries: JooqSliceQueries,
    ): Collection<ENTITY> {
        return repo
            .executeFindManyBy(queries)
            .map(mapper::to)
    }

    override suspend fun executeCountBy(queries: JooqQueries): Long {
        return repo.executeCountBy(queries)
    }

}