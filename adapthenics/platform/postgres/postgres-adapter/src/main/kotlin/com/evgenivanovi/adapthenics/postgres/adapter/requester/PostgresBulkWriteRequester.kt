package com.evgenivanovi.adapthenics.postgres.adapter.requester

import org.jooq.Configuration
import org.jooq.TableRecord
import org.jooq.UpdatableRecord

interface PostgresBulkWriteRequester {

    suspend fun softInsert(
        ctx: Configuration,
        records: Collection<TableRecord<*>>,
    )

    suspend fun forceInsert(
        ctx: Configuration,
        records: Collection<TableRecord<*>>,
    )

    suspend fun softUpdate(
        ctx: Configuration,
        records: Collection<UpdatableRecord<*>>,
    )

    suspend fun forceUpdate(
        ctx: Configuration,
        records: Collection<UpdatableRecord<*>>,
    )

}