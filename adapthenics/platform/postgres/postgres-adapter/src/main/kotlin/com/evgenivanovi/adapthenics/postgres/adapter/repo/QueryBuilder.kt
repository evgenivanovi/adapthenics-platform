package com.evgenivanovi.adapthenics.postgres.adapter.repo

import org.jooq.*

interface QueryBuilder<ID : Any> {

    fun get(
        id: ID
    ): SelectQuery<Record>

    fun get(
        ids: Collection<ID>
    ): SelectQuery<Record>

    fun count(
        where: Condition
    ): SelectQuery<Record1<Long>>

    fun search(
        where: Condition,
        order: Collection<SortField<*>>,
        limit: Long?,
        offset: Long?
    ): SelectQuery<Record>

}