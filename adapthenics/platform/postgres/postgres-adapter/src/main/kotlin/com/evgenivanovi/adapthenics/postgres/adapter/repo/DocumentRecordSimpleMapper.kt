package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.schema.model.api.Document
import org.jooq.Record

interface DocumentRecordSimpleMapper<DOCUMENT : Document<*>> {

    fun map(result: Record): DOCUMENT

}