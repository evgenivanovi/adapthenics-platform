package com.evgenivanovi.adapthenics.postgres.adapter.requester

import org.jooq.*

class PostgresRequesterService(
    private val read: PostgresReadRequester,
    private val write: PostgresWriteRequester,
    private val writeBulk: PostgresBulkWriteRequester,
    private val delete: PostgresDeleteRequester,
    private val deleteBulk: PostgresBulkDeleteRequester,
) : PostgresRequester {

    override suspend fun findMany(
        query: SelectQuery<*>,
    ): Result<Record> {
        return read.findMany(query)
    }

    override suspend fun findOne(
        query: SelectQuery<*>,
    ): Result<Record> {
        return read.findOne(query)
    }

    override suspend fun findCount(
        query: SelectQuery<*>,
    ): Long {
        return read.findCount(query)
    }

    override suspend fun softInsert(
        ctx: Configuration, record: TableRecord<*>,
    ): Record {
        return write.softInsert(ctx, record)
    }

    override suspend fun softInsert(
        ctx: Configuration, record: UpdatableRecord<*>,
    ): Record {
        return write.softInsert(ctx, record)
    }

    override suspend fun forceInsert(
        ctx: Configuration, record: TableRecord<*>,
    ): Record {
        return write.forceInsert(ctx, record)
    }

    override suspend fun forceInsert(
        ctx: Configuration, record: UpdatableRecord<*>,
    ): Record {
        return write.forceInsert(ctx, record)
    }

    override suspend fun softUpdate(
        ctx: Configuration, record: TableRecord<*>,
    ): Record? {
        return write.softUpdate(ctx, record)
    }

    override suspend fun softUpdate(
        ctx: Configuration, record: UpdatableRecord<*>,
    ): Record? {
        return write.softUpdate(ctx, record)
    }

    override suspend fun forceUpdate(
        ctx: Configuration, record: TableRecord<*>,
    ): Record? {
        return write.forceUpdate(ctx, record)
    }

    override suspend fun forceUpdate(
        ctx: Configuration, record: UpdatableRecord<*>,
    ): Record? {
        return write.forceUpdate(ctx, record)
    }

    override suspend fun softMerge(
        ctx: Configuration, record: TableRecord<*>,
    ): Record? {
        return write.softMerge(ctx, record)
    }

    override suspend fun softMerge(
        ctx: Configuration, record: UpdatableRecord<*>,
    ): Record? {
        return write.softMerge(ctx, record)
    }

    override suspend fun softMerge(
        ctx: Configuration, table: Table<*>, fields: Map<Field<*>, Any?>,
    ): Record? {
        return write.softMerge(ctx, table, fields)
    }

    override suspend fun forceMerge(
        ctx: Configuration, record: TableRecord<*>,
    ): Record? {
        return write.forceMerge(ctx, record)
    }

    override suspend fun forceMerge(
        ctx: Configuration, record: UpdatableRecord<*>,
    ): Record? {
        return write.forceMerge(ctx, record)
    }

    override suspend fun forceMerge(
        ctx: Configuration, table: Table<*>, fields: Map<Field<*>, Any?>,
    ): Record? {
        return write.forceMerge(ctx, table, fields)
    }

    override suspend fun softInsert(
        ctx: Configuration, records: Collection<TableRecord<*>>,
    ) {
        return writeBulk.softInsert(ctx, records)
    }

    override suspend fun forceInsert(
        ctx: Configuration, records: Collection<TableRecord<*>>,
    ) {
        return writeBulk.forceInsert(ctx, records)
    }

    override suspend fun softUpdate(
        ctx: Configuration, records: Collection<UpdatableRecord<*>>,
    ) {
        return writeBulk.softUpdate(ctx, records)
    }

    override suspend fun forceUpdate(
        ctx: Configuration, records: Collection<UpdatableRecord<*>>,
    ) {
        return writeBulk.forceUpdate(ctx, records)
    }

    override suspend fun <ID : Any> delete(
        ctx: Configuration, id: ID, table: Table<*>,
    ): Record? {
        return delete.delete(ctx, id, table)
    }

    override suspend fun <ID : Any> delete(
        ctx: Configuration, ids: Collection<ID>, table: Table<*>,
    ): Result<out Record> {
        return deleteBulk.delete(ctx, ids, table)
    }

    override suspend fun delete(
        ctx: Configuration, condition: Condition, table: Table<*>,
    ): Result<out Record> {
        return deleteBulk.delete(ctx, condition, table)
    }

    override suspend fun <ID : Any> archive(
        ctx: Configuration, id: ID, table: Table<*>,
    ): Record? {
        return delete.archive(ctx, id, table)
    }

    override suspend fun <ID : Any> archive(
        ctx: Configuration, ids: Collection<ID>, table: Table<*>,
    ): Result<out Record> {
        return deleteBulk.archive(ctx, ids, table)
    }

    override suspend fun archive(
        ctx: Configuration, condition: Condition, table: Table<*>,
    ): Result<out Record> {
        return deleteBulk.archive(ctx, condition, table)
    }

    override suspend fun <ID : Any> unarchive(
        ctx: Configuration,
        id: ID, table: Table<*>,
    ): Record? {
        return delete.unarchive(ctx, id, table)
    }

    override suspend fun <ID : Any> unarchive(
        ctx: Configuration, ids: Collection<ID>, table: Table<*>,
    ): Result<out Record> {
        return deleteBulk.unarchive(ctx, ids, table)
    }

    override suspend fun unarchive(
        ctx: Configuration, condition: Condition, table: Table<*>,
    ): Result<out Record> {
        return deleteBulk.unarchive(ctx, condition, table)
    }

}