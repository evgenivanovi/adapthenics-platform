package com.evgenivanovi.adapthenics.postgres.adapter.search.query

import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.evgenivanovi.search.query.SliceQuery
import org.jooq.Condition
import org.jooq.SortField

class JooqSliceQueries
private constructor(
    val queries: JooqQueries,
    val slice: SliceQuery,
) {

    /* Overridden methods [Any] */

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object {

        fun of(
            queries: JooqQueries,
            slice: SliceQuery,
        ): JooqSliceQueries {
            return JooqSliceQueries(
                queries,
                slice
            )
        }

        fun of(
            fields: Condition,
            slice: SliceQuery,
        ): JooqSliceQueries {
            return JooqSliceQueries(
                JooqQueries.of(fields),
                slice
            )
        }

        fun of(
            fields: Condition,
            sorts: List<SortField<*>>,
            slice: SliceQuery,
        ): JooqSliceQueries {
            return JooqSliceQueries(
                JooqQueries.of(fields, sorts),
                slice
            )
        }

        private val properties = arrayOf(
            JooqSliceQueries::queries,
            JooqSliceQueries::slice
        )

    }

}