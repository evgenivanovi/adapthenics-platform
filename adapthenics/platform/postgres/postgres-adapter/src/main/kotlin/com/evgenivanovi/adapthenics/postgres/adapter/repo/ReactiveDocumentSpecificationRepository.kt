package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.search.spec.ReactiveSpecificationRepository

interface ReactiveDocumentSpecificationRepository<ID : Any, DOCUMENT : Document<ID>>
    : ReactiveSpecificationRepository<ID, DOCUMENT>