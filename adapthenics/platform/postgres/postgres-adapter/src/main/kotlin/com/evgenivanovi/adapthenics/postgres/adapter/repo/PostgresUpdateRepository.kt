@file:Suppress(
    "DuplicatedCode"
)

package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.service.repo.UpdateRepository

// @formatter:off
interface PostgresUpdateRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    RESULT_ID : Any,
    RESULT_ENTITY : Entity<RESULT_ID>
> : UpdateRepository<ID, ENTITY, RESULT_ID, RESULT_ENTITY>
// @formatter:on