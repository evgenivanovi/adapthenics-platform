@file:Suppress(
    "DuplicatedCode"
)

package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.service.repo.NonAutoSaveRepository

// @formatter:off
interface PostgresNonAutoSaveRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
> : NonAutoSaveRepository<ID, ENTITY>
// @formatter:on