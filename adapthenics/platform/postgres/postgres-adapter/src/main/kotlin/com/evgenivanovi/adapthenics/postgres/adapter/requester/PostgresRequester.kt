package com.evgenivanovi.adapthenics.postgres.adapter.requester

interface PostgresRequester :
    PostgresReadRequester,
    PostgresWriteRequester, PostgresBulkWriteRequester,
    PostgresDeleteRequester, PostgresBulkDeleteRequester