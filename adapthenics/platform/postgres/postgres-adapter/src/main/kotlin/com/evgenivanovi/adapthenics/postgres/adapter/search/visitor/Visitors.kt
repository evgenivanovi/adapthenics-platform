package com.evgenivanovi.adapthenics.postgres.adapter.search.visitor

import com.evgenivanovi.kt.ex.UnreachableExceptionObject
import com.evgenivanovi.kt.lang.cast
import com.evgenivanovi.search.condition.SearchArrayConditionVisitor
import com.evgenivanovi.search.condition.SearchSingleConditionVisitor
import org.jooq.Condition
import org.jooq.TableField

interface Visitors {

    fun <F : Any> getSingleVisitor(
        field: TableField<*, F>,
    ): SearchSingleConditionVisitor<F, Condition>?

    fun <F : Any> getArrayVisitor(
        field: TableField<*, F>,
    ): SearchArrayConditionVisitor<F, Condition>?

    companion object {

        val DEFAULT = object : Visitors {

            override fun <F : Any> getSingleVisitor(
                field: TableField<*, F>,
            ): SearchSingleConditionVisitor<F, Condition> {
                return DefaultSingleFieldQueryBuildingConditionVisitor
                    .forProperty(field)
            }

            override fun <F : Any> getArrayVisitor(
                field: TableField<*, F>,
            ): SearchArrayConditionVisitor<F, Condition> {

                if (field.isArray().not()) {
                    return DefaultArrayFieldQueryBuildingConditionVisitor
                        .forSingleProperty(field)
                }

                if (field.isArray()) {
                    return DefaultArrayFieldQueryBuildingConditionVisitor
                        .forArrayProperty(field.cast())
                }

                throw UnreachableExceptionObject

            }

        }

    }

}
