package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.jooqx.combine
import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqQueries
import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqSliceQueries
import com.evgenivanovi.adapthenics.postgres.adapter.search.spec.JooqSpecificationMapperTemplate
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.search.spec.CountSpecification
import com.evgenivanovi.search.spec.SlicedSpecification
import com.evgenivanovi.search.spec.Specification
import kotlinx.coroutines.flow.Flow

interface BaseReactiveDocumentSpecificationRepository<ID : Any, DOCUMENT : Document<ID>>
    : ReactiveDocumentSpecificationRepository<ID, DOCUMENT> {

    val specificationMapper: JooqSpecificationMapperTemplate

    override fun findR(
        specification: Specification,
    ): Flow<DOCUMENT> {

        val fields = specificationMapper
            .mapFieldQuery(specification)
            .combine()

        val sorts = specificationMapper
            .mapSortQuery(specification)

        val queries = JooqQueries
            .of(fields, sorts)

        return executeFindManyBy(queries)

    }

    override suspend fun find(
        specification: SlicedSpecification,
    ): Collection<DOCUMENT> {

        val fields = specificationMapper
            .mapFieldQuery(specification)
            .combine()

        val sorts = specificationMapper
            .mapSortQuery(specification)

        val slice = specificationMapper
            .mapSliceQuery(specification)

        val queries = JooqSliceQueries
            .of(fields, sorts, slice)

        return executeFindManyBy(queries)

    }

    override suspend fun count(
        specification: CountSpecification,
    ): Long {

        val fields = specificationMapper
            .mapFieldQuery(specification)
            .combine()

        val queries = JooqQueries.of(fields)
        return executeCountBy(queries)

    }

    fun executeFindManyBy(queries: JooqQueries): Flow<DOCUMENT>

    suspend fun executeFindManyBy(queries: JooqSliceQueries): Collection<DOCUMENT>

    suspend fun executeCountBy(queries: JooqQueries): Long

}