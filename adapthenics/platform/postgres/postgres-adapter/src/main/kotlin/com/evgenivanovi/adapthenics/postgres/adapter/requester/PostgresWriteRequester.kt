package com.evgenivanovi.adapthenics.postgres.adapter.requester

import org.jooq.*

interface PostgresWriteRequester {

    suspend fun softInsert(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record

    suspend fun softInsert(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record

    suspend fun forceInsert(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record

    suspend fun forceInsert(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record

    suspend fun softUpdate(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record?

    suspend fun softUpdate(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record?

    suspend fun forceUpdate(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record?

    suspend fun forceUpdate(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record?

    suspend fun softMerge(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record?

    suspend fun softMerge(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record?

    suspend fun forceMerge(
        ctx: Configuration,
        record: TableRecord<*>,
    ): Record?

    suspend fun forceMerge(
        ctx: Configuration,
        record: UpdatableRecord<*>,
    ): Record?

    suspend fun softMerge(
        ctx: Configuration,
        table: Table<*>,
        fields: Map<Field<*>, Any?>,
    ): Record?

    suspend fun forceMerge(
        ctx: Configuration,
        table: Table<*>,
        fields: Map<Field<*>, Any?>,
    ): Record?

}