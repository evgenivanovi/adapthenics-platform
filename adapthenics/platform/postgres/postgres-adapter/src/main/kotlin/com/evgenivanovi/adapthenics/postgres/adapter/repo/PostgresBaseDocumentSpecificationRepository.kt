package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.postgres.adapter.requester.PostgresReadRequester
import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqQueries
import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqSliceQueries
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.kt.ex.UnknownTypeException
import com.evgenivanovi.search.query.chunk.ChunkingQuery
import com.evgenivanovi.search.query.cursor.CursoringQuery
import com.evgenivanovi.search.query.page.PageableQuery
import org.jooq.Record
import org.jooq.Result
import org.jooq.SelectQuery

// @formatter:off
interface PostgresBaseDocumentSpecificationRepository
<
    ID : Any,
    DOCUMENT : Document<ID>,
> : BaseDocumentSpecificationRepository<ID, DOCUMENT> {
// @formatter:on

    val requester: PostgresReadRequester
    val querier: QueryBuilder<ID>
    val mapper: DocumentRecordComplexMapper<DOCUMENT>
    val type: Class<DOCUMENT>

    override suspend fun executeFindManyBy(
        queries: JooqQueries,
    ): Collection<DOCUMENT> {

        val query = buildQuery(queries)

        val entities = requester
            .findMany(query)
            .takeIf(Result<Record>::isNotEmpty)
            ?.let(mapper::mapMulti)

        return entities.orEmpty()

    }

    override suspend fun executeFindManyBy(
        queries: JooqSliceQueries,
    ): Collection<DOCUMENT> {

        val query = buildSliceQuery(queries)

        val entities = requester
            .findMany(query)
            .takeIf(Result<Record>::isNotEmpty)
            ?.let(mapper::mapMulti)

        return entities.orEmpty()

    }

    override suspend fun executeCountBy(queries: JooqQueries): Long {
        val query = buildQuery(queries)
        return requester.findCount(query)
    }

    private fun buildQuery(queries: JooqQueries): SelectQuery<Record> {
        return querier.search(
            where = queries.fields,
            order = queries.sorts,
            limit = null,
            offset = null
        )
    }

    private fun buildSliceQuery(queries: JooqSliceQueries): SelectQuery<Record> {

        return when (val slice = queries.slice) {

            is PageableQuery -> {
                querier.search(
                    where = queries.queries.fields,
                    order = queries.queries.sorts,
                    limit = slice.size(),
                    offset = slice.offset()
                )
            }

            is ChunkingQuery -> {
                querier.search(
                    where = queries.queries.fields,
                    order = queries.queries.sorts,
                    limit = slice.limit(),
                    offset = slice.offset()
                )
            }

            is CursoringQuery -> {
                querier.search(
                    where = queries.queries.fields,
                    order = queries.queries.sorts,
                    limit = slice.limit(),
                    offset = null
                )
            }

            else -> throw UnknownTypeException(slice)

        }

    }

}