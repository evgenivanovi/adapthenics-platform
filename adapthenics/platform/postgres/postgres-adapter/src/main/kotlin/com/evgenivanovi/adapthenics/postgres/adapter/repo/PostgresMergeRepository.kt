@file:Suppress(
    "DuplicatedCode"
)

package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.adapthenics.schema.model.api.PartialDocument
import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.entity.PartialEntity
import com.evgenivanovi.domain.service.repo.MergeRepository

// @formatter:off
interface PostgresMergeRepository
<
    ID : Any,
    ENTITY : PartialEntity<ID>,
    RESULT_ID : Any,
    RESULT_ENTITY : Entity<RESULT_ID>,
    DOCUMENT: PartialDocument<ID>,
    RESULT_DOCUMENT : Document<ID>
> : MergeRepository<ID, ENTITY, RESULT_ID, RESULT_ENTITY> {
// @formatter:on

}