package com.evgenivanovi.adapthenics.postgres.adapter.requester

import com.evgenivanovi.adapthenics.schema.model.api.Document
import org.jooq.Record
import org.jooq.Result
import org.jooq.SelectQuery

interface PostgresSelectRequester {

    suspend fun <T : Document<*>> executeSingle(
        query: SelectQuery<Record>,
        map: (Result<Record>) -> T,
    ): T?

    suspend fun <T : Document<*>> executeMulti(
        query: SelectQuery<Record>,
        map: (Result<Record>) -> Collection<T>,
    ): Collection<T>

    suspend fun execute(
        query: SelectQuery<Record>,
    ): Result<Record>

}