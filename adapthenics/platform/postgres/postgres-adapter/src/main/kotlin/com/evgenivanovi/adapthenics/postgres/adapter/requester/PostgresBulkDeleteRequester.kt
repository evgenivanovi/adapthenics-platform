package com.evgenivanovi.adapthenics.postgres.adapter.requester

import org.jooq.*

interface PostgresBulkDeleteRequester {

    suspend fun <ID : Any> delete(
        ctx: Configuration,
        ids: Collection<ID>,
        table: Table<*>,
    ): Result<out Record>

    suspend fun delete(
        ctx: Configuration,
        condition: Condition,
        table: Table<*>,
    ): Result<out Record>

    suspend fun <ID : Any> archive(
        ctx: Configuration,
        ids: Collection<ID>,
        table: Table<*>,
    ): Result<out Record>

    suspend fun archive(
        ctx: Configuration,
        condition: Condition,
        table: Table<*>,
    ): Result<out Record>

    suspend fun <ID : Any> unarchive(
        ctx: Configuration,
        ids: Collection<ID>,
        table: Table<*>,
    ): Result<out Record>

    suspend fun unarchive(
        ctx: Configuration,
        condition: Condition,
        table: Table<*>,
    ): Result<out Record>

}