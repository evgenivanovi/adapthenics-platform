package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.schema.model.api.Document
import org.jooq.Record
import org.jooq.Result

interface DocumentRecordComplexMapper<DOCUMENT : Document<*>> {

    fun mapSingle(result: Result<Record>): DOCUMENT

    fun mapMulti(result: Result<Record>): Collection<DOCUMENT>

}