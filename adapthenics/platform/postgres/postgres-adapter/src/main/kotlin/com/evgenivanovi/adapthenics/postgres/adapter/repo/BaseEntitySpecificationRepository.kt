package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.adapthenics.jooqx.combine
import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqQueries
import com.evgenivanovi.adapthenics.postgres.adapter.search.query.JooqSliceQueries
import com.evgenivanovi.adapthenics.postgres.adapter.search.spec.JooqSpecificationMapperTemplate
import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.search.EntitySpecificationRepository
import com.evgenivanovi.search.spec.CountSpecification
import com.evgenivanovi.search.spec.SlicedSpecification
import com.evgenivanovi.search.spec.Specification

interface BaseEntitySpecificationRepository<ID : Any, ENTITY : Entity<ID>>
    : EntitySpecificationRepository<ID, ENTITY> {

    val specificationMapper: JooqSpecificationMapperTemplate

    override suspend fun find(
        specification: Specification,
    ): Collection<ENTITY> {

        val fields = specificationMapper
            .mapFieldQuery(specification)
            .combine()

        val sorts = specificationMapper
            .mapSortQuery(specification)

        val queries = JooqQueries
            .of(fields, sorts)

        return executeFindManyBy(queries)

    }

    override suspend fun find(
        specification: SlicedSpecification,
    ): Collection<ENTITY> {

        val fields = specificationMapper
            .mapFieldQuery(specification)
            .combine()

        val sorts = specificationMapper
            .mapSortQuery(specification)

        val slice = specificationMapper
            .mapSliceQuery(specification)

        val queries = JooqSliceQueries
            .of(fields, sorts, slice)

        return executeFindManyBy(queries)

    }

    override suspend fun count(
        specification: CountSpecification,
    ): Long {

        val fields = specificationMapper
            .mapFieldQuery(specification)
            .combine()

        val queries = JooqQueries.of(fields)
        return executeCountBy(queries)

    }

    suspend fun executeFindManyBy(queries: JooqQueries): Collection<ENTITY>

    suspend fun executeFindManyBy(queries: JooqSliceQueries): Collection<ENTITY>

    suspend fun executeCountBy(queries: JooqQueries): Long

}