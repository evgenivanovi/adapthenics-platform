@file:Suppress(
    "DuplicatedCode"
)

package com.evgenivanovi.adapthenics.postgres.adapter.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.entity.EntityData
import com.evgenivanovi.domain.service.repo.AutoSaveRepository

// @formatter:off
interface PostgresAutoSaveRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    ENTITY_DATA : EntityData,
> : AutoSaveRepository<ID, ENTITY, ENTITY_DATA>
// @formatter:on