package com.evgenivanovi.adapthenics.postgres.adapter.search.visitor

import com.evgenivanovi.search.condition.SearchSingleConditionVisitor
import com.evgenivanovi.search.condition.SearchSingleConditions
import org.jooq.Condition
import org.jooq.TableField

/**
 * Condition visitor with only one-to-one capabilities.
 */
class DefaultSingleFieldQueryBuildingConditionVisitor<F : Any>(
    private val property: TableField<*, F>,
) : SearchSingleConditionVisitor<F, Condition> {

    override fun visitEquality(
        condition: SearchSingleConditions.Equality<F>,
    ): Condition {
        return SINGLE_EQUALITY_VISITOR(property, condition)
    }

    override fun visitInequality(
        condition: SearchSingleConditions.Inequality<F>,
    ): Condition {
        return SINGLE_INEQUALITY_VISITOR(property, condition)
    }

    override fun visitGreaterThan(
        condition: SearchSingleConditions.Greater<F>,
    ): Condition {
        return SINGLE_GREATER_THAN_VISITOR(property, condition)
    }

    override fun visitLessThan(
        condition: SearchSingleConditions.Less<F>,
    ): Condition {
        return SINGLE_LESS_THAN_VISITOR(property, condition)
    }

    override fun visitGreaterThanEquality(
        condition: SearchSingleConditions.GreaterEquality<F>,
    ): Condition {
        return SINGLE_GREATER_THAN_EQUALITY_VISITOR(property, condition)
    }

    override fun visitLessThanEquality(
        condition: SearchSingleConditions.LessEquality<F>,
    ): Condition {
        return SINGLE_LESS_THAN_EQUALITY_VISITOR(property, condition)
    }

    override fun visitStartsWith(
        condition: SearchSingleConditions.StartsWith<F>,
    ): Condition {
        return SINGLE_STARTS_WITH_VISITOR(property, condition)
    }

    override fun visitDoesNotStartWith(
        condition: SearchSingleConditions.NotStartWith<F>,
    ): Condition {
        return SINGLE_NOT_START_WITH_VISITOR(property, condition)
    }

    override fun visitEndsWith(
        condition: SearchSingleConditions.EndsWith<F>,
    ): Condition {
        return SINGLE_ENDS_WITH_VISITOR(property, condition)
    }

    override fun visitDoesNotEndWith(
        condition: SearchSingleConditions.NotEndWith<F>,
    ): Condition {
        return SINGLE_NOT_END_WITH_VISITOR(property, condition)
    }

    companion object {

        fun <F : Any> forProperty(
            property: TableField<*, F>,
        ): DefaultSingleFieldQueryBuildingConditionVisitor<F> {
            return DefaultSingleFieldQueryBuildingConditionVisitor(property)
        }

    }

}