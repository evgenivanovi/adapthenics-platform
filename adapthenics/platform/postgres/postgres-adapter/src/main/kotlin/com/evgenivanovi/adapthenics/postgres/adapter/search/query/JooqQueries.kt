package com.evgenivanovi.adapthenics.postgres.adapter.search.query

import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import org.jooq.Condition
import org.jooq.SortField

class JooqQueries
private constructor(
    val fields: Condition,
    val sorts: List<SortField<*>>,
) {

    /* Overridden methods [Any] */

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object {

        fun of(
            fields: Condition,
        ): JooqQueries {
            return of(fields, emptyList())
        }

        fun of(
            fields: Condition,
            sorts: List<SortField<*>>,
        ): JooqQueries {
            return JooqQueries(fields, sorts)
        }

        private val properties = arrayOf(
            JooqQueries::fields,
            JooqQueries::sorts
        )

    }

}