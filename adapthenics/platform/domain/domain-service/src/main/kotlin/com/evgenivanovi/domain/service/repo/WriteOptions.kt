package com.evgenivanovi.domain.service.repo

enum class WriteOptions {

    SOFT,

    FORCE;

}
