package com.evgenivanovi.domain.service.repo

import com.evgenivanovi.domain.model.entity.Entity

// @formatter:off
interface DeleteRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
>
{
// @formatter:on

    suspend fun delete(id: ID): ENTITY?

    suspend fun delete(ids: Collection<ID>): Collection<ENTITY>

    suspend fun archive(id: ID): ENTITY?

    suspend fun archive(ids: Collection<ID>): Collection<ENTITY>

    suspend fun unarchive(id: ID): ENTITY?

    suspend fun unarchive(ids: Collection<ID>): Collection<ENTITY>

}