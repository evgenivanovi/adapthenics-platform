package com.evgenivanovi.domain.service.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.entity.PartialEntity

// @formatter:off
interface MergeRepository
<
    ID : Any,
    ENTITY : PartialEntity<ID>,
    RESULT_ID : Any,
    RESULT_ENTITY : Entity<RESULT_ID>,
>
{
// @formatter:on

    suspend fun merge(
        entity: ENTITY,
        options: WriteOptions,
    ): RESULT_ENTITY?

    suspend fun merge(
        entities: Collection<ENTITY>,
        options: WriteOptions,
    ): Collection<RESULT_ENTITY?>

}