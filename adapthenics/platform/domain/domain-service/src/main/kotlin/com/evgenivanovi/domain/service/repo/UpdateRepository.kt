package com.evgenivanovi.domain.service.repo

import com.evgenivanovi.domain.model.entity.Entity

// @formatter:off
interface UpdateRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    RESULT_ID : Any,
    RESULT_ENTITY : Entity<RESULT_ID>
>
{
// @formatter:on

    suspend fun update(
        entity: ENTITY,
        options: WriteOptions,
    ): RESULT_ENTITY?

    suspend fun update(
        entities: Collection<ENTITY>,
        options: WriteOptions,
    ): Collection<RESULT_ENTITY?>

}