package com.evgenivanovi.domain.service.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.entity.EntityData

// @formatter:off
interface AutoSaveRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    ENTITY_DATA: EntityData,
>
{
// @formatter:on

    suspend fun insert(
        data: ENTITY_DATA,
        options: WriteOptions,
    ): ENTITY

    suspend fun insert(
        datas: Collection<ENTITY_DATA>,
        options: WriteOptions,
    ): Collection<ENTITY>

}