package com.evgenivanovi.domain.service.event

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.event.Event

interface EventPublisher<K : Any, E : Entity<*>, EVENT : Event<K, E>> {

    suspend fun publish(event: EVENT)

}