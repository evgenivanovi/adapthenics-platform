package com.evgenivanovi.domain.service.repo

import com.evgenivanovi.domain.model.entity.Entity

// @formatter:off
interface NonAutoSaveRepository
<
    ID : Any,
    ENTITY : Entity<ID>
>
{
// @formatter:on

    suspend fun insert(
        entity: ENTITY,
        options: WriteOptions,
    ): ENTITY

    suspend fun insert(
        entities: Collection<ENTITY>,
        options: WriteOptions,
    ): Collection<ENTITY>

}