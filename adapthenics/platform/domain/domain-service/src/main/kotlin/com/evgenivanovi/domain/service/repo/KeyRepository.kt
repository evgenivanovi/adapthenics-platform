package com.evgenivanovi.domain.service.repo

import com.evgenivanovi.domain.model.entity.Entity

// @formatter:off
interface KeyRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
>
{
// @formatter:on

    suspend fun find(id: ID): ENTITY?

}