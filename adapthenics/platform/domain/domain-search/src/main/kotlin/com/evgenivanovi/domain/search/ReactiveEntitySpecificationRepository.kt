package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.search.spec.ReactiveSpecificationRepository

interface ReactiveEntitySpecificationRepository<ID : Any, ENTITY : Entity<ID>>
    : ReactiveSpecificationRepository<ID, ENTITY>