package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.id.Identity

// @formatter:off
interface EntityFinder
<
    ID : Any,
    IDENTITY : Identity<ID>,
    ENTITY : Entity<ID>
>
{
// @formatter:on

    suspend fun find(id: IDENTITY): ENTITY?

}