package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.search.service.Seeker

interface EntitySeeker<ID : Any, T : Entity<ID>> : Seeker<T>
