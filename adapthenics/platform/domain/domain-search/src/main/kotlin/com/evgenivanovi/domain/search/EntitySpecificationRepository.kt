package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.search.spec.SpecificationRepository

interface EntitySpecificationRepository<ID : Any, ENTITY : Entity<ID>>
    : SpecificationRepository<ID, ENTITY>