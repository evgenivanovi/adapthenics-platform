package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.search.service.ReactiveSeeker

interface ReactiveEntitySeeker<ID : Any, T : Entity<ID>> : ReactiveSeeker<T>
