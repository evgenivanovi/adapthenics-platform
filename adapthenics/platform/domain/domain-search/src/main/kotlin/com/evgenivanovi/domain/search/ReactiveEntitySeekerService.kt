package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.search.service.BaseReactiveSeekerService
import com.evgenivanovi.search.spec.*

abstract class ReactiveEntitySeekerService<ID : Any, ENTITY : Entity<ID>>(
    pk: PrimaryKeyVisitor<ENTITY, String>,
    search: SearchConfiguration,
    sort: SortableConfiguration,
    factory: SpecificationFactory,
    repo: ReactiveSpecificationRepository<ID, ENTITY>,
) : BaseReactiveSeekerService<ID, ENTITY>(
    pk,
    search,
    sort,
    factory,
    repo
), ReactiveEntitySeeker<ID, ENTITY>