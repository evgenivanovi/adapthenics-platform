package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.search.service.BaseSeekerService
import com.evgenivanovi.search.spec.*

abstract class EntitySeekerService<ID : Any, ENTITY : Entity<ID>>(
    pk: PrimaryKeyVisitor<ENTITY, String>,
    search: SearchConfiguration,
    sort: SortableConfiguration,
    factory: SpecificationFactory,
    repo: SpecificationRepository<ID, ENTITY>,
) : BaseSeekerService<ID, ENTITY>(
    pk,
    search,
    sort,
    factory,
    repo
), EntitySeeker<ID, ENTITY>