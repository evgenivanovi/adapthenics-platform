package com.evgenivanovi.domain.search

import com.evgenivanovi.domain.model.entity.EntityExtension
import com.evgenivanovi.domain.model.id.Identity

// @formatter:off
interface EntityExtensionFinder
<
    ID : Any,
    IDENTITY : Identity<ID>,
    ENTITY : EntityExtension<ID>
>
{
// @formatter:on

    suspend fun find(id: IDENTITY): ENTITY?

}