package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.BigIntegerVersionID
import com.evgenivanovi.domain.model.meta.Metadata
import java.math.BigInteger

// @formatter:off
abstract class BigIntegerBaseVersionEntityExtension<ENTITY_DATA : EntityData>(
    identity: BigIntegerVersionID,
    data: ENTITY_DATA,
    metadata: Metadata
) : BaseVersionEntityExtension<BigInteger, BigIntegerVersionID, ENTITY_DATA>(identity, data, metadata)
// @formatter:on