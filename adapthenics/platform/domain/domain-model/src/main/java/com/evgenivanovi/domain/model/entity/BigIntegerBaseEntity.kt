package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.BigIntegerID
import java.math.BigInteger

// @formatter:off
abstract class BigIntegerBaseEntity<ENTITY_DATA : EntityData>(
    identity: BigIntegerID,
    data: ENTITY_DATA,
) : BaseEntity<BigInteger, BigIntegerID, ENTITY_DATA>(identity, data)
// @formatter:on