package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.BigIntegerVersionID
import java.math.BigInteger

// @formatter:off
abstract class BigIntegerBaseVersionEntity<ENTITY_DATA : EntityData>(
    identity: BigIntegerVersionID,
    data: ENTITY_DATA,
) : BaseVersionEntity<BigInteger, BigIntegerVersionID, ENTITY_DATA>(identity, data)
// @formatter:on