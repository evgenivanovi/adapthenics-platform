package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.any.*
import java.math.BigInteger
import kotlin.reflect.KProperty1

open class BigIntegerID(
    protected val _id: BigInteger,
) : Identity<BigInteger>, MixTypeEquality {

    init {
        @Suppress("LeakingThis")
        BigIntegerIdentityValidator.validate(this)
    }

    override fun id(): BigInteger {
        return _id
    }

    fun withID(newID: BigInteger): BigIntegerID {
        return copy(that = this, id = newID)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is BigIntegerID
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<BigIntegerID>() {

        private fun copy(
            that: BigIntegerID,
            id: BigInteger? = null,
        ): BigIntegerID {
            return BigIntegerID(
                _id = id ?: that._id
            )
        }

        override val properties: Array<KProperty1<BigIntegerID, *>> = arrayOf(
            BigIntegerID::_id
        )

    }

}