package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.errx.ErrorCollector
import com.evgenivanovi.kt.errx.ErrorFormats
import com.evgenivanovi.kt.errx.InvalidError
import com.evgenivanovi.kt.errx.InvalidErrorex
import com.evgenivanovi.kt.math.Numbers
import java.math.BigInteger

internal object BigIntegerIdentityValidator {

    fun safeValidate(id: BigIntegerID): Boolean {
        return isPositive(id.id())
    }

    fun safeValidate(id: BigIntegerVersionID): Boolean {
        return isPositive(id.id())
    }

    /* __________________________________________________ */

    fun validate(id: BigIntegerID) {
        val errs = ErrorCollector
            .create()
            .add(mustPositive(id.id()))

        if (errs.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), errs.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    fun validate(id: BigIntegerVersionID) {
        val errs = ErrorCollector
            .create()
            .add(mustPositive(id.id()))

        if (errs.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), errs.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    /* __________________________________________________ */

    private fun isPositive(id: BigInteger): Boolean {
        return id.signum() == Numbers.ONE_I
    }

    private fun mustPositive(id: BigInteger): String? {
        if (isPositive(id)) return null
        return "Invalid ID: '$id'."
    }

}