package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.BigIntegerID
import com.evgenivanovi.domain.model.meta.Metadata
import java.math.BigInteger

// @formatter:off
abstract class BigIntegerBaseEntityExtension<ENTITY_DATA : EntityData>(
    identity: BigIntegerID,
    data: ENTITY_DATA,
    metadata: Metadata
) : BaseEntityExtension<BigInteger, BigIntegerID, ENTITY_DATA>(identity, data, metadata)
// @formatter:on