package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.any.*
import com.evgenivanovi.kt.math.Numbers.ONE
import java.math.BigInteger
import kotlin.reflect.KProperty1

open class BigIntegerVersionID(
    protected val _id: BigInteger,
    protected val _version: Long = DEFAULT_VERSION,
) : VersionIdentity<BigInteger>, MixTypeEquality {

    init {
        @Suppress("LeakingThis")
        BigIntegerIdentityValidator.validate(this)
    }

    override fun id(): BigInteger {
        return _id
    }

    override fun version(): Long {
        return _version
    }

    fun withID(newID: BigInteger): BigIntegerVersionID {
        return copy(that = this, id = newID)
    }

    fun withVersion(newVersion: Long): BigIntegerVersionID {
        return copy(that = this, version = newVersion)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is BigIntegerVersionID
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<BigIntegerVersionID>() {

        const val DEFAULT_VERSION = ONE.unaryMinus()

        private fun copy(
            that: BigIntegerVersionID,
            id: BigInteger? = null,
            version: Long? = null,
        ): BigIntegerVersionID {
            return BigIntegerVersionID(
                _id = id ?: that._id,
                _version = version ?: that._version
            )
        }

        override val properties: Array<KProperty1<BigIntegerVersionID, *>> = arrayOf(
            BigIntegerVersionID::_id,
            BigIntegerVersionID::_version
        )

    }

}