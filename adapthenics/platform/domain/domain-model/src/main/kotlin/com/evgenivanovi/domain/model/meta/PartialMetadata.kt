@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.domain.model.meta

import com.evgenivanovi.domain.model.entity.PartialEntityData
import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.evgenivanovi.kt.lang.Keeper
import com.evgenivanovi.kt.lang.set
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.reflect.KProperty1

class PartialMetadata
private constructor(
    val createdAt: Keeper<Instant>,
    val updatedAt: Keeper<Instant?>,
    val deletedAt: Keeper<Instant?>,
) : PartialEntityData {

    fun withCreatedAt(value: Keeper<Instant>): PartialMetadata {
        return copy(that = this, createdAt = value)
    }

    fun createdNow(): PartialMetadata {
        return copy(that = this, createdAt = Clock.System.now().set())
    }

    fun withUpdatedAt(value: Keeper<Instant?>): PartialMetadata {
        return copy(that = this, updatedAt = value)
    }

    fun updatedNow(): PartialMetadata {
        return copy(that = this, updatedAt = Clock.System.now().set())
    }

    fun withDeletedAt(value: Keeper<Instant?>): PartialMetadata {
        return copy(that = this, deletedAt = value)
    }

    fun deletedNow(): PartialMetadata {
        return copy(that = this, deletedAt = Clock.System.now().set())
    }

    override fun empty(): Boolean {
        return this.createdAt.unset()
            && this.updatedAt.unset()
            && this.deletedAt.unset()
    }

    override fun valid(): Boolean {
        return true
    }

    /* Overridden methods [Any] */

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<PartialMetadata>() {

        fun of(
            createdAt: Keeper<Instant>,
            updatedAt: Keeper<Instant?>,
            deletedAt: Keeper<Instant?>,
        ): PartialMetadata {
            return PartialMetadata(
                createdAt = createdAt,
                updatedAt = updatedAt,
                deletedAt = deletedAt,
            )
        }

        fun empty(): PartialMetadata {
            return of(
                Keeper.unset(),
                Keeper.unset(),
                Keeper.unset()
            )
        }

        private fun copy(
            that: PartialMetadata,
            createdAt: Keeper<Instant> = Keeper.unset(),
            updatedAt: Keeper<Instant?> = Keeper.unset(),
            deletedAt: Keeper<Instant?> = Keeper.unset(),
        ): PartialMetadata {
            return of(
                createdAt = createdAt.or(that.createdAt),
                updatedAt = updatedAt.or(that.updatedAt),
                deletedAt = deletedAt.or(that.deletedAt)
            )
        }

        override val properties: Array<KProperty1<PartialMetadata, *>> = arrayOf(
            PartialMetadata::createdAt,
            PartialMetadata::updatedAt,
            PartialMetadata::deletedAt
        )

    }

}