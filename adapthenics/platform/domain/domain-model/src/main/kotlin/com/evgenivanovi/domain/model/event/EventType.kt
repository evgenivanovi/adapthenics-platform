package com.evgenivanovi.domain.model.event

enum class EventType {

    SAVED,

    CREATED,

    UPDATED,

    DELETED,

    ARCHIVED,

    UNARCHIVED;

    companion object {

        private val ordinals = entries
            .map(EventType::ordinal)

        private val names = entries
            .associateBy(EventType::name)

        private val indices = entries
            .associateBy(EventType::ordinal)

        fun resolve(value: String): EventType? {
            return names[value.uppercase()]
        }

        fun from(value: String): EventType {
            return resolve(value) ?: throw IllegalArgumentException(
                "Invalid value: '${value}' for ${EventType::class.simpleName}!" +
                    " Has to be one of the following: ${entries.joinToString()}."
            )
        }

        fun resolve(value: Int): EventType? {
            return indices[value]
        }

        fun from(value: Int): EventType {
            return resolve(value) ?: throw IllegalArgumentException(
                "Invalid value: '${value}' for ${EventType::class.simpleName}!" +
                    " Has to be one of the following: ${EventType.ordinals.joinToString()}."
            )
        }

    }

}