package com.evgenivanovi.domain.model

fun <T : Partial> T.or(other: T): T {
    return if (empty()) other
    else this
}

fun <T : Partial> T.or(other: () -> T): T {
    return if (empty()) other()
    else this
}