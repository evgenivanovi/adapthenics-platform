@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.domain.model.meta

import com.evgenivanovi.domain.model.entity.BasePartialEntity
import com.evgenivanovi.domain.model.id.StringID
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import kotlin.reflect.KProperty1

class PartialMeta
private constructor(
    identity: StringID,
    data: PartialMetadata,
) : BasePartialEntity<String, StringID, PartialMetadata>(identity, data) {

    fun withID(newID: StringID): PartialMeta {
        return copy(that = this, identity = newID)
    }

    fun withMetadata(metadata: PartialMetadata): PartialMeta {
        return copy(that = this, metadata = metadata)
    }

    fun updatedNow(): PartialMeta {
        return withMetadata(data().updatedNow())
    }

    fun deletedNow(): PartialMeta {
        return withMetadata(data().deletedNow())
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is PartialMeta
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties,
            superEquals = { super.equals(other) }
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object {

        fun of(
            identity: StringID,
            metadata: PartialMetadata,
        ): PartialMeta {
            return PartialMeta(identity, metadata)
        }

        fun of(
            identity: String,
            metadata: PartialMetadata,
        ): PartialMeta {
            return of(StringID(identity), metadata)
        }

        private fun copy(
            that: PartialMeta,
            identity: StringID? = null,
            metadata: PartialMetadata? = null,
        ): PartialMeta {
            return of(
                identity = identity ?: that.identity(),
                metadata = metadata ?: that.data()
            )
        }

        private val properties = arrayOf<KProperty1<PartialMeta, *>>()

    }

}