package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.StringVersionID

// @formatter:off
abstract class StringBaseVersionPartialEntity<ENTITY_DATA : PartialEntityData>(
    identity: StringVersionID,
    data: ENTITY_DATA,
) : BaseVersionEntity<String, StringVersionID, ENTITY_DATA>(identity, data)
// @formatter:on