package com.evgenivanovi.domain.model.id

interface Identity<T : Any> {

    fun id(): T

}