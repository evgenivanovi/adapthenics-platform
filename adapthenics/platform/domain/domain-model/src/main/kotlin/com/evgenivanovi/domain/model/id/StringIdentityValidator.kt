package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.errx.ErrorCollector
import com.evgenivanovi.kt.errx.ErrorFormats
import com.evgenivanovi.kt.errx.InvalidError
import com.evgenivanovi.kt.errx.InvalidErrorex

object StringIdentityValidator {

    fun safeValidate(id: StringID): Boolean {
        return isPresent(id.id())
    }

    fun safeValidate(id: StringVersionID): Boolean {
        return isPresent(id.id())
    }

    /* __________________________________________________ */

    fun validate(id: StringID) {
        val err = ErrorCollector
            .create()
            .add(mustPresent(id.id()))

        if (err.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), err.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    fun validate(id: StringVersionID) {
        val err = ErrorCollector
            .create()
            .add(mustPresent(id.id()))

        if (err.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), err.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    /* __________________________________________________ */

    private fun isPresent(id: String): Boolean {
        return id.isNotBlank()
    }

    private fun mustPresent(id: String): String? {
        if (isPresent(id)) return null
        return "Invalid ID: '$id'."
    }

}