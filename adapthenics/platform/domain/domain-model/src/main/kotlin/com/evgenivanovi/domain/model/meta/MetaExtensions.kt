package com.evgenivanovi.domain.model.meta

fun Meta.createdNow(): Meta {
    return withMetadata(data().createdNow())
}

fun Meta.updatedNow(): Meta {
    return withMetadata(data().updatedNow())
}

fun Meta.deletedNow(): Meta {
    return withMetadata(data().deletedNow())
}