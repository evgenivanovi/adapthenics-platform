package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.StringID

// @formatter:off
abstract class StringBaseEntity<ENTITY_DATA : EntityData>(
    identity: StringID,
    data: ENTITY_DATA,
) : BaseEntity<String, StringID, ENTITY_DATA>(identity, data)
// @formatter:on