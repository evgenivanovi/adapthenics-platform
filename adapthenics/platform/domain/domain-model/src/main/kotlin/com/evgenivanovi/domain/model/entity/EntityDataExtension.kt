package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.meta.Metadata

interface EntityDataExtension : EntityData {

    fun data(): EntityData

    fun metadata(): Metadata

}