package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.LongID

// @formatter:off
abstract class LongBaseEntity<ENTITY_DATA : EntityData>(
    identity: LongID,
    data: ENTITY_DATA,
) : BaseEntity<Long, LongID, ENTITY_DATA>(identity, data)
// @formatter:on