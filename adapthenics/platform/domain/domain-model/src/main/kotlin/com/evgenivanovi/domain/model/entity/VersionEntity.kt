package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.VersionIdentity

interface VersionEntity<T : Any> : Entity<T> {

    fun version(): Long

    override fun identity(): VersionIdentity<T>

}