package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.Identity
import com.evgenivanovi.kt.any.*
import kotlin.reflect.KProperty1

// @formatter:off
abstract class BasePartialEntity
<
    ID : Any,
    IDENTITY : Identity<ID>,
    ENTITY_DATA : PartialEntityData
>
(
    protected val _identity: IDENTITY,
    protected val _data: ENTITY_DATA,
) : PartialEntity<ID>, MixTypeEquality {
// @formatter:on

    override fun id(): ID {
        return _identity.id()
    }

    override fun identity(): IDENTITY {
        return _identity
    }

    override fun data(): ENTITY_DATA {
        return _data
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is BasePartialEntity<*, *, *>
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<BasePartialEntity<*, *, *>>() {

        override val properties: Array<KProperty1<BasePartialEntity<*, *, *>, *>> = arrayOf(
            BasePartialEntity<*, *, *>::_identity,
            BasePartialEntity<*, *, *>::_data,
        )

    }

}