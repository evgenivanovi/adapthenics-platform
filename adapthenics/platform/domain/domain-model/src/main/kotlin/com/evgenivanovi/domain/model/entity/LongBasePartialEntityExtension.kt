package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.LongID
import com.evgenivanovi.domain.model.meta.Metadata

// @formatter:off
abstract class LongBasePartialEntityExtension<ENTITY_DATA : PartialEntityData>(
    identity: LongID,
    data: ENTITY_DATA,
    metadata: Metadata
) : BaseEntityExtension<Long, LongID, ENTITY_DATA>(identity, data, metadata)
// @formatter:on