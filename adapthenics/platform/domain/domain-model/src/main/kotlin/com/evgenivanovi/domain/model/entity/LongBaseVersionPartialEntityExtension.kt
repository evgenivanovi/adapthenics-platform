package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.LongVersionID
import com.evgenivanovi.domain.model.meta.Metadata

// @formatter:off
abstract class LongBaseVersionPartialEntityExtension<ENTITY_DATA : PartialEntityData>(
    identity: LongVersionID,
    data: ENTITY_DATA,
    metadata: Metadata
) : BaseVersionEntityExtension<Long, LongVersionID, ENTITY_DATA>(identity, data, metadata)
// @formatter:on