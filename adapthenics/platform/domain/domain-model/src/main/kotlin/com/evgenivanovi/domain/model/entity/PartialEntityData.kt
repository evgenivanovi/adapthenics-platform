package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.Partial

interface PartialEntityData : EntityData, Partial