package com.evgenivanovi.domain.model.event

import com.evgenivanovi.domain.model.entity.Entity

interface Event<K : Any, E : Entity<*>> {

    fun key(): K

    fun type(): EventType

    fun entity(): E

}