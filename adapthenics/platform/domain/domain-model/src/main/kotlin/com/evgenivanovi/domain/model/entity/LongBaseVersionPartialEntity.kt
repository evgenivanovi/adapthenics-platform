package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.LongVersionID

// @formatter:off
abstract class LongBaseVersionPartialEntity<ENTITY_DATA : PartialEntityData>(
    identity: LongVersionID,
    data: ENTITY_DATA,
) : BaseVersionEntity<Long, LongVersionID, ENTITY_DATA>(identity, data)
// @formatter:on