package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.any.*
import com.evgenivanovi.kt.math.Numbers.ONE
import kotlin.reflect.KProperty1

open class StringVersionID(
    protected val _id: String,
    protected val _version: Long = DEFAULT_VERSION,
) : VersionIdentity<String>, MixTypeEquality {

    init {
        @Suppress("LeakingThis")
        StringIdentityValidator.validate(this)
    }

    override fun id(): String {
        return _id
    }

    override fun version(): Long {
        return _version
    }

    fun withID(newID: String): StringVersionID {
        return copy(that = this, id = newID)
    }

    fun withVersion(newVersion: Long): StringVersionID {
        return copy(that = this, version = newVersion)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is StringVersionID
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<StringVersionID>() {

        const val DEFAULT_VERSION = ONE.unaryMinus()

        private fun copy(
            that: StringVersionID,
            id: String? = null,
            version: Long? = null,
        ): StringVersionID {
            return StringVersionID(
                _id = id ?: that._id,
                _version = version ?: that._version
            )
        }

        override val properties: Array<KProperty1<StringVersionID, *>> = arrayOf(
            StringVersionID::_id,
            StringVersionID::_version,
        )

    }

}