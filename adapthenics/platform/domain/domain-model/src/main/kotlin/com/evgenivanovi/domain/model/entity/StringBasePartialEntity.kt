package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.StringID

// @formatter:off
abstract class StringBasePartialEntity<ENTITY_DATA : PartialEntityData>(
    identity: StringID,
    data: ENTITY_DATA,
) : BaseEntity<String, StringID, ENTITY_DATA>(identity, data)
// @formatter:on