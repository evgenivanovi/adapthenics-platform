package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.StringVersionID
import com.evgenivanovi.domain.model.meta.Metadata

// @formatter:off
abstract class StringBaseVersionPartialEntityExtension<ENTITY_DATA : PartialEntityData>(
    identity: StringVersionID,
    data: ENTITY_DATA,
    metadata: Metadata
) : BaseVersionEntityExtension<String, StringVersionID, ENTITY_DATA>(identity, data, metadata)
// @formatter:on