package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.any.*
import kotlin.reflect.KProperty1

open class StringID(
    protected val _id: String,
) : Identity<String>, MixTypeEquality {

    init {
        @Suppress("LeakingThis")
        StringIdentityValidator.validate(this)
    }

    override fun id(): String {
        return _id
    }

    fun withID(newID: String): StringID {
        return copy(that = this, id = newID)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is StringID
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<StringID>() {

        private fun copy(
            that: StringID,
            id: String? = null,
        ): StringID {
            return StringID(
                _id = id ?: that._id
            )
        }

        override val properties: Array<KProperty1<StringID, *>> = arrayOf(
            StringID::_id
        )

    }

}