package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.meta.PartialMetadata

interface PartialEntityExtension<ID : Any> : PartialEntity<ID> {

    fun entity(): PartialEntity<ID>

    fun metadata(): PartialMetadata

}