package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.VersionIdentity

interface VersionPartialEntity<T : Any> : PartialEntity<T> {

    fun version(): Long

    override fun identity(): VersionIdentity<T>

}