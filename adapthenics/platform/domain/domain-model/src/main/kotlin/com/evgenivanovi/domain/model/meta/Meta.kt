package com.evgenivanovi.domain.model.meta

import com.evgenivanovi.domain.model.entity.BaseEntity
import com.evgenivanovi.domain.model.id.StringID
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString

class Meta
private constructor(
    identity: StringID,
    data: Metadata,
) : BaseEntity<String, StringID, Metadata>(identity, data) {

    fun withID(newID: StringID): Meta {
        return copy(that = this, identity = newID)
    }

    fun withMetadata(metadata: Metadata): Meta {
        return copy(that = this, metadata = metadata)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is Meta
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties,
            superEquals = { super.equals(other) }
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object {

        fun of(
            identity: StringID,
            metadata: Metadata,
        ): Meta {
            return Meta(identity, metadata)
        }

        fun of(
            identity: String,
            metadata: Metadata,
        ): Meta {
            return of(StringID(identity), metadata)
        }

        /* __________________________________________________ */

        private fun copy(
            that: Meta,
            identity: StringID? = null,
            metadata: Metadata? = null,
        ): Meta {
            return of(
                identity = identity ?: that.identity(),
                metadata = metadata ?: that.data()
            )
        }

        private val properties = arrayOf(
            Meta::_data
        )

    }

}