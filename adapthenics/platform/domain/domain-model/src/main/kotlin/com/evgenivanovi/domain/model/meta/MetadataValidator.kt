package com.evgenivanovi.domain.model.meta

import com.evgenivanovi.kt.errx.ErrorCollector
import com.evgenivanovi.kt.errx.ErrorFormats
import com.evgenivanovi.kt.errx.InvalidError
import com.evgenivanovi.kt.errx.InvalidErrorex
import kotlinx.datetime.Instant

internal object MetadataValidator {

    fun safeValidate(obj: Metadata): Boolean {
        return safeValidate(
            createdAt = obj.createdAt,
            updatedAt = obj.updatedAt,
            deletedAt = obj.deletedAt
        )
    }

    fun safeValidate(
        createdAt: Instant,
        updatedAt: Instant?,
        deletedAt: Instant?,
    ): Boolean {
        return safeValidateDates(
            createdAt = createdAt,
            updatedAt = updatedAt,
            deletedAt = deletedAt
        )
    }

    private fun safeValidateDates(
        createdAt: Instant,
        updatedAt: Instant?,
        deletedAt: Instant?,
    ): Boolean {
        return safeValidateUpdateAndCreate(
            createdAt = createdAt,
            updatedAt = updatedAt
        ).and(
            safeValidateDelete(
                createdAt = createdAt,
                updatedAt = updatedAt,
                deletedAt = deletedAt
            )
        )

    }

    private fun safeValidateUpdateAndCreate(
        createdAt: Instant,
        updatedAt: Instant?,
    ): Boolean {
        if (updatedAt == null) return true
        return updatedAt.epochSeconds >= createdAt.epochSeconds
    }

    private fun safeValidateDelete(
        createdAt: Instant,
        updatedAt: Instant?,
        deletedAt: Instant?,
    ): Boolean {
        return if (deletedAt == null) true
        else if (updatedAt == null) deletedAt >= createdAt
        else deletedAt >= createdAt && deletedAt >= updatedAt
    }

    /* __________________________________________________ */

    fun validate(obj: Metadata) {
        validate(
            createdAt = obj.createdAt,
            updatedAt = obj.updatedAt,
            deletedAt = obj.deletedAt
        )
    }

    fun validate(
        createdAt: Instant,
        updatedAt: Instant?,
        deletedAt: Instant?,
    ) {
        val errs = ErrorCollector
            .create()
            .add(validateUpdateAndCreate(createdAt, updatedAt))
            .add(validateDelete(createdAt, updatedAt, deletedAt))

        if (errs.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), errs.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    private fun validateUpdateAndCreate(
        createdAt: Instant,
        updatedAt: Instant?,
    ): String? {
        if (safeValidateUpdateAndCreate(createdAt, updatedAt)) return null
        return "The update time must be greater than or equal to the creation time."
    }

    private fun validateDelete(
        createdAt: Instant,
        updatedAt: Instant?,
        deletedAt: Instant?,
    ): String? {
        if (safeValidateDelete(createdAt, updatedAt, deletedAt)) return null
        return "The delete time must be greater than or equal to the creation time && update time."
    }

}