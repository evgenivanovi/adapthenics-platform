package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.LongID

// @formatter:off
abstract class LongBasePartialEntity<ENTITY_DATA : PartialEntityData>(
    identity: LongID,
    data: ENTITY_DATA,
) : BasePartialEntity<Long, LongID, ENTITY_DATA>(identity, data)
// @formatter:on