package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.errx.ErrorCollector
import com.evgenivanovi.kt.errx.ErrorFormats
import com.evgenivanovi.kt.errx.InvalidError
import com.evgenivanovi.kt.errx.InvalidErrorex
import com.evgenivanovi.kt.math.isPositive

internal object LongIdentityValidator {

    fun safeValidate(id: LongID): Boolean {
        return isPositive(id.id())
    }

    fun safeValidate(id: LongVersionID): Boolean {
        return isPositive(id.id())
    }

    /* __________________________________________________ */

    fun validate(id: LongID) {
        val errs = ErrorCollector
            .create()
            .add(mustPositive(id.id()))

        if (errs.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), errs.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    fun validate(id: LongVersionID) {
        val errs = ErrorCollector
            .create()
            .add(mustPositive(id.id()))

        if (errs.hasErrors()) {
            val invalid = InvalidError.of(ErrorFormats.defaults(), errs.errors())
            throw InvalidErrorex.of(invalid)
        }
    }

    /* __________________________________________________ */

    private fun isPositive(id: Long): Boolean {
        return id.isPositive()
    }

    private fun mustPositive(id: Long): String? {
        if (isPositive(id)) return null
        return "Invalid ID: '$id'."
    }

}