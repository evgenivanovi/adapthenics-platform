package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.VersionIdentity
import com.evgenivanovi.domain.model.meta.PartialMetadata
import com.evgenivanovi.kt.any.*
import kotlin.reflect.KProperty1

// @formatter:off
abstract class BaseVersionPartialEntityExtension
<
    ID : Any,
    IDENTITY : VersionIdentity<ID>,
    ENTITY_DATA : PartialEntityData
>
(
    protected val _identity: IDENTITY,
    protected val _data: ENTITY_DATA,
    protected val _metadata: PartialMetadata
) : VersionPartialEntityExtension<ID>, MixTypeEquality {
// @formatter:on

    override fun id(): ID {
        return _identity.id()
    }

    override fun version(): Long {
        return _identity.version()
    }

    override fun identity(): IDENTITY {
        return _identity
    }

    override fun data(): ENTITY_DATA {
        return _data
    }

    override fun metadata(): PartialMetadata {
        return _metadata
    }

    override fun entity(): VersionPartialEntity<ID> {
        return this
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is BaseVersionPartialEntityExtension<*, *, *>
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<BaseVersionPartialEntityExtension<*, *, *>>() {

        override val properties: Array<KProperty1<BaseVersionPartialEntityExtension<*, *, *>, *>> = arrayOf(
            BaseVersionPartialEntityExtension<*, *, *>::_identity,
            BaseVersionPartialEntityExtension<*, *, *>::_data,
            BaseVersionPartialEntityExtension<*, *, *>::_metadata,
        )

    }

}