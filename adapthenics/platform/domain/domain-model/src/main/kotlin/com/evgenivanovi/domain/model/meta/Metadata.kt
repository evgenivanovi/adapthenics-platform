package com.evgenivanovi.domain.model.meta

import com.evgenivanovi.domain.model.entity.EntityData
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

data class Metadata(
    val createdAt: Instant,
    val updatedAt: Instant?,
    val deletedAt: Instant?,
) : EntityData {

    init {
        MetadataValidator.validate(this)
    }

    fun withCreatedAt(createdAt: Instant): Metadata {

        val result = MetadataValidator
            .safeValidate(createdAt, updatedAt, deletedAt)

        if (result.not())
            return this

        return copy(createdAt = createdAt)

    }

    fun createdNow(): Metadata {
        return copy(createdAt = Clock.System.now())
    }

    fun withUpdatedAt(updatedAt: Instant?): Metadata {

        val result = MetadataValidator
            .safeValidate(createdAt, updatedAt, deletedAt)

        if (result.not())
            return this

        return copy(updatedAt = updatedAt)

    }

    fun updatedNow(): Metadata {
        return copy(updatedAt = Clock.System.now())
    }

    fun withoutUpdated(): Metadata {
        return copy(updatedAt = null)
    }

    fun withDeletedAt(deletedAt: Instant?): Metadata {

        val result = MetadataValidator
            .safeValidate(createdAt, updatedAt, deletedAt)

        if (result.not())
            return this

        return copy(deletedAt = deletedAt)

    }

    fun deletedNow(): Metadata {
        return copy(deletedAt = Clock.System.now())
    }

    fun withoutDeleted(): Metadata {
        return copy(deletedAt = null)
    }

    companion object {

        fun now(): Metadata {
            val now = Clock.System.now()
            return of(now, null, null)
        }

        fun of(
            createdAt: Instant,
            updatedAt: Instant?,
            deletedAt: Instant?,
        ): Metadata {
            return Metadata(createdAt, updatedAt, deletedAt)
        }

        fun off(
            createdAt: Instant = Clock.System.now(),
            updatedAt: Instant? = null,
            deletedAt: Instant? = null,
        ): Metadata {
            return Metadata(createdAt, updatedAt, deletedAt)
        }

    }

}