package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.StringID
import com.evgenivanovi.domain.model.meta.Metadata

// @formatter:off
abstract class StringBasePartialEntityExtension<ENTITY_DATA : PartialEntityData>(
    identity: StringID,
    data: ENTITY_DATA,
    metadata: Metadata
) : BaseEntityExtension<String, StringID, ENTITY_DATA>(identity, data, metadata)
// @formatter:on