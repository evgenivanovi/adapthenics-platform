package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.VersionIdentity
import com.evgenivanovi.domain.model.meta.Metadata
import com.evgenivanovi.kt.any.*
import kotlin.reflect.KProperty1

// @formatter:off
abstract class BaseVersionEntityExtension
<
    ID : Any,
    IDENTITY : VersionIdentity<ID>,
    ENTITY_DATA : EntityData
>
(
    protected val _identity: IDENTITY,
    protected val _data: ENTITY_DATA,
    protected val _metadata: Metadata
) : VersionEntityExtension<ID>, MixTypeEquality {
// @formatter:on

    override fun id(): ID {
        return _identity.id()
    }

    override fun version(): Long {
        return _identity.version()
    }

    override fun identity(): IDENTITY {
        return _identity
    }

    override fun data(): ENTITY_DATA {
        return _data
    }

    override fun metadata(): Metadata {
        return _metadata
    }

    override fun entity(): VersionEntity<ID> {
        return this
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is BaseVersionEntityExtension<*, *, *>
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<BaseVersionEntityExtension<*, *, *>>() {

        override val properties: Array<KProperty1<BaseVersionEntityExtension<*, *, *>, *>> = arrayOf(
            BaseVersionEntityExtension<*, *, *>::_identity,
            BaseVersionEntityExtension<*, *, *>::_data,
            BaseVersionEntityExtension<*, *, *>::_metadata,
        )

    }

}