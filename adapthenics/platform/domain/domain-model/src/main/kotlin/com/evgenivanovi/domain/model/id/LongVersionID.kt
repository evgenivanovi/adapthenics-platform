package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.any.*
import com.evgenivanovi.kt.math.Numbers.ONE
import kotlin.reflect.KProperty1

open class LongVersionID(
    protected val _id: Long,
    protected val _version: Long = DEFAULT_VERSION,
) : VersionIdentity<Long>, MixTypeEquality {

    init {
        @Suppress("LeakingThis")
        LongIdentityValidator.validate(this)
    }

    override fun id(): Long {
        return _id
    }

    override fun version(): Long {
        return _version
    }

    fun withID(newID: Long): LongVersionID {
        return copy(that = this, id = newID)
    }

    fun withVersion(newVersion: Long): LongVersionID {
        return copy(that = this, version = newVersion)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is LongVersionID
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<LongVersionID>() {

        const val DEFAULT_VERSION = ONE.unaryMinus()

        private fun copy(
            that: LongVersionID,
            id: Long? = null,
            version: Long? = null,
        ): LongVersionID {
            return LongVersionID(
                _id = id ?: that._id,
                _version = version ?: that._version
            )
        }

        override val properties: Array<KProperty1<LongVersionID, *>> = arrayOf(
            LongVersionID::_id,
            LongVersionID::_version
        )

    }

}