package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.meta.PartialMetadata

interface PartialEntityDataExtension : PartialEntityData {

    fun data(): PartialEntityData

    fun metadata(): PartialMetadata

}