package com.evgenivanovi.domain.model.entity

interface EntityDescriptor {

    /* Human-readable name
    * For example: "Exercise", "Routine", "Routine result"
    */
    fun name(): String

    /* Machine-readable code
    * For example: "exercise", "routine", "routine_result"
    */
    fun code(): String

}