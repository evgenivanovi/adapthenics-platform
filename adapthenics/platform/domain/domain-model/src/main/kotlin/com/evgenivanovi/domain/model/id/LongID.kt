package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.any.*
import kotlin.reflect.KProperty1

open class LongID(
    protected val _id: Long,
) : Identity<Long>, MixTypeEquality {

    init {
        @Suppress("LeakingThis")
        LongIdentityValidator.validate(this)
    }

    override fun id(): Long {
        return _id
    }

    fun withID(newID: Long): LongID {
        return copy(that = this, id = newID)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is LongID
    }

    override fun equals(other: Any?): Boolean {
        return kotlinEquals(
            other = other,
            properties = properties
        )
    }

    private val hashCode: Int by lazy {
        kotlinHashCode(properties = properties)
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(properties = properties)
    }

    override fun toString() = toString

    companion object : ContractableCompanion<LongID>() {

        private fun copy(
            that: LongID,
            id: Long? = null,
        ): LongID {
            return LongID(
                _id = id ?: that._id
            )
        }

        override val properties: Array<KProperty1<LongID, *>> = arrayOf(
            LongID::_id
        )

    }

}