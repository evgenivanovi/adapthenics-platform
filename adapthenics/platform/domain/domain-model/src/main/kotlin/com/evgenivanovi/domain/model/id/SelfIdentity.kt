package com.evgenivanovi.domain.model.id

import com.evgenivanovi.kt.any.Self

interface SelfIdentity<T : Any> : Identity<T>, Self<T> {

    override fun id(): T {
        return self()
    }

}
