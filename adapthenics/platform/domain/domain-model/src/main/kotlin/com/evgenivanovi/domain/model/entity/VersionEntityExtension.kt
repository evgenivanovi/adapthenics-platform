package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.VersionIdentity
import com.evgenivanovi.domain.model.meta.Metadata

interface VersionEntityExtension<T : Any> : VersionEntity<T>, EntityExtension<T> {

    override fun id(): T

    override fun version(): Long

    override fun identity(): VersionIdentity<T>

    override fun metadata(): Metadata

    override fun entity(): VersionEntity<T>

}