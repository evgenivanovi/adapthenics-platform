package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.Identity

interface Entity<ID : Any> {

    fun id(): ID

    fun identity(): Identity<ID>

    fun data(): EntityData

}