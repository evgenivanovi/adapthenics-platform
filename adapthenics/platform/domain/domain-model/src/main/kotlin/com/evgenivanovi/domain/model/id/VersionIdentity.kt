package com.evgenivanovi.domain.model.id

interface VersionIdentity<T : Any> : Identity<T> {

    fun version(): Long

}