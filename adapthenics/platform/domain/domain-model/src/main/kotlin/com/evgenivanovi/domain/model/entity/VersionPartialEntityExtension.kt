package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.VersionIdentity
import com.evgenivanovi.domain.model.meta.PartialMetadata

interface VersionPartialEntityExtension<T : Any> : VersionPartialEntity<T>, PartialEntityExtension<T> {

    override fun id(): T

    override fun version(): Long

    override fun identity(): VersionIdentity<T>

    override fun metadata(): PartialMetadata

    override fun entity(): VersionPartialEntity<T>

}