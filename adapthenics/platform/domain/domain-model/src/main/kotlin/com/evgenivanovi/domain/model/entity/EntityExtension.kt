package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.meta.Metadata

interface EntityExtension<ID : Any> : Entity<ID> {

    fun entity(): Entity<ID>

    fun metadata(): Metadata

}