package com.evgenivanovi.domain.model

interface Partial {

    fun empty(): Boolean

    fun valid(): Boolean

}