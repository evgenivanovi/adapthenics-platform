package com.evgenivanovi.domain.model.entity

import com.evgenivanovi.domain.model.id.Identity

interface PartialEntity<T : Any> {

    fun id(): T

    fun identity(): Identity<T>

    fun data(): PartialEntityData

}