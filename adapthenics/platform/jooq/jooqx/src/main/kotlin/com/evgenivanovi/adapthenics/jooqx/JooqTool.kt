package com.evgenivanovi.adapthenics.jooqx

import com.evgenivanovi.kt.lambda.Predicate
import com.evgenivanovi.kt.lambda.and
import com.evgenivanovi.kt.lambda.andNot
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.lang.cast
import org.jooq.*
import org.jooq.impl.DSL

object JooqTool {

    @JvmStatic
    fun intoMap(
        record: Record,
        vararg exclude: String,
    ): Map<String, Any?> {
        val excludes = setOf(*exclude)
        return record.intoMap().filterNot { excludes.contains(it.key) }
    }

    @JvmStatic
    fun intoValuesMap(
        record: Record,
    ): Map<String, Any?> {
        val predicate = Predicate.yeap<Field<*>>()
        return doIntoValuesMap(record, predicate::test)
    }

    @JvmStatic
    fun intoValuesMap(
        record: Record,
        vararg exclude: Field<*>,
    ): Map<String, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)) }

        return doIntoValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoValuesMap(
        record: Record,
        vararg exclude: Name,
    ): Map<String, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.unqualifiedName) }

        return doIntoValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoValuesMap(
        record: Record,
        vararg exclude: String,
    ): Map<String, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.name) }

        return doIntoValuesMap(record, predicate::test)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun intoFieldValuesMap(
        record: Record,
    ): Map<Field<*>, Any?> {
        val predicate = Predicate.yeap<Field<*>>()
        return doIntoFieldValuesMap(record, predicate::test)
    }

    @JvmStatic
    fun intoFieldValuesMap(
        record: Record,
        vararg exclude: Field<*>,
    ): Map<Field<*>, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)) }

        return doIntoFieldValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoFieldValuesMap(
        record: Record,
        vararg exclude: Name,
    ): Map<Field<*>, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.unqualifiedName) }

        return doIntoFieldValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoFieldValuesMap(
        record: Record,
        vararg exclude: String,
    ): Map<Field<*>, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.name) }

        return doIntoFieldValuesMap(record, predicate::test)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun intoChangedValuesMap(
        record: Record,
    ): Map<String, Any?> {

        val predicate = Predicate
            .yeap<Field<*>>()
            .and { field -> record.changed(field) }

        return doIntoValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoChangedValuesMap(
        record: Record,
        vararg exclude: Field<*>,
    ): Map<String, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)) }
            .and { field -> record.changed(field) }

        return doIntoValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoChangedValuesMap(
        record: Record,
        vararg exclude: Name,
    ): Map<String, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.unqualifiedName) }
            .and { field -> record.changed(field) }

        return doIntoValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoChangedValuesMap(
        record: Record,
        vararg exclude: String,
    ): Map<String, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.name) }
            .and { field -> record.changed(field) }

        return doIntoValuesMap(record, predicate::test)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun intoChangedFieldValuesMap(
        record: Record,
    ): Map<Field<*>, Any?> {

        val predicate = Predicate
            .yeap<Field<*>>()
            .and { field -> record.changed(field) }

        return doIntoFieldValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoChangedFieldValuesMap(
        record: Record,
        vararg exclude: Field<*>,
    ): Map<Field<*>, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)) }
            .and { field -> record.changed(field) }

        return doIntoFieldValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoChangedFieldValuesMap(
        record: Record,
        vararg exclude: Name,
    ): Map<Field<*>, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.unqualifiedName) }
            .and { field -> record.changed(field) }

        return doIntoFieldValuesMap(record, predicate::test)

    }

    @JvmStatic
    fun intoChangedFieldValuesMap(
        record: Record,
        vararg exclude: String,
    ): Map<Field<*>, Any?> {

        val excludes = setOf(*exclude)

        val predicate = Predicate
            .yeap<Field<*>>()
            .andNot { field -> excludes.contains(record.field(field)?.name) }
            .and { field -> record.changed(field) }

        return doIntoFieldValuesMap(record, predicate::test)

    }

    /* __________________________________________________ */

    @JvmStatic
    private fun doIntoValuesMap(
        record: Record,
        filter: (Field<*>) -> Boolean,
    ): Map<String, Any?> {

        return record
            .fields()
            .asSequence()
            .filter(filter)
            .associateBy(
                keySelector = { it.name },
                valueTransform = { record.get(it) }
            )

    }

    @JvmStatic
    private fun doIntoFieldValuesMap(
        record: Record,
        filter: (Field<*>) -> Boolean,
    ): Map<Field<*>, Any?> {

        return record
            .fields()
            .asSequence()
            .filter(filter)
            .associateBy(
                keySelector = { it },
                valueTransform = { record.get(it) }
            )

    }

    /* __________________________________________________ */

    @JvmStatic
    fun Map<Field<*>, Any?>.exclude(
        vararg exclude: Field<*>,
    ): Map<Field<*>, Any?> {
        val excludes = setOf(*exclude)
        return this.filterNot { excludes.contains(it.key) }
    }

    @JvmStatic
    fun Map<Field<*>, Any?>.exclude(
        vararg exclude: Name,
    ): Map<Field<*>, Any?> {
        val excludes = setOf(*exclude)
        return this.filterNot { excludes.contains(it.key.unqualifiedName) }
    }

    /* __________________________________________________ */

    @JvmStatic
    fun <T> setValue(
        record: Record, field: Field<T>, value: Any?,
    ) {
        record.set(field, field.dataType.convert(value))
    }

    @JvmStatic
    fun <R : Record, T> setValue(
        record: R, field: TableField<out R, T>, value: Any?,
    ) {
        record.set(field, field.dataType.convert(value))
    }

    /* __________________________________________________ */

    @JvmStatic
    fun indexFail(row: Fields, field: Field<*>): IllegalArgumentException {
        return IllegalArgumentException(
            "Field ($field) is not contained in Row $row"
        )
    }

    @JvmStatic
    fun indexOrFail(row: Fields, field: Field<*>): Int {
        val result = row.indexOf(field)
        if (result < 0) throw indexFail(row, field)
        return result
    }

    @JvmStatic
    fun indexFail(row: Fields, fieldName: String): IllegalArgumentException {
        throw IllegalArgumentException(
            "Field ($fieldName) is not contained in Row $row"
        )
    }

    @JvmStatic
    fun indexOrFail(row: Fields, fieldName: String): Int {
        val result = row.indexOf(fieldName)
        if (result < 0) throw indexFail(row, fieldName)
        return result
    }

    @JvmStatic
    fun indexFail(row: Fields, fieldName: Name): IllegalArgumentException {
        throw IllegalArgumentException(
            "Field ($fieldName) is not contained in Row $row"
        )
    }

    @JvmStatic
    fun indexOrFail(row: Fields, fieldName: Name): Int {
        val result = row.indexOf(fieldName)
        if (result < 0) throw indexFail(row, fieldName)
        return result
    }

    @JvmStatic
    fun indexFail(row: Fields, fieldIndex: Int): IllegalArgumentException {
        throw IllegalArgumentException(
            "Field ($fieldIndex) is not contained in Row $row"
        )
    }

    @JvmStatic
    fun indexOrFail(row: Fields, fieldIndex: Int): Int {
        val result = row.field(fieldIndex) ?: throw indexFail(row, fieldIndex)
        return fieldIndex
    }

    /* __________________________________________________ */

    @JvmStatic
    fun addConditions(query: UpdateQuery<*>, record: Record, vararg fields: Field<*>) {
        fields.forEach { field ->
            addCondition(query, record, field)
        }
    }

    @JvmStatic
    fun <T : Any> addCondition(query: UpdateQuery<*>, record: Record, field: Field<T>) {
        query.addConditions(condition(record, field))
    }

    /* __________________________________________________ */

    @JvmStatic
    fun <T : Any> field(value: Any, field: Field<T>): Field<T> {
        return field(value) { DSL.`val`(value, field) }
    }

    @JvmStatic
    fun <T : Any> field(value: Any, defaultValue: () -> Field<T>): Field<T> {
        return if (value is Field<*>) value.cast()
        else defaultValue()
    }

    /* __________________________________________________ */

    @JvmStatic
    fun <T : Any> condition(
        record: Record,
        field: Field<T>,
    ): Condition {
        return condition(field, record.get(field))
    }

    @JvmStatic
    fun <T : Any> condition(
        field: TableField<out Record, out T>,
        value: T?,
    ): Condition {
        return condition(
            field.asOf<Field<T>>(),
            value
        )
    }

    @JvmStatic
    fun <T : Any> condition(
        field: Field<T>,
        value: T?,
    ): Condition {
        return if (value == null) field.isNull
        else field.eq(value)
    }

    /* __________________________________________________ */

    @JvmStatic
    fun <ID : Any> identityCondition(
        id: ID,
        identity: Identity<out Record, ID>,
    ): Condition {
        return identity.field.eq(id)
    }

    @JvmStatic
    fun <ID : Any> identityCondition(
        ids: Collection<ID>,
        identity: Identity<out Record, ID>,
    ): Condition {
        return identity.field.`in`(ids)
    }

    /* __________________________________________________ */

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNEXISTED_FIELD_EX(table: Table<*>, field: Field<*>): Exception {
        return UNEXISTED_FIELD_EX(table, field.name)
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNEXISTED_FIELD_EX(table: Table<*>, name: String): Exception {
        return IllegalArgumentException(UNEXISTED_FIELD_EX_MSG(table, name))
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNEXISTED_FIELD_EX_MSG(table: Table<*>, field: Field<*>): String {
        return UNEXISTED_FIELD_EX_MSG(table, field.name)
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNEXISTED_FIELD_EX_MSG(table: Table<*>, name: String): String {
        return "Table '${table.name}' does not contain '$name' field."
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNPROVIDED_FIELD_EX(table: Table<*>, field: Field<*>): Exception {
        return UNPROVIDED_FIELD_EX(table, field.name)
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNPROVIDED_FIELD_EX(table: Table<*>, name: String): Exception {
        return IllegalArgumentException(UNPROVIDED_FIELD_MSG(table, name))
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNPROVIDED_FIELD_MSG(table: Table<*>, field: Field<*>): String {
        return UNPROVIDED_FIELD_MSG(table, field.name)
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun UNPROVIDED_FIELD_MSG(table: Table<*>, name: String): String {
        return "Field '$name' is not provided for operation with table '${table.name}'."
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun NULLABLE_FIELD_VALUE_EX(field: Field<*>): Exception {
        return NULLABLE_FIELD_VALUE_EX(field.name)
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun NULLABLE_FIELD_VALUE_EX(field: String): Exception {
        return IllegalArgumentException(NULLABLE_FIELD_VALUE_MSG(field))
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun NULLABLE_FIELD_VALUE_MSG(field: Field<*>): String {
        return NULLABLE_FIELD_VALUE_MSG(field.name)
    }

    @JvmStatic
    @Suppress("FunctionName", "SpellCheckingInspection")
    fun NULLABLE_FIELD_VALUE_MSG(field: String): String {
        return "Field value for $field is null."
    }

    /* __________________________________________________ */

}