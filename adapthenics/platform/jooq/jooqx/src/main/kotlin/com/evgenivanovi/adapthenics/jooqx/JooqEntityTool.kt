package com.evgenivanovi.adapthenics.jooqx

import com.evgenivanovi.adapthenics.jooqx.JooqTool.NULLABLE_FIELD_VALUE_EX
import com.evgenivanovi.adapthenics.jooqx.JooqTool.UNEXISTED_FIELD_EX
import com.evgenivanovi.adapthenics.jooqx.JooqTool.UNPROVIDED_FIELD_EX
import com.evgenivanovi.kt.lang.Keeper
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.lang.isNotNull
import com.evgenivanovi.kt.math.Numbers
import org.apache.commons.lang3.time.DateUtils.MILLIS_PER_SECOND
import org.jooq.*
import org.jooq.impl.DSL
import org.jooq.impl.SQLDataType
import java.sql.Timestamp
import java.time.Clock
import java.time.OffsetDateTime

object JooqEntityTool {

    const val PRIMARY_KEY = "PK"
    const val FOREIGN_KEY = "FK"
    const val UNIQUE_KEY = "UK"

    const val ID_FIELD_NAME = "id"

    val VERSION_FIELD_TYPE: DataType<Long> = SQLDataType.BIGINT
    const val VERSION_FIELD_NAME = "version"

    val TIMESTAMP_FIELD_TYPE: DataType<OffsetDateTime> = SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    const val TIMESTAMP_FIELD_NAME = "timestamp"

    val CREATED_AT_FIELD_TYPE: DataType<OffsetDateTime> = SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    const val CREATED_AT_FIELD_NAME = "created_at"

    val UPDATED_AT_FIELD_TYPE: DataType<OffsetDateTime> = SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    const val UPDATED_AT_FIELD_NAME = "updated_at"

    val DELETED_AT_FIELD_TYPE: DataType<OffsetDateTime> = SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    const val DELETED_AT_FIELD_NAME = "deleted_at"

    /* __________________________________________________ */

    val VERSION_FIELD: Field<Long> = DSL.field(
        DSL.name(VERSION_FIELD_NAME),
        SQLDataType.BIGINT
    )

    val VERSION_IS_NULL_CONDITION: Condition = DSL
        .condition(VERSION_FIELD.isNull)

    val VERSION_IS_NOT_NULL_CONDITION: Condition = DSL
        .condition(VERSION_FIELD.isNotNull)

    val TIMESTAMP_FIELD = DSL.field(
        DSL.name(TIMESTAMP_FIELD_NAME),
        SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    )

    val TIMESTAMP_IS_NULL_CONDITION: Condition = DSL
        .condition(TIMESTAMP_FIELD.isNull)

    val TIMESTAMP_IS_NOT_NULL_CONDITION: Condition = DSL
        .condition(TIMESTAMP_FIELD.isNotNull)

    /* __________________________________________________ */

    val CREATED_AT_FIELD: Field<OffsetDateTime> = DSL.field(
        DSL.name(CREATED_AT_FIELD_NAME),
        SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    )

    val UPDATED_AT_FIELD: Field<OffsetDateTime> = DSL.field(
        DSL.name(UPDATED_AT_FIELD_NAME),
        SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    )

    val DELETED_AT_FIELD: Field<OffsetDateTime> = DSL.field(
        DSL.name(DELETED_AT_FIELD_NAME),
        SQLDataType.TIMESTAMPWITHTIMEZONE(Numbers.SIX_I)
    )

    /* __________________________________________________ */

    val CREATED_AT_IS_NULL_CONDITION: Condition = DSL
        .condition(CREATED_AT_FIELD.isNull)

    val CREATED_AT_IS_NOT_NULL_CONDITION: Condition = DSL
        .condition(CREATED_AT_FIELD.isNotNull)

    val UPDATED_AT_IS_NULL_CONDITION: Condition = DSL
        .condition(UPDATED_AT_FIELD.isNull)

    val UPDATED_AT_IS_NOT_NULL_CONDITION: Condition = DSL
        .condition(UPDATED_AT_FIELD.isNotNull)

    val DELETED_AT_IS_NULL_CONDITION: Condition = DSL
        .condition(DELETED_AT_FIELD.isNull)

    val DELETED_AT_IS_NOT_NULL_CONDITION: Condition = DSL
        .condition(DELETED_AT_FIELD.isNotNull)

    /* __________________________________________________ */

    @JvmStatic
    fun <R : Record> isIdentityAvailable(
        table: Table<out R>,
    ): Boolean {
        return table.identity != null
    }

    @JvmStatic
    fun <R : TableRecord<out R>> isIdentityAvailable(
        record: TableRecord<out R>,
    ): Boolean {
        return isIdentityAvailable(record.table)
    }

    @JvmStatic
    fun <R : Record> identityField(
        table: Table<out R>,
    ): TableField<out R, out Any> {
        return getIdentityField(table)
            ?: throw UNEXISTED_FIELD_EX(table, PRIMARY_KEY)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> identityField(
        record: TableRecord<out R>,
    ): TableField<out R, out Any> {
        return getIdentityField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, PRIMARY_KEY)
    }

    @JvmStatic
    fun <R : Record> getIdentityField(
        table: Table<out R>,
    ): TableField<out R, out Any>? {
        return table.identity?.field
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getIdentityField(
        record: TableRecord<out R>,
    ): TableField<out R, out Any>? {
        return getIdentityField(record.table)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> identityFieldValue(
        record: TableRecord<out R>,
    ): Pair<TableField<out R, out Any>, Any> {
        return identityFieldValue(
            record.table,
            JooqTool.intoFieldValuesMap(record)
        )
    }

    @JvmStatic
    fun <R : Record> identityFieldValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Pair<TableField<out R, out Any>, Any> {

        val field: TableField<out R, out Any> = getIdentityField(table)
            ?: throw UNEXISTED_FIELD_EX(table, PRIMARY_KEY)

        if (fields.contains(field).not()) {
            throw UNPROVIDED_FIELD_EX(table, PRIMARY_KEY)
        }

        val value: Any = fields
            .getValue(field)
            ?: throw NULLABLE_FIELD_VALUE_EX(field)

        return field to value

    }

    @JvmStatic
    fun <R : TableRecord<out R>> getIdentityCondition(
        record: TableRecord<out R>,
    ): Condition? {

        val field: TableField<out R, *> = getIdentityField(record)
            ?: return null

        return JooqTool.condition(record, field)

    }

    @JvmStatic
    fun <R : TableRecord<out R>> identityCondition(
        record: TableRecord<out R>,
    ): Condition {

        val field: TableField<out R, *> = getIdentityField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, PRIMARY_KEY)

        return JooqTool.condition(record, field)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun <R : Record> isVersionAvailable(
        table: Table<out R>,
    ): Boolean {
        return table.recordVersion != null
    }

    @JvmStatic
    fun <R : TableRecord<out R>> isVersionAvailable(
        record: TableRecord<out R>,
    ): Boolean {
        return isVersionAvailable(record.table)
    }

    @JvmStatic
    fun <R : Record> versionField(
        table: Table<out R>,
    ): TableField<out R, out Any> {
        return getVersionField(table)
            ?: throw UNEXISTED_FIELD_EX(table, VERSION_FIELD_NAME)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> versionField(
        record: TableRecord<out R>,
    ): TableField<out R, out Any> {
        return getVersionField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, VERSION_FIELD_NAME)
    }

    @JvmStatic
    fun <R : Record> getVersionField(
        table: Table<out R>,
    ): TableField<out R, out Any>? {
        return table.recordVersion
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getVersionField(
        record: TableRecord<out R>,
    ): TableField<out R, out Any>? {
        return getVersionField(record.table)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getVersionValue(
        record: TableRecord<out R>,
    ): Keeper<Long?> {

        val field: TableField<out R, *>? = getVersionField(record)

        if (field != null) {
            val value: Long? = record.get(field, Long::class.java)
            return Keeper.set(value)
        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : Record> getVersionValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<Long?> {

        val field: TableField<out R, *>? = getVersionField(table)

        if (field != null && fields.containsKey(field)) {
            val value: Long? = fields[field]?.asOf(Long::class)
            return Keeper.set(value)
        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> versionFieldValue(
        record: TableRecord<out R>,
    ): Pair<TableField<out R, out Any>, Long?> {
        return versionFieldValue(
            record.table,
            JooqTool.intoFieldValuesMap(record)
        )
    }

    @JvmStatic
    fun <R : Record> versionFieldValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Pair<TableField<out R, out Any>, Long?> {

        val field: TableField<out R, out Any> = getVersionField(table)
            ?: throw UNEXISTED_FIELD_EX(table, VERSION_FIELD_NAME)

        if (fields.contains(field).not()) {
            throw UNPROVIDED_FIELD_EX(table, VERSION_FIELD_NAME)
        }

        val value: Long = fields
            .getValue(field)
            ?.asOf()
            ?: throw NULLABLE_FIELD_VALUE_EX(field)

        return field to value

    }

    @JvmStatic
    fun <R : TableRecord<out R>> setVersionValue(
        record: TableRecord<out R>,
        version: Long?,
    ) {

        if (version != null && isVersionAvailable(record.table)) {
            val field: TableField<out R, *> = versionField(record)
            JooqTool.setValue(record, field, version)
        }

    }

    @JvmStatic
    fun <R : TableRecord<out R>> getVersionCondition(
        record: TableRecord<out R>,
    ): Condition? {

        val field: TableField<out R, *> = getVersionField(record)
            ?: return null

        return JooqTool.condition(record, field)

    }

    @JvmStatic
    fun <R : TableRecord<out R>> versionCondition(
        record: TableRecord<out R>,
    ): Condition {

        val field: TableField<out R, *> = getVersionField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, VERSION_FIELD_NAME)

        return JooqTool.condition(record, field)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun <R : Record> isTimestampAvailable(
        table: Table<out R>,
    ): Boolean {
        return table.recordTimestamp != null
    }

    @JvmStatic
    fun <R : TableRecord<out R>> isTimestampAvailable(
        record: TableRecord<out R>,
    ): Boolean {
        return isTimestampAvailable(record.table)
    }

    @JvmStatic
    fun <R : Record> timestampField(
        table: Table<out R>,
    ): TableField<out R, out Any> {
        return getTimestampField(table)
            ?: throw UNEXISTED_FIELD_EX(table, TIMESTAMP_FIELD_NAME)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> timestampField(
        record: TableRecord<out R>,
    ): TableField<out R, out Any> {
        return getTimestampField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, TIMESTAMP_FIELD_NAME)
    }

    @JvmStatic
    fun <R : Record> getTimestampField(
        table: Table<out R>,
    ): TableField<out R, out Any>? {
        return table.recordTimestamp
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getTimestampField(
        record: TableRecord<out R>,
    ): TableField<out R, out Any>? {
        return getTimestampField(record.table)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getTimestampValue(
        record: TableRecord<out R>,
    ): Keeper<Timestamp?> {

        val field: TableField<out R, *>? = getTimestampField(record)

        if (field != null) {
            val value: Timestamp? = record.get(field, Timestamp::class.java)
            return Keeper.set(value)
        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : Record> getTimestampValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<Timestamp?> {

        val field: TableField<out R, *>? = getTimestampField(table)

        if (field != null && fields.containsKey(field)) {
            val value: Timestamp? = fields[field]?.asOf(Timestamp::class)
            return Keeper.set(value)
        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> timestampFieldValue(
        record: TableRecord<out R>,
    ): Pair<TableField<out R, out Any>, Timestamp?> {
        return timestampFieldValue(
            record.table,
            JooqTool.intoFieldValuesMap(record)
        )
    }

    @JvmStatic
    fun <R : Record> timestampFieldValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Pair<TableField<out R, out Any>, Timestamp?> {

        val field: TableField<out R, out Any> = getTimestampField(table)
            ?: throw UNEXISTED_FIELD_EX(table, TIMESTAMP_FIELD_NAME)

        if (fields.contains(field).not()) {
            throw UNPROVIDED_FIELD_EX(table, TIMESTAMP_FIELD_NAME)
        }

        val value: Timestamp = fields
            .getValue(field)
            ?.asOf()
            ?: throw NULLABLE_FIELD_VALUE_EX(field)

        return field to value

    }

    @JvmStatic
    fun nowTimestamp(
        clock: Clock,
        type: DataType<*>,
    ): Timestamp {
        val timestamp = Timestamp(clock.millis())
        return truncate(timestamp, type)
    }

    private val TRUNCATE = longArrayOf(
        Numbers.THOUSAND,
        Numbers.HUNDRED,
        Numbers.TEN,
        Numbers.ONE
    )

    /** TableRecordImpl$367 **/
    private fun truncate(timestamp: Timestamp, type: DataType<*>): Timestamp {
        return if (type.isDate) Timestamp(timestamp.toInstant().epochSecond * MILLIS_PER_SECOND)
        else if (!type.precisionDefined() || type.precision() >= 3) timestamp
        else Timestamp((timestamp.time / TRUNCATE[type.precision()]) * TRUNCATE[type.precision()])
    }

    @JvmStatic
    fun <R : TableRecord<out R>> setTimestampValue(
        record: TableRecord<out R>,
        timestamp: Timestamp?,
    ) {

        if (timestamp != null && isTimestampAvailable(record.table)) {
            val field: TableField<out R, *> = timestampField(record)
            JooqTool.setValue(record, field, timestamp)
        }

    }

    @JvmStatic
    fun <R : TableRecord<out R>> getTimestampCondition(
        record: TableRecord<out R>,
    ): Condition? {

        val field: TableField<out R, *> = getTimestampField(record)
            ?: return null

        return JooqTool.condition(record, field)

    }

    @JvmStatic
    fun <R : TableRecord<out R>> timestampCondition(
        record: TableRecord<out R>,
    ): Condition {

        val field: TableField<out R, *> = getTimestampField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, TIMESTAMP_FIELD_NAME)

        return JooqTool.condition(record, field)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun <R : Record> isCreatedAtAvailable(
        table: Table<out R>,
    ): Boolean {
        return table
            .field(CREATED_AT_FIELD.unqualifiedName)
            .isNotNull()
    }

    @JvmStatic
    fun <R : TableRecord<out R>> isCreatedAtAvailable(
        record: TableRecord<out R>,
    ): Boolean {
        return isCreatedAtAvailable(record.table)
    }

    @JvmStatic
    fun <R : Record> createdAtField(
        table: Table<out R>,
    ): Field<*> {
        return getCreatedAtField(table)
            ?: throw UNEXISTED_FIELD_EX(table, CREATED_AT_FIELD)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> createdAtField(
        record: TableRecord<out R>,
    ): Field<*> {
        return getCreatedAtField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, CREATED_AT_FIELD)
    }

    @JvmStatic
    fun <R : Record> getCreatedAtField(
        table: Table<out R>,
    ): Field<*>? {
        return table.field(CREATED_AT_FIELD.unqualifiedName)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getCreatedAtField(
        record: TableRecord<out R>,
    ): Field<*>? {
        return getCreatedAtField(record.table)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getCreatedAtValue(
        record: TableRecord<out R>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getCreatedAtField(record)

        if (field != null) {

            val value: OffsetDateTime = record
                .get(field, OffsetDateTime::class.java)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : Record> getCreatedAtValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getCreatedAtField(table)

        if (field != null && fields.containsKey(field)) {

            val value: OffsetDateTime? = fields
                .getValue(field)
                ?.asOf(OffsetDateTime::class)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> getCreatedAtValue(
        record: TableRecord<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getCreatedAtField(record)

        if (field != null && fields.containsKey(field)) {

            val value: OffsetDateTime? = fields
                .getValue(field)
                ?.asOf(OffsetDateTime::class)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> createdAtFieldValue(
        record: TableRecord<out R>,
    ): Pair<Field<*>, OffsetDateTime> {
        return createdAtFieldValue(
            record.table,
            JooqTool.intoFieldValuesMap(record)
        )
    }

    @JvmStatic
    fun <R : Record> createdAtFieldValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Pair<Field<*>, OffsetDateTime> {

        val field: Field<*> = getCreatedAtField(table)
            ?: throw UNEXISTED_FIELD_EX(table, CREATED_AT_FIELD)

        if (fields.contains(field).not()) {
            throw UNPROVIDED_FIELD_EX(table, CREATED_AT_FIELD_NAME)
        }

        val value: OffsetDateTime = fields
            .getValue(field)
            ?.asOf()
            ?: throw NULLABLE_FIELD_VALUE_EX(field)

        return field to value

    }

    @JvmStatic
    fun <R : TableRecord<out R>> setCreatedAtValue(
        record: TableRecord<out R>,
        createdAt: OffsetDateTime,
    ) {

        if (isCreatedAtAvailable(record.table)) {
            val field: Field<*> = createdAtField(record)
            JooqTool.setValue(record, field, createdAt)
        }

    }

    /* __________________________________________________ */

    @JvmStatic
    fun <R : Record> isUpdatedAtAvailable(
        table: Table<out R>,
    ): Boolean {
        return table
            .field(UPDATED_AT_FIELD.unqualifiedName)
            .isNotNull()
    }

    @JvmStatic
    fun <R : TableRecord<out R>> isUpdatedAtAvailable(
        record: TableRecord<out R>,
    ): Boolean {
        return isUpdatedAtAvailable(record.table)
    }

    @JvmStatic
    fun <R : Record> updatedAtField(
        table: Table<out R>,
    ): Field<*> {
        return getUpdatedAtField(table)
            ?: throw UNEXISTED_FIELD_EX(table, UPDATED_AT_FIELD)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> updatedAtField(
        record: TableRecord<out R>,
    ): Field<*> {
        return getUpdatedAtField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, UPDATED_AT_FIELD)
    }

    @JvmStatic
    fun <R : Record> getUpdatedAtField(
        table: Table<out R>,
    ): Field<*>? {
        return table.field(UPDATED_AT_FIELD.unqualifiedName)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getUpdatedAtField(
        record: TableRecord<out R>,
    ): Field<*>? {
        return getUpdatedAtField(record.table)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getUpdatedAtValue(
        record: TableRecord<out R>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getUpdatedAtField(record)

        if (field != null) {

            val value: OffsetDateTime? = record
                .get(field, OffsetDateTime::class.java)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : Record> getUpdatedAtValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getUpdatedAtField(table)

        if (field != null && fields.containsKey(field)) {

            val value: OffsetDateTime? = fields
                .getValue(field)
                ?.asOf(OffsetDateTime::class)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> getUpdatedAtValue(
        record: TableRecord<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getUpdatedAtField(record)

        if (field != null && fields.containsKey(field)) {

            val value: OffsetDateTime? = fields
                .getValue(field)
                ?.asOf(OffsetDateTime::class)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> updatedAtFieldValue(
        record: TableRecord<out R>,
    ): Pair<Field<*>, OffsetDateTime?> {
        return updatedAtFieldValue(
            record.table,
            JooqTool.intoFieldValuesMap(record)
        )
    }

    @JvmStatic
    fun <R : Record> updatedAtFieldValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Pair<Field<*>, OffsetDateTime?> {

        val field: Field<*> = getUpdatedAtField(table)
            ?: throw UNEXISTED_FIELD_EX(table, UPDATED_AT_FIELD)

        if (fields.contains(field).not()) {
            throw UNPROVIDED_FIELD_EX(table, UPDATED_AT_FIELD_NAME)
        }

        val value: OffsetDateTime? = fields
            .getValue(field)
            ?.asOf()

        return field to value

    }

    @JvmStatic
    fun <R : TableRecord<out R>> setUpdatedAtValue(
        record: TableRecord<out R>,
        updatedAt: OffsetDateTime?,
    ) {

        if (isUpdatedAtAvailable(record.table)) {
            val field: Field<*> = updatedAtField(record)
            JooqTool.setValue(record, field, updatedAt)
        }

    }

    /* __________________________________________________ */

    @JvmStatic
    fun <R : Record> isDeletedAtAvailable(
        table: Table<out R>,
    ): Boolean {
        return table
            .field(DELETED_AT_FIELD.unqualifiedName)
            .isNotNull()
    }

    @JvmStatic
    fun <R : TableRecord<out R>> isDeletedAtAvailable(
        record: TableRecord<out R>,
    ): Boolean {
        return isDeletedAtAvailable(record.table)
    }

    @JvmStatic
    fun <R : Record> deletedAtField(
        table: Table<out R>,
    ): Field<*> {
        return getDeletedAtField(table)
            ?: throw UNEXISTED_FIELD_EX(table, DELETED_AT_FIELD)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> deletedAtField(
        record: TableRecord<out R>,
    ): Field<*> {
        return getDeletedAtField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, DELETED_AT_FIELD)
    }

    @JvmStatic
    fun <R : Record> getDeletedAtField(
        table: Table<out R>,
    ): Field<*>? {
        return table.field(DELETED_AT_FIELD.unqualifiedName)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getDeletedAtField(
        record: TableRecord<out R>,
    ): Field<*>? {
        return getDeletedAtField(record.table)
    }

    @JvmStatic
    fun <R : TableRecord<out R>> getDeletedAtValue(
        record: TableRecord<out R>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getDeletedAtField(record)

        if (field != null) {

            val value: OffsetDateTime? = record
                .get(field, OffsetDateTime::class.java)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : Record> getDeletedAtValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getDeletedAtField(table)

        if (field != null && fields.containsKey(field)) {

            val value: OffsetDateTime? = fields
                .getValue(field)
                ?.asOf(OffsetDateTime::class)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> getDeletedAtValue(
        record: TableRecord<out R>,
        fields: Map<Field<*>, Any?>,
    ): Keeper<OffsetDateTime?> {

        val field: Field<*>? = getDeletedAtField(record)

        if (field != null && fields.containsKey(field)) {

            val value: OffsetDateTime? = fields
                .getValue(field)
                ?.asOf(OffsetDateTime::class)

            return Keeper.set(value)

        }

        return Keeper.unset()

    }

    @JvmStatic
    fun <R : TableRecord<out R>> deletedAtFieldValue(
        record: TableRecord<out R>,
    ): Pair<Field<*>, OffsetDateTime?> {
        return deletedAtFieldValue(
            record.table,
            JooqTool.intoFieldValuesMap(record)
        )
    }

    @JvmStatic
    fun <R : Record> deletedAtFieldValue(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
    ): Pair<Field<*>, OffsetDateTime?> {

        val field: Field<*> = getDeletedAtField(table)
            ?: throw UNEXISTED_FIELD_EX(table, DELETED_AT_FIELD)

        if (fields.contains(field).not()) {
            throw UNPROVIDED_FIELD_EX(table, DELETED_AT_FIELD_NAME)
        }

        val value: OffsetDateTime? = fields
            .getValue(field)
            ?.asOf()

        return field to value

    }

    @JvmStatic
    fun <R : TableRecord<out R>> setDeletedAtValue(
        record: TableRecord<out R>,
        deletedAt: OffsetDateTime?,
    ) {

        if (isDeletedAtAvailable(record.table)) {
            val field: Field<*> = deletedAtField(record)
            JooqTool.setValue(record, field, deletedAt)
        }

    }

    @JvmStatic
    fun <R : TableRecord<out R>> getUndeletedCondition(
        record: TableRecord<out R>,
    ): Condition? {

        val field: Field<*> = getDeletedAtField(record)
            ?: return null

        return field.isNull

    }

    @JvmStatic
    fun <R : TableRecord<out R>> undeletedCondition(
        record: TableRecord<out R>,
    ): Condition {

        val field: Field<*> = getDeletedAtField(record)
            ?: throw UNEXISTED_FIELD_EX(record.table, DELETED_AT_FIELD_NAME)

        return field.isNull

    }

    /* __________________________________________________ */

}