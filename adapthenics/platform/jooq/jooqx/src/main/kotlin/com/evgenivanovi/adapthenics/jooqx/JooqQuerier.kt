package com.evgenivanovi.adapthenics.jooqx

import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.CREATED_AT_FIELD
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_FIELD
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.DELETED_AT_IS_NULL_CONDITION
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.TIMESTAMP_FIELD_TYPE
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.getTimestampCondition
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.getUndeletedCondition
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.getVersionCondition
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.identityCondition
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.identityField
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.identityFieldValue
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.isCreatedAtAvailable
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.isDeletedAtAvailable
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.isTimestampAvailable
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.isUpdatedAtAvailable
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.isVersionAvailable
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.nowTimestamp
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.setCreatedAtValue
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.setDeletedAtValue
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.setTimestampValue
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.setUpdatedAtValue
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.setVersionValue
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.timestampField
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.updatedAtField
import com.evgenivanovi.adapthenics.jooqx.JooqEntityTool.versionField
import com.evgenivanovi.adapthenics.jooqx.JooqTool.condition
import com.evgenivanovi.adapthenics.jooqx.JooqTool.exclude
import com.evgenivanovi.adapthenics.jooqx.JooqTool.intoChangedFieldValuesMap
import com.evgenivanovi.adapthenics.jooqx.JooqTool.intoFieldValuesMap
import com.evgenivanovi.kt.coll.plusNN
import com.evgenivanovi.kt.lambda.INC_LONG_NULL
import com.evgenivanovi.kt.math.Numbers
import com.evgenivanovi.kt.math.incOr
import com.evgenivanovi.kt.time.OffsetUTC
import io.github.oshai.kotlinlogging.KotlinLogging
import org.jooq.*
import org.jooq.impl.DSL
import java.sql.Timestamp
import java.time.Clock
import java.time.OffsetDateTime

object JooqQuerier {

    private val log = KotlinLogging.logger {}

    /**
     * SOFT INSERT
     * Builds insertion query, resetting the object's metadata to its standard initial values.
     */
    @JvmStatic
    fun <R : TableRecord<out R>> createSoftInsertQuery(
        dsl: DSLContext,
        record: R,
    ): InsertSetMoreStep<out R> {

        if (isVersionAvailable(record)) {

            val action: (Long?) -> Unit = { value ->
                if (value == null || value != Numbers.ZERO) {
                    setVersionValue(record, Numbers.ZERO)
                }
            }

            JooqEntityTool
                .getVersionValue(record)
                .peek(action)

        }

        if (isCreatedAtAvailable(record)) {

            val action: (OffsetDateTime?) -> Unit = { _ ->
                setCreatedAtValue(record, OffsetUTC.now())
            }

            JooqEntityTool
                .getCreatedAtValue(record)
                .peek(action)

        }

        if (isUpdatedAtAvailable(record)) {

            val action: (OffsetDateTime?) -> Unit = { value ->
                if (value != null) {
                    setUpdatedAtValue(record, null)
                }
            }

            JooqEntityTool
                .getUpdatedAtValue(record)
                .peek(action)

        }

        if (isDeletedAtAvailable(record)) {

            val action: (OffsetDateTime?) -> Unit = { value ->
                if (value != null) {
                    setDeletedAtValue(record, null)
                }
            }

            JooqEntityTool
                .getDeletedAtValue(record)
                .peek(action)

        }

        return dsl
            .insertInto(record.table)
            .set(record)

    }

    /**
     * Look [JooqQuerier.createSoftInsertQuery]
     */
    @JvmStatic
    fun <R : UpdatableRecord<out R>> createSoftInsertQuery(
        dsl: DSLContext,
        record: R,
    ): InsertSetMoreStep<out R> {

        if (isVersionAvailable(record)) {

            val action: (Long?) -> Unit = { value ->
                if (value == null || value != Numbers.ZERO) {
                    setVersionValue(record, Numbers.ZERO)
                }
            }

            JooqEntityTool
                .getVersionValue(record)
                .peek(action)

        }

        if (isCreatedAtAvailable(record)) {

            val action: (OffsetDateTime?) -> Unit = { _ ->
                setCreatedAtValue(record, OffsetUTC.now())
            }

            JooqEntityTool
                .getCreatedAtValue(record)
                .peek(action)

        }

        if (isUpdatedAtAvailable(record)) {

            val action: (OffsetDateTime?) -> Unit = { value ->
                if (value != null) {
                    setUpdatedAtValue(record, null)
                }
            }

            JooqEntityTool
                .getUpdatedAtValue(record)
                .peek(action)

        }

        if (isDeletedAtAvailable(record)) {

            val action: (OffsetDateTime?) -> Unit = { value ->
                if (value != null) {
                    setDeletedAtValue(record, null)
                }
            }

            JooqEntityTool
                .getDeletedAtValue(record)
                .peek(action)

        }

        return dsl
            .insertInto(record.table)
            .set(record)

    }

    /* __________________________________________________ */

    /**
     * FORCE INSERT
     * Builds insertion query.
     */
    @JvmStatic
    fun <R : TableRecord<out R>> createForceInsertQuery(
        dsl: DSLContext,
        record: R,
    ): InsertSetMoreStep<out R> {

        return dsl
            .insertInto(record.table)
            .set(record)

    }

    /**
     * Look [JooqQuerier.createForceInsertQuery]
     */
    @JvmStatic
    fun <R : UpdatableRecord<out R>> createForceInsertQuery(
        dsl: DSLContext,
        record: R,
    ): InsertSetMoreStep<out R> {

        return dsl
            .insertInto(record.table)
            .set(record)

    }

    /* __________________________________________________ */

    /**
     * SOFT UPDATE
     * Builds updation query of object that matches its ID and version by updating `version`, 'updatedAt' fields.
     */
    @JvmStatic
    fun <R : TableRecord<out R>> createSoftUpdateQuery(
        dsl: DSLContext,
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = Conditioner.create()
            .required(identityCondition(record))
            .optional(getVersionCondition(record))
            .optional(getTimestampCondition(record))
            .optional(getUndeletedCondition(record))
            .build()

        setSoftUpdatedOptimisticLocking(record, dsl.clock())

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)
            .plusNN(CREATED_AT_FIELD.unqualifiedName)
            .plusNN(DELETED_AT_FIELD.unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    /**
     * Look [JooqQuerier.createSoftUpdateQuery]
     */
    @JvmStatic
    fun <R : UpdatableRecord<out R>> createSoftUpdateQuery(
        dsl: DSLContext,
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = Conditioner.create()
            .required(identityCondition(record))
            .optional(getVersionCondition(record))
            .optional(getTimestampCondition(record))
            .optional(getUndeletedCondition(record))
            .build()

        setSoftUpdatedOptimisticLocking(record, dsl.clock())

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)
            .plusNN(CREATED_AT_FIELD.unqualifiedName)
            .plusNN(DELETED_AT_FIELD.unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    private fun setSoftUpdatedOptimisticLocking(record: TableRecord<*>, clock: Clock) {

        JooqEntityTool
            .getVersionValue(record)
            .map(INC_LONG_NULL::apply)
            .peek { setVersionValue(record, it) }

        JooqEntityTool
            .getTimestampValue(record)
            .map { nowTimestamp(clock, TIMESTAMP_FIELD_TYPE) }
            .peek { setTimestampValue(record, it) }

        JooqEntityTool
            .getUpdatedAtValue(record)
            .map { OffsetUTC.now() }
            .peek { setUpdatedAtValue(record, it) }

    }

    /* __________________________________________________ */

    /**
     * FORCE UPDATE
     * Builds updation query of object that matches its ID.
     */
    @JvmStatic
    fun <R : TableRecord<out R>> createForceUpdateQuery(
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = identityCondition(record)

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    /**
     * Look [JooqQuerier.createForceUpdateQuery]
     */
    @JvmStatic
    fun <R : UpdatableRecord<out R>> createForceUpdateQuery(
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = identityCondition(record)

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun <R : TableRecord<out R>> createSoftMergeQuery(
        dsl: DSLContext,
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = Conditioner.create()
            .required(identityCondition(record))
            .optional(getVersionCondition(record))
            .optional(getTimestampCondition(record))
            .optional(getUndeletedCondition(record))
            .build()

        setSoftUpdatedOptimisticLocking(record, dsl.clock())

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)
            .plusNN(CREATED_AT_FIELD.unqualifiedName)
            .plusNN(DELETED_AT_FIELD.unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoChangedFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    @JvmStatic
    fun <R : UpdatableRecord<out R>> createSoftMergeQuery(
        dsl: DSLContext,
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = Conditioner.create()
            .required(identityCondition(record))
            .optional(getVersionCondition(record))
            .optional(getTimestampCondition(record))
            .optional(getUndeletedCondition(record))
            .build()

        setSoftUpdatedOptimisticLocking(record, dsl.clock())

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)
            .plusNN(CREATED_AT_FIELD.unqualifiedName)
            .plusNN(DELETED_AT_FIELD.unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoChangedFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun <R : TableRecord<out R>> createForceMergeQuery(
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = identityCondition(record)

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoChangedFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    @JvmStatic
    fun <R : UpdatableRecord<out R>> createForceMergeQuery(
        record: R,
    ): UpdateConditionStep<out R> {

        val condition: Condition = identityCondition(record)

        val excludes: Array<Name> = arrayOf<Name>()
            .plus(identityField(record).unqualifiedName)

        val fields: Map<Field<*>, Any?> = intoChangedFieldValuesMap(record, *excludes)
        return createUpdateQuery(record.table, fields, condition)

    }

    /* __________________________________________________ */

    @JvmStatic
    fun createSoftMergeQuery(
        dsl: DSLContext,
        table: Table<*>,
        fields: Map<Field<*>, Any?>,
    ): UpdateConditionStep<out Record> {

        val result: MutableMap<Field<*>, Any?> = fields.toMutableMap()

        val (identity: TableField<out Record, out Any>, identityValue: Any) =
            identityFieldValue(table, fields)

        val condition = Conditioner.create()
            .required(condition(identity, identityValue))
            .required(DELETED_AT_IS_NULL_CONDITION)

        if (isVersionAvailable(table)) {

            val field: TableField<out Record, out Any> = versionField(table)

            val action: (Long?) -> Unit = { value ->
                result[field] = value.incOr(Numbers.ZERO)
            }

            val conditionAction: (Long?) -> Unit = { value ->
                condition.required(condition(field, value))
            }

            JooqEntityTool
                .getVersionValue(table, fields)
                .peek(action)
                .peek(conditionAction)

        }

        if (isTimestampAvailable(table)) {

            val field: TableField<out Record, out Any> = timestampField(table)

            val action: (Timestamp?) -> Unit = { _ ->
                result[field] = nowTimestamp(dsl.clock(), field.dataType)
            }

            val conditionAction: (Timestamp?) -> Unit = { value ->
                condition.required(condition(field, value))
            }

            JooqEntityTool
                .getTimestampValue(table, fields)
                .orEmpty()
                .peek(action)
                .peek(conditionAction)

        }

        if (isUpdatedAtAvailable(table)) {

            val field: Field<*> = updatedAtField(table)

            val action: (OffsetDateTime?) -> Unit = { _ ->
                result[field] = OffsetUTC.now()
            }

            JooqEntityTool
                .getUpdatedAtValue(table, fields)
                .orEmpty()
                .peek(action)

        }

        val excludes: Array<Name> = arrayOf<Name>()
            .plusNN(identity.unqualifiedName)
            .plusNN(CREATED_AT_FIELD.unqualifiedName)
            .plusNN(DELETED_AT_FIELD.unqualifiedName)

        return createUpdateQuery(
            table,
            result.exclude(*excludes),
            condition.build()
        )

    }

    @JvmStatic
    fun createForceMergeQuery(
        table: Table<*>,
        fields: Map<Field<*>, Any?>,
    ): UpdateConditionStep<out Record> {

        val (identity: TableField<out Record, out Any>, identityValue: Any?) =
            identityFieldValue(table, fields)

        val condition = Conditioner.create()
            .required(condition(identity, identityValue))

        val excludes: Array<Name> = arrayOf<Name>()
            .plusNN(identity.unqualifiedName)

        return createUpdateQuery(
            table,
            fields.exclude(*excludes),
            condition.build()
        )

    }

    /* __________________________________________________ */

    private fun <R : Record> createUpdateQuery(
        table: Table<out R>,
        fields: Map<Field<*>, Any?>,
        condition: Condition,
    ): UpdateConditionStep<out R> {
        return DSL
            .update(table)
            .set(fields)
            .where(condition)
    }

    /* __________________________________________________ */

}