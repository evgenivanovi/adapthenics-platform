package com.evgenivanovi.adapthenics.jooqx

import org.jooq.Condition
import org.jooq.DSLContext
import org.jooq.impl.DSL
import java.time.Clock

fun DSLContext.clock(): Clock {
    return this.configuration().clock()
}

fun Collection<Condition>.combine(): Condition {
    return fold(DSL.trueCondition()) { current: Condition, next: Condition ->
        current.and(next)
    }
}

fun Condition.andNotNull(other: Condition?): Condition {
    if (other == null) return this
    return this.and(other)
}

fun Condition.orNotNull(other: Condition?): Condition {
    if (other == null) return this
    return this.or(other)
}
