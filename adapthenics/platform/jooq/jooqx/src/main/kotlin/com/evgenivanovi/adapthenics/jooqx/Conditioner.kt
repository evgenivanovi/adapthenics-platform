package com.evgenivanovi.adapthenics.jooqx

import com.evgenivanovi.kt.lang.asOf
import org.jooq.Condition
import org.jooq.impl.DSL
import java.util.function.Supplier

class Conditioner {

    private val conditions: MutableList<Condition> = ArrayList()

    internal constructor()

    internal constructor(condition: Condition) {
        this.conditions.add(condition)
    }

    fun optional(condition: Condition?): Conditioner {
        if (condition != null) {
            conditions.add(condition)
        }
        return this
    }

    fun optional(condition: Supplier<Condition?>): Conditioner {
        return optional(condition.get())
    }

    fun required(condition: Condition): Conditioner {
        conditions.add(condition)
        return this
    }

    fun required(condition: Supplier<Condition>): Conditioner {
        conditions.add(condition.get())
        return this
    }

    fun build(): Condition {
        return conditions.fold(DSL.trueCondition().asOf()) { accumulator, condition ->
            accumulator.and(condition)
        }
    }

    companion object {

        fun create(): Conditioner {
            return Conditioner()
        }

        fun create(condition: Condition): Conditioner {
            return Conditioner(condition)
        }

    }

}
