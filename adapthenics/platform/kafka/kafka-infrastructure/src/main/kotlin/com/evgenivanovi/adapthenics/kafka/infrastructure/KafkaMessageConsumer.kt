package com.evgenivanovi.adapthenics.kafka.infrastructure

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext

interface KafkaMessageConsumer<K : Any, V : Any> {

    suspend fun consume(message: KafkaMessageContext<K, V>)

}