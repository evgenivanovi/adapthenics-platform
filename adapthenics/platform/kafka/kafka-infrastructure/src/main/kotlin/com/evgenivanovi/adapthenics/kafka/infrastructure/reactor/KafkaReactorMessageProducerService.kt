package com.evgenivanovi.adapthenics.kafka.infrastructure.reactor

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext
import com.evgenivanovi.adapthenics.kafka.adapter.core.service.KafkaMessageProducer
import com.evgenivanovi.adapthenics.kafka.infrastructure.toProducerRecord
import com.evgenivanovi.adapthenics.kafka.infrastructure.toProducerRecords
import com.evgenivanovi.adapthenics.support.logging.asyncDebug
import com.evgenivanovi.adapthenics.support.reactor.onNext
import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.SchedulerCoroutineDispatcher
import kotlinx.coroutines.reactor.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import org.apache.kafka.clients.producer.ProducerRecord
import reactor.core.publisher.Flux
import reactor.core.scheduler.Scheduler
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.SenderResult
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

class KafkaReactorMessageProducerService(
    private val sender: KafkaSender<String, String>,
    private val mapper: ObjectMapper,
    private val scheduler: Scheduler,
) : KafkaMessageProducer<String, Any> {

    private val log = KotlinLogging.logger {}

    private val dispatcher: SchedulerCoroutineDispatcher =
        scheduler.asCoroutineDispatcher()

    override suspend fun send(
        message: KafkaMessageContext<String, Any>,
    ): Unit = coroutineScope {
        val record = message
            .mapMessageValueS(::serialize)
            .toProducerRecord()
        doSendRecord(record)
    }

    override suspend fun send(
        messages: List<KafkaMessageContext<String, Any>>,
    ): Unit = coroutineScope {
        val records = messages
            .map { msg -> msg.mapMessageValueS(::serialize) }
            .toProducerRecords()
        doSendRecords(records)
    }

    private suspend fun serialize(
        value: Any,
    ): String = coroutineScope {
        withContext(dispatcher) {
            return@withContext mapper.writeValueAsString(value)
        }
    }

    private suspend fun doSendRecord(
        record: ProducerRecord<String, String>,
    ): SenderResult<String> = coroutineScope {
        withContext(dispatcher) {
            return@withContext doSend(record).asFlow().first()
        }
    }

    private fun doSend(
        record: ProducerRecord<String, String>,
    ): Flux<SenderResult<String>> {
        return SenderRecord
            .create(record, record.value()::class.simpleName.orEmpty())
            .let { sender.send(it.toMono()) }
            .onNext { sent ->
                log.asyncDebug {
                    "Message ${sent.correlationMetadata()} sent successfully," +
                        " topic=${sent.recordMetadata().topic()};" +
                        " partition=${sent.recordMetadata().partition()};" +
                        " offset=${sent.recordMetadata().offset()}" +
                        System.lineSeparator()
                }
            }
    }

    private suspend fun doSendRecords(
        records: List<ProducerRecord<String, String>>,
    ): Flow<SenderResult<String>> = coroutineScope {
        withContext(dispatcher) {
            return@withContext doSend(records).asFlow()
        }
    }

    private fun doSend(
        records: List<ProducerRecord<String, String>>,
    ): Flux<SenderResult<String>> {
        return records
            .map { SenderRecord.create(it, it.value()::class.simpleName.orEmpty()) }
            .let { sender.send(it.toFlux()) }
            .publishOn(scheduler)
            .onNext { sent ->
                log.asyncDebug {
                    "Message ${sent.correlationMetadata()} sent successfully," +
                        " topic=${sent.recordMetadata().topic()};" +
                        " partition=${sent.recordMetadata().partition()};" +
                        " offset=${sent.recordMetadata().offset()}" +
                        System.lineSeparator()
                }
            }
    }

}