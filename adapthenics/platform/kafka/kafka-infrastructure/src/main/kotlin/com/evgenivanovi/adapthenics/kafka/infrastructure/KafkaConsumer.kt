package com.evgenivanovi.adapthenics.kafka.infrastructure

interface KafkaConsumer {

    fun start()

    fun stop()

}