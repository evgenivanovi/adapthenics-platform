package com.evgenivanovi.adapthenics.kafka.infrastructure.reactor

import com.evgenivanovi.adapthenics.kafka.adapter.core.service.KafkaProducer
import com.evgenivanovi.adapthenics.support.logging.asyncDebug
import com.evgenivanovi.adapthenics.support.reactor.onNext
import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.SchedulerCoroutineDispatcher
import kotlinx.coroutines.reactor.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import org.apache.kafka.clients.producer.ProducerRecord
import reactor.core.publisher.Flux
import reactor.core.scheduler.Scheduler
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.SenderResult
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

class KafkaReactorProducerService(
    private val sender: KafkaSender<String, String>,
    private val mapper: ObjectMapper,
    private val scheduler: Scheduler,
) : KafkaProducer<String, Any> {

    private val dispatcher: SchedulerCoroutineDispatcher =
        scheduler.asCoroutineDispatcher()

    private val log = KotlinLogging.logger {}

    override suspend fun <T> produce(
        topic: String,
        message: T,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ) {
        val record = createProducerRecord(
            topic, message, keyExtractor, valueExtractor
        )
        doSendRecord(record)
    }

    override suspend fun <T> produce(
        topic: String,
        messages: List<T>,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ) {
        val records = createProducerRecords(
            topic, messages, keyExtractor, valueExtractor
        )
        doSendRecords(records)
    }

    override suspend fun produce(
        topic: String,
        key: String,
        value: Any,
    ) {
        val record = createProducerRecord(
            topic, key, value
        )
        doSendRecord(record)
    }

    override suspend fun produce(
        topic: String,
        messages: List<Pair<String, Any>>,
    ) {
        val records = createProducerRecords(
            topic, messages
        )
        doSendRecords(records)
    }

    /* __________________________________________________ */

    private suspend fun <T> createProducerRecord(
        topic: String,
        message: T,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ): ProducerRecord<String, String> {
        return ProducerRecord<String, String>(
            topic,
            keyExtractor(message),
            serialize(valueExtractor(message))
        )
    }

    private suspend fun <T> createProducerRecords(
        topic: String,
        messages: List<T>,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ): List<ProducerRecord<String, String>> {
        return messages.map {
            createProducerRecord(topic, it, keyExtractor, valueExtractor)
        }
    }

    private suspend fun createProducerRecord(
        topic: String,
        key: String,
        value: Any,
    ): ProducerRecord<String, String> {
        return ProducerRecord(topic, key, serialize(value))
    }

    private suspend fun createProducerRecords(
        topic: String,
        messages: List<Pair<String, Any>>,
    ): List<ProducerRecord<String, String>> {
        return messages.map {
            createProducerRecord(topic, it.first, it.second)
        }
    }

    /* __________________________________________________ */

    private suspend fun serialize(
        value: Any,
    ): String = coroutineScope {
        withContext(dispatcher) {
            return@withContext mapper.writeValueAsString(value)
        }
    }

    /* __________________________________________________ */

    private suspend fun doSendRecord(
        record: ProducerRecord<String, String>,
    ): SenderResult<String> = coroutineScope {
        withContext(dispatcher) {
            return@withContext doSend(record).asFlow().first()
        }
    }

    private fun doSend(
        record: ProducerRecord<String, String>,
    ): Flux<SenderResult<String>> {
        return SenderRecord
            .create(record, record.value()::class.simpleName.orEmpty())
            .let { sender.send(it.toMono()) }
            .onNext { sent ->
                log.asyncDebug {
                    "Message ${sent.correlationMetadata()} sent successfully," +
                        " topic=${sent.recordMetadata().topic()};" +
                        " partition=${sent.recordMetadata().partition()};" +
                        " offset=${sent.recordMetadata().offset()}" +
                        System.lineSeparator()
                }
            }
    }

    private suspend fun doSendRecords(
        records: List<ProducerRecord<String, String>>,
    ): Flow<SenderResult<String>> = coroutineScope {
        withContext(dispatcher) {
            return@withContext doSend(records).asFlow()
        }
    }

    private fun doSend(
        records: List<ProducerRecord<String, String>>,
    ): Flux<SenderResult<String>> {
        return records
            .map { SenderRecord.create(it, it.value()::class.simpleName.orEmpty()) }
            .let { sender.send(it.toFlux()) }
            .publishOn(scheduler)
            .onNext { sent ->
                log.asyncDebug {
                    "Message ${sent.correlationMetadata()} sent successfully," +
                        " topic=${sent.recordMetadata().topic()};" +
                        " partition=${sent.recordMetadata().partition()};" +
                        " offset=${sent.recordMetadata().offset()}" +
                        System.lineSeparator()
                }
            }
    }

}