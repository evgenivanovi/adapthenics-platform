package com.evgenivanovi.adapthenics.kafka.infrastructure

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext
import com.evgenivanovi.adapthenics.support.logging.asyncInfo
import io.github.oshai.kotlinlogging.KotlinLogging

class NoopKafkaMessageConsumer<K: Any, V: Any> : KafkaMessageConsumer<K, V> {

    private val log = KotlinLogging.logger {}

    private constructor()

    override suspend fun consume(message: KafkaMessageContext<K, V>) {
        log.asyncInfo { ("Successfully processed message: '$message'") }
    }

    companion object {

        private val INSTANCE = NoopKafkaMessageConsumer<Any, Any>()

        fun <K: Any, V: Any> create(): NoopKafkaMessageConsumer<K, V> {
            return NoopKafkaMessageConsumer()
        }

    }

}