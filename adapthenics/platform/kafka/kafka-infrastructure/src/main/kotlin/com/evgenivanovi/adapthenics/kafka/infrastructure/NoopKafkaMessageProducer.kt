package com.evgenivanovi.adapthenics.kafka.infrastructure

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext
import com.evgenivanovi.adapthenics.kafka.adapter.core.service.KafkaMessageProducer
import com.evgenivanovi.adapthenics.support.logging.asyncInfo
import io.github.oshai.kotlinlogging.KotlinLogging

class NoopKafkaMessageProducer<K : Any, V : Any> : KafkaMessageProducer<K, V> {

    private val log = KotlinLogging.logger {}

    private constructor()

    override suspend fun send(message: KafkaMessageContext<K, V>) {
        log.asyncInfo { "Message sent successfully: '$message'." }
    }

    override suspend fun send(messages: List<KafkaMessageContext<K, V>>) {
        log.asyncInfo { "Messages sent successfully: '$messages'." }
    }

    companion object {

        private val INSTANCE = NoopKafkaMessageProducer<Any, Any>()

        fun <K : Any, V : Any> create(): NoopKafkaMessageProducer<K, V> {
            return NoopKafkaMessageProducer()
        }

    }

}