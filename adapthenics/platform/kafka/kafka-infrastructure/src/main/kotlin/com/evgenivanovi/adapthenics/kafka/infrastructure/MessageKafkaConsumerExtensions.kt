package com.evgenivanovi.adapthenics.kafka.infrastructure

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessage
import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext
import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageHeader
import org.apache.kafka.common.header.Header
import org.apache.kafka.common.header.Headers
import reactor.kafka.receiver.ReceiverRecord

internal fun <K : Any, V : Any>
    List<ReceiverRecord<K, V>>.toMessageContexts(): List<KafkaMessageContext<K, V>> {
    return map(ReceiverRecord<K, V>::toMessageContext)
}

internal fun <K : Any, V : Any>
    ReceiverRecord<K, V>.toMessageContext(): KafkaMessageContext<K, V> {
    return KafkaMessageContext(
        topic = this.topic(),
        message = this.toMessage()
    )
}

internal fun <K : Any, V : Any>
    List<ReceiverRecord<K, V>>.toMessages(): List<KafkaMessage<K, V>> {
    return map(ReceiverRecord<K, V>::toMessage)
}

internal fun <K : Any, V : Any>
    ReceiverRecord<K, V>.toMessage(): KafkaMessage<K, V> {
    return KafkaMessage.of(
        key = this.key(),
        value = this.value(),
        headers = this.headers().toHeaderList()
    )
}

internal fun Headers.toHeaderList(): List<KafkaMessageHeader> {
    return map<Header, KafkaMessageHeader>(
        Header::toHeader
    )
}

internal fun Header.toHeader(): KafkaMessageHeader {
    return KafkaMessageHeader(
        this.key(),
        this.value().toString(Charsets.UTF_8)
    )
}
