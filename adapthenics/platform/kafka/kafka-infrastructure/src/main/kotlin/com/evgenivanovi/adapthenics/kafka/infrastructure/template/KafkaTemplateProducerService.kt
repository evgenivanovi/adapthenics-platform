package com.evgenivanovi.adapthenics.kafka.infrastructure.template

import com.evgenivanovi.adapthenics.kafka.adapter.core.service.KafkaProducer
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.kafka.core.KafkaOperations
import org.springframework.kafka.support.SendResult
import java.util.concurrent.CompletableFuture

class KafkaTemplateProducerService(
    private val client: KafkaOperations<String, Any>,
    private val dispatcher: CoroutineDispatcher,
) : KafkaProducer<String, Any> {

    private val log = KotlinLogging.logger {}

    override suspend fun <T> produce(
        topic: String,
        message: T,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ) {
        val record = createProducerRecord(topic, message, keyExtractor, valueExtractor)
        doSendRecord(record)
    }

    override suspend fun <T> produce(
        topic: String,
        messages: List<T>,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ) {
        val records = createProducerRecords(topic, messages, keyExtractor, valueExtractor)
        doSendRecords(records)
    }

    override suspend fun produce(
        topic: String,
        key: String,
        value: Any,
    ) {
        val record = createProducerRecord(topic, key, value)
        doSendRecord(record)
    }

    override suspend fun produce(
        topic: String,
        messages: List<Pair<String, Any>>,
    ) {
        val records = createProducerRecords(topic, messages)
        doSendRecords(records)
    }

    /* __________________________________________________ */

    private fun <T> createProducerRecord(
        topic: String,
        message: T,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ): ProducerRecord<String, Any> {
        return ProducerRecord<String, Any>(
            topic,
            keyExtractor.invoke(message),
            valueExtractor.invoke(message)
        )
    }

    private fun <T> createProducerRecords(
        topic: String,
        messages: List<T>,
        keyExtractor: (T) -> String,
        valueExtractor: (T) -> Any,
    ): List<ProducerRecord<String, Any>> {
        return messages.map { createProducerRecord(topic, it, keyExtractor, valueExtractor) }
    }

    private fun createProducerRecord(
        topic: String,
        key: String,
        value: Any,
    ): ProducerRecord<String, Any> {
        return ProducerRecord(topic, key, value)
    }

    private fun createProducerRecords(
        topic: String,
        messages: List<Pair<String, Any>>,
    ): List<ProducerRecord<String, Any>> {
        return messages.map { createProducerRecord(topic, it.first, it.second) }
    }

    /* __________________________________________________ */

    private suspend fun doSendRecord(
        record: ProducerRecord<String, Any>,
    ) = coroutineScope {
        withContext(dispatcher) {
            doSend(record).get()
        }
    }

    private fun doSend(
        record: ProducerRecord<String, Any>,
    ): CompletableFuture<SendResult<String, Any>> {
        return client.send(record)
    }

    private suspend fun doSendRecords(
        records: List<ProducerRecord<String, Any>>,
    ): List<SendResult<String, Any>> = coroutineScope {
        withContext(dispatcher) {
            doSend(records).map { it.get() }
        }
    }

    private fun doSend(
        records: List<ProducerRecord<String, Any>>,
    ): List<CompletableFuture<SendResult<String, Any>>> {
        return client.executeInTransaction { op ->
            return@executeInTransaction records.map { record ->
                op.send(record)
            }
        } ?: emptyList()
    }

}
