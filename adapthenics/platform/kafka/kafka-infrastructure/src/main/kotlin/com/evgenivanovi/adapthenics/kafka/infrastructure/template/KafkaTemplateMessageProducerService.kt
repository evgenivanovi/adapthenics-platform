@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "VariableNaming",
    "ConstructorParameterNaming",
    "MemberVisibilityCanBePrivate",
    "LongParameterList",
    "TooManyFunctions",
    "ReturnCount",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.kafka.infrastructure.template

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext
import com.evgenivanovi.adapthenics.kafka.adapter.core.service.KafkaMessageProducer
import com.evgenivanovi.adapthenics.kafka.infrastructure.toProducerRecord
import com.evgenivanovi.adapthenics.kafka.infrastructure.toProducerRecords
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.kafka.core.KafkaOperations
import org.springframework.kafka.support.SendResult
import java.util.concurrent.CompletableFuture

class KafkaTemplateMessageProducerService(
    private val client: KafkaOperations<String, Any>,
    private val dispatcher: CoroutineDispatcher,
) : KafkaMessageProducer<String, Any> {

    private val log = KotlinLogging.logger {}

    override suspend fun send(
        message: KafkaMessageContext<String, Any>,
    ) {
        doSendRecord(message.toProducerRecord())
    }

    override suspend fun send(
        messages: List<KafkaMessageContext<String, Any>>,
    ) {
        doSendRecords(messages.toProducerRecords())
    }

    private suspend fun doSendRecord(
        record: ProducerRecord<String, Any>,
    ) = coroutineScope {
        withContext(dispatcher) {
            doSend(record).get()
        }
    }

    private fun doSend(
        record: ProducerRecord<String, Any>,
    ): CompletableFuture<SendResult<String, Any>> {
        return client.send(record)
    }

    private suspend fun doSendRecords(
        records: List<ProducerRecord<String, Any>>,
    ): List<SendResult<String, Any>> = coroutineScope {
        withContext(dispatcher) {
            doSend(records).map { it.get() }
        }
    }

    private fun doSend(
        records: List<ProducerRecord<String, Any>>,
    ): List<CompletableFuture<SendResult<String, Any>>> {
        return client.executeInTransaction { op ->
            return@executeInTransaction records.map { record ->
                op.send(record)
            }
        } ?: emptyList()
    }

}
