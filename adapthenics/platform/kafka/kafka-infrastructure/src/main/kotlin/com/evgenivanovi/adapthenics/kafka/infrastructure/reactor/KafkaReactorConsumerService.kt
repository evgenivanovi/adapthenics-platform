package com.evgenivanovi.adapthenics.kafka.infrastructure.reactor

import com.evgenivanovi.adapthenics.kafka.infrastructure.KafkaConsumer
import com.evgenivanovi.adapthenics.kafka.infrastructure.KafkaMessageConsumer
import com.evgenivanovi.adapthenics.kafka.infrastructure.toMessageContext
import com.evgenivanovi.kt.func.no
import com.evgenivanovi.kt.func.yes
import com.evgenivanovi.kt.lang.asOf
import com.evgenivanovi.kt.lang.isNotNull
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.reactor.SchedulerCoroutineDispatcher
import kotlinx.coroutines.reactor.asCoroutineDispatcher
import kotlinx.coroutines.reactor.mono
import reactor.core.Disposable
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOffset
import reactor.kafka.receiver.ReceiverRecord

class KafkaReactorConsumerService<K : Any, V : Any>(
    private val receiver: KafkaReceiver<K, V?>,
    private val consumer: KafkaMessageConsumer<K, V>,
    private val scheduler: Scheduler,
) : KafkaConsumer {

    private val log = KotlinLogging.logger {}

    private val dispatcher: SchedulerCoroutineDispatcher =
        scheduler.asCoroutineDispatcher()

    private lateinit var disposable: Disposable

    /* __________________________________________________ */

    override fun start() {
        this::disposable.isInitialized
            .no { disposable = consume() }
    }

    override fun stop() {
        this::disposable.isInitialized
            .and(disposable.isDisposed.not())
            .yes(disposable::dispose)
    }

    /* __________________________________________________ */

    private fun consume(): Disposable {
        return receiver
            .receive()
            .publishOn(scheduler)
            .groupFlux()
            .concatMap { mono(dispatcher) { handle(it) } }
            .last()
            .map(::commit)
            .subscribe()
    }

    private fun Flux<ReceiverRecord<K, V?>>.groupFlux(): Flux<Collection<ReceiverRecord<K, V>>> {
        return this
            .collectList()
            .map(::group)
            .map { it.values }
            .flatMapMany { Flux.fromIterable(it) }
    }

    private fun group(
        messages: Collection<ReceiverRecord<K, V?>>,
    ): Map<K, Collection<ReceiverRecord<K, V>>> {
        return messages
            .withoutNulls()
            .groupBy(ReceiverRecord<K, V>::key)
    }

    private suspend fun handle(
        messages: Collection<ReceiverRecord<K, V>>,
    ): Collection<ReceiverRecord<K, V>> {
        messages.forEach { handle(it) }
        return messages
    }

    private suspend fun handle(message: ReceiverRecord<K, V>) {
        try {
            consumer.consume(message.toMessageContext())
            ack(message)
        } catch (ex: Exception) {
            log.error(ex) { "Error when processing record occurred." }
        }
    }

    private fun commit(messages: Collection<ReceiverRecord<K, V>>): Mono<Void> {
        return messages
            .map(ReceiverRecord<K, V>::receiverOffset)
            .maxBy(ReceiverOffset::offset)
            .commit()
    }

    private fun commit(message: ReceiverRecord<K, V>): Mono<Void> {
        return message.receiverOffset().commit()
    }

    private fun ack(messages: Collection<ReceiverRecord<K, V>>) {
        return messages
            .map(ReceiverRecord<K, V>::receiverOffset)
            .maxBy(ReceiverOffset::offset)
            .acknowledge()
    }

    private fun ack(message: ReceiverRecord<K, V>) {
        return message.receiverOffset().acknowledge()
    }

    private fun Collection<ReceiverRecord<K, V?>>.withoutNulls(): Collection<ReceiverRecord<K, V>> {
        return this
            .filter { it.value().isNotNull() }
            .map(ReceiverRecord<K, V?>::asOf)
    }

}