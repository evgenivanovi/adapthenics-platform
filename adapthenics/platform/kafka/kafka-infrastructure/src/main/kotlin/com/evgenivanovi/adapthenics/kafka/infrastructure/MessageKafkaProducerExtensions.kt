package com.evgenivanovi.adapthenics.kafka.infrastructure

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext
import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageHeader
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.Header
import org.apache.kafka.common.header.Headers
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders

internal fun <K : Any, V : Any>
    List<KafkaMessageContext<K, V>>.toProducerRecords(): List<ProducerRecord<K, V>> {
    return map(KafkaMessageContext<K, V>::toProducerRecord)
}

internal fun <K : Any, V : Any>
    KafkaMessageContext<K, V>.toProducerRecord(): ProducerRecord<K, V> {
    return ProducerRecord<K, V>(
        this.topic,
        null,
        this.message.key,
        this.message.value,
        this.message.headers.toHeaderList()
    )
}

internal fun List<KafkaMessageHeader>.toHeaders(): Headers {
    return map(KafkaMessageHeader::toHeader)
        .let(::RecordHeaders)
}

internal fun List<KafkaMessageHeader>.toHeaderList(): List<Header> {
    return map(KafkaMessageHeader::toHeader)
}

internal fun KafkaMessageHeader.toHeader(): Header {
    return RecordHeader(
        this.key,
        this.value.toByteArray(Charsets.UTF_8)
    )
}
