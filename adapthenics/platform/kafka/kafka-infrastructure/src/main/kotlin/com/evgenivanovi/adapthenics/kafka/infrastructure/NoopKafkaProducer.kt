package com.evgenivanovi.adapthenics.kafka.infrastructure

import com.evgenivanovi.adapthenics.kafka.adapter.core.service.KafkaProducer
import com.evgenivanovi.adapthenics.support.logging.asyncInfo
import io.github.oshai.kotlinlogging.KotlinLogging

class NoopKafkaProducer<K : Any, V : Any> : KafkaProducer<K, V> {

    private val log = KotlinLogging.logger {}

    private constructor()

    override suspend fun <T> produce(
        topic: String,
        message: T,
        keyExtractor: (T) -> K,
        valueExtractor: (T) -> V,
    ) {
        produce(
            topic = topic,
            key = keyExtractor.invoke(message),
            value = valueExtractor.invoke(message)
        )
    }

    override suspend fun <T> produce(
        topic: String,
        messages: List<T>,
        keyExtractor: (T) -> K,
        valueExtractor: (T) -> V,
    ) {
        messages.forEach {
            produce(topic, it, keyExtractor, valueExtractor)
        }
    }

    override suspend fun produce(
        topic: String,
        key: K,
        value: V,
    ) {
        log.asyncInfo {
            "Message sent successfully." +
                " Key: '$key'." +
                " Value: '$value'." +
                " Topic: '$topic'."
        }
    }

    override suspend fun produce(
        topic: String,
        messages: List<Pair<K, V>>,
    ) {
        messages.forEach { msg ->
            produce(
                topic = topic,
                key = msg.first,
                value = msg.second
            )
        }
    }

    companion object {

        private val INSTANCE = NoopKafkaProducer<Any, Any>()

        fun <K : Any, V : Any> create(): NoopKafkaProducer<K, V> {
            return NoopKafkaProducer()
        }

    }

}