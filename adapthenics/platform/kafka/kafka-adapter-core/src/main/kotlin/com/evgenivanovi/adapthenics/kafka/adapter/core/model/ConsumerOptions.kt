package com.evgenivanovi.adapthenics.kafka.adapter.core.model

class ConsumerOptions(
    val topic: String,
)