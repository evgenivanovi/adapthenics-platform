package com.evgenivanovi.adapthenics.kafka.adapter.core.service

interface KafkaProducer<K : Any, V : Any> {

    suspend fun <T> produce(
        topic: String,
        message: T,
        keyExtractor: (T) -> K,
        valueExtractor: (T) -> V,
    )

    suspend fun <T> produce(
        topic: String,
        messages: List<T>,
        keyExtractor: (T) -> K,
        valueExtractor: (T) -> V,
    )

    suspend fun produce(
        topic: String,
        key: K,
        value: V,
    )

    suspend fun produce(
        topic: String,
        messages: List<Pair<K, V>>,
    )

}