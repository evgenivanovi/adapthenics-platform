package com.evgenivanovi.adapthenics.kafka.adapter.core.model

import com.evgenivanovi.kt.coll.asList

data class KafkaMessage<K : Any, V : Any>(
    val key: K,
    val value: V,
    val headers: List<KafkaMessageHeader>,
) {

    fun withKey(newKey: K): KafkaMessage<K, V> {
        return copy(key = newKey)
    }

    fun withValue(newValue: V): KafkaMessage<K, V> {
        return copy(value = newValue)
    }

    fun withHeader(newHeader: KafkaMessageHeader): KafkaMessage<K, V> {
        return withHeaders(newHeader.asList())
    }

    fun withHeaders(newHeaders: List<KafkaMessageHeader>): KafkaMessage<K, V> {
        return copy(headers = newHeaders)
    }

    fun addHeader(newHeader: KafkaMessageHeader): KafkaMessage<K, V> {
        return withHeaders(headers.plus(newHeader))
    }

    fun addHeaders(newHeaders: List<KafkaMessageHeader>): KafkaMessage<K, V> {
        return withHeaders(headers.plus(newHeaders))
    }

    fun <R : Any> mapKey(
        mapper: (key: Any) -> R,
    ): KafkaMessage<R, V> {
        return of(mapper.invoke(key), value, headers)
    }

    suspend fun <R : Any> mapKeyS(
        mapper: suspend (key: Any) -> R,
    ): KafkaMessage<R, V> {
        return of(mapper.invoke(key), value, headers)
    }

    fun <R : Any> mapValue(
        mapper: (value: Any) -> R,
    ): KafkaMessage<K, R> {
        return of(key, mapper.invoke(value), headers)
    }

    suspend fun <R : Any> mapValueS(
        mapper: suspend (value: Any) -> R,
    ): KafkaMessage<K, R> {
        return of(key, mapper.invoke(value), headers)
    }

    companion object {

        fun <K : Any, V : Any> of(
            key: K,
            value: V,
            headers: List<KafkaMessageHeader>,
        ): KafkaMessage<K, V> {
            return KafkaMessage(
                key = key,
                value = value,
                headers = headers
            )
        }

    }

}
