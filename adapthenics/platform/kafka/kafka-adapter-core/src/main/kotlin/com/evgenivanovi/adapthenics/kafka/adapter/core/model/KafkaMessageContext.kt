package com.evgenivanovi.adapthenics.kafka.adapter.core.model

import com.evgenivanovi.kt.contract.requireNotBlank

data class KafkaMessageContext<K : Any, V : Any>(
    val topic: String,
    val message: KafkaMessage<K, V>,
) {

    init {

        // validation topic
        requireNotBlank(topic)

    }

    fun withTopic(topic: String): KafkaMessageContext<K, V> {
        return copy(topic = topic)
    }

    fun withMessage(message: KafkaMessage<K, V>): KafkaMessageContext<K, V> {
        return copy(message = message)
    }

    fun withMessageKey(messageKey: K): KafkaMessageContext<K, V> {
        return copy(message = message.withKey(messageKey))
    }

    fun withMessageValue(messageValue: V): KafkaMessageContext<K, V> {
        return copy(message = message.withValue(messageValue))
    }

    fun <R : Any> mapMessageKey(
        mapper: (key: Any) -> R,
    ): KafkaMessageContext<R, V> {
        return of(topic = topic, message = message.mapKey(mapper))
    }

    suspend fun <R : Any> mapMessageKeyS(
        mapper: suspend (key: Any) -> R,
    ): KafkaMessageContext<R, V> {
        return of(topic = topic, message = message.mapKeyS(mapper))
    }

    fun <R : Any> mapMessageValue(
        mapper: (value: Any) -> R,
    ): KafkaMessageContext<K, R> {
        return of(topic = topic, message = message.mapValue(mapper))
    }

    suspend fun <R : Any> mapMessageValueS(
        mapper: suspend (value: Any) -> R,
    ): KafkaMessageContext<K, R> {
        return of(topic = topic, message = message.mapValueS(mapper))
    }

    companion object {

        fun <K : Any, V : Any> of(
            topic: String,
            message: KafkaMessage<K, V>,
        ): KafkaMessageContext<K, V> {
            return KafkaMessageContext(topic, message)
        }

    }

}
