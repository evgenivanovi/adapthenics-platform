package com.evgenivanovi.adapthenics.kafka.adapter.core.model

class ProducerOptions(
    val topic: String,
)