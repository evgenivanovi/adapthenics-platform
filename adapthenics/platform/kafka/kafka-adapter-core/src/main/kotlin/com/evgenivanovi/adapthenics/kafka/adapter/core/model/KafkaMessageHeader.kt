package com.evgenivanovi.adapthenics.kafka.adapter.core.model

data class KafkaMessageHeader(
    val key: String,
    val value: String,
)
