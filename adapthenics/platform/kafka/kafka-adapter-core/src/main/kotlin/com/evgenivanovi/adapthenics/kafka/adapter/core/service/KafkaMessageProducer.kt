package com.evgenivanovi.adapthenics.kafka.adapter.core.service

import com.evgenivanovi.adapthenics.kafka.adapter.core.model.KafkaMessageContext

interface KafkaMessageProducer<KEY : Any, VALUE : Any> {

    suspend fun send(
        message: KafkaMessageContext<KEY, VALUE>,
    )

    suspend fun send(
        messages: List<KafkaMessageContext<KEY, VALUE>>,
    )

}
