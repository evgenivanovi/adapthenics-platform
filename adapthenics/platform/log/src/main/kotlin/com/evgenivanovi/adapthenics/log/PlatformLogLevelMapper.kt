package com.evgenivanovi.adapthenics.log

import ch.qos.logback.classic.Level
import com.evgenivanovi.kt.coll.put
import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap

object PlatformLogLevelMapper {

    val LOGBACK_DEFAULT_LEVEL: Level = Level.INFO

    val PLATFORM_DEFAULT_LEVEL: PlatformLogLevel = PlatformLogLevel.INFO

    private val LOGBACK_LEVEL_MAPPING: BiMap<Level, PlatformLogLevel> = HashBiMap
        .create<Level, PlatformLogLevel>()
        .apply { put(Level.ERROR to PlatformLogLevel.ERROR) }
        .apply { put(Level.WARN to PlatformLogLevel.WARN) }
        .apply { put(Level.INFO to PlatformLogLevel.INFO) }
        .apply { put(Level.DEBUG to PlatformLogLevel.DEBUG) }

    fun fromLogback(level: Level): PlatformLogLevel {
        return LOGBACK_LEVEL_MAPPING[level] ?: PLATFORM_DEFAULT_LEVEL
    }

    fun toLogback(level: PlatformLogLevel): Level {
        return LOGBACK_LEVEL_MAPPING.inverse()[level] ?: LOGBACK_DEFAULT_LEVEL
    }

}