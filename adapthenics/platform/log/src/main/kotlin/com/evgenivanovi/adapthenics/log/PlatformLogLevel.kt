package com.evgenivanovi.adapthenics.log

import com.evgenivanovi.kt.any.EnumResolver

enum class PlatformLogLevel(
    val code: Int,
) {

    ERROR(0),

    WARN(1),

    INFO(2),

    DEBUG(3);

    companion object : EnumResolver<PlatformLogLevel>(PlatformLogLevel::class.java) {

        fun checkCodeBounds(code: Int): Boolean {
            return code >= ERROR.code && code <= DEBUG.code
        }

    }

}