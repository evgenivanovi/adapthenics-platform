package com.evgenivanovi.adapthenics.rtc.etcd.adapter.service

import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage
import com.evgenivanovi.adapthenics.rtc.domain.service.EventReadRepository
import com.evgenivanovi.adapthenics.rtc.etcd.adapter.model.ETCDReadRequester
import io.github.oshai.kotlinlogging.KotlinLogging

class ETCDReadRepositoryService(
    private val requester: ETCDReadRequester,
) : EventReadRepository {

    private val log = KotlinLogging.logger {}

    override suspend fun read(key: String): PropertyMessage? {
        return requester
            .read(key)
            ?.let(PropertyMapper::map)
            ?: return null
    }

    override suspend fun readAll(key: String): List<PropertyMessage> {
        return requester
            .readAll(key)
            .map(PropertyMapper::map)
    }

}