package com.evgenivanovi.adapthenics.rtc.etcd.adapter.model

import com.evgenivanovi.adapthenics.rtc.schema.MultiPropertyModel

interface ETCDListener {

    suspend fun onNext(event: MultiPropertyModel)

    suspend fun onFailure(ex: Throwable)

    suspend fun onComplete()

}