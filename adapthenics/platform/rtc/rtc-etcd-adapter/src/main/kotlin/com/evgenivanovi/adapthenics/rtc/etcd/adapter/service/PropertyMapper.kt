package com.evgenivanovi.adapthenics.rtc.etcd.adapter.service

import com.evgenivanovi.adapthenics.rtc.domain.model.Property
import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage
import com.evgenivanovi.adapthenics.rtc.domain.model.parameter.Parameter
import com.evgenivanovi.adapthenics.rtc.schema.*
import com.evgenivanovi.adapthenics.rtc.schema.parameter.*
import java.math.BigInteger
import kotlin.time.toKotlinDuration

object PropertyMapper {

    fun map(model: PropertyMessageModel): PropertyMessage {
        val key = model.key
        val value = map(model.value)
        return PropertyMessage.of(key = key, value = value)
    }

    fun map(model: PropertyModel): Property {
        return when (model) {
            is MultiPropertyModel -> map(model)
            is SinglePropertyModel -> map(model)
        }
    }

    fun map(model: SinglePropertyModel): Property {
        return when (model.type) {
            PropertyTypeModel.CREATED -> mapCreated(model)
            PropertyTypeModel.UPDATED -> mapUpdated(model)
            PropertyTypeModel.DELETED -> mapDeleted(model)
            PropertyTypeModel.UNKNOWN -> mapUnknown(model)
        }
    }

    internal fun mapCreated(model: SinglePropertyModel): Property {

        require(model.type == PropertyTypeModel.CREATED) {
            "Event type must be ${PropertyTypeModel.CREATED}."
        }

        val key: String = model.key
        val decoded: Parameter<out Any> = map(model.value)

        return Property.created(
            key = key,
            value = decoded
        )

    }

    internal fun mapUpdated(event: SinglePropertyModel): Property {

        require(event.type == PropertyTypeModel.UPDATED) {
            "Event type must be ${PropertyTypeModel.UPDATED}."
        }

        val key: String = event.key
        val decoded: Parameter<out Any> = map(event.value)

        return Property.updated(
            key = key,
            value = decoded
        )

    }

    internal fun mapDeleted(model: SinglePropertyModel): Property {

        require(model.type == PropertyTypeModel.DELETED) {
            "Event type must be ${PropertyTypeModel.DELETED}."
        }

        val key: String = model.key
        return Property.deleted(key)

    }

    internal fun mapUnknown(model: SinglePropertyModel): Property {
        val key: String = model.key
        return Property.unknown(key)
    }

    fun map(model: ParameterModel<out Any>): Parameter<out Any> {

        return when (model) {

            is BooleanParameterModel -> Parameter.boolean(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue(),
            )

            is StringParameterModel -> Parameter.string(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue(),
                possibleValues = model
                    .view
                    .flatMap(ParameterViewModel<String>::enum)
                    .orElseGet(::emptyList)
            )

            is JsonParameterModel -> Parameter.json(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue(),
            )

            is IntParameterModel -> Parameter.int(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue(),
                possibleValues = model
                    .view
                    .flatMap(ParameterViewModel<Int>::enum)
                    .orElseGet(::emptyList)
            )

            is Int64ParameterModel -> Parameter.int64(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue(),
                possibleValues = model
                    .view
                    .flatMap(ParameterViewModel<Long>::enum)
                    .orElseGet(::emptyList)
            )

            is Uint64ParameterModel -> Parameter.uint64(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue(),
                possibleValues = model
                    .view
                    .flatMap(ParameterViewModel<BigInteger>::enum)
                    .orElseGet(::emptyList)
            )

            is Float64ParameterModel -> Parameter.float64(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue(),
                possibleValues = model
                    .view
                    .flatMap(ParameterViewModel<Double>::enum)
                    .orElseGet(::emptyList)
            )

            is DurationParameterModel -> Parameter.duration(
                usage = model.usage.get(),
                group = model.group.get(),
                value = model.getValue()?.toKotlinDuration(),
            )

        }

    }

}