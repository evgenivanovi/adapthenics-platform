package com.evgenivanovi.adapthenics.rtc.etcd.adapter.service

import com.evgenivanovi.kt.regex.Regexes
import com.evgenivanovi.kt.regex.escape
import com.evgenivanovi.kt.string.Strings

class PropertyNameResolver(
    val prefix: String,
) {

    fun combine(name: String): String {
        return prefix + name.lowercase()
    }

    fun resolve(name: String): String {
        val regex = (Regexes.START + prefix.escape()).toRegex()
        return name.replaceFirst(regex, Strings.EMPTY).lowercase()
    }

    companion object {

        val ASIS = PropertyNameResolver(Strings.EMPTY)

        fun of(
            prefix: String,
            delimiter: String,
            vararg params: String,
        ): PropertyNameResolver {
            val prefixed = buildPrefix(
                prefix = prefix,
                delimiter = delimiter,
                params = params
            )
            return PropertyNameResolver(prefixed)
        }

        private fun buildPrefix(
            prefix: String,
            delimiter: String,
            vararg params: String,
        ): String {
            var prefixed = prefix

            if (prefixed.startsWith(delimiter).not())
                prefixed = delimiter.plus(prefixed)

            if (prefixed.endsWith(delimiter).not())
                prefixed = prefixed.plus(delimiter)

            val parameterized = params
                .joinToString(separator = delimiter)

            return prefixed.plus(parameterized).plus(delimiter)
        }

    }

}