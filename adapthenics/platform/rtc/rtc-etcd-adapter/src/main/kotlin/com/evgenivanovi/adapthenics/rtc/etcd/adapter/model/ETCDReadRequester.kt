package com.evgenivanovi.adapthenics.rtc.etcd.adapter.model

import com.evgenivanovi.adapthenics.rtc.schema.PropertyMessageModel

interface ETCDReadRequester {

    suspend fun read(key: String): PropertyMessageModel?

    suspend fun readAll(key: String): List<PropertyMessageModel>

}