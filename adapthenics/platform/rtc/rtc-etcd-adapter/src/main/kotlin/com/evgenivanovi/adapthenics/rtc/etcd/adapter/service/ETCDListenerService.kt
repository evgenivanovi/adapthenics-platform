package com.evgenivanovi.adapthenics.rtc.etcd.adapter.service

import com.evgenivanovi.adapthenics.rtc.domain.service.EventCourier
import com.evgenivanovi.adapthenics.rtc.etcd.adapter.model.ETCDListener
import com.evgenivanovi.adapthenics.rtc.schema.MultiPropertyModel
import io.github.oshai.kotlinlogging.KotlinLogging

class ETCDListenerService(
    private val resolver: PropertyNameResolver,
    private val courier: EventCourier,
) : ETCDListener {

    private val log = KotlinLogging.logger {}

    override suspend fun onNext(event: MultiPropertyModel) {
        log.debug { "onNext" }

        event
            .events
            .map { it.mapKey(resolver::resolve) }
            .map(PropertyMapper::map)
            .forEach { courier.deliver(it) }
    }

    override suspend fun onFailure(ex: Throwable) {
        log.debug(ex) { "onFailure" }
    }

    override suspend fun onComplete() {
        log.debug { "onComplete" }
    }

}