package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kotlin.reflect.KProperty1

@JsonIgnoreProperties(ignoreUnknown = true)
class BooleanParameterModel : ParameterModel<Boolean> {

    private var value: Boolean?

    constructor() : super(ParameterTypeModel.BOOL) {
        value = null
    }

    constructor(value: Boolean? = null) : super(ParameterTypeModel.BOOL) {
        this.value = value
    }

    override fun getValue(): Boolean? {
        return value
    }

    fun setValue(value: Boolean?) {
        this.value = value
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is BooleanParameterModel
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
        superEquals = { super.equals(other) }
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<BooleanParameterModel>() {

        override val properties = arrayOf<KProperty1<BooleanParameterModel, *>>(
            BooleanParameterModel::value
        )

    }

}