package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.evgenivanovi.kt.time.DurationParser
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.Duration
import kotlin.reflect.KProperty1

@JsonIgnoreProperties(ignoreUnknown = true)
class DurationParameterModel : ParameterModel<Duration> {

    private var value: String? = null

    constructor() : super(ParameterTypeModel.DURATION) {
        setValue(null)
    }

    constructor(
        value: String? = null,
    ) : super(ParameterTypeModel.DURATION) {
        setValue(value)
    }

    override fun getValue(): Duration? {
        val variable = value
        return if (variable == null) null
        else parse(variable)
    }

    fun setValue(value: String?) {
        if (canParse(value)) {
            this.value = value
        }
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is DurationParameterModel
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
        superEquals = { super.equals(other) }
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<DurationParameterModel>() {

        override val properties = arrayOf<KProperty1<DurationParameterModel, *>>(
            DurationParameterModel::value
        )

        private fun canParse(value: String?): Boolean {

            if (value == null) {
                return false
            }

            try {
                parse(value)
            } catch (ex: IllegalArgumentException) {
                return false
            }

            return true

        }

        private fun parse(value: String): Duration {
            return DurationParser
                .parseOptionally(value)
                .orThrow(IllegalArgumentException("Could not parse duration for '$value'."))
        }

    }

}