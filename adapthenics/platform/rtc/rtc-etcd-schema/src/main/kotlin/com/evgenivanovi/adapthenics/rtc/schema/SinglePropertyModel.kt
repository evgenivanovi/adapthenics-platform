package com.evgenivanovi.adapthenics.rtc.schema

import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel

class SinglePropertyModel : PropertyModel {

    val type: PropertyTypeModel

    val msg: PropertyMessageModel

    val key: String
        get() = msg.key

    val value: ParameterModel<out Any>
        get() = msg.value

    constructor(
        key: String,
        value: ParameterModel<Any>,
        type: PropertyTypeModel,
    ) : this(PropertyMessageModel(key, value), type)

    constructor(
        msg: PropertyMessageModel,
        type: PropertyTypeModel,
    ) {
        this.msg = msg
        this.type = type
    }

    fun withMessage(
        msg: PropertyMessageModel,
    ): SinglePropertyModel {
        return of(msg, type)
    }

    fun withType(
        type: PropertyTypeModel,
    ): SinglePropertyModel {
        return of(msg, type)
    }

    fun mapKey(
        mapper: (key: String) -> String,
    ): SinglePropertyModel {
        return withMessage(msg.mapKey(mapper))
    }

    fun <R : Any> mapValue(
        mapper: (value: ParameterModel<out Any>) -> ParameterModel<out Any>,
    ): SinglePropertyModel {
        return withMessage(msg.mapValue(mapper))
    }

    companion object {

        fun of(
            key: String,
            value: ParameterModel<Any>,
            type: PropertyTypeModel,
        ): SinglePropertyModel {
            return SinglePropertyModel(key, value, type)
        }

        fun of(
            msg: PropertyMessageModel,
            type: PropertyTypeModel,
        ): SinglePropertyModel {
            return SinglePropertyModel(msg, type)
        }

    }

}