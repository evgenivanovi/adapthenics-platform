package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.math.BigInteger
import kotlin.reflect.KProperty1

@JsonIgnoreProperties(ignoreUnknown = true)
class Uint64ParameterModel : ParameterModel<BigInteger> {

    private var value: BigInteger?

    constructor(value: BigInteger? = null) : super(ParameterTypeModel.UINT64) {
        this.value = value
    }

    override fun getValue(): BigInteger? {
        return value
    }

    fun setValue(value: BigInteger?) {
        this.value = value
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is Uint64ParameterModel
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
        superEquals = { super.equals(other) }
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<Uint64ParameterModel>() {

        override val properties = arrayOf<KProperty1<Uint64ParameterModel, *>>(
            Uint64ParameterModel::value
        )

    }

}