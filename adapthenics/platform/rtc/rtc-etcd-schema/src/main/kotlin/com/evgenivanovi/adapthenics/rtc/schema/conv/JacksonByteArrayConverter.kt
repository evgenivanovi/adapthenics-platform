package com.evgenivanovi.adapthenics.rtc.schema.conv

import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException

class JacksonByteArrayConverter<T : Any>(
    private val clazz: Class<T>,
    private val mapper: ObjectMapper,
) : Converter<ByteArray, T> {

    @Throws(IOException::class)
    override fun convert(source: ByteArray?): T? {
        return mapper.readValue(source, clazz)
    }

}