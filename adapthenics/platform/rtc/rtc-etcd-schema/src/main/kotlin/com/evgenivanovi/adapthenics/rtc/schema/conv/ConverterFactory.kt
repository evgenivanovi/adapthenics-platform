package com.evgenivanovi.adapthenics.rtc.schema.conv

object ConverterFactory {

    inline fun <reified T : Any> createJsonConverter(): Converter<ByteArray, T> {
        val mapper = ObjectMapperFactory.createJsonMapper()
        return JacksonByteArrayConverter(T::class.java, mapper)
    }

    inline fun <reified T : Any> createYamlConverter(): Converter<ByteArray, T> {
        val mapper = ObjectMapperFactory.createYamlMapper()
        return JacksonByteArrayConverter(T::class.java, mapper)
    }

}