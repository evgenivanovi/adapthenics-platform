package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kotlin.reflect.KProperty1

@JsonIgnoreProperties(ignoreUnknown = true)
class Int64ParameterModel : ParameterModel<Long> {

    private var value: Long?

    constructor() : super(ParameterTypeModel.INT64) {
        value = null
    }

    constructor(value: Long? = null) : super(ParameterTypeModel.INT64) {
        this.value = value
    }

    override fun getValue(): Long? {
        return value
    }

    fun setValue(value: Long?) {
        this.value = value
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is Int64ParameterModel
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
        superEquals = { super.equals(other) }
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<Int64ParameterModel>() {

        override val properties = arrayOf<KProperty1<Int64ParameterModel, *>>(
            Int64ParameterModel::value
        )

    }

}