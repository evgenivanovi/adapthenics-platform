package com.evgenivanovi.adapthenics.rtc.schema.conv

import com.evgenivanovi.adapthenics.support.jackson.json
import com.evgenivanovi.adapthenics.support.jackson.ser.DurationModule
import com.evgenivanovi.adapthenics.support.jackson.yaml
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.kotlinModule

object ObjectMapperFactory {

    fun createJsonMapper(): ObjectMapper {
        return json()
            .registerModule(kotlinModule())
            .registerModule(Jdk8Module())
            .registerModule(JavaTimeModule())
            .registerModule(DurationModule())
    }

    fun createYamlMapper(): ObjectMapper {
        return yaml()
            .registerModule(kotlinModule())
            .registerModule(Jdk8Module())
            .registerModule(JavaTimeModule())
            .registerModule(DurationModule())
    }

}