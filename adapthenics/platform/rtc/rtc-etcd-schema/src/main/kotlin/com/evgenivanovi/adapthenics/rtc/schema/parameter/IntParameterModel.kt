package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kotlin.reflect.KProperty1

@JsonIgnoreProperties(ignoreUnknown = true)
class IntParameterModel : ParameterModel<Int> {

    private var value: Int?

    constructor() : super(ParameterTypeModel.INT) {
        value = null
    }

    constructor(value: Int? = null) : super(ParameterTypeModel.INT) {
        this.value = value
    }

    override fun getValue(): Int? {
        return value
    }

    fun setValue(value: Int?) {
        this.value = value
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is IntParameterModel
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
        superEquals = { super.equals(other) }
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<IntParameterModel>() {

        override val properties = arrayOf<KProperty1<IntParameterModel, *>>(
            IntParameterModel::value
        )

    }

}