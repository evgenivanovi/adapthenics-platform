package com.evgenivanovi.adapthenics.rtc.schema

class MultiPropertyModel : PropertyModel {

    val events: List<SinglePropertyModel>

    constructor(events: List<SinglePropertyModel>) {
        this.events = events
    }

    companion object {

        fun of(events: List<SinglePropertyModel>): MultiPropertyModel {
            return MultiPropertyModel(events)
        }

    }

}