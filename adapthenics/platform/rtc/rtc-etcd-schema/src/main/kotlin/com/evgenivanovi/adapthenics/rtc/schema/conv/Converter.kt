package com.evgenivanovi.adapthenics.rtc.schema.conv

interface Converter<SOURCE : Any, TARGET : Any> {

    fun convert(source: SOURCE?): TARGET?

}