package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.adapthenics.rtc.schema.parameter.*
import com.evgenivanovi.kt.any.*
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.util.*
import kotlin.reflect.KProperty1

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = ParameterTypeModel.TYPE_FIELD_NAME
)
@JsonSubTypes(
    value = [
        JsonSubTypes.Type(
            value = BooleanParameterModel::class,
            name = ParameterTypeModel.TYPE_BOOL
        ),
        JsonSubTypes.Type(
            value = StringParameterModel::class,
            name = ParameterTypeModel.TYPE_STRING
        ),
        JsonSubTypes.Type(
            value = JsonParameterModel::class,
            name = ParameterTypeModel.TYPE_JSON
        ),
        JsonSubTypes.Type(
            value = IntParameterModel::class,
            name = ParameterTypeModel.TYPE_INT
        ),
        JsonSubTypes.Type(
            value = Int64ParameterModel::class,
            name = ParameterTypeModel.TYPE_INT64
        ),
        JsonSubTypes.Type(
            value = Uint64ParameterModel::class,
            name = ParameterTypeModel.TYPE_UINT64
        ),
        JsonSubTypes.Type(
            value = Float64ParameterModel::class,
            name = ParameterTypeModel.TYPE_FLOAT64
        ),
        JsonSubTypes.Type(
            value = DurationParameterModel::class,
            name = ParameterTypeModel.TYPE_DURATION
        )
    ]
)
@JsonIgnoreProperties(ignoreUnknown = true)
sealed class ParameterModel<T : Any> : MixTypeEquality {

    lateinit var usage: Optional<String>

    lateinit var group: Optional<String>

    lateinit var type: Optional<ParameterTypeModel>

    lateinit var writable: Optional<Boolean>

    var view: Optional<ParameterViewModel<T>> = Optional.empty()

    abstract fun getValue(): T?

    constructor()

    constructor(type: ParameterTypeModel) {
        this.type = Optional.of(type)
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is ParameterModel<*>
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<ParameterModel<*>>() {

        override val properties: Array<KProperty1<ParameterModel<*>, *>> = arrayOf(
            ParameterModel<*>::usage,
            ParameterModel<*>::group,
            ParameterModel<*>::type,
            ParameterModel<*>::writable,
            ParameterModel<*>::view,
        )

    }

}