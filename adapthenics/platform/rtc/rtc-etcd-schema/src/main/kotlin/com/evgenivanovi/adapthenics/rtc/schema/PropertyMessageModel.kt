package com.evgenivanovi.adapthenics.rtc.schema

import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel

class PropertyMessageModel(
    val key: String,
    val value: ParameterModel<out Any>,
) {

    fun mapKey(
        mapper: (key: String) -> String,
    ): PropertyMessageModel {
        return of(mapper.invoke(key), value)
    }

    fun mapValue(
        mapper: (value: ParameterModel<out Any>) -> ParameterModel<out Any>,
    ): PropertyMessageModel {
        return of(key, mapper.invoke(value))
    }

    companion object {

        fun of(
            key: String,
            value: ParameterModel<out Any>,
        ): PropertyMessageModel {
            return PropertyMessageModel(key, value)
        }

    }

}