package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigInteger
import java.time.Duration

enum class ParameterTypeModel {

    @JsonProperty(TYPE_BOOL)
    BOOL(
        BooleanParameterModel::class.java,
        Boolean::class.java,
    ),

    @JsonProperty(TYPE_STRING)
    STRING(
        StringParameterModel::class.java,
        String::class.java
    ),

    @JsonProperty(TYPE_JSON)
    JSON(
        JsonParameterModel::class.java,
        String::class.java
    ),

    @JsonProperty(TYPE_INT)
    INT(
        IntParameterModel::class.java,
        Int::class.java,
    ),

    @JsonProperty(TYPE_INT64)
    INT64(
        Int64ParameterModel::class.java,
        Long::class.java,
    ),

    @JsonProperty(TYPE_UINT64)
    UINT64(
        Uint64ParameterModel::class.java,
        BigInteger::class.java
    ),

    @JsonProperty(TYPE_FLOAT64)
    FLOAT64(
        Float64ParameterModel::class.java,
        Double::class.java,
    ),

    @JsonProperty(TYPE_DURATION)
    DURATION(
        DurationParameterModel::class.java,
        Duration::class.java
    );

    val typeClass: Class<out ParameterModel<*>>
    val valueClasses: Array<out Class<*>>

    constructor(
        typeClass: Class<out ParameterModel<*>>,
        vararg valueClasses: Class<*>,
    ) {
        this.typeClass = typeClass
        this.valueClasses = valueClasses
    }

    companion object {

        const val TYPE_FIELD_NAME = "type"

        const val TYPE_BOOL = "bool"
        const val TYPE_STRING = "string"
        const val TYPE_JSON = "json"
        const val TYPE_INT = "int"
        const val TYPE_INT64 = "int64"
        const val TYPE_UINT64 = "uint64"
        const val TYPE_FLOAT64 = "float64"
        const val TYPE_DURATION = "duration"

    }

}