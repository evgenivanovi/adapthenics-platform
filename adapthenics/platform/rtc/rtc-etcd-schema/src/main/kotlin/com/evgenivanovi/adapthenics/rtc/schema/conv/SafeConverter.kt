package com.evgenivanovi.adapthenics.rtc.schema.conv

import io.github.oshai.kotlinlogging.KotlinLogging

class SafeConverter<SOURCE : Any, TARGET : Any>(
    private val delegate: Converter<SOURCE, TARGET>,
) : Converter<SOURCE, TARGET> {

    private val log = KotlinLogging.logger {}

    override fun convert(source: SOURCE?): TARGET? {
        return try {
            delegate.convert(source)
        } catch (ex: Exception) {
            log.debug { "Failed to convert source='$source'" }
            log.debug { "Falling back to null conversion result" }
            null
        }
    }

}