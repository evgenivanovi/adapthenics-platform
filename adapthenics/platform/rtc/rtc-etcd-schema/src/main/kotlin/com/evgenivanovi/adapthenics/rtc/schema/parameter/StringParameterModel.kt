package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kotlin.reflect.KProperty1

@JsonIgnoreProperties(ignoreUnknown = true)
class StringParameterModel : ParameterModel<String> {

    private var value: String?

    constructor() : super(ParameterTypeModel.STRING) {
        this.value = null
    }

    constructor(value: String? = null) : super(ParameterTypeModel.STRING) {
        this.value = value
    }

    override fun getValue(): String? {
        return value
    }

    fun setValue(value: String?) {
        this.value = value
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is StringParameterModel
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
        superEquals = { super.equals(other) }
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<StringParameterModel>() {

        override val properties = arrayOf<KProperty1<StringParameterModel, *>>(
            StringParameterModel::value
        )

    }

}