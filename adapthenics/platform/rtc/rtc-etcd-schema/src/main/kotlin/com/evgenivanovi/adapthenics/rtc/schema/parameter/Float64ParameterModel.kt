package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.any.kotlinEquals
import com.evgenivanovi.kt.any.kotlinHashCode
import com.evgenivanovi.kt.any.kotlinToString
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kotlin.reflect.KProperty1

@JsonIgnoreProperties(ignoreUnknown = true)
class Float64ParameterModel : ParameterModel<Double> {

    private var value: Double?

    constructor() : super(ParameterTypeModel.FLOAT64) {
        value = null
    }

    constructor(value: Double? = null) : super(ParameterTypeModel.FLOAT64) {
        this.value = value
    }

    override fun getValue(): Double? {
        return value
    }

    fun setValue(value: Double?) {
        this.value = value
    }

    /* Overridden methods [Any] */

    override fun canEqual(other: Any?): Boolean {
        return other is Float64ParameterModel
    }

    override fun equals(other: Any?) = kotlinEquals(
        other = other,
        properties = properties,
        superEquals = { super.equals(other) }
    )

    private val hashCode: Int by lazy {
        kotlinHashCode(
            properties = properties,
            superHashCode = { super.hashCode() }
        )
    }

    override fun hashCode() = hashCode

    private val toString: String by lazy {
        kotlinToString(
            properties = properties,
            superToString = { super.toString() }
        )
    }

    override fun toString() = toString

    companion object : ContractableCompanion<Float64ParameterModel>() {

        override val properties = arrayOf<KProperty1<Float64ParameterModel, *>>(
            Float64ParameterModel::value
        )

    }

}