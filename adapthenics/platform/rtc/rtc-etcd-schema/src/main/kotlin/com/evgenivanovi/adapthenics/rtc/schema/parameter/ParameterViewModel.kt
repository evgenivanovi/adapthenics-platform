package com.evgenivanovi.adapthenics.rtc.schema.parameter

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
class ParameterViewModel<T : Any> {

    var enum: Optional<List<T>> = Optional.empty()

}