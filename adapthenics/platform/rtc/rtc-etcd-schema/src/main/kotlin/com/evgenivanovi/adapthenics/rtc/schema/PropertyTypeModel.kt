package com.evgenivanovi.adapthenics.rtc.schema

import com.evgenivanovi.kt.any.EnumResolver

enum class PropertyTypeModel {

    CREATED,

    UPDATED,

    DELETED,

    UNKNOWN;

    companion object : EnumResolver<PropertyTypeModel>(PropertyTypeModel::class.java)

}