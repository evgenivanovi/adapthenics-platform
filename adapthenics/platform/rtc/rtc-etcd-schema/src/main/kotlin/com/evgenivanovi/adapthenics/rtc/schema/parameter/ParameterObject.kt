package com.evgenivanovi.adapthenics.rtc.schema.parameter

@Target(
    AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.FIELD,
    AnnotationTarget.CLASS,
)
@Retention(
    AnnotationRetention.RUNTIME
)
annotation class ParameterObject