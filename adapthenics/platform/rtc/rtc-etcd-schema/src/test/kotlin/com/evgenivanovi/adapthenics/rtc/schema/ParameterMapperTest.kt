package com.evgenivanovi.adapthenics.rtc.schema

import com.evgenivanovi.adapthenics.rtc.schema.conv.JacksonByteArrayConverter
import com.evgenivanovi.adapthenics.rtc.schema.conv.ObjectMapperFactory
import com.evgenivanovi.adapthenics.rtc.schema.parameter.*
import com.evgenivanovi.kt.lang.Booleans
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.math.BigInteger
import java.util.*

class ParameterMapperTest {

    @ParameterizedTest
    @MethodSource("parameterJsonModels")
    fun shouldMapJson(
        input: String,
        expected: ParameterModel<*>?,
    ) {

        // given
        val converter = JacksonByteArrayConverter(
            ParameterModel::class.java,
            ObjectMapperFactory.createJsonMapper()
        )

        // when
        val actual = converter.convert(input.toByteArray())

        // then
        Assertions.assertAll(
            {
                assertEquals(expected, actual)
            },
            {
                assertEquals(expected?.getValue(), actual?.getValue())
            },
        )

    }

    @ParameterizedTest
    @MethodSource("parameterYamlModels")
    fun shouldMapYaml(
        input: String,
        expected: ParameterModel<*>?,
    ) {

        // given
        val converter = JacksonByteArrayConverter(
            ParameterModel::class.java,
            ObjectMapperFactory.createYamlMapper()
        )

        // when
        val actual = converter.convert(input.toByteArray())

        // then
        Assertions.assertAll(
            {
                assertEquals(expected, actual)
            },
            {
                assertEquals(expected?.getValue(), actual?.getValue())
            },
        )

    }

    companion object {

        @JvmStatic
        fun parameterJsonModels(): Collection<Arguments> = emptyList<Arguments>()
            .asSequence()
            .plus(booleanJsonParameters())
            .plus(stringJsonParameters())
            .plus(jsonJsonParameters())
            .plus(intJsonParameters())
            .plus(int64JsonParameters())
            .plus(uint64JsonParameters())
            .plus(float64JsonParameters())
            .plus(durationJsonParameters())
            .toList()

        private fun booleanJsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "bool",
                        "writable": "true"
                    }
                """.trimIndent(),
                BooleanParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "true",
                        "type": "bool",
                        "writable": "true"
                    }
                """.trimIndent(),
                BooleanParameterModel(value = true)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun stringJsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "string",
                        "writable": "true"
                    }
                """.trimIndent(),
                StringParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "TEST_VALUE",
                        "type": "string",
                        "writable": "true"
                    }
                """.trimIndent(),
                StringParameterModel(value = "TEST_VALUE")
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun jsonJsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "json",
                        "writable": "true"
                    }
                """.trimIndent(),
                JsonParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "{\"key\":\"value\"}",
                        "type": "json",
                        "writable": "true"
                    }
                """.trimIndent(),
                JsonParameterModel(value = "{\"key\":\"value\"}")
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun intJsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "int",
                        "writable": "true"
                    }
                """.trimIndent(),
                IntParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": 999,
                        "type": "int",
                        "writable": "true"
                    }
                """.trimIndent(),
                IntParameterModel(value = 999)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun int64JsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "int64",
                        "writable": "true"
                    }
                """.trimIndent(),
                Int64ParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": 999,
                        "type": "int64",
                        "writable": "true"
                    }
                """.trimIndent(),
                Int64ParameterModel(value = 999)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun uint64JsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "uint64",
                        "writable": "true"
                    }
                """.trimIndent(),
                Uint64ParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "999",
                        "type": "uint64",
                        "writable": "true"
                    }
                """.trimIndent(),
                Uint64ParameterModel(value = BigInteger.valueOf(999))
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun float64JsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "float64",
                        "writable": "true"
                    }
                """.trimIndent(),
                Float64ParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "999.9",
                        "type": "float64",
                        "writable": "true"
                    }
                """.trimIndent(),
                Float64ParameterModel(value = 999.9)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun durationJsonParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": null,
                        "type": "duration",
                        "writable": "true"
                    }
                """.trimIndent(),
                DurationParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "9d9m9s",
                        "type": "duration",
                        "writable": "true"
                    }
                """.trimIndent(),
                DurationParameterModel(value = "9d9m9s")
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        /* __________________________________________________ */

        @JvmStatic
        fun parameterYamlModels(): Collection<Arguments> = emptyList<Arguments>()
            .asSequence()
            .plus(booleanYamlParameters())
            .plus(stringYamlParameters())
            .plus(jsonYamlParameters())
            .plus(intYamlParameters())
            .plus(int64YamlParameters())
            .plus(uint64YamlParameters())
            .plus(float64YamlParameters())
            .plus(durationYamlParameters())
            .toList()

        private fun booleanYamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: bool
                    writable: true
                """.trimIndent(),
                BooleanParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: true
                    type: bool
                    writable: true
                """.trimIndent(),
                BooleanParameterModel(value = true)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun stringYamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: string
                    writable: true
                """.trimIndent(),
                StringParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: TEST_VALUE
                    type: string
                    writable: true
                """.trimIndent(),
                StringParameterModel(value = "TEST_VALUE")
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun jsonYamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: json
                    writable: true
                """.trimIndent(),
                JsonParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: '{"key":"value"}'
                    type: json
                    writable: true
                """.trimIndent(),
                JsonParameterModel(value = "{\"key\":\"value\"}")
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun intYamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: int
                    writable: true
                """.trimIndent(),
                IntParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: 999
                    type: int
                    writable: true
                """.trimIndent(),
                IntParameterModel(value = 999)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun int64YamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: int64
                    writable: true
                """.trimIndent(),
                Int64ParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: 999
                    type: int64
                    writable: true
                """.trimIndent(),
                Int64ParameterModel(value = 999)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun uint64YamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: uint64
                    writable: true
                """.trimIndent(),
                Uint64ParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: 999
                    type: uint64
                    writable: true
                """.trimIndent(),
                Uint64ParameterModel(value = BigInteger.valueOf(999))
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun float64YamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: float64
                    writable: true
                """.trimIndent(),
                Float64ParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "999.9",
                        "type": "float64",
                        "writable": "true"
                    }
                """.trimIndent(),
                Float64ParameterModel(value = 999.9)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun durationYamlParameters(): List<Arguments> = listOf(
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: null
                    type: duration
                    writable: true
                """.trimIndent(),
                DurationParameterModel(value = null)
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            ),
            Arguments.of(
                """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: 9d9m9s
                    type: duration
                    writable: true
                """.trimIndent(),
                DurationParameterModel(value = "9d9m9s")
                    .apply { usage = "TEST_USAGE".optional() }
                    .apply { group = "TEST_GROUP".optional() }
                    .apply { writable = Booleans.TRUE.optional() }
            )
        )

        private fun <T : Any> T?.optional(): Optional<T> {
            return Optional.ofNullable(this)
        }

    }

}