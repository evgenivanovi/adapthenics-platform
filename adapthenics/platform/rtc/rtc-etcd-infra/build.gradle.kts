project(ProjectModules.RTC_ETCD_INFRA) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.JACKSON_SUPPORT))
        testImplementation(project(ProjectModules.JACKSON_SUPPORT))

        implementation(project(ProjectModules.ETCD_CLIENT))
        testImplementation(project(ProjectModules.ETCD_CLIENT))

        implementation(project(ProjectModules.RTC_ETCD_SCHEMA))
        testImplementation(project(ProjectModules.RTC_ETCD_SCHEMA))

        implementation(project(ProjectModules.RTC_ETCD_ADAPTER))
        testImplementation(project(ProjectModules.RTC_ETCD_ADAPTER))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(ktDependenciesCatalog.bundles.jackson.json.impl)
    implementation(ktDependenciesCatalog.bundles.jackson.yaml.impl)

    implementation(libs.bundles.etcd.impl)
    testImplementation(libs.bundles.etcd.impl)
    testImplementation(libs.bundles.etcd.testimpl)
}

tasks {

    test {
        maxParallelForks = 1
    }

}