package com.evgenivanovi.adapthenics.rtc.etcd.infra

import com.evgenivanovi.adapthenics.rtc.schema.PropertyMessageModel
import com.evgenivanovi.adapthenics.rtc.schema.PropertyTypeModel
import com.evgenivanovi.adapthenics.rtc.schema.SinglePropertyModel
import com.evgenivanovi.adapthenics.rtc.schema.conv.Converter
import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel
import io.etcd.jetcd.KeyValue
import io.etcd.jetcd.kv.GetResponse
import io.etcd.jetcd.watch.WatchEvent
import io.etcd.jetcd.watch.WatchEvent.EventType
import io.etcd.jetcd.watch.WatchResponse

/* __________________________________________________ */

internal fun GetResponse.toMessages(
    converter: Converter<ByteArray, ParameterModel<Any>>,
): List<PropertyMessageModel> {
    return kvs.mapNotNull { it.toMessage(converter) }
}

internal fun KeyValue.toMessage(
    converter: Converter<ByteArray, ParameterModel<Any>>,
): PropertyMessageModel? {

    val key: String = key.bytes.toString()

    val value: ParameterModel<Any> = converter
        .convert(value.bytes)
        ?: return null

    return PropertyMessageModel(
        key = key,
        value = value
    )

}

/* __________________________________________________ */

internal fun WatchResponse.toProperties(
    converter: Converter<ByteArray, ParameterModel<Any>>,
): List<SinglePropertyModel> {
    return events.mapNotNull { it.toProperty(converter) }
}

internal fun WatchEvent.toProperty(
    converter: Converter<ByteArray, ParameterModel<Any>>,
): SinglePropertyModel? {

    val key: String = keyValue.key.toString()

    val value: ParameterModel<Any> = converter
        .convert(keyValue.value.bytes)
        ?: return null

    return SinglePropertyModel(
        key = key,
        value = value,
        type = eventType.toEventType(),
    )

}

private fun EventType.toEventType(): PropertyTypeModel {
    return when (this) {
        EventType.PUT -> PropertyTypeModel.UPDATED
        EventType.DELETE -> PropertyTypeModel.DELETED
        EventType.UNRECOGNIZED -> PropertyTypeModel.UNKNOWN
    }
}

/* __________________________________________________ */