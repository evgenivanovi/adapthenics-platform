package com.evgenivanovi.adapthenics.rtc.etcd.infra

import com.evgenivanovi.adapthenics.rtc.etcd.adapter.model.ETCDListener
import com.evgenivanovi.adapthenics.rtc.schema.MultiPropertyModel
import com.evgenivanovi.adapthenics.rtc.schema.conv.Converter
import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel
import com.evgenivanovi.adapthenics.support.reactor.onError
import com.evgenivanovi.kt.func.no
import com.evgenivanovi.kt.func.yes
import com.evgenivanovi.kt.reactive.CoroutineSubscriber
import com.evgenivanovi.kt.reactive.Subscription
import com.evgenivanovi.kt.string.Strings
import io.etcd.jetcd.Response
import io.etcd.jetcd.reactive.ReactiveWatch
import io.etcd.jetcd.watch.WatchResponse
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.reactor.SchedulerCoroutineDispatcher
import kotlinx.coroutines.reactor.asCoroutineDispatcher
import kotlinx.coroutines.reactor.mono
import reactor.core.Disposable
import reactor.core.scheduler.Scheduler

class ETCDWatcherService(
    private val key: String,
    private val watch: ReactiveWatch,
    private val scheduler: Scheduler,
    converter: Converter<ByteArray, ParameterModel<Any>>,
    listener: ETCDListener,
) : ETCDWatcher {

    private val listener: WatchResponseListener =
        WatchResponseListener(converter, listener)

    private val dispatcher: SchedulerCoroutineDispatcher =
        scheduler.asCoroutineDispatcher()

    private lateinit var disposable: Disposable

    override fun start() {
        this::disposable.isInitialized
            .no { disposable = watch() }
    }

    override fun stop() {
        this::disposable.isInitialized
            .and(disposable.isDisposed.not())
            .yes { disposable.dispose() }
    }

    private fun watch(): Disposable {
        return WatcherFactory
            .create(watch, key)
            .publishOn(scheduler)
            .concatMap { mono(dispatcher) { listen(it) } }
            .onError(::listen)
            .onErrorComplete()
            .subscribe()
    }

    private suspend fun listen(message: WatchResponse): WatchResponse {
        listener.onNext(message)
        return message
    }

    private suspend fun listen(ex: Throwable) {
        return listener.onError(ex)
    }

    internal class WatchResponseListener(
        private val converter: Converter<ByteArray, ParameterModel<Any>>,
        private val listener: ETCDListener,
    ) : CoroutineSubscriber<WatchResponse> {

        private val log = KotlinLogging.logger {}

        override suspend fun onSubscribe(subscription: Subscription) {
            log.debug { "Subscribed to ETCD Watcher" }
        }

        override suspend fun onNext(event: WatchResponse) {

            if (event.isProgressNotify) {
                log.debug {
                    "Received message from ETCD: ${buildLogMessage(event)} "
                }
                return
            }

            return doOnNext(event)

        }

        private suspend fun doOnNext(response: WatchResponse) {
            val frame = response
                .toProperties(converter)
                .let(::MultiPropertyModel)
            listener.onNext(frame)
        }

        override suspend fun onError(ex: Throwable) {
            log.debug(ex) { "Received error from ETCD: {}" }
            listener.onFailure(ex)
        }

        override suspend fun onComplete() {
            log.debug { "Received completion from ETCD: {}" }
            listener.onComplete()
        }

        private fun buildLogMessage(response: WatchResponse): String {

            val header: Response.Header = response.header
                ?: return Strings.EMPTY

            return StringBuilder()
                .append("Revision: ")
                .append(header.revision)
                .append(", ClusterId: ")
                .append(header.clusterId)
                .append(", MemberId: ")
                .append(header.memberId)
                .toString()

        }

    }

}