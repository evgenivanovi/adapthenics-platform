package com.evgenivanovi.adapthenics.rtc.etcd.infra

interface ETCDWatcher {

    fun start()

    fun stop()

}