package com.evgenivanovi.adapthenics.rtc.etcd.infra

import com.evgenivanovi.adapthenics.rtc.etcd.adapter.model.ETCDReadRequester
import com.evgenivanovi.adapthenics.rtc.schema.PropertyMessageModel
import com.evgenivanovi.adapthenics.rtc.schema.conv.Converter
import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel
import io.etcd.jetcd.ByteSequence
import io.etcd.jetcd.kv.GetResponse
import io.etcd.jetcd.options.GetOption
import io.etcd.jetcd.reactive.ReactiveKV
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.reactor.awaitSingle

class ETCDReadRequesterService(
    private val kv: ReactiveKV,
    private val converter: Converter<ByteArray, ParameterModel<Any>>,
) : ETCDReadRequester {

    private val log = KotlinLogging.logger {}

    override suspend fun read(key: String): PropertyMessageModel? {
        val response = doReadSingle(key)
        return response.toMessages(converter).firstOrNull()
    }

    override suspend fun readAll(key: String): List<PropertyMessageModel> {
        val response = doReadMulti(key)
        return response.toMessages(converter)
    }

    private suspend fun doReadSingle(key: String): GetResponse {

        val keyBytes = key.toByteArray()
            .let(ByteSequence::from)

        val option = GetOption.builder()
            .isPrefix(false)
            .build()

        return kv
            .get(keyBytes, option)
            .awaitSingle()

    }

    private suspend fun doReadMulti(key: String): GetResponse {

        val keyBytes = key.toByteArray()
            .let(ByteSequence::from)

        val option = GetOption.builder()
            .isPrefix(true)
            .build()

        return kv
            .get(keyBytes, option)
            .awaitSingle()

    }

}