package com.evgenivanovi.adapthenics.rtc.etcd.infra

import io.etcd.jetcd.ByteSequence
import io.etcd.jetcd.options.WatchOption
import io.etcd.jetcd.reactive.ReactiveClient
import io.etcd.jetcd.reactive.ReactiveWatch
import io.etcd.jetcd.watch.WatchResponse
import reactor.core.publisher.Flux
import java.nio.charset.StandardCharsets

internal object WatcherFactory {

    private val option: () -> WatchOption = {
        WatchOption
            .builder()
            .isPrefix(true)
            .withProgressNotify(true)
            .build()
    }

    fun create(
        client: ReactiveClient,
        key: String,
    ): Flux<WatchResponse> {
        val keyBytes = ByteSequence.from(key, StandardCharsets.UTF_8)
        return client.watchClient.watch(keyBytes, option())
    }

    fun create(
        client: ReactiveWatch,
        key: String,
    ): Flux<WatchResponse> {
        val keyBytes = ByteSequence.from(key, StandardCharsets.UTF_8)
        return client.watch(keyBytes, option())
    }

}