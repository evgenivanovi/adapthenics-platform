package com.evgenivanovi.adapthenics.rtc.etcd.infra

import com.evgenivanovi.adapthenics.kt.test.DisabledOnCI
import com.evgenivanovi.adapthenics.rtc.etcd.infra.PropertyTestUtil.insertJsonParameter
import com.evgenivanovi.adapthenics.rtc.etcd.infra.PropertyTestUtil.stringParameter
import com.evgenivanovi.adapthenics.rtc.schema.PropertyMessageModel
import com.evgenivanovi.adapthenics.rtc.schema.conv.Converter
import com.evgenivanovi.adapthenics.rtc.schema.conv.ConverterFactory
import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel
import com.evgenivanovi.kt.math.Numbers
import io.etcd.jetcd.Client
import io.etcd.jetcd.reactive.ReactiveClient
import io.etcd.jetcd.reactive.ReactiveKV
import io.etcd.jetcd.test.EtcdClusterExtension
import kotlinx.coroutines.runBlocking
import org.apache.commons.collections.CollectionUtils
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

/**
 * Beware of using this test class in parallel with other tests.
 * Use unique keys for each test.
 */
@DisabledOnCI
class ETCDReadRequesterServiceTest {

    lateinit var client: Client

    lateinit var reactiveClient: ReactiveClient

    lateinit var kv: ReactiveKV

    val converter: Converter<ByteArray, ParameterModel<Any>> =
        ConverterFactory.createJsonConverter()

    @BeforeEach
    fun beforeEach() {
        initClients()
    }

    @AfterEach
    fun afterEach() {
        closeClients()
    }

    private fun initClients() {

        client = Client
            .builder()
            .endpoints(extension.clientEndpoints())
            .build()

        reactiveClient = ReactiveClient
            .builder(client)
            .build()

        kv = reactiveClient.kvClient

    }

    private fun closeClients() {
        kv.close()
        reactiveClient.close()
        client.close()
    }

    companion object {

        const val TIMEOUT_MS_VALUE = 5000L
        const val TIMEOUT_MS_GAP_VALUE = 500L
        const val TIMEOUT_MS_FULL_VALUE = TIMEOUT_MS_VALUE + TIMEOUT_MS_GAP_VALUE

        @RegisterExtension
        @JvmStatic
        val extension: EtcdClusterExtension = EtcdClusterExtension
            .builder()
            .withNodes(Numbers.ONE_I)
            .build()

    }

    @Test
    fun shouldReadByKey() = runBlocking {

        // given
        val testee = ETCDReadRequesterService(
            kv = kv,
            converter = converter
        )

        // init
        insertJsonParameter(
            kv = kv,
            key = "etcd_rocks",
            value = "KEY_1"
        )

        // when
        val actual = testee.read("etcd_rocks")

        // then
        val expected = stringParameter("KEY_1")

        assertAll(
            {
                assertNotNull(actual)
            },
            {
                assertEquals(expected, actual?.value)
            }
        )

    }

    @Test
    fun shouldReadByPrefixedKey() = runBlocking {

        // given
        val testee = ETCDReadRequesterService(
            kv = kv,
            converter = converter
        )

        // init
        insertJsonParameter(
            kv = kv,
            prefix = "/etcd/rocks",
            key = "KEY_1",
            value = "KEY_1"
        )

        insertJsonParameter(
            kv = kv,
            prefix = "/etcd/rocks",
            key = "KEY_2",
            value = "KEY_2"
        )

        // when
        val actual = testee.readAll("/etcd/rocks")

        // then
        val expected = listOf(
            stringParameter("KEY_1"),
            stringParameter("KEY_2"),
        )

        assertAll(
            {
                val messages = actual.map(PropertyMessageModel::value)
                val equality = CollectionUtils.isEqualCollection(expected, messages)
                assertTrue(equality)
            }
        )

    }

}