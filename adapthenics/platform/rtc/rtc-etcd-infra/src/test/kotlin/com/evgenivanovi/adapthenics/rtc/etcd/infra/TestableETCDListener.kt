package com.evgenivanovi.adapthenics.rtc.etcd.infra

import com.evgenivanovi.adapthenics.rtc.etcd.adapter.model.ETCDListener
import com.evgenivanovi.adapthenics.rtc.schema.MultiPropertyModel
import com.evgenivanovi.kt.math.Numbers
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger

class TestableETCDListener(
    expectedEvents: Int = Numbers.ZERO_I,
    expectedExceptions: Int = Numbers.ZERO_I,
) : ETCDListener {

    val eventLock = CountDownLatch(expectedEvents)
    val eventCounter: AtomicInteger = AtomicInteger()
    val eventAccumulator: CopyOnWriteArrayList<MultiPropertyModel> = CopyOnWriteArrayList()

    val exLock = CountDownLatch(expectedExceptions)
    val exCounter: AtomicInteger = AtomicInteger()
    val exAccumulator: CopyOnWriteArrayList<Throwable> = CopyOnWriteArrayList()

    override suspend fun onNext(event: MultiPropertyModel) {
        eventCounter.incrementAndGet()
        eventAccumulator.add(event)
        eventLock.countDown()
    }

    override suspend fun onFailure(ex: Throwable) {
        exCounter.incrementAndGet()
        exAccumulator.add(ex)
        exLock.countDown()
    }

    // @formatter:off
    override suspend fun onComplete() { }
    // @formatter:on

}