package com.evgenivanovi.adapthenics.rtc.etcd.infra

import com.evgenivanovi.adapthenics.kt.test.DisabledOnCI
import com.evgenivanovi.adapthenics.rtc.etcd.infra.PropertyTestUtil.insertJsonParameter
import com.evgenivanovi.adapthenics.rtc.schema.conv.Converter
import com.evgenivanovi.adapthenics.rtc.schema.conv.ConverterFactory
import com.evgenivanovi.adapthenics.rtc.schema.parameter.ParameterModel
import io.etcd.jetcd.Client
import io.etcd.jetcd.reactive.ReactiveClient
import io.etcd.jetcd.reactive.ReactiveKV
import io.etcd.jetcd.reactive.ReactiveWatch
import io.etcd.jetcd.test.EtcdClusterExtension
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.extension.RegisterExtension
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Beware of using this test class in parallel with other tests.
 * Use unique keys for each test.
 */
@DisabledOnCI
class ETCDWatcherServiceTest {

    lateinit var client: Client

    lateinit var reactiveClient: ReactiveClient

    lateinit var kv: ReactiveKV

    lateinit var watch: ReactiveWatch

    val scheduler: Scheduler = Schedulers
        .newSingle("TEST_SCHEDULER")

    val converter: Converter<ByteArray, ParameterModel<Any>> = ConverterFactory
        .createJsonConverter()

    @BeforeEach
    fun beforeEach() {
        initClients()
    }

    @AfterEach
    fun afterEach() {
        closeClients()
        closeScheduler()
    }

    private fun initClients() {

        client = Client
            .builder()
            .endpoints(extension.clientEndpoints())
            .build()

        reactiveClient = ReactiveClient
            .builder(client)
            .build()

        kv = reactiveClient.kvClient

        watch = reactiveClient.watchClient

    }

    private fun closeClients() {
        watch.close()
        kv.close()
        reactiveClient.close()
        client.close()
    }

    private fun closeScheduler() {
        scheduler
            .disposeGracefully()
            .timeout(java.time.Duration.ofSeconds(10))
            .onErrorResume { _ -> Mono.empty() }
            .block()
    }

    companion object {

        const val TIMEOUT_MS_VALUE = 30000L
        const val TIMEOUT_MS_FULL_VALUE = TIMEOUT_MS_VALUE + 500L

        @RegisterExtension
        @JvmStatic
        val extension: EtcdClusterExtension = EtcdClusterExtension.builder()
            .withNodes(1)
            .build()

    }

    @Test
    @Timeout(
        value = TIMEOUT_MS_FULL_VALUE,
        unit = TimeUnit.MILLISECONDS
    )
    fun shouldWatch() = runBlocking {

        // given
        val key = "etcd_rocks"
        val listener = TestableETCDListener(expectedEvents = 2)

        // init
        val testee = ETCDWatcherService(
            key = key,
            watch = watch,
            scheduler = scheduler,
            converter = converter,
            listener = listener,
        ).apply { start() }

        // when
        insertJsonParameter(kv, key, "KEY_1")
        insertJsonParameter(kv, key, "KEY_2")
        listener.eventLock.wait()

        // then
        Assertions.assertAll(
            {
                assertEquals(2, listener.eventCounter.get())
            },
            {
                assertEquals(2, listener.eventAccumulator.size)
            }
        )

        // cleaning
        testee.stop()

    }

    private fun CountDownLatch.wait() {
        await(TIMEOUT_MS_VALUE, TimeUnit.MILLISECONDS)
    }

}