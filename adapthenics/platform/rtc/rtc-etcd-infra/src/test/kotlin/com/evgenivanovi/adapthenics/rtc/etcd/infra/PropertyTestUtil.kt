package com.evgenivanovi.adapthenics.rtc.etcd.infra

import com.evgenivanovi.adapthenics.rtc.schema.parameter.StringParameterModel
import com.evgenivanovi.kt.lang.Booleans
import com.evgenivanovi.kt.lang.asNotNull
import com.evgenivanovi.kt.string.Strings
import io.etcd.jetcd.reactive.ReactiveKV
import io.etcd.jetcd.reactive.put
import java.io.File
import java.util.*

object PropertyTestUtil {

    suspend fun insertJsonParameter(
        kv: ReactiveKV,
        key: String,
        value: String,
    ) {
        val v = parameterAsJsonString(value)
        kv.put(key, v)
    }

    suspend fun insertJsonParameter(
        kv: ReactiveKV,
        prefix: String,
        key: String,
        sep: String = File.pathSeparator,
        value: String,
    ) {
        val k = compound(prefix = prefix, key = key, sep = sep)
        val v = parameterAsJsonString(value)
        kv.put(k, v)
    }

    suspend fun insertYamlParameter(
        kv: ReactiveKV,
        key: String,
        value: String,
    ) {
        val v = parameterAsYamlString(value)
        kv.put(key, v)
    }

    suspend fun insertYamlParameter(
        kv: ReactiveKV,
        prefix: String,
        key: String,
        sep: String = File.pathSeparator,
        value: String,
    ) {
        val k = compound(prefix = prefix, key = key, sep = sep)
        val v = parameterAsYamlString(value)
        kv.put(k, v)
    }

    fun stringParameter(value: String): StringParameterModel {
        return StringParameterModel(value = value)
            .apply { usage = "TEST_USAGE".optional() }
            .apply { group = "TEST_GROUP".optional() }
            .apply { writable = Booleans.TRUE.optional() }
    }

    fun parameterAsJsonString(value: String): String {
        return """
                    {
                        "usage": "TEST_USAGE",
                        "group": "TEST_GROUP",
                        "value": "$value",
                        "type": "string",
                        "writable": "true"
                    }
                """.trimIndent()
    }

    fun parameterAsYamlString(value: String): String {
        return """
                    usage: TEST_USAGE
                    group: TEST_GROUP
                    value: $value
                    type: string
                    writable: true
                """.trimIndent()
    }

    private fun compound(
        prefix: String? = Strings.EMPTY,
        sep: String = File.pathSeparator,
        key: String?,
    ): String {

        if (prefix.isNullOrBlank() && key.isNullOrBlank())
            throw IllegalArgumentException()

        if (prefix.isNullOrBlank() && key.isNullOrBlank().not()) {
            return key.asNotNull()
        }

        if (prefix.isNullOrBlank().not() && key.isNullOrBlank()) {
            return prefix.asNotNull()
        }

        return prefix + sep + key

    }

    private fun <T : Any> T?.optional(): Optional<T> {
        return Optional.ofNullable(this)
    }

}