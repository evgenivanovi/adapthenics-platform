package com.evgenivanovi.adapthenics.rtc.domain.service.filter

import com.evgenivanovi.kt.util.Relation
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

class KeyFilterConfigurerTest {

    @ParameterizedTest
    @MethodSource("keyFilterSource")
    fun shouldMatch(
        expected: Boolean,
        inputKey: String,
        inputFilter: KeyFilter,
    ) {

        // when
        val actual = inputFilter.matches(inputKey)

        // when && then
        Assertions.assertAll(
            {
                assertEquals(expected, actual)
            }
        )

    }

    companion object {

        @JvmStatic
        fun keyFilterSource() = listOf(

            Arguments.of(
                true,
                "",
                KeyFilter
                    .configure()
                    .build()
            ),

            Arguments.of(
                true,
                "",
                KeyFilter
                    .configure()
                    .build()
            ),

            /* __________________________________________________ */

            Arguments.of(
                true,
                "",
                KeyFilter
                    .configure()
                    .start(Relation.OR)
                    .build()
            ),

            Arguments.of(
                true,
                "",
                KeyFilter
                    .configure()
                    .start(Relation.AND)
                    .build()
            ),

            /* __________________________________________________ */

            Arguments.of(
                true,
                "",
                KeyFilter
                    .configure()
                    .start(Relation.OR)
                    .start(Relation.OR)
                    .start(Relation.AND)
                    .start(Relation.AND)
                    .build()
            ),

            Arguments.of(
                true,
                "",
                KeyFilter
                    .configure()
                    .start(Relation.AND)
                    .start(Relation.AND)
                    .start(Relation.OR)
                    .start(Relation.OR)
                    .build()
            ),

            /* __________________________________________________ */

            Arguments.of(
                true,
                "yo",
                KeyFilter
                    .configure()
                    .start(Relation.OR)
                    .and(KeyFilterFactory.allowed("yo_1"))
                    .and(KeyFilterFactory.allowed("yo"))
                    .and(KeyFilterFactory.allowed("yo_2"))
                    .build()
            ),

            // inverted version of above
            Arguments.of(
                false,
                "yo&&yo",
                KeyFilter
                    .configure()
                    .start(Relation.OR)
                    .and(KeyFilterFactory.allowed("yo_1"))
                    .and(KeyFilterFactory.allowed("yo"))
                    .and(KeyFilterFactory.allowed("yo_2"))
                    .build()
            ),

            /* __________________________________________________ */

            Arguments.of(
                true,
                "yo",
                KeyFilter
                    .configure()
                    .start(Relation.AND)
                    .and(KeyFilterFactory.regex("yo.*"))
                    .and(KeyFilterFactory.allowed("yo"))
                    .and(KeyFilterFactory.disallowed("yo_1"))
                    .and(KeyFilterFactory.disallowed("yo_2"))
                    .build()
            ),

            // inverted version of above
            Arguments.of(
                false,
                "yo&&yo",
                KeyFilter
                    .configure()
                    .start(Relation.AND)
                    .and(KeyFilterFactory.regex("yo.*"))
                    .and(KeyFilterFactory.allowed("yo"))
                    .and(KeyFilterFactory.disallowed("yo_1"))
                    .and(KeyFilterFactory.disallowed("yo_2"))
                    .build()
            ),

            /* __________________________________________________ */

            Arguments.of(
                true,
                "yo",
                KeyFilter
                    .configure()
                    .start(Relation.AND)
                    .and(KeyFilterFactory.regex("yo.*"))
                    .start(Relation.AND)
                    .and(KeyFilterFactory.allowed("yo"))
                    .start(Relation.AND)
                    .and(KeyFilterFactory.disallowed("yo_1"))
                    .start(Relation.AND)
                    .and(KeyFilterFactory.disallowed("yo_2"))
                    .build()
            ),

            // inverted version of above
            Arguments.of(
                false,
                "yo&&yo",
                KeyFilter
                    .configure()
                    .start(Relation.AND)
                    .and(KeyFilterFactory.regex("yo.*"))
                    .start(Relation.AND)
                    .and(KeyFilterFactory.allowed("yo"))
                    .start(Relation.AND)
                    .and(KeyFilterFactory.disallowed("yo_1"))
                    .start(Relation.AND)
                    .and(KeyFilterFactory.disallowed("yo_2"))
                    .build()
            )

        )

    }

}