package com.evgenivanovi.adapthenics.rtc.domain.service

import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage

interface EventReadRepository {

    suspend fun read(key: String): PropertyMessage?

    suspend fun readAll(key: String): List<PropertyMessage>

}