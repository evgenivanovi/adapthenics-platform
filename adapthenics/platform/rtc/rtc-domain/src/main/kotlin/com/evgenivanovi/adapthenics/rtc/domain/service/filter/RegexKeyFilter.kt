package com.evgenivanovi.adapthenics.rtc.domain.service.filter

import java.util.regex.Pattern

internal class RegexKeyFilter : KeyFilter {

    private val regex: Regex

    constructor(regex: Regex) {
        this.regex = regex
    }

    override fun matches(key: String): Boolean {
        return regex.matches(key)
    }

    companion object {

        fun of(regex: String): RegexKeyFilter {
            return RegexKeyFilter(Regex(regex))
        }

        fun of(regex: Pattern): RegexKeyFilter {
            return RegexKeyFilter(regex.toRegex())
        }

        fun of(regex: Regex): RegexKeyFilter {
            return RegexKeyFilter(regex)
        }

    }

}