package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

class IntParameter
private constructor(
    usage: String,
    group: String,
    value: Int?,
    possibleValues: Optional<List<Int>>,
) : AbstractParameter<Int>(
    usage = usage,
    group = group,
    type = ParameterType.INT,
    value = value,
    possibleValues = possibleValues
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: Int?,
            possibleValues: List<Int> = emptyList(),
        ): IntParameter {

            val optionalPossibleValues = Optional
                .of(possibleValues)
                .filter(List<Int>::isNotEmpty)

            return IntParameter(
                usage = usage,
                group = group,
                value = value,
                possibleValues = optionalPossibleValues
            )

        }

    }

}