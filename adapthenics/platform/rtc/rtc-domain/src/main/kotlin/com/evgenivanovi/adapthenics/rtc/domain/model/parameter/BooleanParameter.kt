package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

class BooleanParameter
private constructor(
    usage: String,
    group: String,
    value: Boolean?,
) : AbstractParameter<Boolean>(
    usage = usage,
    group = group,
    type = ParameterType.BOOLEAN,
    value = value,
    possibleValues = Optional.empty()
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: Boolean?,
        ): BooleanParameter {
            return BooleanParameter(
                usage = usage,
                group = group,
                value = value,
            )
        }

    }

}