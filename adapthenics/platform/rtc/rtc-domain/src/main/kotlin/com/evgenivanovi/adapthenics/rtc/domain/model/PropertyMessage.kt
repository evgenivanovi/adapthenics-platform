package com.evgenivanovi.adapthenics.rtc.domain.model

import com.evgenivanovi.adapthenics.rtc.domain.model.parameter.Parameter
import com.evgenivanovi.kt.any.ContractableCompanion
import com.evgenivanovi.kt.lang.Keeper
import com.evgenivanovi.kt.lang.set
import kotlin.reflect.KProperty1

typealias PropertyKey = String

typealias PropertyValue = Parameter<out Any>

class PropertyMessage
private constructor(
    val key: String,
    val value: Parameter<out Any>?,
) {

    fun withKey(newKey: PropertyKey): PropertyMessage {
        return copy(
            that = this,
            key = newKey.set()
        )
    }

    fun withValue(newValue: Parameter<out Any>?): PropertyMessage {
        return copy(
            that = this,
            value = newValue.set()
        )
    }

    fun mapKey(
        mapper: (key: Any) -> PropertyKey,
    ): PropertyMessage {
        return of(mapper.invoke(key), value)
    }

    suspend fun mapKeyS(
        mapper: suspend (key: Any) -> PropertyKey,
    ): PropertyMessage {
        return of(mapper.invoke(key), value)
    }

    fun mapValue(
        mapper: (value: Any?) -> PropertyValue,
    ): PropertyMessage {
        return of(key, mapper.invoke(value))
    }

    suspend fun mapValueS(
        mapper: suspend (value: Any?) -> PropertyValue,
    ): PropertyMessage {
        return of(key, mapper.invoke(value))
    }

    companion object : ContractableCompanion<PropertyMessage>() {

        override val properties: Array<KProperty1<PropertyMessage, *>> = arrayOf(
            PropertyMessage::key,
            PropertyMessage::value,
        )

        private fun copy(
            that: PropertyMessage,
            key: Keeper<PropertyKey> = Keeper.unset(),
            value: Keeper<PropertyValue?> = Keeper.unset(),
        ): PropertyMessage {
            return PropertyMessage(
                key = key.orElse(that.key),
                value = value.orElse(that.value),
            )
        }

        fun of(
            key: PropertyKey,
            value: PropertyValue?,
        ): PropertyMessage {
            return PropertyMessage(
                key = key,
                value = value,
            )
        }

    }

}