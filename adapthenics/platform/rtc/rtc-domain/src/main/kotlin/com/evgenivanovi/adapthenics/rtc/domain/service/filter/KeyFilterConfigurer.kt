package com.evgenivanovi.adapthenics.rtc.domain.service.filter

import com.evgenivanovi.kt.contract.requireNotEmpty
import com.evgenivanovi.kt.math.Numbers.ONE_I
import com.evgenivanovi.kt.math.Numbers.ZERO_I
import com.evgenivanovi.kt.util.Relation

class KeyFilterConfigurer {

    private var current: Key

    private val filters: LinkedHashMap<Key, MutableList<KeyFilter>>

    internal constructor(relation: Relation) {
        this.current = Key.first(relation)
        this.filters = linkedMapOf()
    }

    fun start(relation: Relation): KeyFilterConfigurer {
        current = next(relation)
        return this
    }

    fun and(filter: KeyFilter): KeyFilterConfigurer {
        return add(current(), filter)
    }

    private fun add(key: Key, filter: KeyFilter): KeyFilterConfigurer {
        filters.computeIfPresent(key) { _, list -> list.apply { add(filter) } }
        filters.computeIfAbsent(key) { mutableListOf(filter) }
        return this
    }

    fun build(): KeyFilter {
        val filters = filters.keys.map(::build)

        if (filters.isEmpty())
            return KeyFilter.ALL

        return makeAND(filters)
    }

    private fun build(key: Key): KeyFilter {
        val filters = filters.getValue(key)

        if (filters.isEmpty())
            return KeyFilter.ALL

        return when (key.relation) {
            Relation.OR -> makeOR(filters)
            Relation.AND -> makeAND(filters)
        }
    }

    private fun makeOR(filters: List<KeyFilter>): KeyFilter {
        filters.requireNotEmpty()
        return KeyFilter { key ->
            filters.any { filter -> filter.matches(key) }
        }
    }

    private fun makeAND(filters: List<KeyFilter>): KeyFilter {
        filters.requireNotEmpty()
        return KeyFilter { key ->
            filters.all { filter -> filter.matches(key) }
        }
    }

    private fun current(): Key {
        return current
    }

    private fun next(relation: Relation): Key {
        return current.next(relation)
    }

    private data class Key(
        val counter: Int,
        val relation: Relation,
    ) {

        fun next(relation: Relation): Key {
            return Key(counter + ONE_I, relation)
        }

        companion object {

            fun first(relation: Relation): Key {
                return Key(ZERO_I, relation)
            }

        }

    }

}