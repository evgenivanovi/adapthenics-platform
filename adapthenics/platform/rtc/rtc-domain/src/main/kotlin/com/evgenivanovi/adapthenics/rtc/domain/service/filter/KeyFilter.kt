package com.evgenivanovi.adapthenics.rtc.domain.service.filter

import com.evgenivanovi.kt.util.Relation

fun interface KeyFilter {

    fun matches(key: String): Boolean

    companion object {

        internal val ALL: KeyFilter = KeyFilter { true }

        internal val NONE: KeyFilter = KeyFilter { false }

        fun configure(relation: Relation = Relation.AND): KeyFilterConfigurer {
            return KeyFilterConfigurer(relation)
        }

    }

}