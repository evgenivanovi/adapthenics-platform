package com.evgenivanovi.adapthenics.rtc.domain.service

import com.evgenivanovi.adapthenics.rtc.domain.model.Property
import com.evgenivanovi.adapthenics.rtc.domain.service.filter.KeyFilter
import com.google.common.collect.Maps
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.*

class EventCourier(
    dispatcher: CoroutineDispatcher,
) {

    private val log: KLogger = KotlinLogging
        .logger {}

    private val listeners: MutableMap<EventListener.ID, EventListenerDefinition> = Maps
        .newConcurrentMap()

    private val scope: CoroutineScope = CoroutineScope(
        dispatcher.plus(SupervisorJob())
    )

    fun register(listener: EventListener, filter: KeyFilter): EventListenerDefinition {
        val definition = EventListenerDefinition(
            EventListener.ID(), filter, listener
        )
        return listeners.computeIfAbsent(definition.id()) { definition }
    }

    fun unregister(id: EventListener.ID): EventListenerDefinition? {
        return listeners.remove(id)
    }

    suspend fun deliver(event: Property) = coroutineScope {
        listeners.values
            .filter { listener -> interested(listener.filter(), event) }
            .forEach { listener -> scope.launch { listener.listen(event) } }
    }

    private fun interested(filter: KeyFilter, event: Property): Boolean {
        if (filter.matches(event.key())) {
            log.debug {
                "Event $event is interested by key ${event.key()}"
            }
            return true
        }

        log.debug {
            "Event $event is not interested by key ${event.key()}"
        }
        return false
    }

}