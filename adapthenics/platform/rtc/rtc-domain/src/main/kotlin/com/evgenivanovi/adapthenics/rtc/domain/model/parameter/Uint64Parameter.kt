package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.math.BigInteger
import java.util.*

class Uint64Parameter
private constructor(
    usage: String,
    group: String,
    value: BigInteger?,
    possibleValues: Optional<List<BigInteger>>,
) : AbstractParameter<BigInteger>(
    usage = usage,
    group = group,
    type = ParameterType.UINT64,
    value = value,
    possibleValues = possibleValues
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: BigInteger?,
            possibleValues: List<BigInteger> = emptyList(),
        ): Uint64Parameter {

            val optionalPossibleValues = Optional
                .of(possibleValues)
                .filter(List<BigInteger>::isNotEmpty)

            return Uint64Parameter(
                usage = usage,
                group = group,
                value = value,
                possibleValues = optionalPossibleValues
            )

        }

    }

}