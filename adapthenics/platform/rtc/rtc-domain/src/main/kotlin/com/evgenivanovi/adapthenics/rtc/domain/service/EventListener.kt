package com.evgenivanovi.adapthenics.rtc.domain.service

import com.evgenivanovi.adapthenics.rtc.domain.model.Property
import java.util.concurrent.atomic.AtomicLong

fun interface EventListener {

    fun listen(event: Property)

    class ID
    internal constructor() {

        private val key: Long = Sequencer.next()

        fun get(): Long {
            return key
        }

        override fun hashCode(): Int {
            return key.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            return key == other
        }

        companion object {

            internal object Sequencer {

                private val sequence: AtomicLong = AtomicLong()

                fun next(): Long {
                    return sequence.getAndIncrement()
                }

            }

        }

    }

}