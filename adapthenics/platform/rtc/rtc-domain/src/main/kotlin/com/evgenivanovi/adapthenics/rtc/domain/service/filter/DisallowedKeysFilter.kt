package com.evgenivanovi.adapthenics.rtc.domain.service.filter

internal class DisallowedKeysFilter : KeyFilter {

    private val keys: Set<String>

    constructor(keys: Set<String>) {
        this.keys = keys
    }

    override fun matches(key: String): Boolean {
        return keys.contains(key).not()
    }

    companion object {

        fun of(key: String): DisallowedKeysFilter {
            return DisallowedKeysFilter(setOf(key))
        }

        fun of(keys: Set<String>): DisallowedKeysFilter {
            return DisallowedKeysFilter(keys)
        }

    }

}