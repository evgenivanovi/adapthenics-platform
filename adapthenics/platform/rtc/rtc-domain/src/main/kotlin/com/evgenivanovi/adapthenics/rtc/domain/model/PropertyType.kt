package com.evgenivanovi.adapthenics.rtc.domain.model

import com.evgenivanovi.kt.any.EnumResolver

enum class PropertyType {

    CREATED,

    UPDATED,

    DELETED,

    UNKNOWN;

    companion object : EnumResolver<PropertyType>(PropertyType::class.java)

}