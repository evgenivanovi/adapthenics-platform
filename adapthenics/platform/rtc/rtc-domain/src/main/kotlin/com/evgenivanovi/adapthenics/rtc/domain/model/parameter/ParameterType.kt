package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import com.evgenivanovi.kt.any.EnumResolver

enum class ParameterType {

    EMPTY,

    BOOLEAN,

    STRING,

    JSON,

    INT,

    INT64,

    UINT64,

    FLOAT64,

    DURATION;

    companion object : EnumResolver<ParameterType>(ParameterType::class.java)

}