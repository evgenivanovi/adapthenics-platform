package com.evgenivanovi.adapthenics.rtc.domain.model

import com.evgenivanovi.kt.any.ContractableCompanion
import kotlin.reflect.KProperty1

class Property
private constructor(
    val msg: PropertyMessage,
    val type: PropertyType,
) {

    fun key(): String {
        return msg.key
    }

    fun value(): PropertyValue? {
        return msg.value
    }

    fun withType(type: PropertyType): Property {
        return copy(that = this, type = type)
    }

    companion object : ContractableCompanion<Property>() {

        override val properties: Array<KProperty1<Property, *>> = arrayOf(
            Property::msg,
            Property::type,
        )

        private fun copy(
            that: Property,
            msg: PropertyMessage? = null,
            type: PropertyType? = null,
        ): Property {
            return Property(
                msg = msg ?: that.msg,
                type = type ?: that.type
            )
        }

        fun of(
            key: PropertyKey,
            value: PropertyValue?,
            type: PropertyType,
        ): Property {
            val msg = PropertyMessage.of(
                key = key,
                value = value,
            )
            return Property(
                msg = msg,
                type = type
            )
        }

        fun of(
            msg: PropertyMessage,
            type: PropertyType,
        ): Property {
            return Property(
                msg = msg,
                type = type
            )
        }

        fun created(
            key: PropertyKey,
            value: PropertyValue?,
        ): Property {
            return of(
                key = key,
                value = value,
                type = PropertyType.CREATED
            )
        }

        fun updated(
            key: PropertyKey,
            value: PropertyValue?,
        ): Property {
            return of(
                key = key,
                value = value,
                type = PropertyType.UPDATED
            )
        }

        fun deleted(
            key: PropertyKey,
        ): Property {
            return of(
                key = key,
                value = null,
                type = PropertyType.DELETED
            )
        }

        fun unknown(
            key: PropertyKey,
        ): Property {
            return of(
                key = key,
                value = null,
                type = PropertyType.UNKNOWN
            )
        }

    }

}