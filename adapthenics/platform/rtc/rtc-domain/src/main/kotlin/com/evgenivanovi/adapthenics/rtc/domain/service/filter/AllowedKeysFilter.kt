package com.evgenivanovi.adapthenics.rtc.domain.service.filter

internal class AllowedKeysFilter : KeyFilter {

    private val keys: Set<String>

    constructor(keys: Set<String>) {
        this.keys = keys
    }

    override fun matches(key: String): Boolean {
        return keys.contains(key)
    }

    companion object {

        fun of(key: String): AllowedKeysFilter {
            return AllowedKeysFilter(setOf(key))
        }

        fun of(keys: Set<String>): AllowedKeysFilter {
            return AllowedKeysFilter(keys)
        }

    }

}