package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

class JsonParameter
private constructor(
    usage: String,
    group: String,
    value: String?,
) : AbstractParameter<String>(
    usage = usage,
    group = group,
    type = ParameterType.JSON,
    value = value,
    possibleValues = Optional.empty()
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: String?,
        ): JsonParameter {
            return JsonParameter(
                usage = usage,
                group = group,
                value = value,
            )
        }

    }

}