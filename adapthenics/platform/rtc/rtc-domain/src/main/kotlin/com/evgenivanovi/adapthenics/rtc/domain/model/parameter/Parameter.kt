package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.math.BigInteger
import java.util.*
import kotlin.time.Duration

sealed interface Parameter<T : Any> {

    fun usage(): String

    fun group(): String

    fun type(): ParameterType

    fun value(): T?

    fun possibleValues(): Optional<List<T>>

    companion object {

        fun empty(): EmptyParameter = EmptyParameter.INSTANCE

        fun boolean(
            usage: String,
            group: String,
            value: Boolean?,
        ): BooleanParameter {
            return BooleanParameter.of(
                usage = usage,
                group = group,
                value = value,
            )
        }

        fun string(
            usage: String,
            group: String,
            value: String?,
            possibleValues: List<String>,
        ): StringParameter {
            return StringParameter.of(
                usage = usage,
                group = group,
                value = value,
                possibleValues = possibleValues
            )
        }

        fun json(
            usage: String,
            group: String,
            value: String?,
        ): JsonParameter {
            return JsonParameter.of(
                usage = usage,
                group = group,
                value = value,
            )
        }

        fun int(
            usage: String,
            group: String,
            value: Int?,
            possibleValues: List<Int>,
        ): IntParameter {
            return IntParameter.of(
                usage = usage,
                group = group,
                value = value,
                possibleValues = possibleValues
            )
        }

        fun int64(
            usage: String,
            group: String,
            value: Long?,
            possibleValues: List<Long>,
        ): Int64Parameter {
            return Int64Parameter.of(
                usage = usage,
                group = group,
                value = value,
                possibleValues = possibleValues
            )
        }

        fun uint64(
            usage: String,
            group: String,
            value: BigInteger?,
            possibleValues: List<BigInteger>,
        ): Uint64Parameter {
            return Uint64Parameter.of(
                usage = usage,
                group = group,
                value = value,
                possibleValues = possibleValues
            )
        }

        fun float64(
            usage: String,
            group: String,
            value: Double?,
            possibleValues: List<Double>,
        ): Float64Parameter {
            return Float64Parameter.of(
                usage = usage,
                group = group,
                value = value,
                possibleValues = possibleValues
            )
        }

        fun duration(
            usage: String,
            group: String,
            value: Duration?,
        ): DurationParameter {
            return DurationParameter.of(
                usage = usage,
                group = group,
                value = value,
            )
        }

    }

}

