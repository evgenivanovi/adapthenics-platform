package com.evgenivanovi.adapthenics.rtc.domain.service

import com.evgenivanovi.adapthenics.rtc.domain.model.PropertyMessage

interface EventWriteRepository {

    suspend fun write(message: PropertyMessage): PropertyMessage

}