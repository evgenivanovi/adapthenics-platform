package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

class Float64Parameter
private constructor(
    usage: String,
    group: String,
    value: Double?,
    possibleValues: Optional<List<Double>>,
) : AbstractParameter<Double>(
    usage = usage,
    group = group,
    type = ParameterType.FLOAT64,
    value = value,
    possibleValues = possibleValues
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: Double?,
            possibleValues: List<Double> = emptyList(),
        ): Float64Parameter {

            val optionalPossibleValues = Optional
                .of(possibleValues)
                .filter(List<Double>::isNotEmpty)

            return Float64Parameter(
                usage = usage,
                group = group,
                value = value,
                possibleValues = optionalPossibleValues
            )

        }

    }

}