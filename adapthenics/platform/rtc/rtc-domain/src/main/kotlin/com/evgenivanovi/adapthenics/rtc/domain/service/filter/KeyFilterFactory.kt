package com.evgenivanovi.adapthenics.rtc.domain.service.filter

import java.util.regex.Pattern

object KeyFilterFactory {

    fun all(): KeyFilter {
        return KeyFilter.ALL
    }

    fun none(): KeyFilter {
        return KeyFilter.NONE
    }

    fun regex(regex: String): KeyFilter {
        return RegexKeyFilter.of(regex)
    }

    fun regex(regex: Pattern): KeyFilter {
        return RegexKeyFilter.of(regex)
    }

    fun regex(regex: Regex): KeyFilter {
        return RegexKeyFilter.of(regex)
    }

    fun allowed(key: String): KeyFilter {
        return AllowedKeysFilter.of(key)
    }

    fun allowed(keys: Set<String>): KeyFilter {
        return AllowedKeysFilter.of(keys)
    }

    fun disallowed(key: String): KeyFilter {
        return DisallowedKeysFilter.of(key)
    }

    fun disallowed(keys: Set<String>): KeyFilter {
        return DisallowedKeysFilter.of(keys)
    }

}