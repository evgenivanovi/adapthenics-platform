package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*
import kotlin.time.Duration

class DurationParameter
private constructor(
    usage: String,
    group: String,
    value: Duration?,
) : AbstractParameter<Duration>(
    usage = usage,
    group = group,
    type = ParameterType.DURATION,
    value = value,
    possibleValues = Optional.empty()
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: Duration?,
        ): DurationParameter {
            return DurationParameter(
                usage = usage,
                group = group,
                value = value,
            )
        }

    }

}