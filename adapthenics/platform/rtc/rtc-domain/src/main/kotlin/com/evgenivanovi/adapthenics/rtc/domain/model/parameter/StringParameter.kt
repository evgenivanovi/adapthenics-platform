package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

class StringParameter
private constructor(
    usage: String,
    group: String,
    value: String?,
    possibleValues: Optional<List<String>>,
) : AbstractParameter<String>(
    usage = usage,
    group = group,
    type = ParameterType.STRING,
    value = value,
    possibleValues = possibleValues
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: String?,
            possibleValues: List<String> = emptyList(),
        ): StringParameter {

            val optionalPossibleValues = Optional
                .of(possibleValues)
                .filter(List<String>::isNotEmpty)

            return StringParameter(
                usage = usage,
                group = group,
                value = value,
                possibleValues = optionalPossibleValues
            )

        }

    }

}