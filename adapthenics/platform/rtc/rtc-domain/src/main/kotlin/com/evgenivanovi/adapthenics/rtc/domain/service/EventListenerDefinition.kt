package com.evgenivanovi.adapthenics.rtc.domain.service

import com.evgenivanovi.adapthenics.rtc.domain.model.Property
import com.evgenivanovi.adapthenics.rtc.domain.service.filter.KeyFilter

class EventListenerDefinition : EventListener {

    private val _id: EventListener.ID

    private val _filter: KeyFilter

    private val _listener: EventListener

    internal constructor(
        id: EventListener.ID,
        filter: KeyFilter,
        handler: EventListener,
    ) {
        this._id = id
        this._filter = filter
        this._listener = handler
    }

    fun id(): EventListener.ID {
        return _id
    }

    fun filter(): KeyFilter {
        return _filter
    }

    override fun listen(event: Property) {
        _listener.listen(event)
    }

}