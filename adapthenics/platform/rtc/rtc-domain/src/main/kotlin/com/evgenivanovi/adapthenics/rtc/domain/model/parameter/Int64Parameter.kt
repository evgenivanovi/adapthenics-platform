package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

class Int64Parameter
private constructor(
    usage: String,
    group: String,
    value: Long?,
    possibleValues: Optional<List<Long>>,
) : AbstractParameter<Long>(
    usage = usage,
    group = group,
    type = ParameterType.INT64,
    value = value,
    possibleValues = possibleValues
) {

    companion object {

        fun of(
            usage: String,
            group: String,
            value: Long?,
            possibleValues: List<Long> = emptyList(),
        ): Int64Parameter {

            val optionalPossibleValues = Optional
                .of(possibleValues)
                .filter(List<Long>::isNotEmpty)

            return Int64Parameter(
                usage = usage,
                group = group,
                value = value,
                possibleValues = optionalPossibleValues
            )
            
        }

    }

}