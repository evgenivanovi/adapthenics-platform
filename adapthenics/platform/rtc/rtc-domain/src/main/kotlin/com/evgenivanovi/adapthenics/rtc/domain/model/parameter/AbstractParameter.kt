package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

open class AbstractParameter<T : Any>
protected constructor(
    protected val usage: String,
    protected val group: String,
    protected val type: ParameterType,
    protected val value: T?,
    protected val possibleValues: Optional<List<T>>,
) : Parameter<T> {

    override fun usage(): String = usage

    override fun group(): String = group

    override fun type(): ParameterType = type

    override fun value(): T? = value

    override fun possibleValues(): Optional<List<T>> = possibleValues

}


