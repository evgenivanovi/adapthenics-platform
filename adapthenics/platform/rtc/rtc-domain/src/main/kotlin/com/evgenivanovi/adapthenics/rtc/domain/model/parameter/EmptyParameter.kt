package com.evgenivanovi.adapthenics.rtc.domain.model.parameter

import java.util.*

class EmptyParameter
private constructor() : Parameter<Any> {

    override fun usage(): String {
        throw UnsupportedOperationException()
    }

    override fun group(): String {
        throw UnsupportedOperationException()
    }

    override fun type(): ParameterType {
        throw UnsupportedOperationException()
    }

    override fun value(): Any? {
        throw UnsupportedOperationException()
    }

    override fun possibleValues(): Optional<List<Any>> {
        throw UnsupportedOperationException()
    }

    companion object {

        val INSTANCE = EmptyParameter()

    }

}