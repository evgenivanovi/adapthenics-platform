package com.evgenivanovi.adapthenics.support.jackson.ser

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.datatype.jsr310.ser.InstantSerializer
import kotlinx.datetime.Instant
import kotlinx.datetime.toJavaInstant

class KotlinInstantSerializer : StdSerializer<Instant>(Instant::class.java) {

    override fun serialize(
        value: Instant,
        gen: JsonGenerator,
        ser: SerializerProvider
    ) {
        return InstantSerializer.INSTANCE
            .serialize(value.toJavaInstant(), gen, ser)
    }

}