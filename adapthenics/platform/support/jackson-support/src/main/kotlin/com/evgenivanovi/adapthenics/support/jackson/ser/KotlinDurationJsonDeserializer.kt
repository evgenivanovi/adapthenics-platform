package com.evgenivanovi.adapthenics.support.jackson.ser

import com.evgenivanovi.kt.time.DurationParser
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.datatype.jsr310.deser.DurationDeserializer
import java.io.IOException
import java.time.Duration
import java.util.*
import kotlin.time.toKotlinDuration

class KotlinDurationJsonDeserializer(
    private val fallback: DurationDeserializer = DurationDeserializer.INSTANCE,
) : JsonDeserializer<kotlin.time.Duration>() {

    @Throws(IOException::class)
    override fun deserialize(
        parser: JsonParser,
        ctx: DeserializationContext
    ): kotlin.time.Duration? {

        if (parser.currentToken() != JsonToken.VALUE_STRING) {
            return fallback(parser, ctx)
        }

        return DurationParser
            .parse(parser.text)
            ?.toKotlinDuration()
            ?: fallback(parser, ctx)

    }

    private fun fallback(
        parser: JsonParser,
        ctx: DeserializationContext
    ): kotlin.time.Duration? {
        return fallback
            .deserialize(parser, ctx)
            ?.let(Duration::toKotlinDuration)
    }

}