package com.evgenivanovi.adapthenics.support.jackson.ser

import com.fasterxml.jackson.databind.module.SimpleModule

class DurationModule : SimpleModule() {

    init {
        mixins()
        ser()
    }

    private fun mixins() {
        //
    }

    private fun ser() {
        addDeserializer(java.time.Duration::class.java, JavaDurationJsonDeserializer())
        addDeserializer(kotlin.time.Duration::class.java, KotlinDurationJsonDeserializer())
    }

}