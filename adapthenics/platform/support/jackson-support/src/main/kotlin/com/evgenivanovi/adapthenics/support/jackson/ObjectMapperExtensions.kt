package com.evgenivanovi.adapthenics.support.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper

fun json(block: JsonMapper.Builder.() -> Unit = {}): ObjectMapper {
    val builder = JsonMapper.builder()
    builder.block()
    return builder.build()
}

fun yaml(block: YAMLMapper.Builder.() -> Unit = {}): YAMLMapper {
    val builder = YAMLMapper.builder()
    builder.block()
    return builder.build()
}

fun ObjectMapper.addMixins(mixins: Map<Class<*>, Class<*>>): ObjectMapper {
    mixins.forEach { addMixIn(it.key, it.value) }
    return this
}
