package com.evgenivanovi.adapthenics.support.jackson.ser

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer
import kotlinx.datetime.Instant
import kotlinx.datetime.toKotlinInstant

class KotlinInstantDeserializer : JsonDeserializer<Instant>() {

    override fun deserialize(
        parser: JsonParser, ctx: DeserializationContext
    ): Instant? {
        return InstantDeserializer.INSTANT
            .deserialize(parser, ctx)
            ?.toKotlinInstant()
    }

}