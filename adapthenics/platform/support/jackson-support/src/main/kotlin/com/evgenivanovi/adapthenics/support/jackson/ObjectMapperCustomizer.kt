package com.evgenivanovi.adapthenics.support.jackson

import com.fasterxml.jackson.databind.ObjectMapper

fun interface ObjectMapperCustomizer<T : ObjectMapper> {

    fun customize(mapper: T)

}