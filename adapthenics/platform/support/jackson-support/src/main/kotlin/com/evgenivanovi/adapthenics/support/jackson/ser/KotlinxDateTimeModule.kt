package com.evgenivanovi.adapthenics.support.jackson.ser

import com.fasterxml.jackson.databind.module.SimpleModule
import kotlinx.datetime.Instant

class KotlinxDateTimeModule : SimpleModule() {

    init {
        _mixins = mixins().toMap(HashMap<Class<*>, Class<*>>())
        ser()
    }

    private fun mixins(): Map<Class<*>, Class<*>> {
        val mixins = mutableListOf<Pair<Class<*>, Class<*>>>()
        return mapOf(*mixins.toTypedArray())
    }

    private fun ser() {
        addSerializer(Instant::class.java, KotlinInstantSerializer())
        addDeserializer(Instant::class.java, KotlinInstantDeserializer())
    }

}