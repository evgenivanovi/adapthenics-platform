package com.evgenivanovi.adapthenics.support.logging.mdc

fun Pair<String, String?>.toMDCEntry(): MDCEntry {
    return MDCEntry.from(this)
}

fun Pair<String, () -> String?>.toMDCEntrySupplied(): MDCEntry {
    return MDCEntry.fromSupplied(this)
}

fun Map.Entry<String, String?>.toMDCEntry(): MDCEntry {
    return MDCEntry.from(this.key, this.value)
}

fun Map.Entry<String, () -> String?>.toMDCEntrySupplied(): MDCEntry {
    return MDCEntry.fromSupplied(this.key, this.value)
}

fun Map<String, String?>.toMDCEntries(): MDCEntries {
    return map(Map.Entry<String, String?>::toMDCEntry)
        .toTypedArray()
        .let(MDCEntries.Companion::from)
}

fun Map<String, () -> String?>.toMDCEntriesSupplied(): MDCEntries {
    return map(Map.Entry<String, () -> String?>::toMDCEntrySupplied)
        .toTypedArray()
        .let(MDCEntries.Companion::from)
}
