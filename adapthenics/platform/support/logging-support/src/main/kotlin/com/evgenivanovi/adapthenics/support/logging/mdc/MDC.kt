package com.evgenivanovi.adapthenics.support.logging.mdc

import org.slf4j.MDC

fun MDCEntries.cleanup() {
    entries.forEach {
        MDC.remove(it.name)
    }
}

fun Collection<MDCEntry>.cleanup() {
    forEach {
        MDC.remove(it.name)
    }
}