package com.evgenivanovi.adapthenics.support.logging.mdc

class MDCEntry
internal constructor(
    val name: String,
    val provider: () -> String?,
) {

    companion object {

        fun from(key: String, value: String?): MDCEntry {
            return MDCEntry(key) { value }
        }

        fun fromSupplied(key: String, value: () -> String?): MDCEntry {
            return MDCEntry(key, value)
        }

        fun from(entry: Pair<String, String?>): MDCEntry {
            return MDCEntry(entry.first) { entry.second }
        }

        fun fromSupplied(entry: Pair<String, () -> String?>): MDCEntry {
            return MDCEntry(entry.first, entry.second)
        }

    }

}