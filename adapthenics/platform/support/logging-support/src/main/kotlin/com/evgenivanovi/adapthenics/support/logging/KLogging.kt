package com.evgenivanovi.adapthenics.support.logging

import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KMarkerFactory
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.slf4j.MDCContext
import org.slf4j.Marker

/**
 * Documentation:
 * https://github.com/MicroUtils/kotlin-logging/wiki#mapped-diagnostic-context-mdc-support
 *
 * Issue:
 * https://github.com/oshai/kotlin-logging/issues/165
 */

typealias LoggingContext = MutableMap<String, String?>

/**
 * Lazy add a log message if isTraceEnabled is true
 */
suspend fun KLogger.asyncTrace(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                trace(msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isTraceEnabled is true
 */
fun KLogger.syncTrace(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            trace(msg)
        }
    }
}

/**
 * Lazy add a log message if isDebugEnabled is true
 */
suspend fun KLogger.asyncDebug(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                debug(msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isDebugEnabled is true
 */
fun KLogger.syncDebug(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            debug(msg)
        }
    }
}

/**
 * Lazy add a log message if isInfoEnabled is true
 */
suspend fun KLogger.asyncInfo(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                info(msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isInfoEnabled is true
 */
fun KLogger.syncInfo(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            info(msg)
        }
    }
}

/**
 * Lazy add a log message if isWarnEnabled is true
 */
suspend fun KLogger.asyncWarn(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                warn(msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isWarnEnabled is true
 */
fun KLogger.syncWarn(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            warn(msg)
        }
    }
}

/**
 * Lazy add a log message if isErrorEnabled is true
 */
suspend fun KLogger.asyncError(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                error(msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isErrorEnabled is true
 */
fun KLogger.syncError(
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            error(msg)
        }
    }
}

/* __________________________________________________ */

/**
 * Lazy add a log message with throwable payload if isTraceEnabled is true
 */
suspend fun KLogger.asyncTrace(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                trace(t, msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isTraceEnabled is true
 */
fun KLogger.syncTrace(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            trace(t, msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isDebugEnabled is true
 */
suspend fun KLogger.asyncDebug(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                debug(t, msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isDebugEnabled is true
 */
fun KLogger.syncDebug(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            debug(t, msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isInfoEnabled is true
 */
suspend fun KLogger.asyncInfo(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                info(t, msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isInfoEnabled is true
 */
fun KLogger.syncInfo(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            info(t, msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isWarnEnabled is true
 */
suspend fun KLogger.asyncWarn(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                warn(t, msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isWarnEnabled is true
 */
fun KLogger.syncWarn(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            warn(t, msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isErrorEnabled is true
 */
suspend fun KLogger.asyncError(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                error(t, msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isErrorEnabled is true
 */
fun KLogger.syncError(
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            error(t, msg)
        }
    }
}

/* __________________________________________________ */

/**
 * Lazy add a log message if isTraceEnabled is true
 */
suspend fun KLogger.asyncTrace(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                trace(null as Throwable?, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isTraceEnabled is true
 */
fun KLogger.syncTrace(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            trace(null as Throwable?, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message if isDebugEnabled is true
 */
suspend fun KLogger.asyncDebug(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                debug(null as Throwable?, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isDebugEnabled is true
 */
fun KLogger.syncDebug(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            debug(null as Throwable?, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message if isInfoEnabled is true
 */
suspend fun KLogger.asyncInfo(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                info(null as Throwable?, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isInfoEnabled is true
 */
fun KLogger.syncInfo(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            info(null as Throwable?, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message if isWarnEnabled is true
 */
suspend fun KLogger.asyncWarn(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                warn(null as Throwable?, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isWarnEnabled is true
 */
fun KLogger.syncWarn(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            warn(null as Throwable?, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message if isErrorEnabled is true
 */
suspend fun KLogger.asyncError(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                error(null as Throwable?, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message if isErrorEnabled is true
 */
fun KLogger.syncError(
    marker: Marker?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            error(null as Throwable?, mark(marker), msg)
        }
    }
}

/* __________________________________________________ */

/**
 * Lazy add a log message with throwable payload if isTraceEnabled is true
 */
suspend fun KLogger.asyncTrace(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                trace(t, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isTraceEnabled is true
 */
fun KLogger.syncTrace(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            trace(t, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isDebugEnabled is true
 */
suspend fun KLogger.asyncDebug(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                debug(t, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isDebugEnabled is true
 */
fun KLogger.syncDebug(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            debug(t, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isInfoEnabled is true
 */
suspend fun KLogger.asyncInfo(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                info(t, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isInfoEnabled is true
 */
fun KLogger.syncInfo(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            info(t, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isWarnEnabled is true
 */
suspend fun KLogger.asyncWarn(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                warn(t, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isWarnEnabled is true
 */
fun KLogger.syncWarn(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            warn(t, mark(marker), msg)
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isErrorEnabled is true
 */
suspend fun KLogger.asyncError(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) = coroutineScope {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                error(t, mark(marker), msg)
            }
        }
    }
}

/**
 * Lazy add a log message with throwable payload if isErrorEnabled is true
 */
fun KLogger.syncError(
    marker: Marker?,
    t: Throwable?,
    ctx: (LoggingContext) -> Unit = { },
    msg: () -> Any?,
) {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            error(t, mark(marker), msg)
        }
    }
}

/* __________________________________________________ */

suspend fun KLogger.asyncTrace(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                error { "$format $args" }
            }
        }
    }
}

fun KLogger.syncTrace(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            error { "$format $args" }
        }
    }
}

suspend fun KLogger.asyncDebug(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) = coroutineScope {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                debug { "$format $args" }
            }
        }
    }
}

fun KLogger.syncDebug(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) {
    if (isDebugEnabled()) {
        withLoggingContext(ctx) {
            debug { "$format $args" }
        }
    }
}

suspend fun KLogger.asyncInfo(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) = coroutineScope {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                info { "$format $args" }
            }
        }
    }
}

fun KLogger.syncInfo(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) {
    if (isInfoEnabled()) {
        withLoggingContext(ctx) {
            info { "$format $args" }
        }
    }
}

suspend fun KLogger.asyncWarn(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) = coroutineScope {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                warn { "$format $args" }
            }
        }
    }
}

fun KLogger.syncWarn(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) {
    if (isWarnEnabled()) {
        withLoggingContext(ctx) {
            warn { "$format $args" }
        }
    }
}

suspend fun KLogger.asyncError(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) = coroutineScope {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                error { "$format $args" }
            }
        }
    }
}

fun KLogger.syncError(
    ctx: (LoggingContext) -> Unit = { },
    format: String,
    vararg args: Any?,
) {
    if (isErrorEnabled()) {
        withLoggingContext(ctx) {
            error { "$format $args" }
        }
    }
}

/* __________________________________________________ */

/**
 * Add a log message with all the supplied parameters along with method name
 */
fun KLogger.entry(
    vararg argArray: Any?,
    ctx: (LoggingContext) -> Unit = { },
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            entry(argArray)
        }
    }
}

/**
 * Add log message indicating exit of a method
 */
suspend fun KLogger.exit(
    ctx: (LoggingContext) -> Unit = { },
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                exit()
            }
        }
    }
}

/**
 * Add log message indicating exit of a method
 */
fun KLogger.syncExit(
    ctx: (LoggingContext) -> Unit = { },
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            exit()
        }
    }
}

/**
 * Add a log message with the return value of a method
 */
suspend fun <T : Any?> KLogger.exit(
    result: T,
    ctx: (LoggingContext) -> Unit = { },
): T = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                exit(result)
            }
        }
    }
    return@coroutineScope result
}

/**
 * Add a log message with the return value of a method
 */
fun <T : Any?> KLogger.syncExit(
    result: T,
    ctx: (LoggingContext) -> Unit = { },
): T {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            exit(result)
        }
    }
    return result
}

/**
 * Add a log message indicating an exception will be thrown along with the stack trace.
 */
suspend fun <T : Throwable> KLogger.throwing(
    throwable: T,
    ctx: (LoggingContext) -> Unit = { },
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                throwing(throwable)
            }
        }
    }
}

/**
 * Add a log message indicating an exception will be thrown along with the stack trace.
 */
fun <T : Throwable> KLogger.syncThrowing(
    throwable: T,
    ctx: (LoggingContext) -> Unit = { },
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            throwing(throwable)
        }
    }
}

/**
 * Add a log message indicating an exception is caught along with the stack trace.
 */
suspend fun <T : Throwable> KLogger.catching(
    throwable: T,
    ctx: (LoggingContext) -> Unit = { },
) = coroutineScope {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            launch(MDCContext()) {
                catching(throwable)
            }
        }
    }
}

/**
 * Add a log message indicating an exception is caught along with the stack trace.
 */
fun <T : Throwable> KLogger.syncCatching(
    throwable: T,
    ctx: (LoggingContext) -> Unit = { },
) {
    if (isTraceEnabled()) {
        withLoggingContext(ctx) {
            catching(throwable)
        }
    }
}

inline fun <T> withLoggingContext(
    ctx: (LoggingContext) -> Unit = { },
    restore: Boolean = true,
    body: () -> T,
): T {
    return io.github.oshai.kotlinlogging.withLoggingContext(
        mutableMapOf<String, String?>().apply { ctx(this) },
        restore,
        body
    )
}

private fun mark(marker: Marker?): io.github.oshai.kotlinlogging.Marker? {
    return if (marker == null) null
    else KMarkerFactory.getMarker(marker.name)
}
