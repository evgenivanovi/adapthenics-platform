package com.evgenivanovi.adapthenics.support.logging.mdc

class MDCEntries
internal constructor(
    val entries: MutableCollection<MDCEntry>,
) {

    fun with(key: String, value: String?): MDCEntries {
        return MDCEntry
            .from(key, value)
            .let(this::with)
    }

    fun with(entry: Pair<String, String?>): MDCEntries {
        return MDCEntry
            .from(entry)
            .let(this::with)
    }

    fun with(entries: Map<String, () -> String?>): MDCEntries {
        return entries
            .toList()
            .map(MDCEntry::fromSupplied)
            .let(this::with)
    }

    fun with(entry: MDCEntry): MDCEntries {
        this.entries.add(entry)
        return this
    }

    fun with(entries: Collection<MDCEntry>): MDCEntries {
        this.entries.addAll(entries)
        return this
    }

    fun newMap(): Map<String, String> {
        val result = HashMap<String, String>()

        entries.forEach { entry ->
            runCatching { entry.provider() }
                .getOrNull()
                ?.let { mdc: String -> result[entry.name] = mdc }
        }

        return result
    }

    companion object {

        fun create(): MDCEntries {
            return MDCEntries(mutableListOf())
        }

        fun from(vararg entries: MDCEntry): MDCEntries {
            return MDCEntries(entries.toMutableList())
        }

    }

}