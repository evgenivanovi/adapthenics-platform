package com.evgenivanovi.adapthenics.etcd

import com.evgenivanovi.adapthenics.etcd.client.SecurityUtils
import com.evgenivanovi.adapthenics.kt.test.ResourceTool
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.security.cert.CertificateException

class SecurityUtilsTest {

    @Test
    fun shouldParseSec1() {

        // given
        val key = "security/test.sec1.key"
        val resource = ResourceTool.getContentEx(key)

        // when
        val action = { SecurityUtils.parsePrivateKey(resource) }

        // then
        Assertions.assertDoesNotThrow { action() }

    }

    @Test
    fun shouldParsePkcs8() {

        // given
        val key = "security/test.pkcs8.key"
        val resource = ResourceTool.getContentEx(key)

        // when
        val action = { SecurityUtils.parsePrivateKey(resource) }

        // then
        Assertions.assertDoesNotThrow { action() }

    }

    @Test
    fun shouldParseCertificate() {

        // given
        val key = "security/test.crt"
        val resource = ResourceTool.getContentEx(key)

        // when
        val action = { SecurityUtils.parseCertificate(resource) }

        // then
        Assertions.assertDoesNotThrow { action() }

    }

    @Test
    fun shouldThrowCertificateExceptionOnUnknownKey() {

        // given
        val key = "security/test.crt"
        val resource = ResourceTool.getContentEx(key)

        // when
        val action = { SecurityUtils.parsePrivateKey(resource) }

        // then
        Assertions.assertThrows(CertificateException::class.java) { action() }

    }

}