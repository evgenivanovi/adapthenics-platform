package com.evgenivanovi.adapthenics.etcd

import com.evgenivanovi.adapthenics.etcd.client.ClientParameters
import com.evgenivanovi.adapthenics.etcd.client.sync.PlatformClient
import io.etcd.jetcd.Client
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class PlatformClientTest {

    @MockK(relaxed = true)
    lateinit var mockedDelegate: Client

    @MockK(relaxed = true)
    lateinit var mockedParameters: ClientParameters

    lateinit var testee: PlatformClient

    @BeforeEach
    fun before() {
        testee = PlatformClient(mockedDelegate, mockedParameters)
    }

    @Test
    fun shouldCloseDelegate() {
        testee.close()
        verify(exactly = 1) { mockedDelegate.close() }
    }

    @Test
    fun shouldGetAuthClientFromDelegate() {
        verifyDelegateCalled { it.authClient }
    }

    @Test
    fun shouldGetKVClientFromDelegate() {
        verifyDelegateCalled { it.kvClient }
    }

    @Test
    fun shouldGetClusterClientFromDelegate() {
        verifyDelegateCalled { it.clusterClient }
    }

    @Test
    fun shouldGetMaintenanceClientFromDelegate() {
        verifyDelegateCalled { it.maintenanceClient }
    }

    @Test
    fun shouldGetLeaseClientFromDelegate() {
        verifyDelegateCalled { it.leaseClient }
    }

    @Test
    fun shouldGetWatchClientFromDelegate() {
        verifyDelegateCalled { it.watchClient }
    }

    @Test
    fun shouldGetLockClientFromDelegate() {
        verifyDelegateCalled { it.lockClient }
    }

    @Test
    fun shouldGetElectionClientFromDelegate() {
        verifyDelegateCalled { it.electionClient }
    }

    private fun verifyDelegateCalled(function: (Client) -> Any?) {
        function.invoke(testee)
        verify(exactly = 1) { function(mockedDelegate) }
    }

}