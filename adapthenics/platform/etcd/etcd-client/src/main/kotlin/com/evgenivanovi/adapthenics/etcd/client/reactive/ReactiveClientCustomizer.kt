package com.evgenivanovi.adapthenics.etcd.client.reactive

import io.etcd.jetcd.reactive.ReactiveClientBuilder

fun interface ReactiveClientCustomizer {

    fun customize(client: ReactiveClientBuilder)

}