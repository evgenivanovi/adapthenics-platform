package com.evgenivanovi.adapthenics.etcd.client

import com.evgenivanovi.kt.lang.asOf
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openssl.PEMKeyPair
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.StringReader
import java.nio.charset.StandardCharsets
import java.security.PrivateKey
import java.security.Security
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate

internal object SecurityUtils {

    private const val X509 = "X.509"

    /**
     * Java does not support private keys in PKCS#1 format by default.
     * We add BouncyCastle provider to security because ETCD private key is stored using PKCS#1 format in K8S.
     */
    init {
        Security.addProvider(BouncyCastleProvider())
    }

    @Throws(
        IOException::class,
        CertificateException::class,
    )
    fun parsePrivateKey(pk: String): PrivateKey {

        val pem: Any = StringReader(pk)
            .let(::PEMParser)
            .use(PEMParser::readObject)

        val converter: JcaPEMKeyConverter = JcaPEMKeyConverter()
            .setProvider(BouncyCastleProvider.PROVIDER_NAME)

        return when (pem) {
            is PEMKeyPair -> converter.getKeyPair(pem).private
            is PrivateKeyInfo -> converter.getPrivateKey(pem)
            else -> throw CertificateException(
                "Unknown type '${pem.javaClass}' of PEM object"
            )
        }

    }

    @Throws(
        IOException::class,
        CertificateException::class,
    )
    fun parseCertificate(cert: String): X509Certificate {
        return cert
            .toByteArray(StandardCharsets.UTF_8)
            .let(::ByteArrayInputStream)
            .use { CertificateFactory.getInstance(X509).generateCertificate(it).asOf() }
    }

}