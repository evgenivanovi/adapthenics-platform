package com.evgenivanovi.adapthenics.etcd.client

interface ClientParameters {

    /**
     * ETCD endpoints for client to be connected to.
     */
    val endpoints: Collection<String>

    /**
     * Project identity, can be used as ETCD-property prefix part.
     */
    val project: String

    /**
     * Project version identity, can be used as ETCD-property prefix part.
     */
    val version: String

    /**
     * Client PKCS #8 private key in PEM format, can be `null`.
     */
    val clientPrivateKeyPem: String?

    /**
     * Client X.509 certificate chain in PEM format, can be `null`.
     */
    val clientCertificatePem: String?

    /**
     * Trusted X.509 certificate collection in PEM format, can be `null`.
     */
    val trustedCertificatesPem: String?

}