package com.evgenivanovi.adapthenics.etcd.client.sync

import io.etcd.jetcd.Client

fun interface ClientFactory {

    fun create(): Client

}