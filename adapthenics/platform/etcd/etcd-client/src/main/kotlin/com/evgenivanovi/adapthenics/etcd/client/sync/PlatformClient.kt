package com.evgenivanovi.adapthenics.etcd.client.sync

import com.evgenivanovi.adapthenics.etcd.client.ClientParameters
import io.etcd.jetcd.*

class PlatformClient : Client {

    val delegate: Client

    private val parameters: ClientParameters

    constructor(
        delegate: Client,
        parameters: ClientParameters,
    ) {
        this.delegate = delegate
        this.parameters = parameters
    }

    override fun getAuthClient(): Auth {
        return delegate.authClient
    }

    override fun getKVClient(): KV {
        return delegate.kvClient
    }

    override fun getClusterClient(): Cluster {
        return delegate.clusterClient
    }

    override fun getMaintenanceClient(): Maintenance {
        return delegate.maintenanceClient
    }

    override fun getLeaseClient(): Lease {
        return delegate.leaseClient
    }

    override fun getWatchClient(): Watch {
        return delegate.watchClient
    }

    override fun getLockClient(): Lock {
        return delegate.lockClient
    }

    override fun getElectionClient(): Election {
        return delegate.electionClient
    }

    override fun close() {
        delegate.close()
    }

}