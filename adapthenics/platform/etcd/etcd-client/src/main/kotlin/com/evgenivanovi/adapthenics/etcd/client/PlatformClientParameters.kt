package com.evgenivanovi.adapthenics.etcd.client

data class PlatformClientParameters(
    override val endpoints: Collection<String>,

    override val project: String,
    override val version: String,

    override val clientPrivateKeyPem: String? = null,
    override val clientCertificatePem: String? = null,
    override val trustedCertificatesPem: String? = null,
) : ClientParameters