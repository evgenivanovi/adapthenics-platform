package com.evgenivanovi.adapthenics.etcd.client.reactive

import io.etcd.jetcd.reactive.ReactiveClient

fun interface ReactiveClientFactory {

    fun create(): ReactiveClient

}