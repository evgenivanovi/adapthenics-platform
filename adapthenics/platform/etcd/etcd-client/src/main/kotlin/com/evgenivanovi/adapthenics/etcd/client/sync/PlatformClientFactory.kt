package com.evgenivanovi.adapthenics.etcd.client.sync

import com.evgenivanovi.adapthenics.etcd.client.ClientParameters
import com.evgenivanovi.adapthenics.etcd.client.SecurityUtils.parseCertificate
import com.evgenivanovi.adapthenics.etcd.client.SecurityUtils.parsePrivateKey
import com.evgenivanovi.kt.coll.asArray
import io.etcd.jetcd.Client
import io.etcd.jetcd.ClientBuilder
import io.github.oshai.kotlinlogging.KotlinLogging
import io.netty.handler.ssl.SslContextBuilder
import java.io.ByteArrayInputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.security.cert.CertificateException
import java.util.function.Consumer

class PlatformClientFactory : ClientFactory {

    private val parameters: ClientParameters

    private val customizers: List<ClientCustomizer>

    private val logger = KotlinLogging.logger {}

    constructor(
        parameters: ClientParameters,
        customizers: List<ClientCustomizer> = emptyList(),
    ) {
        this.parameters = parameters
        this.customizers = customizers
    }

    override fun create(): Client {
        return try {
            doCreate()
        } catch (ex: Exception) {
            throw IllegalArgumentException("Failed to create ETCD client", ex)
        }
    }

    @Throws(
        IOException::class,
        CertificateException::class
    )
    private fun doCreate(): Client {

        logger.debug { "Creating ETCD client with parameters $parameters" }

        val client = Client
            .builder()
            .endpoints(*parameters.endpoints.asArray())
            .configureSsl()

        customizers.forEach { customizer ->
            customizer.customize(client)
        }

        return client.build()

    }

    private fun ClientBuilder.configureSsl(): ClientBuilder {

        val ssl = attachSsl()

        if (ssl !== NOOP_CUSTOMIZER) {
            logger.debug { "Attaching SSL to ETCD client" }
            return sslContext(ssl)
        }

        return this

    }

    private fun attachSsl(): Consumer<SslContextBuilder> {
        return NOOP_CUSTOMIZER
            .run { attachTrustedCertificate(this) }
            .run { attachClientCertificate(this) }
    }

    private fun attachTrustedCertificate(
        ssl: Consumer<SslContextBuilder>,
    ): Consumer<SslContextBuilder> {

        val cert = parameters.trustedCertificatesPem

        if (cert.isNullOrBlank()) return ssl

        logger.debug { "Attaching trusted certificates to ETCD client" }

        return cert
            .toByteArray(StandardCharsets.UTF_8)
            .let(::ByteArrayInputStream)
            .use { `in` -> ssl.andThen { it.trustManager(`in`) } }

    }

    private fun attachClientCertificate(
        ssl: Consumer<SslContextBuilder>,
    ): Consumer<SslContextBuilder> {

        val pk = parameters.clientPrivateKeyPem
        val cert = parameters.clientCertificatePem

        if (!pk.isNullOrBlank() && !cert.isNullOrBlank()) {
            logger.debug { "Attaching client certificate to ETCD client" }
            return ssl.andThen {
                it.keyManager(
                    parsePrivateKey(pk),
                    parseCertificate(cert)
                )
            }
        }

        return ssl

    }

    companion object {

        private val NOOP_CUSTOMIZER: Consumer<SslContextBuilder> =
            Consumer { _: SslContextBuilder -> }

    }

}