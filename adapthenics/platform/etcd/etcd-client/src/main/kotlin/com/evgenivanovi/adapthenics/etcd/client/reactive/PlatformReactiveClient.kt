package com.evgenivanovi.adapthenics.etcd.client.reactive

import com.evgenivanovi.adapthenics.etcd.client.ClientParameters
import io.etcd.jetcd.reactive.*

class PlatformReactiveClient : ReactiveClient {

    val delegate: ReactiveClient

    private val parameters: ClientParameters

    constructor(
        delegate: ReactiveClient,
        parameters: ClientParameters,
    ) {
        this.delegate = delegate
        this.parameters = parameters
    }

    override fun getAuthClient(): ReactiveAuth {
        return delegate.authClient
    }

    override fun getKVClient(): ReactiveKV {
        return delegate.kvClient
    }

    override fun getClusterClient(): ReactiveCluster {
        return delegate.clusterClient
    }

    override fun getMaintenanceClient(): ReactiveMaintenance {
        return delegate.maintenanceClient
    }

    override fun getLeaseClient(): ReactiveLease {
        return delegate.leaseClient
    }

    override fun getWatchClient(): ReactiveWatch {
        return delegate.watchClient
    }

    override fun getLockClient(): ReactiveLock {
        return delegate.lockClient
    }

    override fun getElectionClient(): ReactiveElection {
        return delegate.electionClient
    }

    override fun close() {
        delegate.close()
    }

}