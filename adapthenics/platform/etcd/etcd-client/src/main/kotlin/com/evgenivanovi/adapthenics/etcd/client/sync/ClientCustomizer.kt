package com.evgenivanovi.adapthenics.etcd.client.sync

import io.etcd.jetcd.ClientBuilder

fun interface ClientCustomizer {

    fun customize(client: ClientBuilder)

}