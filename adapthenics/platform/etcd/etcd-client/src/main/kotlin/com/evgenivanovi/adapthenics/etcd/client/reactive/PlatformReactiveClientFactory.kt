package com.evgenivanovi.adapthenics.etcd.client.reactive

import io.etcd.jetcd.Client
import io.etcd.jetcd.reactive.ReactiveClient

class PlatformReactiveClientFactory : ReactiveClientFactory {

    private val client: Client

    private val customizers: List<ReactiveClientCustomizer>

    constructor(
        client: Client,
        customizers: List<ReactiveClientCustomizer> = emptyList(),
    ) {
        this.client = client
        this.customizers = customizers
    }

    override fun create(): ReactiveClient {
        val client = ReactiveClient.builder(client)
        customizers.forEach { it.customize(client) }
        return client.build()
    }

}