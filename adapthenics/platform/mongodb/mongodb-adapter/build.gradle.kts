project(ProjectModules.MONGODB_ADAPTER) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(searchCatalog.search)
        testImplementation(searchCatalog.search)
        implementation(searchCatalog.searchQuery)
        testImplementation(searchCatalog.searchQuery)

        implementation(jsCatalog.jsApi)
        testImplementation(jsCatalog.jsApi)
        implementation(jsCatalog.jsSpec)
        testImplementation(jsCatalog.jsSpec)

        implementation(project(ProjectModules.BASE))
        testImplementation(project(ProjectModules.BASE))

        implementation(project(ProjectModules.SEARCH_SERVICE))
        testImplementation(project(ProjectModules.SEARCH_SERVICE))

        implementation(project(ProjectModules.DOMAIN_MODEL))
        testImplementation(project(ProjectModules.DOMAIN_MODEL))

        implementation(project(ProjectModules.DOMAIN_SERVICE))
        testImplementation(project(ProjectModules.DOMAIN_SERVICE))

        implementation(project(ProjectModules.DOMAIN_SEARCH))
        testImplementation(project(ProjectModules.DOMAIN_SEARCH))

        implementation(project(ProjectModules.SCHEMA_MAPPER))
        testImplementation(project(ProjectModules.SCHEMA_MAPPER))

        /* Support Libraries */
        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)
}