package com.evgenivanovi.adapthenics.mongodb.adapter.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.entity.EntityData
import com.evgenivanovi.domain.service.repo.AutoSaveRepository

// @formatter:off
interface MongoAutoSaveRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    ENTITY_DATA : EntityData,
> : AutoSaveRepository<ID, ENTITY, ENTITY_DATA>
// @formatter:on