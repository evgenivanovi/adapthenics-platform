package com.evgenivanovi.adapthenics.mongodb.adapter.requester

import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.model.api.Document

interface MongoWriteRequester {

    suspend fun <T : Document<*>> softInsert(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T

    suspend fun <T : Document<*>> forceInsert(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T

    suspend fun <T : Document<*>> softUpdate(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T?

    suspend fun <T : Document<*>> forceUpdate(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T?

}