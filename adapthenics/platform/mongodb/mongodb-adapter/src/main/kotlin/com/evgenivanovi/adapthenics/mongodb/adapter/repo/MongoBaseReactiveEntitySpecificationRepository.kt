package com.evgenivanovi.adapthenics.mongodb.adapter.repo

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoReadRequester
import com.evgenivanovi.adapthenics.schema.definition.api.DocDefinition
import com.evgenivanovi.adapthenics.schema.mapper.DocumentMapper
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.search.query.Queries
import com.evgenivanovi.search.query.SliceQueries
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toCollection

// @formatter:off
interface MongoBaseReactiveEntitySpecificationRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    DOCUMENT : Document<ID>
> : BaseReactiveEntitySpecificationRepository<ID, ENTITY> {
// @formatter:on

    val requester: MongoReadRequester
    val mapper: DocumentMapper<ID, ENTITY, DOCUMENT>

    val type: Class<DOCUMENT>
    val definition: DocDefinition<*>

    fun collection(): String {
        return definition.catalog()
    }

    override fun executeFindManyBy(queries: Queries): Flow<ENTITY> {
        return requester
            .findMany(queries, collection(), type)
            .map(mapper::to)
    }

    override suspend fun executeFindManyBy(queries: SliceQueries): Collection<ENTITY> {
        return requester
            .findMany(queries, collection(), type)
            .toCollection(ArrayList())
            .map(mapper::to)
    }

    override suspend fun executeCountBy(queries: Queries): Long {
        return requester.findCount(queries, collection(), type)
    }

}