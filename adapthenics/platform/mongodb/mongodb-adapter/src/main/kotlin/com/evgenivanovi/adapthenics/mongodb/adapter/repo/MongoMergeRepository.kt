package com.evgenivanovi.adapthenics.mongodb.adapter.repo

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoMergeRequester
import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.mapper.DocumentMapper
import com.evgenivanovi.adapthenics.schema.mapper.PartialDocumentMapper
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.adapthenics.schema.model.api.PartialDocument
import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.model.entity.PartialEntity
import com.evgenivanovi.domain.service.repo.MergeRepository
import com.evgenivanovi.domain.service.repo.WriteOptions

// @formatter:off
interface MongoMergeRepository
<
    ID : Any,
    ENTITY : PartialEntity<ID>,
    DOCUMENT : PartialDocument<ID>,
    RESULT_ID : Any,
    RESULT_ENTITY : Entity<RESULT_ID>,
    RESULT_DOCUMENT : Document<RESULT_ID>
> : MergeRepository<ID, ENTITY, RESULT_ID, RESULT_ENTITY> {
// @formatter:on

    val requester: MongoMergeRequester

    val mapper: DocumentMapper<RESULT_ID, RESULT_ENTITY, RESULT_DOCUMENT>
    val partialMapper: PartialDocumentMapper<ID, ENTITY, DOCUMENT>

    val type: Class<DOCUMENT>
    val resultType: Class<RESULT_DOCUMENT>
    val definition: DocumentDefinition<RESULT_DOCUMENT>

    fun collection(): String {
        return definition.catalog()
    }

    override suspend fun merge(
        entity: ENTITY,
        options: WriteOptions,
    ): RESULT_ENTITY? {
        return partialMapper
            .from(entity)
            .let { merge(it, options) }
            ?.let(mapper::to)
    }

    private suspend fun merge(
        document: DOCUMENT,
        options: WriteOptions,
    ): RESULT_DOCUMENT? {
        return when (options) {
            WriteOptions.SOFT ->
                requester.softMerge(collection(), definition, document, resultType)

            WriteOptions.FORCE ->
                requester.forceMerge(collection(), definition, document, resultType)
        }
    }

    override suspend fun merge(
        entities: Collection<ENTITY>,
        options: WriteOptions,
    ): Collection<RESULT_ENTITY?> {
        return entities.map { merge(it, options) }
    }

}