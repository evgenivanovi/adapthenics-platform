package com.evgenivanovi.adapthenics.mongodb.adapter.repo

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoWriteRequester
import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.mapper.DocumentMapper
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.service.repo.NonAutoSaveRepository
import com.evgenivanovi.domain.service.repo.WriteOptions
import com.evgenivanovi.kt.func.slet

// @formatter:off
interface MongoNonAutoSaveRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    DOCUMENT : Document<ID>
> : NonAutoSaveRepository<ID, ENTITY> {
// @formatter:on

    val requester: MongoWriteRequester
    val mapper: DocumentMapper<ID, ENTITY, DOCUMENT>

    val type: Class<DOCUMENT>
    val definition: DocumentDefinition<DOCUMENT>

    fun collection(): String {
        return definition.catalog()
    }

    override suspend fun insert(
        entity: ENTITY,
        options: WriteOptions,
    ): ENTITY {
        return mapper
            .from(entity)
            .slet { doInsert(it, options) }
            .let(mapper::to)
    }

    override suspend fun insert(
        entities: Collection<ENTITY>,
        options: WriteOptions,
    ): Collection<ENTITY> {
        return entities.map { insert(it, options) }
    }

    private suspend fun doInsert(
        document: DOCUMENT,
        options: WriteOptions,
    ): DOCUMENT {
        return when (options) {
            WriteOptions.SOFT ->
                requester.softInsert(collection(), definition, document)

            WriteOptions.FORCE ->
                requester.forceInsert(collection(), definition, document)
        }
    }

}