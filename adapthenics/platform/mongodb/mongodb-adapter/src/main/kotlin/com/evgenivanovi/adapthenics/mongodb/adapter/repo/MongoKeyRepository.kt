package com.evgenivanovi.adapthenics.mongodb.adapter.repo

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoReadRequester
import com.evgenivanovi.adapthenics.schema.definition.api.DocDefinition
import com.evgenivanovi.adapthenics.schema.mapper.DocumentMapper
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.service.repo.KeyRepository

// @formatter:off
interface MongoKeyRepository
<
    ID : Any,
    ENTITY : Entity<ID>,
    DOCUMENT : Document<ID>
> : KeyRepository<ID, ENTITY> {
// @formatter:on

    val requester: MongoReadRequester
    val mapper: DocumentMapper<ID, ENTITY, DOCUMENT>

    val type: Class<DOCUMENT>
    val definition: DocDefinition<*>

    fun collection(): String {
        return definition.catalog()
    }

    override suspend fun find(id: ID): ENTITY? {
        return runCatching { executeFind(id) }.fold(
            onSuccess = { it },
            onFailure = { throw it }
        )
    }

    private suspend fun executeFind(id: ID): ENTITY? {
        return requester
            .findById(id, collection(), type)
            ?.let(mapper::to)
    }

}