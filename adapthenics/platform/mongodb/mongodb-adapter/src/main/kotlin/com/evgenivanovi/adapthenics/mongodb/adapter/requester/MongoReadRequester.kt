package com.evgenivanovi.adapthenics.mongodb.adapter.requester

import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.search.query.Queries
import com.evgenivanovi.search.query.SliceQueries
import kotlinx.coroutines.flow.Flow

interface MongoReadRequester {

    fun <T : Document<*>> findMany(
        queries: Queries,
        collection: String,
        type: Class<T>,
    ): Flow<T>

    fun <T : Document<*>> findMany(
        queries: SliceQueries,
        collection: String,
        type: Class<T>,
    ): Flow<T>

    suspend fun <T : Document<*>> findOne(
        queries: Queries,
        collection: String,
        type: Class<T>,
    ): T?

    suspend fun <T : Document<*>> findCount(
        queries: Queries,
        collection: String,
        type: Class<T>,
    ): Long

    suspend fun <T : Document<*>> findById(
        id: Any,
        collection: String,
        type: Class<T>,
    ): T?

}