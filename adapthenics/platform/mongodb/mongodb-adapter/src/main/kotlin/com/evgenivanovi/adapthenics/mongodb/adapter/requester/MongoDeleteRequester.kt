package com.evgenivanovi.adapthenics.mongodb.adapter.requester

import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.model.api.Document

interface MongoDeleteRequester {

    suspend fun <T : Document<ID>, ID : Any> delete(
        collection: String,
        id: ID,
        definition: DocumentDefinition<T>,
        type: Class<T>,
    ): T?

    suspend fun <T : Document<ID>, ID : Any> archive(
        collection: String,
        id: ID,
        definition: DocumentDefinition<T>,
        type: Class<T>,
    ): T?

}