package com.evgenivanovi.adapthenics.mongodb.adapter.repo

import com.evgenivanovi.domain.model.entity.Entity
import com.evgenivanovi.domain.search.EntitySpecificationRepository
import com.evgenivanovi.search.spec.CountSpecification
import com.evgenivanovi.search.spec.SlicedSpecification
import com.evgenivanovi.search.spec.Specification
import com.evgenivanovi.search.query.Queries
import com.evgenivanovi.search.query.SliceQueries
import com.evgenivanovi.search.query.adapter.StringSpecificationMapper

interface BaseEntitySpecificationRepository<ID : Any, ENTITY : Entity<ID>>
    : EntitySpecificationRepository<ID, ENTITY> {

    val specificationMapper: StringSpecificationMapper

    override suspend fun find(
        specification: Specification,
    ): Collection<ENTITY> {

        val fields = specificationMapper
            .mapFieldQuery(specification)

        val sorts = specificationMapper
            .mapSortQuery(specification)

        val queries = Queries.of(fields, sorts)
        return executeFindManyBy(queries)

    }

    override suspend fun find(
        specification: SlicedSpecification,
    ): Collection<ENTITY> {

        val fields = specificationMapper
            .mapFieldQuery(specification)

        val sorts = specificationMapper
            .mapSortQuery(specification)

        val slice = specificationMapper
            .mapSliceQuery(specification)

        val queries = SliceQueries.of(fields, sorts, slice)
        return executeFindManyBy(queries)

    }

    override suspend fun count(
        specification: CountSpecification,
    ): Long {

        val fields = specificationMapper
            .mapFieldQuery(specification)

        val queries = Queries.of(fields)
        return executeCountBy(queries)

    }

    suspend fun executeFindManyBy(queries: Queries): Collection<ENTITY>

    suspend fun executeFindManyBy(queries: SliceQueries): Collection<ENTITY>

    suspend fun executeCountBy(queries: Queries): Long

}