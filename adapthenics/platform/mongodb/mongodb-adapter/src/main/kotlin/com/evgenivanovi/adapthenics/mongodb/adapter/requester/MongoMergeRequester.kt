package com.evgenivanovi.adapthenics.mongodb.adapter.requester

import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.adapthenics.schema.model.api.PartialDocument

interface MongoMergeRequester {

    suspend fun <T : PartialDocument<*>, R : Document<*>> softMerge(
        collection: String,
        definition: DocumentDefinition<R>,
        document: T,
        type: Class<R>,
    ): R?

    suspend fun <T : PartialDocument<*>, R : Document<*>> forceMerge(
        collection: String,
        definition: DocumentDefinition<R>,
        document: T,
        type: Class<R>,
    ): R?

}