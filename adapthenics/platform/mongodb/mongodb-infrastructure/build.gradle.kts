project(ProjectModules.MONGODB_INFRASTRUCTURE) {

    dependencies {
        implementation(ktCatalog.kt)
        testImplementation(ktCatalog.ktTest)

        implementation(jsCatalog.jsApi)
        testImplementation(jsCatalog.jsApi)

        implementation(jsCatalog.jsSpec)
        testImplementation(jsCatalog.jsSpec)

        implementation(mongodbCatalog.mongodbClient)
        testImplementation(mongodbCatalog.mongodbClient)

        implementation(mongodbCatalog.mongodbClientSpring)
        testImplementation(mongodbCatalog.mongodbClientSpring)

        testImplementation(mongodbCatalog.mongodbEnvotur)
        testImplementation(mongodbCatalog.mongodbEnvoturJunit)

        implementation(project(ProjectModules.BASE))
        testImplementation(project(ProjectModules.BASE))

        implementation(project(ProjectModules.SEARCH_MONGO))
        testImplementation(project(ProjectModules.SEARCH_MONGO))

        implementation(project(ProjectModules.MONGODB_ADAPTER))
        testImplementation(project(ProjectModules.MONGODB_ADAPTER))

        implementation(project(ProjectModules.REACTOR_SUPPORT))
        testImplementation(project(ProjectModules.REACTOR_SUPPORT))

        implementation(project(ProjectModules.LOGGING_SUPPORT))
        testImplementation(project(ProjectModules.LOGGING_SUPPORT))
    }

}

dependencies {
    implementation(ktDependenciesCatalog.bundles.java.imp)
    implementation(ktDependenciesCatalog.bundles.kotlin.jvm.imp)
    testImplementation(ktDependenciesCatalog.bundles.assert.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.reactor.impl)
    testImplementation(ktDependenciesCatalog.bundles.reactor.testimpl)
    implementation(ktDependenciesCatalog.bundles.coroutine.jvm.impl)
    testImplementation(ktDependenciesCatalog.bundles.coroutine.jvm.testimpl)

    implementation(ktDependenciesCatalog.bundles.java.reflect.imp)
    implementation(ktDependenciesCatalog.bundles.log.impl)

    implementation(libs.bundles.mongo.client.impl)
    implementation(libs.bundles.mongo.client.spring.impl)
    testImplementation(ktDependenciesCatalog.bundles.testcontainers.mongo.impl)

    implementation(searchCatalog.searchQuery)
    testImplementation(searchCatalog.searchQuery)

    testImplementation(tools.envotur.impl)
    testImplementation(tools.envotur.junit.impl)
}