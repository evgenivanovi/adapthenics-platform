package com.evgenivanovi.adapthenics.mongodb.infrastructure

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoReadRequester
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.kt.math.Numbers.ZERO
import com.evgenivanovi.search.mongodb.QueriesMapper
import com.evgenivanovi.search.query.Queries
import com.evgenivanovi.search.query.SliceQueries
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.SchedulerCoroutineDispatcher
import kotlinx.coroutines.reactor.asCoroutineDispatcher
import kotlinx.coroutines.reactor.awaitSingleOrNull
import kotlinx.coroutines.withContext
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Query
import reactor.core.scheduler.Scheduler

class MongoReadRequesterService(
    private val ops: ReactiveMongoOperations,
    private val scheduler: Scheduler,
) : MongoReadRequester {

    private val dispatcher: SchedulerCoroutineDispatcher =
        scheduler.asCoroutineDispatcher()

    override fun <T : Document<*>> findMany(
        queries: Queries,
        collection: String,
        type: Class<T>,
    ): Flow<T> {
        return doExecuteMany(queries, collection, type)
            .flowOn(dispatcher)
    }

    private fun <T : Document<*>> doExecuteMany(
        queries: Queries,
        collection: String,
        clazz: Class<T>,
    ): Flow<T> {

        val query: Query = QueriesMapper
            .mapToQuery(queries)

        return ops
            .find(query, clazz, collection)
            .publishOn(scheduler)
            .onErrorMap(ExceptionTranslator::translate)
            .asFlow()
            .flowOn(dispatcher)
            .catch { throw ExceptionTranslator.translate(it) }

    }

    override fun <T : Document<*>> findMany(
        queries: SliceQueries,
        collection: String,
        type: Class<T>,
    ): Flow<T> {
        return doExecuteMany(queries, collection, type)
            .flowOn(dispatcher)
    }

    private fun <T : Document<*>> doExecuteMany(
        queries: SliceQueries,
        collection: String,
        clazz: Class<T>,
    ): Flow<T> {

        val query: Query = QueriesMapper
            .mapToPageQuery(queries)

        return ops
            .find(query, clazz, collection)
            .publishOn(scheduler)
            .onErrorMap(ExceptionTranslator::translate)
            .asFlow()
            .flowOn(dispatcher)
            .catch { throw ExceptionTranslator.translate(it) }

    }

    override suspend fun <T : Document<*>> findOne(
        queries: Queries,
        collection: String,
        type: Class<T>,
    ): T? = coroutineScope {
        withContext(dispatcher) {
            doExecuteOne(queries, collection, type)
        }
    }

    private suspend fun <T : Document<*>> doExecuteOne(
        queries: Queries,
        collection: String,
        clazz: Class<T>,
    ): T? {

        val query: Query = QueriesMapper
            .mapToQuery(queries)

        return ops
            .findOne(query, clazz, collection)
            .publishOn(scheduler)
            .onErrorMap(ExceptionTranslator::translate)
            .awaitSingleOrNull()

    }

    override suspend fun <T : Document<*>> findCount(
        queries: Queries,
        collection: String,
        type: Class<T>,
    ): Long = coroutineScope {
        withContext(dispatcher) {
            doExecuteCount(queries, collection, type)
        }
    }

    private suspend fun <T : Document<*>> doExecuteCount(
        queries: Queries,
        collection: String,
        clazz: Class<T>,
    ): Long {

        val query: Query = QueriesMapper
            .mapToQuery(queries)

        return ops
            .count(query, clazz, collection)
            .publishOn(scheduler)
            .onErrorMap(ExceptionTranslator::translate)
            .awaitSingleOrNull()
            ?: ZERO

    }

    override suspend fun <T : Document<*>> findById(
        id: Any,
        collection: String,
        type: Class<T>,
    ): T? = coroutineScope {
        withContext(dispatcher) {
            doFindById(id, collection, type)
        }
    }

    private suspend fun <T : Document<*>> doFindById(
        id: Any,
        collection: String,
        clazz: Class<T>,
    ): T? {
        return ops
            .findById(id, clazz, collection)
            .publishOn(scheduler)
            .onErrorMap(ExceptionTranslator::translate)
            .awaitSingleOrNull()
    }

}