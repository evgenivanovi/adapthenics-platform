package com.evgenivanovi.adapthenics.mongodb.infrastructure

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoWriteRequester
import com.evgenivanovi.adapthenics.mongodb.client.MongoDocumentOps
import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.model.api.Document
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class MongoWriteRequesterService(
    private val ops: MongoDocumentOps,
) : MongoWriteRequester {

    override suspend fun <T : Document<*>> softInsert(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T = coroutineScope {
        withContext(coroutineContext) {
            doExecuteSoftInsert(collection, document, definition)
        }
    }

    private suspend fun <T : Document<*>> doExecuteSoftInsert(
        collection: String,
        document: T,
        definition: DocumentDefinition<T>,
    ): T {
        try {
            return ops.softInsert(collection, document, definition)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

    override suspend fun <T : Document<*>> forceInsert(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T = coroutineScope {
        withContext(coroutineContext) {
            doExecuteForceInsert(collection, document, definition)
        }
    }

    private suspend fun <T : Document<*>> doExecuteForceInsert(
        collection: String,
        document: T,
        definition: DocumentDefinition<T>,
    ): T {
        try {
            return ops.forceInsert(collection, document, definition)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

    override suspend fun <T : Document<*>> softUpdate(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T? = coroutineScope {
        withContext(coroutineContext) {
            doExecuteSoftUpdate(collection, document, definition)
        }
    }

    private suspend fun <T : Document<*>> doExecuteSoftUpdate(
        collection: String,
        document: T,
        definition: DocumentDefinition<T>,
    ): T? {
        try {
            return ops.softUpdate(collection, document, definition)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

    override suspend fun <T : Document<*>> forceUpdate(
        collection: String,
        definition: DocumentDefinition<T>,
        document: T,
    ): T? = coroutineScope {
        withContext(coroutineContext) {
            doExecuteForceUpdate(collection, document, definition)
        }
    }

    private suspend fun <T : Document<*>> doExecuteForceUpdate(
        collection: String,
        document: T,
        definition: DocumentDefinition<T>,
    ): T? {
        try {
            return ops.forceUpdate(collection, document, definition)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

}