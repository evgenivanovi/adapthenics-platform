package com.evgenivanovi.adapthenics.mongodb.infrastructure

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoDeleteRequester
import com.evgenivanovi.adapthenics.mongodb.client.MongoDocumentOps
import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.model.api.Document
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class MongoDeleteRequesterService(
    private val ops: MongoDocumentOps,
) : MongoDeleteRequester {

    override suspend fun <T : Document<ID>, ID : Any> archive(
        collection: String,
        id: ID,
        definition: DocumentDefinition<T>,
        type: Class<T>,
    ): T? = coroutineScope {
        withContext(coroutineContext) {
            doArchive(collection, id, definition, type)
        }
    }

    private suspend fun <T : Document<ID>, ID : Any> doArchive(
        collection: String,
        id: ID,
        definition: DocumentDefinition<T>,
        type: Class<T>,
    ): T? {
        try {
            return ops.softDelete(collection, id, type, definition)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

    override suspend fun <T : Document<ID>, ID : Any> delete(
        collection: String,
        id: ID,
        definition: DocumentDefinition<T>,
        type: Class<T>,
    ): T? = coroutineScope {
        withContext(coroutineContext) {
            doDelete(collection, id, definition, type)
        }
    }

    private suspend fun <T : Document<ID>, ID : Any> doDelete(
        collection: String,
        id: ID,
        definition: DocumentDefinition<T>,
        type: Class<T>,
    ): T? {
        try {
            return ops.forceDelete(collection, id, type, definition)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

}