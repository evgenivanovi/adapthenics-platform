package com.evgenivanovi.adapthenics.mongodb.infrastructure

import com.evgenivanovi.adapthenics.base.errx.*
import com.evgenivanovi.kt.ex.exception
import org.springframework.dao.DuplicateKeyException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.dao.PessimisticLockingFailureException

/**
 * DataAccessResourceFailureException == com.mongodb.MongoTimeoutException
 */
internal object ExceptionTranslator {

    fun translate(ex: Throwable): PersistenceErrorex {
        return when (ex) {
            is DuplicateKeyException -> {
                DuplicateErrorex.of(ex)
            }

            is EmptyResultDataAccessException -> {
                AbsenceErrorex.of(ex)
            }

            is OptimisticLockingFailureException -> {
                OptimisticLockErrorex.of(ex)
            }

            is PessimisticLockingFailureException -> {
                PessimisticLockErrorex.of(ex)
            }

            else -> {
                UncategorizedErrorex.of(ex.exception())
            }
        }
    }

}