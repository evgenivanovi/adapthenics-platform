package com.evgenivanovi.adapthenics.mongodb.infrastructure

import com.evgenivanovi.adapthenics.mongodb.adapter.requester.MongoMergeRequester
import com.evgenivanovi.adapthenics.mongodb.client.MongoDocumentOps
import com.evgenivanovi.adapthenics.schema.definition.api.DocumentDefinition
import com.evgenivanovi.adapthenics.schema.model.api.Document
import com.evgenivanovi.adapthenics.schema.model.api.PartialDocument
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class MongoMergeRequesterService(
    private val ops: MongoDocumentOps,
) : MongoMergeRequester {

    override suspend fun <T : PartialDocument<*>, R : Document<*>> softMerge(
        collection: String,
        definition: DocumentDefinition<R>,
        document: T,
        type: Class<R>,
    ): R? = coroutineScope {
        withContext(coroutineContext) {
            doExecuteSoftMerge(collection, document, definition, type)
        }
    }

    private suspend fun <T : PartialDocument<*>, R : Document<*>> doExecuteSoftMerge(
        collection: String,
        document: T,
        definition: DocumentDefinition<R>,
        clazz: Class<R>,
    ): R? {
        try {
            return ops.softMerge(collection, document, definition, clazz)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

    override suspend fun <T : PartialDocument<*>, R : Document<*>> forceMerge(
        collection: String,
        definition: DocumentDefinition<R>,
        document: T,
        type: Class<R>,
    ): R? = coroutineScope {
        withContext(coroutineContext) {
            doExecuteForceMerge(collection, document, definition, type)
        }
    }

    private suspend fun <T : PartialDocument<*>, R : Document<*>> doExecuteForceMerge(
        collection: String,
        document: T,
        definition: DocumentDefinition<R>,
        clazz: Class<R>,
    ): R? {
        try {
            return ops.forceMerge(collection, document, definition, clazz)
        } catch (e: Exception) {
            throw ExceptionTranslator.translate(e)
        }
    }

}