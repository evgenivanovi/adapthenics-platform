package com.evgenivanovi.adapthenics.mongodb.infrastructure

import com.evgenivanovi.adapthenics.envotur.api.Envo
import com.evgenivanovi.adapthenics.mongodb.client.cfg.MongoConnectionConfigurer
import com.evgenivanovi.adapthenics.mongodb.envotur.extension.MongoGlobalExtension
import com.evgenivanovi.kt.lang.isNotNull
import com.evgenivanovi.kt.meta.ClasspathTypeAliasProvider
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

class RegisterMongoGlobalExtensionTest {

    companion object {

        private val MONGO: Envo = Envo("mongo", 27017)

        @JvmStatic
        @RegisterExtension
        val extension: MongoGlobalExtension = MongoGlobalExtension.extension()
            .withOptions { options ->
                options
                    .withGitlabLocation("../../../../.gitlab-ci.yml")
                    .withGitlabJob("test")
                    .withEnvo(MONGO)
            }
            .withSettings { configurer ->
                configurer
                    .databasePersistence {
                        database = MongoConnectionConfigurer.DEFAULT_DATABASE
                    }
                    .types(
                        ClasspathTypeAliasProvider(
                            listOf("com.evgenivanovi.adapthenics.schema.model")
                        )
                    )
            }
            .build()

    }

    @Test
    fun assert() {

        assertTrue(
            extension.runtime().url().isNotNull()
        )

    }

}