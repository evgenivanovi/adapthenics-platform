package com.evgenivanovi.adapthenics.base.errx

import com.evgenivanovi.kt.errx.Errorex
import com.evgenivanovi.kt.ex.message

abstract class PersistenceErrorex
protected constructor(err: PersistenceError) : Errorex(err) {

    fun error(): String {
        return err.error()
    }

}

class DuplicateErrorex
internal constructor(err: DuplicateError) : PersistenceErrorex(err) {

    companion object {

        fun of(text: String): DuplicateErrorex {
            val err = DuplicateError.of(text)
            return DuplicateErrorex(err)
        }

        fun of(err: DuplicateError): DuplicateErrorex {
            return DuplicateErrorex(err)
        }

        fun of(ex: Exception): DuplicateErrorex {
            val err = DuplicateError.of(ex.message())
            return DuplicateErrorex(err)
        }

    }

}

class AbsenceErrorex
internal constructor(err: AbsenceError) : PersistenceErrorex(err) {

    companion object {

        fun of(text: String): AbsenceErrorex {
            val err = AbsenceError.of(text)
            return AbsenceErrorex(err)
        }

        fun of(err: AbsenceError): AbsenceErrorex {
            return AbsenceErrorex(err)
        }

        fun of(ex: Exception): AbsenceErrorex {
            val err = AbsenceError.of(ex.message())
            return AbsenceErrorex(err)
        }

    }

}

class UncategorizedErrorex
internal constructor(err: UncategorizedError) : PersistenceErrorex(err) {

    companion object {

        fun of(text: String): UncategorizedErrorex {
            val err = UncategorizedError.of(text)
            return UncategorizedErrorex(err)
        }

        fun of(err: UncategorizedError): UncategorizedErrorex {
            return UncategorizedErrorex(err)
        }

        fun of(ex: Exception): UncategorizedErrorex {
            val err = UncategorizedError.of(ex.message())
            return UncategorizedErrorex(err)
        }

    }

}

sealed class ConcurrentErrorex(err: ConcurrentError) : PersistenceErrorex(err)

class OptimisticLockErrorex
internal constructor(err: OptimisticLockError) : ConcurrentErrorex(err) {

    companion object {

        fun of(text: String): OptimisticLockErrorex {
            val err = OptimisticLockError.of(text)
            return OptimisticLockErrorex(err)
        }

        fun of(err: OptimisticLockError): OptimisticLockErrorex {
            return OptimisticLockErrorex(err)
        }

        fun of(ex: Exception): OptimisticLockErrorex {
            val err = OptimisticLockError.of(ex.message())
            return OptimisticLockErrorex(err)
        }

    }

}

class PessimisticLockErrorex
internal constructor(err: PessimisticLockError) : ConcurrentErrorex(err) {

    companion object {

        fun of(text: String): PessimisticLockErrorex {
            val err = PessimisticLockError.of(text)
            return PessimisticLockErrorex(err)
        }

        fun of(err: PessimisticLockError): PessimisticLockErrorex {
            return PessimisticLockErrorex(err)
        }

        fun of(ex: Exception): PessimisticLockErrorex {
            val err = PessimisticLockError.of(ex.message())
            return PessimisticLockErrorex(err)
        }

    }

}
