package com.evgenivanovi.adapthenics.base.schema

interface ObjectPropertyDescriptor : PropertyDescriptor {

    fun properties(): List<PropertyDescriptor> {
        return emptyList()
    }

    companion object

}