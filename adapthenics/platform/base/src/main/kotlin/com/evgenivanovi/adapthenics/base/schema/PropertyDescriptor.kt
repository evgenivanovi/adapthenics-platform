package com.evgenivanovi.adapthenics.base.schema

import com.evgenivanovi.kt.string.Strings.DOT

interface PropertyDescriptor {

    fun name(): String

    fun path(): String

    companion object {

        const val SEPARATOR: String = DOT

    }

}