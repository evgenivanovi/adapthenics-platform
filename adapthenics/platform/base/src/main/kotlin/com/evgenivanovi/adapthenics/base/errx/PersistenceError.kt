package com.evgenivanovi.adapthenics.base.errx

import com.evgenivanovi.kt.errx.Error
import com.evgenivanovi.kt.errx.Errors
import com.evgenivanovi.kt.errx.Is

sealed class PersistenceError(
    private val err: Error,
) : Error by err, Is {

    override fun isis(err: Error): Boolean {
        return err is PersistenceError
    }

}

class DuplicateError
private constructor(err: Error) : PersistenceError(err) {

    override fun isis(err: Error): Boolean {
        return err is DuplicateError
    }

    companion object {

        fun of(text: String): DuplicateError {
            return DuplicateError(Errors.single(text))
        }

        fun of(text: String, cause: Error): DuplicateError {
            return DuplicateError(Errors.extend(text, cause))
        }

    }

}

class AbsenceError
private constructor(err: Error) : PersistenceError(err) {

    override fun isis(err: Error): Boolean {
        return err is AbsenceError
    }

    companion object {

        fun of(text: String): AbsenceError {
            return AbsenceError(Errors.single(text))
        }

        fun of(text: String, cause: Error): AbsenceError {
            return AbsenceError(Errors.extend(text, cause))
        }

    }

}

class UncategorizedError
private constructor(err: Error) : PersistenceError(err) {

    override fun isis(err: Error): Boolean {
        return err is UncategorizedError
    }

    companion object {

        fun of(text: String): UncategorizedError {
            return UncategorizedError(Errors.single(text))
        }

        fun of(text: String, cause: Error): UncategorizedError {
            return UncategorizedError(Errors.extend(text, cause))
        }

    }

}

sealed class ConcurrentError(err: Error) : PersistenceError(err)

class OptimisticLockError
private constructor(err: Error) : ConcurrentError(err) {

    override fun isis(err: Error): Boolean {
        return err is OptimisticLockError
    }

    companion object {

        fun of(text: String): OptimisticLockError {
            return OptimisticLockError(Errors.single(text))
        }

        fun of(text: String, cause: Error): OptimisticLockError {
            return OptimisticLockError(Errors.extend(text, cause))
        }

    }

}

class PessimisticLockError
private constructor(err: Error) : ConcurrentError(err) {

    override fun isis(err: Error): Boolean {
        return err is PessimisticLockError
    }

    companion object {

        fun of(text: String): PessimisticLockError {
            return PessimisticLockError(Errors.single(text))
        }

        fun of(text: String, cause: Error): PessimisticLockError {
            return PessimisticLockError(Errors.extend(text, cause))
        }

    }

}
