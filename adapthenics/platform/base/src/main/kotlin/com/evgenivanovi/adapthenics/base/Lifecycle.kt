package com.evgenivanovi.adapthenics.base

interface Lifecycle {

    fun start()

    fun stop()

}