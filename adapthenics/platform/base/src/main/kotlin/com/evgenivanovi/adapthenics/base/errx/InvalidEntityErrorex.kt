package com.evgenivanovi.adapthenics.base.errx

import com.evgenivanovi.kt.errx.*

class InvalidEntityError
private constructor(
    private val entity: String,
    private val err: Error,
) : MultiError, Is {

    fun entity(): String {
        return entity
    }

    override fun error(): String {
        return "Invalid entity [$entity]: ${err.error()}"
    }

    override fun errors(): Collection<Error> {
        if (err is MultiError) {
            return err.errors()
        }
        return listOf(err)
    }

    override fun isis(err: Error): Boolean {
        return err is InvalidEntityError
            && err.entity == this.entity
    }

    companion object {

        fun of(
            entity: String,
            text: String,
        ): InvalidEntityError {
            return InvalidEntityError(
                entity,
                Errors.single(text)
            )
        }

        fun of(
            entity: String,
            format: (Collection<Error>) -> String = ErrorFormats.defaults(),
            vararg texts: String,
        ): InvalidEntityError {
            return InvalidEntityError(
                entity,
                Errors.multiText(format, *texts)
            )
        }

        fun of(
            entity: String,
            format: (Collection<Error>) -> String = ErrorFormats.defaults(),
            texts: Collection<String>,
        ): InvalidEntityError {
            return InvalidEntityError(
                entity,
                Errors.multiText(format, texts)
            )
        }

    }

}

class InvalidEntityErrorex
private constructor(
    err: InvalidEntityError,
) : Errorex(err) {

    fun entity(): String {
        return (err as InvalidEntityError).entity()
    }

    companion object {

        fun of(
            err: InvalidEntityError,
        ): InvalidEntityErrorex {
            return InvalidEntityErrorex(err)
        }

    }

}