package com.evgenivanovi.adapthenics.base.app

object ServiceModeConstants {

    const val ON: String = "on"
    const val OFF: String = "off"

    const val TRUE: String = "true"
    const val FALSE: String = "false"

    const val ENABLED: String = "enabled"
    const val DISABLED: String = "disabled"

}