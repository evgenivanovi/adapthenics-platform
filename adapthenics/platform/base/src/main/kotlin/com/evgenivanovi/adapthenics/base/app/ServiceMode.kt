package com.evgenivanovi.adapthenics.base.app

enum class ServiceMode {

    ON(
        ServiceModeConstants.ON,
        ServiceModeConstants.TRUE,
        ServiceModeConstants.ENABLED,
    ),

    OFF(
        ServiceModeConstants.OFF,
        ServiceModeConstants.FALSE,
        ServiceModeConstants.DISABLED,
    );

    private val _aliases: Set<String>

    constructor(vararg aliases: String) {
        this._aliases = setOf(*aliases)
    }

    fun aliases(): Set<String> {
        return _aliases
    }

    companion object {

        val ALL_ALIASES: List<String> = entries
            .flatMap(ServiceMode::aliases)
            .map(String::uppercase)

        val ALL_ALIASES_MAP: Map<String, ServiceMode> = entries
            .map { elem -> elem.aliases() to elem }
            .flatMap { elem -> elem.first.map { alias -> alias to elem.second } }
            .associate { elem -> elem.first.uppercase() to elem.second }

        fun resolve(alias: String): ServiceMode? {
            return ALL_ALIASES_MAP[alias.uppercase()]
        }

        fun from(alias: String): ServiceMode {
            return requireNotNull(resolve(alias))
        }

    }

}