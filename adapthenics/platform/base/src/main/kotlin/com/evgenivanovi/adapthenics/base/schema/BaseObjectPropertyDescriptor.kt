@file:Suppress(
    "unused",
    "FunctionName",
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "MemberVisibilityCanBePrivate",
    "HasPlatformType",
    "NOTHING_TO_INLINE",
)

package com.evgenivanovi.adapthenics.base.schema

import com.evgenivanovi.adapthenics.base.schema.PropertyDescriptor.Companion.SEPARATOR
import com.evgenivanovi.kt.string.Strings

open class BaseObjectPropertyDescriptor : ObjectPropertyDescriptor {

    private val name: String

    private val path: String

    constructor(
        name: String = Strings.EMPTY,
        path: String = Strings.EMPTY,
    ) {
        this.name = name
        this.path = path
    }

    // Inherited: It means this is subtype.
    constructor(
        superType: PropertyDescriptor,
    ) {
        this.name = superType
            .name()

        this.path = superType
            .path()
            .removeSuffix(SEPARATOR.plus(superType.name()))
    }

    override fun name(): String {
        return name
    }

    override fun path(): String {
        return when (path.isBlank()) {
            true -> name
            false -> path + SEPARATOR + name
        }
    }

}