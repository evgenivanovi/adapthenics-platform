package com.evgenivanovi.adapthenics.base.errx

import com.evgenivanovi.kt.errx.*
import com.evgenivanovi.kt.string.Strings.COLON_SPACE
import com.evgenivanovi.kt.string.Strings.SINGLE_QUOTE

class HeaderError : MultiError, Is {

    val missing: Collection<String>

    val invalid: Map<String, String>

    private val err: MultiError

    internal constructor(
        missing: Collection<String>,
        invalid: Map<String, String>,
    ) {
        this.missing = missing
        this.invalid = invalid

        this.err = Errors
            .collect()
            .addIf(hasMissing(), missingError())
            .addIf(hasInvalid(), invalidError())
            .multi(ErrorFormats.defaults())
    }

    override fun error(): String {
        return err.error()
    }

    override fun errors(): Collection<Error> {
        return err.errors()
    }

    override fun isis(err: Error): Boolean {
        return err is HeaderError
    }

    fun hasMissing(): Boolean {
        return missing.isNotEmpty()
    }

    fun hasInvalid(): Boolean {
        return invalid.isNotEmpty()
    }

    private fun missingError(): String {
        return "Missing required headers: " + missing.joinToString()
    }

    private fun invalidError(): String {
        val join: (Map.Entry<String, String>) -> String = { it ->
            SINGLE_QUOTE + it.key + COLON_SPACE + it.value + SINGLE_QUOTE
        }
        return "Invalid headers: " + invalid.map(join).joinToString()
    }

    companion object {

        fun of(
            missing: Collection<String>,
            invalid: Map<String, String>,
        ): HeaderError {
            return HeaderError(missing, invalid)
        }

    }

}

class HeaderErrorex
internal constructor(
    err: HeaderError,
) : Errorex(err) {

    companion object {

        fun of(err: HeaderError): HeaderErrorex {
            return HeaderErrorex(err)
        }

    }

}