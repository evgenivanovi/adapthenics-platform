package com.evgenivanovi.adapthenics.base.app

data class ApplicationMetadata(
    val id: String,
    val name: String,
    val version: String,
)