@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

import com.evgenivanovi.adapthenics.lib.tool.onlyLibraries

plugins {

    /* Gradle Plugins */
    id("idea")
    id("org.gradle.base")
    id("org.gradle.java-platform")
    id("org.gradle.distribution")
    id("org.gradle.maven-publish")

    /* Adapthenics Plugins */
    id("com.evgenivanovi.adapthenics.lib")

}

javaPlatform {
    allowDependencies()
}

dependencies {

    constraints {
        rootProject.subprojects
            .onlyLibraries()
            .forEach { api(project(it.path)) }
    }

    api(platform(project(ProjectModules.PLATFORM_DEPENDENCIES_BOM)))

}

afterEvaluate {

    publishing {

        publications {

            register("platformBom", MavenPublication::class) {
                from(components["javaPlatform"])
                pom {
                    name.set("Adapthenics Platform BOM")
                    description.set("Adapthenics Platform BOM")
                }
            }

            repositories {
                gitlab()
            }

        }

    }

}