@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

plugins {

    /* Gradle Plugins */
    id("idea")
    id("org.gradle.base")
    id("org.gradle.java-platform")
    id("org.gradle.distribution")
    id("org.gradle.maven-publish")
    id("org.gradle.version-catalog")

    /* Adapthenics Plugins */
    id("com.evgenivanovi.adapthenics.lib")

}

afterEvaluate {

    catalog {
        versionCatalog {
            from(files("$rootDir/gradle/libs.versions.toml"))
        }
    }

    publishing {

        publications {

            register("dependenciesCatalog", MavenPublication::class) {
                from(components["versionCatalog"])
                pom {
                    name.set("Adapthenics Platform Dependencies Catalog")
                    description.set("Adapthenics Platform Dependencies Catalog")
                }
            }

            repositories {
                gitlab()
            }

        }

    }

}