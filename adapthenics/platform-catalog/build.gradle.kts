@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

import com.evgenivanovi.adapthenics.lib.tool.onlyLibraries
import com.evgenivanovi.adapthenics.lib.util.Numbers
import com.evgenivanovi.adapthenics.lib.util.Strings
import org.gradle.configurationcache.extensions.capitalized

plugins {

    /* Gradle Plugins */
    id("idea")
    id("org.gradle.base")
    id("org.gradle.java-platform")
    id("org.gradle.distribution")
    id("org.gradle.maven-publish")
    id("org.gradle.version-catalog")

    /* Adapthenics Plugins */
    id("com.evgenivanovi.adapthenics.lib")

}

afterEvaluate {

    fun VersionCatalogBuilder.generateLibrary(project: Project) {

        val group: String = project.group.toString()
        val artifact: String = project.name
        val version: String = project.version.toString()

        val alias: String = artifact
            .split(Strings.DASH)
            .withIndex()
            .joinToString(separator = Strings.EMPTY) { value ->
                if (value.index != Numbers.ZERO_I)
                    value.value.capitalized()
                else value.value
            }

        library(alias, group, artifact).version(version)

    }

    catalog {

        versionCatalog {
            rootProject.subprojects
                .onlyLibraries()
                .forEach(::generateLibrary)
        }

    }

    publishing {

        publications {

            register("platformCatalog", MavenPublication::class) {
                from(components["versionCatalog"])
                pom {
                    name.set("Adapthenics Platform Catalog")
                    description.set("Adapthenics Platform Catalog")
                }
            }

            repositories {
                gitlab()
            }

        }

    }

}