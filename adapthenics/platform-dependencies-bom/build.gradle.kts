@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

val ADAPTHENICS_KT_V: String by project

plugins {

    /* Gradle Plugins */
    id("idea")
    id("org.gradle.base")
    id("org.gradle.java-platform")
    id("org.gradle.distribution")
    id("org.gradle.maven-publish")

    /* Adapthenics Plugins */
    id("com.evgenivanovi.adapthenics.lib")

}

javaPlatform {
    allowDependencies()
}

dependencies {

    // [org.gradle.kotlin.dsl.support.delegates.DependencyHandlerDelegate#enforcedPlatform]
    // The versions used in this file will override any other version found in the graph.

    api(enforcedPlatform(libs.boms.netty))
    api(enforcedPlatform(libs.boms.grpc))

    api(enforcedPlatform(libs.boms.spring))
    api(enforcedPlatform(libs.boms.springboot))
    api(enforcedPlatform(libs.boms.springdata))
    api(enforcedPlatform(libs.boms.springcloud))
    api(enforcedPlatform(libs.boms.springint))

    /** Hope last one will override any transitive dependencies **/
    api(enforcedPlatform("com.evgenivanovi.adapthenics.kt:kt-dependencies-bom:$ADAPTHENICS_KT_V"))

    constraints {

        // Kafka
        api(libs.kafka.spring)

        // Mongo
        api(libs.mongo.sync)
        api(libs.mongo.async)

        // Validation
        api(libs.jakexp)
        api(libs.jakvalidation)
        api(libs.hibvalidation)

        // JDBC
        api(libs.hikari)
        api(libs.postgres.jdbc)
        api(libs.postgres.r2dbc)

        // ETCD
        api(libs.etcd)

    }

}

afterEvaluate {

    publishing {

        publications {

            register("platformDependenciesBom", MavenPublication::class) {
                from(components["javaPlatform"])
                pom {
                    name.set("Adapthenics Platform Dependencies BOM")
                    description.set("Adapthenics Platform Dependencies BOM")
                }
            }

            repositories {
                gitlab()
            }

        }

    }

}

tasks.withType<GenerateModuleMetadata> {
    /**
     * In general publishing dependencies to enforced platforms is a mistake:
     * enforced platforms shouldn't be used for published components
     * because they behave like forced dependencies and leak to consumers.
     * This can result in hard to diagnose dependency resolution errors.
     * If you did this intentionally you can disable this check
     * by adding 'enforced-platform' to the suppressed validations of the
     */
    suppressedValidationErrors.add("enforced-platform")
}