@file:Suppress(
    "PropertyName",
    "LocalVariableName",
    "PrivatePropertyName",
    "UsePropertyAccessSyntax",
)

rootProject.name = "adapthenics-platform"

/**
 * Default catalog named 'libs' will be implicitly created
 * from file located 'gradle/libs.versions.toml'
 */

dependencyResolutionManagement {

    @Suppress("UnstableApiUsage")
    repositories {

        /* __________________________________________________ */

        google()
        mavenCentral()
        gradlePluginPortal()

        /* __________________________________________________ */
        // Look at RepositoryHandler.gitlab() function in buildSrc
        /* __________________________________________________ */

        val CI: String? = System.getenv("CI")

        val gitlab = {

            // You can pull packages from the group or project level, but publishing only works at the project level.
            // The group level is essentially an aggregated view of all projects in the group.
            val publish = "https://gitlab.com/api/v4/projects/41857929/packages/maven"

            maven {
                name = "GitLab"
                url = java.net.URI.create(publish)
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }

        }

        val local = {
            mavenLocal()
        }

        when (CI == "true") {
            true -> {
                println("Gradle is running on GitLab CI.")
                println("Gradle build is going to use Gitlab Repository.")
                gitlab()
            }
            false -> {
                println("Gradle is running locally.")
                println("Gradle build is going to use Maven Local Repository.")
                local()
            }
        }

    }

    val ADAPTHENICS_KT_V: String by settings
    val ADAPTHENICS_ENVOTUR_V: String by settings
    val ADAPTHENICS_SEARCH_V: String by settings
    val ADAPTHENICS_JS_V: String by settings
    val ADAPTHENICS_MDB_V: String by settings

    @Suppress("UnstableApiUsage")
    versionCatalogs {

        create("ktCatalog") {
            from("com.evgenivanovi.adapthenics.kt:kt-catalog:$ADAPTHENICS_KT_V")
        }

        create("ktDependenciesCatalog") {
            from("com.evgenivanovi.adapthenics.kt:kt-dependencies-catalog:$ADAPTHENICS_KT_V")
        }

        create("tools") {

            /* Envotur Configuration Catalog */
            library(
                "envotur-impl",
                "com.evgenivanovi.adapthenics.envotur:envotur:$ADAPTHENICS_ENVOTUR_V"
            )

            library(
                "envotur-junit-impl",
                "com.evgenivanovi.adapthenics.envotur:envotur-junit:$ADAPTHENICS_ENVOTUR_V"
            )

            library(
                "envotur-postgres-impl",
                "com.evgenivanovi.adapthenics.envotur:envotur-postgres:$ADAPTHENICS_ENVOTUR_V"
            )

            library(
                "envotur-postgres-junit-impl",
                "com.evgenivanovi.adapthenics.envotur:envotur-postgres-junit:$ADAPTHENICS_ENVOTUR_V"
            )

        }

        create("searchCatalog") {
            from("com.evgenivanovi.adapthenics.search:search-catalog:$ADAPTHENICS_SEARCH_V")
        }

        create("jsCatalog") {
            from("com.evgenivanovi.adapthenics.js:js-catalog:$ADAPTHENICS_JS_V")
        }

        create("mongodbCatalog") {
            from("com.evgenivanovi.adapthenics.mongodb:mongodb-catalog:$ADAPTHENICS_MDB_V")
        }

    }

}

pluginManagement {

    repositories {

        /* __________________________________________________ */

        google()
        mavenCentral()
        gradlePluginPortal()

        /* __________________________________________________ */
        // Look at RepositoryHandler.gitlab() function in buildSrc
        /* __________________________________________________ */

        val CI: String? = System.getenv("CI")

        val gitlab = {

            // You can pull packages from the group or project level, but publishing only works at the project level.
            // The group level is essentially an aggregated view of all projects in the group.
            val publish = "https://gitlab.com/api/v4/projects/41857929/packages/maven"

            maven {
                name = "GitLab"
                url = java.net.URI.create(publish)
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }

        }

        val local = {
            mavenLocal()
        }

        when (CI == "true") {
            true -> {
                println("Gradle is running on GitLab CI.")
                println("Gradle build is going to use Gitlab Repository.")
                gitlab()
            }
            false -> {
                println("Gradle is running locally.")
                println("Gradle build is going to use Maven Local Repository.")
                local()
            }
        }

    }

    val KOTLIN_LANG_V: String by settings
    val GRADLE_PROPS_PLUGIN_V: String by settings

    val ADAPTHENICS_ENVOTUR_V: String by settings

    plugins {

        /* Gradle Extended Plugins */
        id("net.saliman.properties") version GRADLE_PROPS_PLUGIN_V

        /* Kotlin Plugins */
        id("org.jetbrains.kotlin.jvm") version KOTLIN_LANG_V
        id("org.jetbrains.kotlin.plugin.serialization") version KOTLIN_LANG_V

        /* Adapthenics Plugins */
        id("com.evgenivanovi.adapthenics.gradle.plugin.envotur.pg") version ADAPTHENICS_ENVOTUR_V

    }

}

buildscript {

    plugins {
        id("net.saliman.properties")
    }

}

// __________________________________________________ ###

/* Platform Properties */
val platformBomName = "platform-bom"
val platformBomModule = "adapthenics:platform-bom"

val platformCatalogName = "platform-catalog"
val platformCatalogModule = "adapthenics:platform-catalog"

val platformDependenciesBomName = "platform-dependencies-bom"
val platformDependenciesBomModule = "adapthenics:platform-dependencies-bom"

val platformDependenciesCatalogName = "platform-dependencies-catalog"
val platformDependenciesCatalogModule = "adapthenics:platform-dependencies-catalog"

/* Spring Properties */
val springCoreName = "spring-core"
val springCoreModule = "adapthenics:platform:spring:spring-core"

val springWebfluxName = "spring-webflux"
val springWebfluxModule = "adapthenics:platform:spring:spring-webflux"

val springRealtimeAdapter = "spring-rtc-adapter"
val springRealtimeAdapterModule = "adapthenics:platform:spring:spring-rtc-adapter"

val springRealtimeInfra = "spring-rtc-infra"
val springRealtimeInfraModule = "adapthenics:platform:spring:spring-rtc-infra"

val springStarterCoreName = "spring-starter-core"
val springStarterCoreModule = "adapthenics:platform:spring:spring-starter-core"

val springStarterLoggingName = "spring-starter-logging"
val springStarterLoggingModule = "adapthenics:platform:spring:spring-starter-logging"

val springStarterMongoName = "spring-starter-mongo"
val springStarterMongoModule = "adapthenics:platform:spring:spring-starter-mongo"

val springStarterPostgresName = "spring-starter-postgres"
val springStarterPostgresModule = "adapthenics:platform:spring:spring-starter-postgres"

val springStarterJooqName = "spring-starter-jooq"
val springStarterJooqModule = "adapthenics:platform:spring:spring-starter-jooq"

val springStarterKafkaName = "spring-starter-kafka"
val springStarterKafkaModule = "adapthenics:platform:spring:spring-starter-kafka"

val springStarterEtcdName = "spring-starter-etcd"
val springStarterEtcdModule = "adapthenics:platform:spring:spring-starter-etcd"

val springStarterRealtimeConfigName = "spring-starter-rtc"
val springStarterRealtimeConfigModule = "adapthenics:platform:spring:spring-starter-rtc"

val springStarterServerHTTPName = "spring-starter-server-http"
val springStarterServerHTTPModule = "adapthenics:platform:spring:spring-starter-server-http"

val springStarterServerRSocketName = "spring-starter-server-rsocket"
val springStarterServerRSocketModule = "adapthenics:platform:spring:spring-starter-server-rsocket"

/* Core Properties */
val log = "log"
val logModule = "adapthenics:platform:log"

val baseName = "base"
val baseModule = "adapthenics:platform:base"

/* Search Properties */
val searchServiceName = "search-service"
val searchServiceModule = "adapthenics:platform:search:search-service"

val searchMongoName = "search-mongodb"
val searchMongoModule = "adapthenics:platform:search:search-mongodb"

val searchPostgresName = "search-postgres"
val searchPostgresModule = "adapthenics:platform:search:search-postgres"

/* Support Library Properties */
val jacksonSupportName = "jackson-support"
val jacksonSupportModule = "adapthenics:platform:support:jackson-support"

val loggingSupportName = "logging-support"
val loggingSupportModule = "adapthenics:platform:support:logging-support"

val reactorSupportName = "reactor-support"
val reactorSupportModule = "adapthenics:platform:support:reactor-support"

/* Domain Properties */
val domainModelName = "domain-model"
val domainModelModule = "adapthenics:platform:domain:domain-model"

val domainServiceName = "domain-service"
val domainServiceModule = "adapthenics:platform:domain:domain-service"

val domainSearchName = "domain-search"
val domainSearchModule = "adapthenics:platform:domain:domain-search"

/* Schema Properties */
val schemaMapperName = "schema-mapper"
val schemaMapperModule = "adapthenics:platform:schema:schema-mapper"

/* Mongo DB Properties */
val mongodbAdapterName = "mongodb-adapter"
val mongodbAdapterModule = "adapthenics:platform:mongodb:mongodb-adapter"

val mongodbInfrastructureName = "mongodb-infrastructure"
val mongodbInfrastructureModule = "adapthenics:platform:mongodb:mongodb-infrastructure"

/* PostgreSQL Properties */
val postgresAdapterName = "postgres-adapter"
val postgresAdapterModule = "adapthenics:platform:postgres:postgres-adapter"

val postgresInfrastructureName = "postgres-infrastructure"
val postgresInfrastructureModule = "adapthenics:platform:postgres:postgres-infrastructure"

val postgresInfrastructureAllTestName="postgres-infrastructure-all-test"
val postgresInfrastructureAllTestModule="adapthenics:platform:postgres:postgres-infrastructure-all-test"

/* API Operations Properties */
val apiOperationsCoreName = "api-operations-core"
val apiOperationsCoreModule = "adapthenics:platform:api:api-operations:api-operations-core"

val apiOperationsSearchName = "api-operations-search"
val apiOperationsSearchModule = "adapthenics:platform:api:api-operations:api-operations-search"

val apiOperationsHTTPName = "api-operations-http"
val apiOperationsHTTPModule = "adapthenics:platform:api:api-operations:api-operations-http"

val apiOperationsRSocketName = "api-operations-rsocket"
val apiOperationsRSocketModule = "adapthenics:platform:api:api-operations:api-operations-rsocket"

/* API Properties */
val apiName = "api"
val apiModule = "adapthenics:platform:api:api:api"

val apiSpecName = "api-spec"
val apiSpecModule = "adapthenics:platform:api:api-spec"

val apiStructsName = "api-structs"
val apiStructsModule = "adapthenics:platform:api:api:api-structs"

val apiExchangeName = "api-exchange"
val apiExchangeModule = "adapthenics:platform:api:api:api-exchange"

val apiJacksonName = "api-jackson"
val apiJacksonModule = "adapthenics:platform:api:api:api-jackson"

val apiHandlersName = "api-handlers"
val apiHandlersModule = "adapthenics:platform:api:api:api-handlers"

/* API Search Properties */
val apiSearchJacksonName = "api-search-jackson"
val apiSearchJacksonModule = "adapthenics:platform:api:api-search:api-search-jackson"

val apiSearchHandlersName = "api-search-handlers"
val apiSearchHandlersModule = "adapthenics:platform:api:api-search:api-search-handlers"

/* jOOQ Properties */
val jooqxName = "jooqx"
val jooqxModule = "adapthenics:platform:jooq:jooqx"

/* ETCD Properties */
val etcdClientName = "etcd-client"
val etcdClientModule = "adapthenics:platform:etcd:etcd-client"

/* Kafka Properties */
val kafkaAdapterCoreName = "kafka-adapter-core"
val kafkaAdapterCoreModule = "adapthenics:platform:kafka:kafka-adapter-core"

val kafkaInfrastructureName = "kafka-infrastructure"
val kafkaInfrastructureModule = "adapthenics:platform:kafka:kafka-infrastructure"

/* RT CFG Properties */
val rtcDomainName = "rtc-domain"
val rtcDomainModule = "adapthenics:platform:rtc:rtc-domain"

/* RT CFG ETCD Properties */
val rtcSchemaETCDName = "rtc-etcd-schema"
val rtcSchemaETCDModule = "adapthenics:platform:rtc:rtc-etcd-schema"

val rtcAdapterETCDName = "rtc-etcd-adapter"
val rtcAdapterETCDModule = "adapthenics:platform:rtc:rtc-etcd-adapter"

val rtcInfraETCDName = "rtc-etcd-infra"
val rtcInfraETCDModule = "adapthenics:platform:rtc:rtc-etcd-infra"

/* __________________________________________________ */

includeBuild("lib")

/* Platform Properties Inclusion */
include(platformBomModule)
include(platformCatalogModule)
include(platformDependenciesBomModule)
include(platformDependenciesCatalogModule)

/* Spring Properties Inclusion */
include(springCoreModule)
include(springWebfluxModule)

include(springRealtimeAdapterModule)
include(springRealtimeInfraModule)

include(springStarterCoreModule)
include(springStarterLoggingModule)
include(springStarterMongoModule)
include(springStarterPostgresModule)
include(springStarterJooqModule)
include(springStarterKafkaModule)
include(springStarterEtcdModule)
include(springStarterRealtimeConfigModule)
include(springStarterServerHTTPModule)
include(springStarterServerRSocketModule)

/* Core Properties Inclusion */
include(logModule)

include(baseModule)

/* Search Properties Inclusion */
include(searchServiceModule)
include(searchMongoModule)
include(searchPostgresModule)

/* Support Library Properties Inclusion */
include(jacksonSupportModule)
include(loggingSupportModule)
include(reactorSupportModule)

/* Domain Properties Inclusion */
include(domainModelModule)
include(domainServiceModule)
include(domainSearchModule)

/* Mongo DB Properties Inclusion */
include(mongodbAdapterModule)
include(mongodbInfrastructureModule)

/* Mongo DB Schema Properties Inclusion */
include(schemaMapperModule)

/* PostgreSQL Properties Inclusion */
include(postgresAdapterModule)
include(postgresInfrastructureModule)
include(postgresInfrastructureAllTestModule)

/* API Operations Properties Inclusion */
include(apiOperationsCoreModule)
include(apiOperationsSearchModule)
include(apiOperationsHTTPModule)
include(apiOperationsRSocketModule)

/* API Properties Inclusion */
include(apiModule)
include(apiSpecModule)
include(apiStructsModule)
include(apiExchangeModule)
include(apiJacksonModule)
include(apiHandlersModule)

/* API Search Properties Inclusion */
include(apiSearchJacksonModule)
include(apiSearchHandlersModule)

/* ETCD Properties Inclusion */
include(etcdClientModule)

/* Kafka Properties Inclusion */
include(kafkaAdapterCoreModule)
include(kafkaInfrastructureModule)

/* jOOQ Properties Inclusion */
include(jooqxModule)

/* RTC Properties Inclusion */
include(rtcDomainModule)
include(rtcSchemaETCDModule)

/* RTC ETCD Properties Inclusion */
include(rtcAdapterETCDModule)
include(rtcInfraETCDModule)
